# -*- coding: utf-8 -*-
"""
    Simple Survey App
"""
from flask import Flask, jsonify, render_template, request, redirect, url_for
import numpy as np
import os
from os.path import join
from smartpalette import Common as cm
import shutil

app = Flask(__name__)


@app.route('/_add_numbers')
def add_numbers():
    """Add two numbers server side, ridiculous but well..."""
    a = request.args.get('a', 0, type=int)
    b = request.args.get('b', 0, type=int)
    return jsonify(result=a + b)


@app.route('/submit')
def submit():
    """
    process the submitted data
    :return: None
    """
    name = request.args.get('name', '', type=str)
    email = request.args.get('email', '', type=str)
    if name == '' or email == '':
        return render_template('failure.html')

    rates = {}
    for f in app.files:
        rates[f] = request.args.get(f, 0, type=int)
        if rates[f] == 0:
            return render_template('failure.html')

    data = np.array((name, email, rates), dtype=np.dtype([('name', 'a', 36), ('email', 'a', 128), ('rating', 'O')]))
    np.save('data.npy', data)
    return render_template('thankyou.html')


@app.route('/')
def index():
    idir = join('static', 'images')
    #shutil.rmtree(idir)
    #shutil.copytree(join(cm.PATH, 'results', 'BSDS500G'), idir)
    app.files = sorted([f[5:] for f in os.listdir(idir) if f.startswith('hue__')])

    return render_template('index.html', files=app.files, prefices=['hue', 'ori', 'int'],
                           n_imgs=len(app.files), img_dir='images/')


if __name__ == '__main__':
    app.run(host='0.0.0.0')
    #app.run(debug=True)
