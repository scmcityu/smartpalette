"""
Created on 29 Oct, 2014

@author: phan
"""

from os.path import join
import os

from smartpalette import Common as cm
# from smartpalette.fast.Distance import hausdorff, parallel_hausdorff
from smartpalette.LLE import LocallyLinearEmbedding, barycenter_weights
from smartpalette.coloropt.Colorizer import ColorOptColorizer

# from matplotlib import pyplot as plt
# from skimage.io import imread, imsave
from skimage.color import rgb2gray
from skimage.segmentation import slic, felzenszwalb

# #import KernelKMeans
# from KernelKMeans import KernelKMeans
# import KernelKMeans as km
from sklearn.cluster import KMeans
# import GPy

# scipy - numpy
from scipy.spatial.distance import euclidean as euc
import numpy as np
# from numpy.random import multivariate_normal

# from sklearn.utils import array2d
from sklearn.utils import check_array


class Colorizer(LocallyLinearEmbedding):
    def __init__(self, n_components=2, n_neighbors=12, reg=4e-1, method='standard'):
        """
        Constructor

        works best with method = 'modified'
        """
        super(Colorizer, self).__init__(eigen_solver='auto', n_components=n_components,
                                        n_neighbors=n_neighbors, reg=reg, method=method)

    def get_weights(self, X):
        X = check_array(X)
        ind = self.nbrs_.kneighbors(X, n_neighbors=self.n_neighbors,
                                    return_distance=False)
        weights = barycenter_weights(X, self.nbrs_._fit_X[ind], reg=self.reg)

        return weights

    def precompute_params(self, image, colorized_image, lmda=0.4, sparse=True):
        if sparse:
            from scipy.sparse import csr_matrix, eye, issparse, diags
        else:
            from numpy import eye

        if len(image.shape) == 3:
            w, h, d = image.shape
        else:
            w, d = image.shape
            h = 1

        pixels = image.reshape((w * h, d))
        try:
            self.fit(pixels)
        except Exception as e:
            print e
            return colorized_image

        M = self.M
        I = eye(len(pixels))

        # List of target colors
        G = colorized_image.reshape((w * h, d))

        Lambda = np.array([np.any(row != 0) for row in G]).astype(np.int32) * lmda
        if sparse:
            Lambda = diags([Lambda], [0])
        else:
            Lambda = Lambda * I

        A = M + Lambda

        if sparse:
            G = csr_matrix(G)
        b = Lambda.dot(G)

        if sparse:
            A = A.asformat('csc')
            b = b.asformat('csc')

        return A, b, M, Lambda, (w, h, d)

    def colorize_lle(self, image, colorized_image, lmda=0.4, sparse=True, params=None):
        """
        Xiaowu Chen, Dongqing Zou, Qinping Zhao, and Ping Tan. 2012. Manifold preserving edit
        propagation. ACM Trans. Graph. 31, 6, Article 132 (November 2012), 7 pages.
        DOI=10.1145/2366145.2366151 http://doi.acm.org/10.1145/2366145.2366151
        :param image:
        :param colorized_image:
        :param lmda:
        :param sparse:
        :param params:
        :return:
        """

        if sparse:
            from scipy.sparse import csr_matrix, eye, diags
            from scipy.sparse.linalg import spsolve as solve
        else:
            from scipy.linalg import solve

        if params is None:
            A, b, M, Lambda, img_dims = self.precompute_params(image, colorized_image, lmda, sparse)
        else:
            A, b, M, Lambda, img_dims = params

        w, h, d = img_dims
        G = colorized_image.reshape((w * h, d))
        if sparse:
            G = csr_matrix(G)

        b = Lambda.dot(G)
        newimage = solve(A, b)
        if sparse:
            newimage = newimage.toarray().reshape((w, h, d))
        else:
            newimage = newimage.reshape((w, h, d))

        return np.squeeze(newimage)

    @staticmethod
    def _weighted_euclidean(x, y, ws):
        return np.sqrt((ws[0] * x[0] - y[0]) ** 2 + ws[1] * (x[1] - y[1]) ** 2 + ws[2] * (x[2] - y[2]) ** 2)

    def colorize(self, image, model, aligner, n_colors, n_appx_segments=20, lle_params=None, lmda=0.4):
        """
        Colorize an image by segmentation
        :param image: RGB image
        :param model: The model for color suggestion (GPLVM, PCA-GMM)
        :param aligner: to align new palettes to model.Y
        :param n_colors: number of colors in a palette, must be compatible with model.Y.shape[1]
        :param lle_params: precomputed LLE parameters
        :param lmda:
        :return:
        """
        w, h, d = image.shape
        image_nlab = cm.rgb2nlab(image)
        segments = slic(image, n_segments=n_appx_segments, compactness=0.1, sigma=1, multichannel=True, convert2lab=True)
        # segments = felzenszwalb(image, scale=1, sigma=0.8, min_size=20)
        seg_ids = np.unique(segments)
        n_segs = len(seg_ids)
        seg_mean_colors = np.array([image_nlab[segments == seg_id].mean(axis=0) for seg_id in seg_ids])
        adj_mat = Colorizer.find_all_neighbors(segments, n_colors=n_colors)

        cur_pals = np.zeros((n_segs, n_colors, 3))
        new_pals = np.zeros((n_segs, n_colors, 3))
        for seg_id in range(n_segs):
            neighbors = np.nonzero(adj_mat[seg_id])[0]
            cur_pals[seg_id, 0] = seg_mean_colors[seg_id]
            cur_pals[seg_id, 1:] = seg_mean_colors[neighbors]
            cur_pal, cur_idx = aligner.align(cur_pals[seg_id][None, ...])
            # cur_pals[seg_id] = cur_pal
            new_x, _ = model.infer_newX(cur_pal.reshape((1, -1)), optimize=True)
            new_pal, _ = model.predict(new_x, full_cov=True)
            new_pals[seg_id] = new_pal.reshape((n_colors, 3))[np.argsort(cur_idx)]

            # cur_pals[seg_id, 0] = seg_mean_colors[seg_id]

        # new_pals[np.bitwise_not(np.isfinite(new_pals))] = 1e-5
        # cur_pals[np.bitwise_not(np.isfinite(cur_pals))] = 1e-5
        # cur_pals, new_pals = np.clip(cur_pals, 0, 1), np.clip(new_pals, 0, 1)

        # plt.subplot(131)
        # plt.imshow(cm.nlab2rgb(new_pals), interpolation='nearest')
        # plt.subplot(132)
        # plt.imshow(cm.nlab2rgb(cur_pals), interpolation='nearest')
        # plt.show()

        # new_pals[np.random.permutation(n_segs)[:n_segs - n_segs/2]] = 0
        # new_pals = self.colorize_lle(cur_pals, new_pals.copy(), lmda=lmda, params=lle_params, sparse=False)
        new_image = np.zeros_like(image)
        for seg_id in seg_ids:
            new_image[segments == seg_id] = new_pals[seg_id, 0]
            # _yy, _xx = np.where(segments == seg_id)
            # _perm = np.random.permutation(len(_xx))
            # new_image[_yy[_perm[:len(_yy) / 8]], _xx[_perm[:len(_xx) / 8]]] = new_pals[seg_id, 0]

        # new_image = self.colorize_lle(image, new_image, lmda=lmda, params=lle_params, sparse=False)


        # plt.subplot(121)
        # plt.imshow(cm.nlab2rgb(new_image), interpolation='nearest')

        n_clusters = 80
        km = KMeans(n_clusters=n_clusters)
        labels = km.fit_predict(image_nlab.reshape((-1, 3)))
        label_mask = labels.reshape((w, h))
        centers = km.cluster_centers_
        targets = centers.copy()

        perms = np.random.permutation(n_clusters)
        for k in perms[:40]:
            targets[k] = new_image[label_mask == k].mean(axis=0)

        for k in perms[40:]:
            targets[k] = 0

        opt_targets = self.colorize_lle(centers, targets, lmda=lmda, params=lle_params, sparse=False)
        for k in range(n_clusters):
            new_image[label_mask == k] = opt_targets[k]
        return cm.nlab2rgb(new_image)


        # cur_pals, new_pals = cm.nlab2rgb(cur_pals), cm.nlab2rgb(new_pals)
        # colorizer = ColorOptColorizer(10, 10, ann_per=0.1)
        # seg_masks = [segments == i for i in range(n_segs)]
        # new_image = colorizer.colorize(image, seg_masks, new_pals[:, 0])
        #
        # plt.subplot(122)
        # plt.imshow(cm.nlab2rgb(new_image), interpolation='nearest')
        # plt.show()

        # return cm.nlab2rgb(new_image)

    @staticmethod
    def find_all_neighbors(segments, n_colors):
        """
        find the neighbors of the segments (seg_ids)

        :param segments: segmented image
        :param n_colors:
        :return:
        """
        seg_ids = np.unique(segments)
        n_segs = len(seg_ids)

        adj_mat = np.zeros((n_segs, n_segs), dtype='int32')
        np.fill_diagonal(adj_mat, 0)

        # find the centroids of each segments
        seg_centroids = np.zeros((n_segs, 2))
        for seg_id in seg_ids:
            yy, xx = np.where(segments == seg_id)
            seg_centroids[seg_id] = np.array([yy.mean(), xx.mean()])

        for i in range(n_segs):
            dists = [euc(seg_centroids[i], seg_centroids[j]) for j in range(n_segs)]
            sorted_ids = np.argsort(dists)[1:n_colors]
            adj_mat[i, sorted_ids] = 1

        return adj_mat
    

if __name__ == '__main__':
    from skimage.transform import rescale
    from skimage import img_as_float
    from skimage.io import imread
    from smartpalette import FeatureAlignment
    from matplotlib import pyplot as plt
    import GPy

    n_colors = 7
    collection = 'luce'
    path = cm.PATH
    test_image = 'image_1.jpg'
    imagefile = join(cm.PATH, 'images', 'distinctive', test_image)

    # test_image = 'image_48.png'
    # imagefile = join(cm.PATH, 'images', 'decorative', test_image)

    # test_image = 'image_4.jpg'
    # imagefile = join(cm.PATH, 'images', 'benchmark_unstylized', test_image)

    im = img_as_float(imread(imagefile))[:, :, :3]
    im = rescale(im, 0.5)
    cz = Colorizer(n_components=2, n_neighbors=10, reg=4e-1, method='standard')

    # Load up ML models and data
    datafile = join(path, 'data', collection, 'clusters_nc%d_l%d.npz' % (n_colors, 0))
    modelfile = join(path, 'data', collection, 'models_nc%d_l%d.npy' % (n_colors, 0))

    tmpdata = np.load(datafile)
    training_data = np.squeeze(tmpdata['normalized_clustered_lab']) # [::2]
    n_pals, n_cols, n_chas = training_data.shape
    training_data, newindices, _, order, _ = \
        FeatureAlignment.sort_palettes(training_data, n_colors, sort_palettes=True,
                                       n_themes=None, labels=None, step=1, sorter='binary', viz='none')

    Q = 4
    m = GPy.models.GPLVM(training_data.reshape((-1, n_cols * n_chas)), Q)
    m.optimize('lbfgs', messages=0, max_iters=2000, gtol=1e-4)
    np.save(modelfile, m)

    aligner = FeatureAlignment.PaletteAligner(method='data',
                                              data=training_data,
                                              match_method='munkres',
                                              n_examples=10)

    # colorize image
    new_image = cz.colorize(im, model=m, aligner=aligner, n_colors=n_colors, n_appx_segments=20, lmda=0.4, lle_params=None)
    fig = plt.figure(test_image)
    ax = fig.add_subplot(111)
    ax.imshow(new_image, interpolation='nearest')
    plt.show()