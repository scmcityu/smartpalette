# -*- coding: utf-8 -*-
"""
Created on Fri Sep 26 08:55:12 2014

@author: phan
"""
import os, sys
from Common import PATH
import Common
from os.path import join
import numpy as np
#import shutil
from sklearn.cluster import KMeans
#from KernelKMeans import KernelKMeans
from skimage.util import img_as_float
from skimage.io import imread
from skimage.color import rgb2lab
from numpy.random import multivariate_normal
from smartpalette.HGMM import HGMM
from sklearn.mixture import GMM
from skimage.transform import rescale
from matplotlib import pyplot as plt


def convert_from_matlab(collections=('turk', 'xkuler')):
    from scipy.io import loadmat
    for collection in collections:
        mat = loadmat(join(PATH, 'data', collection, 'themesData.mat'))
        rgb = mat['datapoints'][0,0]['rgb'].reshape((-1, 5, 3))
        ratings = mat['datapoints'][0,0]['targets']

        #nlab = Common.rgb2lab(rgb)
        lab = Common.rgb2lab(rgb)

        np.savez(join(PATH, 'data', collection, 'dpaintings5.npz'), clustered_lab=[lab], rating=[ratings], orig_rgb=[rgb],
                 orig_lab=[lab], fnames=[])


def prepare_auto_segment_based_palettes(collections, n_segments=10, n_colors=5, cap=40, step=1, use_slic=False):
    """
    learn a Hierachical Gaussian Mixture Model
    :param collections:
    :param n_segments:
    :param n_colors:
    :param cap: max number of images
    :param step: regularly skipping some regions
    :param use_slic: whether to use slic
    :return:
    """
    from skimage.segmentation import slic


    for collection in collections:
        image_path = join(PATH, 'images', collection)
        data_path = join(PATH, 'data', collection)

        imagefiles = sorted([f for f in os.listdir(image_path) if f.endswith('.jpg')])
        n_images = min(len(imagefiles), cap)
        clustered_lab = np.zeros((1, n_segments * n_images, n_colors, 3))
        # clustered_lab = []

        for fx, fname in enumerate(imagefiles[:n_images]):
            print fname,
            img = imread(join(image_path, fname))
            img = rescale(img, 0.4)

            h, w, d = img.shape
            assert(d == 3)
            lab = rgb2lab(img).reshape((-1, d))
            nlab = Common.rgb2nlab(img)

            # OLD WAY OF SEGMENTATON
            if not use_slic:
                locations = np.array([(x / float(w), y / float(h)) for y in range(h) for x in range(w)])
                X = np.hstack((nlab.reshape((-1, d)), locations))
                # X = nlab.reshape((-1, d))
                km = KMeans(n_clusters=n_segments)
                labels = km.fit_predict(X)
            else:
                # SLIC-BASED SEGMENTATION
                segment_mask = slic(img, n_segments=int(n_segments * 1.5), compactness=10.,
                                    multichannel=True, convert2lab=True, enforce_connectivity=True)
                labels = segment_mask.flatten()

            # plt.imshow(labels.reshape((h, w)), interpolation='nearest')
            # plt.show()

            km1 = KMeans(n_clusters=n_colors)
            # n_actual_segments = len(np.unique(labels))
            # for s in range(n_actual_segments):
            #     palette = km1.fit(lab[labels == s]).cluster_centers_
            #     clustered_lab.append(palette)

            for s in range(n_segments):
                palette = km1.fit(lab[labels == s]).cluster_centers_
                clustered_lab[0, fx * n_segments + s] = palette

        # clustered_lab = np.concatenate(clustered_lab)[None, :]

        if not os.path.isdir(data_path):
            os.mkdir(data_path)

        np.savez(join(data_path, 'dpaintings%d.npz' % n_colors), fnames=imagefiles, clustered_lab=clustered_lab, method='auto_segment')


def prepare_segment_based_palettes(n_colors, collections):
    CAP = 40
    for collection in collections:
        image_path = join(PATH, 'images', collection)
        data_path = join(PATH, 'data', collection)

        data = np.load(join(data_path, 'data.npy')).tolist()
        imagefiles = sorted([f for f in os.listdir(image_path) if f.endswith('.jpg')])

        clustered_lab = [[]]
        orig_lab = [[]]
        orig_rgb = [[]]
        km = KMeans(n_clusters=n_colors, n_init=2)

        for fname in imagefiles[:CAP]:
            print fname,
            img = imread(join(image_path, fname))
            lab_img = img_as_float(rgb2lab(img))
            anns = data[fname]['attrs'].tolist()

            for mx, mc in anns.iteritems():
                print '.',
                color_, mask_ = mc
                yy, xx = np.where(mask_)
                orig_lab_ = lab_img[yy, xx, :]
                orig_rgb[0].append(img[yy, xx, :])
                orig_lab[0].append(orig_lab_)

                if len(orig_lab_) > n_colors * 10:
                    km.fit(np.ascontiguousarray(orig_lab_))
                    clustered_lab[0].append(km.cluster_centers_)

            print

        np.savez(join(data_path, 'dpaintings%d.npz' % n_colors), clustered_lab=clustered_lab, orig_rgb=np.array(orig_rgb),
                 orig_lab=np.array(orig_lab), fnames=imagefiles[:CAP])


def prepare_sliding_palettes(n_colors, collections, isizes=[250], steps=[2]):
    for artist in collections:
        print artist
        IPATH = join(PATH, join('images', artist))
        OPATH = join(PATH, join('data', artist))
        if not os.path.isdir(OPATH):
            os.makedirs(OPATH)

        CAP = 40
        files = sorted([f for f in os.listdir(IPATH) if os.path.isfile(join(IPATH, f)) and f.endswith('.jpg')])
            
        cap = min(CAP, len(files))
        #for nc in n_colors:
        orig_rgb = [[] for _ in range(len(isizes))]
        orig_lab = [[] for _ in range(len(isizes))]
        clustered_lab = [[] for _ in range(len(isizes))]
        stored_files = []
        for f in files[:cap]:
            stored_files.append(f)
            print '.',
            img = imread(join(IPATH,f))
            lab_img = img_as_float(rgb2lab(img)) #img_as_float(Common.rgb2nlab(img))
            for iz in range(len(isizes)):
                WIN_SIZE = isizes[iz]
                STEP_FACTOR = steps[iz]
                rows, cols, _ = lab_img.shape

                leap = int(WIN_SIZE / STEP_FACTOR)
                for i in np.arange(0, cols, leap ):
                    for j in np.arange(0, rows, leap):
                        if i + leap > cols or j + leap > rows:
                            break

                        # X, Y = multivariate_normal([i + leap, j + leap],
                        # [[WIN_SIZE * 4, 0], [0, WIN_SIZE * 4]], WIN_SIZE * 4).T
                        # np.clip(X, 0, cols - 1, X)
                        # np.clip(Y, 0, rows - 1, Y)
                        # X = X.astype(int)
                        # Y = Y.astype(int)

                        X, Y = range(i, i + WIN_SIZE, 5), range(j, j + WIN_SIZE, 5)

                        orig_rgb[iz].append(img[Y, X])
                        orig_lab[iz].append(lab_img[Y, X])

                        km = KMeans(n_clusters=n_colors, n_jobs=2, n_init=2)
                        km.fit(lab_img[Y, X])
                        clustered_lab[iz].append(km.cluster_centers_)

                        # km = KMeans(n_clusters=n_colors * 4, n_jobs=2, n_init=2)
                        # labels = km.fit_predict(lab_img[Y, X])
                        # tmp_order = np.argsort([(labels == l).sum() for l in range(n_colors * 4)])[::-1]
                        # clustered_lab[iz].append(km.cluster_centers_[tmp_order[:n_colors]])

        for iz in range(len(isizes)):
            data_size = len(orig_lab[iz])
            if data_size > 400:
                print 'data size larger than 250'
                step = data_size / 400
                orig_lab[iz] = orig_lab[iz][::step]
                orig_rgb[iz] = orig_rgb[iz][::step]
                clustered_lab[iz] = clustered_lab[iz][::step]

            print 'data size', data_size

        np.savez(join(OPATH, 'dpaintings%d.npz' % n_colors), clustered_lab=clustered_lab, orig_rgb=np.array(orig_rgb),
                 orig_lab=np.array(orig_lab), fnames=stored_files)

        print

if __name__ == '__main__':
    #prepare_segment_based_palettes(7, ['gogh'])
    #prepare_sliding_palettes(7, collections=['luce'], isizes=[100], steps=[1])
    prepare_segment_based_palettes(50, ['luce'])