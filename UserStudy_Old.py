# -*- coding: utf-8 -*-
"""
Created on Thu Dec 18 19:41:08 2014

@author: phan
"""
import numpy as np
from matplotlib import pyplot as plt
import matplotlib as mpl

from math import sin, cos
from os.path import join
import os
from Common import PATH
import shutil


def draw_rects(ctx, w, h , nrows = 20, ncols = 20, colors=None):
    cwidth = w / ncols
    cheight = h / nrows
    # ctx.set_source_rgb (0, 0, 1)
    # ctx.paint()
    # hshift = cwidth / 2
    for row in range(nrows):
        for col in range(ncols):
            # if row % 2 == 0:
            #     shift = hshift
            # else:
            #     shift = 0
                
            if row % 2 == 0 and col % 2 == 0:
                rad = cwidth / 2 - 1
            else:
                rad = cwidth / 2 - 1
                
            cy = (row + 1) * cwidth - cwidth/2 
            cx = (col + 1) * cheight - cheight/2 # + shift
            if colors is not None:
                r,g,b = tuple(colors[row, col])
            
            ctx.set_source_rgb (r, g, b)
            
            ctx.move_to(cx - rad, cy - rad)
            ctx.line_to(cx + rad, cy - rad)
            ctx.line_to(cx + rad, cy + rad)
            ctx.line_to(cx - rad, cy + rad)
            ctx.line_to(cx - rad, cy - rad)     
            
            ctx.fill()
            # ctx.stroke()
            # ctx.arc(cx, cy, rad , 0, 2 * np.pi)
             
    ctx.set_source_rgb(1, 1, 1)
    ctx.fill()
    ctx.stroke() 


def draw_circles(ctx, w, h):
    nrows = 10
    ncols = 10
    
    cwidth = w / ncols
    cheight = h / nrows
    ctx.set_source_rgb (0, 0, 1)
    
    hshift = cwidth / 2 
    for row in range(nrows):
        for col in range(ncols):
            if row % 2 == 0:
                shift = hshift
            else:
                shift = 0
                
            if row % 2 == 0 and col % 2 == 0:
                rad = cwidth / 2 - 10
            else:
                rad = cwidth / 2 - 20
                
            cy = row * cwidth - cwidth/2 
            cx = col * cheight - cheight/2 + shift
            
            ctx.move_to(cx, cy)
            ctx.arc(cx, cy, rad , 0, 2 * np.pi)
             
    ctx.set_source_rgb(1, 0.5, 0.5)
    ctx.fill()
    ctx.stroke() 


def draw_triangle(ctx, cx, cy, rad, orientation='up'):
    if orientation == 'up':
        ctx.move_to(cx, cy - rad)
        dx = cos(np.pi / 6) * rad
        dy = sin(np.pi / 6) * rad
        
        ctx.line_to(cx + dx, cy + dy)
        ctx.line_to(cx - dx, cy + dy)
        ctx.line_to(cx , cy - rad)
        
    if orientation == 'down':
        ctx.move_to(cx, cy + rad)
        dx = cos(np.pi / 6) * rad
        dy = sin(np.pi / 6) * rad
        
        ctx.line_to(cx + dx, cy - dy)
        ctx.line_to(cx - dx, cy - dy)
        ctx.line_to(cx , cy + rad)        


def draw_triangles(ctx, w, h):
    nrows = 10
    ncols = 10
    
    cwidth = (w + 200) / ncols
    cheight = h / nrows
    
    ctx.set_source_rgb (0, 0, 1)
    
    shift = cwidth / 2 
    for row in range(nrows*2):
        for col in range(ncols * 2):
            rad = cwidth / 2
            dx = cos(np.pi / 6) * rad
            dy = sin(np.pi / 6) * rad
            
            if col % 2 == 0:
                hshift = dy
                ori = 'up'
            else:
                hshift = 0
                ori = 'down'

            if row % 2 == 0:
                rshift = dx
            else:
                rshift = 0
            # draw perfect triangle
            cy = row * (rad + dy)  + hshift
            cx = col * dx + rshift
            
            draw_triangle(ctx, cx, cy, rad - 10, orientation=ori)
            # ctx.arc(cx, cy, rad , 0, 2 * np.pi)
             
    ctx.set_source_rgb(1, 0.5, 0.5)
    ctx.fill()
    ctx.stroke() 


def normalize(r,g,b):
    return (r / 255., g / 255., b / 255.)


def generate_test(fname, nrows, ncols, mode='', colors=None):
    import cairo
    
    WIDTH, HEIGHT = nrows * 120, ncols * 120
    
    surface = cairo.ImageSurface (cairo.FORMAT_RGB24, WIDTH, HEIGHT)
    ctx = cairo.Context(surface)
    
    ctx.set_source_rgb (*(normalize(0, 0, 0)))
    ctx.set_operator (cairo.OPERATOR_SOURCE)
    ctx.paint()

    # draw_spiral (ctx, WIDTH, HEIGHT)
    if mode == 'circle':
        draw_circles(ctx, WIDTH, HEIGHT)
    elif mode == 'triangle':
        draw_triangles(ctx, WIDTH, HEIGHT)
    elif mode == 'rectangle':
        draw_rects(ctx, WIDTH, HEIGHT, nrows, ncols, colors)
        
    surface.write_to_png(fname)


def sample_images(artists, nrows=3,ncols=3):
    from Common import PATH
    from sklearn.cluster import KMeans
    from os.path import join
    import os
    
    n_colors = 7
    # nrows, ncols = 4, 4
    
    km = KMeans(n_clusters=nrows * ncols, n_jobs=4, n_init=4)
    # all_colors= []
    for artist in artists:
        print artist
        data = np.load(join(PATH, 'data', artist, 'dpaintings%d.npz' % n_colors))
        rgbs = data['orig_rgb'] / 255.
        
        img_path = join(PATH, 'data', artist, 'user_test')
        if not os.path.isdir(img_path):
            os.mkdir(img_path)
        else:
            shutil.rmtree(img_path)
            os.mkdir(img_path)
                
        for i, rgb in enumerate(rgbs[1::2]):
            km.fit(rgb)
            colors = km.cluster_centers_
            perm = np.random.permutation(nrows * ncols)
            colors = np.clip(colors[perm], 0, 1)
            #all_colors.append(colors)

            fname = join(img_path, 'test_%d.png' % i)
            generate_test(fname, nrows, ncols, colors=colors.reshape((nrows, ncols, 3)), mode='rectangle')


def plan_tests(artists, take=10):
    # from scipy.spatial.distance import euclidean as euc
    from skimage.io import imread
    from fast.Distance import hausdorff
    
    for artist in artists:
        img_path = join(PATH, 'data', artist, 'user_test')
        user_data_path = join(PATH, 'user_data')
        images = [f for f in os.listdir(img_path) if f.endswith('.png')]

        perm = np.random.permutation(len(images))
        if not os.path.isdir(join(user_data_path, 'test_images', artist)):
            os.makedirs(join(user_data_path, 'test_images', artist))
        else:
            pngs = [join(user_data_path, 'test_images', artist, f) 
            for f in os.listdir(join(user_data_path, 'test_images', artist)) if f.endswith('.png')]

            for png in pngs:
                os.remove(png)
           
        count = 0
        selected = []
        indices = np.array(zip(np.repeat(np.arange(3), 3), np.tile((np.arange(3)), 3))) * 120 + 60
#        print indices
        dists = []       
        for ix, i in enumerate(perm):
            if count == take:
                break
            
            img = images[i]
            imgdata = imread(join(img_path, img))[indices[:,0], indices[:,1], :] / 255.
            
            if imgdata.reshape((-1, 3)).std(axis=0).mean() < 0.2:
                continue
            
            OK = True
            for simg in selected:
                #if euc(simg.ravel(), imgdata.ravel()) < 5600:
                d = hausdorff(imgdata, simg, mode=1)
                dists.append(d)
                if d < 0.13:
                    OK = False
                    break
            
            if not OK:
                continue
            
            
            selected.append(imgdata)
            shutil.copy(join(img_path, img), join(user_data_path, 'test_images', artist, 'test_%d.png' % count))
            count += 1
        
        print np.max(dists), np.mean(dists)
        # latinsquare = np.loadtxt(join(user_data_path, '%s.txt' % artist)).astype(int)


def analyse(path, userids=[0,1,3,4,6], n_methods=2):
    import datetime
    todt = lambda st: datetime.datetime.strptime( st, "%Y-%m-%d %H-%M-%S-%f")
    table = np.zeros((len(userids), n_methods))
    for uix, userid in enumerate(userids):
        datafile = join(path, 'user' + str(userid), 'data.txt')
        table_type = np.dtype([('datetime', 'S24'), ('artist', '<S10'), \
        ('action', 'S2'), ('user_id', 'i8'),  ('method', 'i8'), ('test_id', 'i8'),\
        ('x', 'i8'), ('y', 'i8'), ('R', 'f8'), ('G', 'f8'), ('B', 'f8')])
        
        data = np.loadtxt(datafile, dtype=table_type, delimiter='\t')
        
        # print data['test_id']
        prev_time = todt(data['datetime'][0])
        method_mask = np.zeros(len(data))
        for i in range(0, len(data)):
            
            # userid = data['user_id'][i]
            method = data['method'][i]
            method_mask[i] = int(method)
            
        method_mask = np.asarray(method_mask, dtype=bool)
        
        data_methods = [None, None]
        data_methods[0] =  data[method_mask]
        data_methods[1] = data[np.logical_not(method_mask)]
        
        for method in range(2):
            dta = data_methods[method]
            # pass
            # table[userid, method] =
            start = todt(dta['datetime'][0])
            end = todt(dta['datetime'][-1])
            duration = (end - start).seconds
            table[uix, method] = duration
            '''
            if data['action'][i].startswith('P'):
                diff = todt(data['datetime'][i]) - prev_time
                prev_time = todt(data['datetime'][i])
                print diff.seconds
            '''
            #print data[i]
    print (table[:,0] - table[:, 1]).mean()
    return table


def visualize_survey(datafile):
    # table_type = np.dtype([('easiness', float),('reponsiveness', float),('visibility', float),('accuracy', float)])
    data = np.loadtxt(datafile, dtype='float', delimiter='\t', skiprows=1)
    
    means = data.mean(axis = 0)
    cmap = mpl.colors.ListedColormap(['g', 'r'])
    fig = plt.figure()
    for i, mean in enumerate(means):
        ax = fig.add_subplot(4, 1, i)
        bounds = [1, mean, 5]
        norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
        cb2 = mpl.colorbar.ColorbarBase(ax, cmap=cmap,
                                             norm=norm,
                                             # to use 'extend', you must
                                             # specify two extra boundaries:
                                             boundaries=[0]+bounds+[13],
                                             extend='both',
                                             #ticks=bounds, # optional
                                             spacing='proportional',
                                             orientation='horizontal')
                                             
        #ax.axis('off')
        #cb2.set_label('Discrete intervals, some other units')
    plt.show()


def ANOVA(data):
    '''
    data.shape[0] = number of subjects
    data.shape[1] = number of related groups (levels)
    '''
    n_subs = data.shape[0]
    n_groups = data.shape[1]
    grand_mean = data.mean()
    SS_time = np.sum(n_subs * (np.mean(data, axis=0) - grand_mean)**2 )
    SS_w = np.sum((data - np.mean(data, axis=0)) ** 2)
    SS_subs = np.sum((np.mean(data, axis=1) - grand_mean)**2) * n_groups
    
    SS_err = SS_w - SS_subs
    MS_time = SS_time / (n_groups - 1)
    MS_err = SS_err / ((n_subs - 1) * (n_groups - 1))
    F = MS_time / MS_err
    
    p_value = (np.random.f(n_groups - 1, ((n_subs - 1) * (n_groups - 1)), size=100000) > F).sum() / 100000.
    
    return F, p_value
    
if __name__ == '__main__':
    from scipy.stats import f_oneway,ttest_rel,ttest_ind
    '''
    fname='.tmp/user_study2.png'
    generate_test(fname, 'rectangle')
    
    from skimage.io import imread
    im = imread(fname)
    plt.imshow(im)
    plt.show()
    '''
    #sample_images(['renoir','cezanne', 'luce', 'gas', 'chase','urbino'])
    #generate_test('.tmp/blank.png', 3, 3, mode='rectangle', colors=np.ones((3,3,3), dtype=np.float))
    #plan_tests(['renoir','cezanne', 'luce', 'gas', 'chase','urbino'])
    
    table = analyse('/media/phan/SMALLDATA/DPAINTINGS/user_data', userids=[0,1,2,3,4,5,6])
    print table
    
    bias = np.array([[0,0,0,0,0,0,0],[0,0,0,0,0,0,0]])
    table += bias.T
    #print ANOVA(table)
    
    #visualize_survey('/media/phan/SMALLDATA/DPAINTINGS/user_data/survey.txt')
    print f_oneway(table[:,0],table[:,1])