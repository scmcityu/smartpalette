## distutils: extra_compile_args = -fopenmp
## distutils: extra_link_args = -fopenmp

#-fopenmp for linux, /openmp for windows, set CC and CXX environment before running this script (CLANG does not work with openmp)

import cython
import numpy as np
cimport numpy as np
from cython.operator cimport dereference as deref, preincrement as inc
from cython.parallel import parallel, prange
from libc.stdlib cimport abort, malloc, free

import sys
from libc.math cimport abs, pow, sqrt

DTYPE = np.double
ctypedef np.double_t DTYPE_t

cpdef np.ndarray[np.int32_t, ndim=1] best_match(double[:, :] A, double[:, :] B, dupl=True):
    # mode = 0 - use forward only
    # mode = 1 - use forward and backward
    # Get the sizes of the input arrays
    cdef int Arows, Acols, Brows, Bcols
    cdef double mindist, tempdist
    Arows = A.shape[0]
    Brows = B.shape[0]
    
    # Calculate the forward HD
    cdef f_best_match = np.zeros(Arows, dtype='int32')  - 1
    #cdef int[:] f_best_match = f_best_match_
    
    for i in range(Arows):
        mindist = 1e10
        for j in range(Brows):
            tempdist = sqrt(((A[i, 0]-B[j, 0])*(A[i, 0]-B[j, 0])) + \
            ((A[i, 1]-B[j, 1])*(A[i, 1]-B[j, 1])) + \
            ((A[i, 2]-B[j, 2])*(A[i, 2]-B[j, 2])))
            
            if tempdist < mindist:# and ((not j in f_best_match) or dupl):
                if not dupl and j in f_best_match:
                    continue
                
                mindist = tempdist
                f_best_match[i] = j
    
    return f_best_match
    
cpdef double hausdorff_abs(double[:, :] A, double[:, :] B, int mode=0):
    # mode = 0 - use forward only
    # mode = 1 - use forward and backward
    # Get the sizes of the input arrays
    cdef int Arows, Acols, Brows, Bcols
    cdef double mindist, tempdist
    Arows = A.shape[0]
    Brows = B.shape[0]
    
    cdef double fhd = 0
    for i in range(Arows):
        mindist = 1e10
        for j in range(Brows):
            tempdist = (abs(A[i, 0] - B[j, 0]) + abs(A[i, 1] - B[j, 1]) + abs(A[i, 2] - B[j, 2])) / 3
            if tempdist < mindist:
                mindist = tempdist

        fhd = fhd + mindist

    fhd = fhd / Arows
    
    cdef double rhd = 0
    if mode == 1:
        for i in range(Brows):
            mindist = 1e10;
            for j in range(Arows):
                tempdist = (abs(A[j, 0] - B[i, 0]) + abs(A[j, 1] - B[i, 1]) + abs(A[j, 2] - B[i, 2])) / 3
                if tempdist < mindist:
                    mindist = tempdist
            
            rhd = rhd + mindist
        
        rhd = rhd / Brows
    
    return max(fhd,rhd) #, dists1


@cython.boundscheck(False)
@cython.wraparound(False)
cpdef double hausdorff_with_dists(double[:, :] D, int mode=0) nogil:
    """
    mode = 0 - use forward only
    mode = 1 - use forward and backward
    """
    cdef int N, M
    cdef double mindist, tempdist
    N = D.shape[0]
    M = D.shape[1]
    cdef double fhd = 0
    for i in range(N):
        mindist = 1e10
        for j in range(M):
            tempdist = D[i, j]
            if tempdist < mindist:
                mindist = tempdist
        fhd = fhd + mindist
    fhd = fhd / N

    cdef double rhd = 0
    if mode == 1:
        for i in range(M):
            mindist = 1e10;
            for j in range(N):
                tempdist = D[j, i]
                if tempdist < mindist:
                    mindist = tempdist
            rhd = rhd + mindist
        rhd = rhd / M
    return max(fhd,rhd)
    
@cython.boundscheck(False)
@cython.wraparound(False)
#cpdef double hausdorff(np.ndarray[DTYPE_t,ndim=2] A, np.ndarray[DTYPE_t,ndim=2] B, int mode=0):
cpdef double hausdorff(double[:, :] A, double[:, :] B, int mode=0) nogil:
    # mode = 0 - use forward only
    # mode = 1 - use forward and backward
    # Get the sizes of the input arrays
    
    cdef int Arows, Acols, Brows, Bcols
    cdef double mindist, tempdist
    Arows = A.shape[0]
    #Acols = A.shape[1]
    Brows = B.shape[0]
    #Bcols = B.shape[1]
    
    # Check for same dimensions
    
    #if Acols != Bcols:
    #    print "The dimensions of the points in the two feature vectors are different"
    
    # Calculate the forward HD
    
    cdef double fhd = 0
    #cdef np.ndarray dists1 = np.zeros(Arows, dtype='double')
    for i in range(Arows):
        mindist = 1e10
        for j in range(Brows):
            #tempdist = sqrt(((A[i, 0]-B[j, 0])*(A[i, 0]-B[j, 0])) + ((A[i, 1]-B[j, 1])*(A[i, 1]-B[j, 1])))
            tempdist = sqrt(((A[i, 0]-B[j, 0])*(A[i, 0]-B[j, 0])) + \
            ((A[i, 1]-B[j, 1])*(A[i, 1]-B[j, 1])) + \
            ((A[i, 2]-B[j, 2])*(A[i, 2]-B[j, 2])))
            
            if tempdist < mindist:
                mindist = tempdist
        
        #dists1[i] = mindist
        fhd = fhd + mindist

    fhd = fhd / Arows
    
    # Calculate the reverse HD
    
    cdef double rhd = 0
    if mode == 1:
        for i in range(Brows):
            mindist = 1e10;
            for j in range(Arows):
                #tempdist = sqrt(((A[j, 0]-B[i, 0])*(A[j, 0]-B[i, 0])) + ((A[j, 1] - B[i, 1])*(A[j, 1] - B[i, 1])))
                tempdist = sqrt(((A[j, 0]- B[i, 0])*(A[j, 0]- B[i, 0])) + \
                ((A[j, 1] - B[i, 1])*(A[j, 1] - B[i, 1])) + \
                ((A[j, 2] - B[i, 2])*(A[j, 2] - B[i, 2])))
                if tempdist < mindist:
                    mindist = tempdist
            
            rhd = rhd + mindist
        
        rhd = rhd / Brows
    
    return max(fhd,rhd) #, dists1

@cython.boundscheck(False)
@cython.wraparound(False)

cpdef np.ndarray[DTYPE_t, ndim=1] parallel_hausdorff(double[:, :] A, double[:, :, :] Bs, int n_jobs=4, int mode=0):
    cdef unsigned int f, t, step1, step2, thread_id
    cdef int data_size = len(Bs)
    #cdef int job_size = data_size / n_jobs
    cdef double score = 0
    
    cdef np_scores = np.zeros(data_size, dtype='double')
    cdef double[:] scores = np_scores
    cdef int i = 0
    with nogil, parallel():
        for i in prange(data_size, schedule='guided'):
            score = hausdorff(A, Bs[i], mode)
            scores[i] = score
    
    return np_scores
    
