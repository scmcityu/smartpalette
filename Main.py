# -*- coding: utf-8 -*-
"""
Created on Sat Dec 20 16:10:55 2014

@author: phan
"""
from KernelKMeans import KernelKMeans
#from features import pmk
from fast.Distance import hausdorff
from sklearn.svm import SVR
import numpy as np
from sklearn.metrics import mean_squared_error, mean_absolute_error
from sklearn.linear_model import Lasso
from os.path import join
from scipy.io import loadmat, savemat
from scipy.spatial.distance import  euclidean as euc
from numpy.random import permutation
import os
from matplotlib import pyplot as plt
import Common
from Common import PATH
from sklearn.mixture import GMM
import GPy
from sklearn.decomposition import PCA

FINEST_SIDE_LENGTH = 0.01
SIDE_LENGTH_FACTOR = 1.5
DISCRETIZE_ORDER = 4
N_COLORS = 5


def find_best_params_gmm(Xtrain, n_components_range=None, cv_types=None, criterion='aic'):
    
    lowest_bic = np.infty
    bic = []
    if n_components_range is None:
        n_components_range = np.arange(20, 60, 2)
        
    if cv_types is None:
        cv_types = ['spherical', 'tied', 'diag', 'full']
    
    for cv_type in cv_types:
        print 'cv_type ', cv_type
        for n_components in n_components_range:
            print '.',
            # Fit a mixture of Gaussians with EM
            gmm = GMM(n_components=n_components, covariance_type=cv_type)
            gmm.fit(Xtrain)
            if criterion == 'bic':
                bic.append(gmm.bic(Xtrain))
            else:
                bic.append(gmm.aic(Xtrain))
                
            if bic[-1] < lowest_bic:
                lowest_bic = bic[-1]
                best_gmm = gmm
                
    print 'best gmm ', best_gmm.n_components, best_gmm.covariance_type     
    return best_gmm


def test_palette_prediction(data_path, artist, method, test_author=None, max_tests=1e6, n_missings=3, vis=0, vis_sort='none',
                            n_colors=N_COLORS, level=0, sorter='binary', optimize=True):
    from Common import rgb2nlab, nlab2rgb,normalize_lab
    from sklearn.metrics import mean_squared_error
    from skimage.color import lab2rgb
    from FeatureAlignment import align_colors_naive, sort_palettes
    from scipy.spatial.distance import euclidean
    
    mat = loadmat(join(data_path, 'data_old', artist, 'tests.mat'))
    missing_ids = mat['missing_ids']
    data = np.load(join(data_path, 'data_old', artist, 'dpaintings%d.npz' % n_colors))
    # order = data['concat_order']
    concat_lab = data['lab']
    # concat_lab, newindices, _, order, _ = sort_palettes(concat_lab, n_colors, sort_palettes=sorter in ['binary', 'lightness'],
    #                                                     n_themes=None, labels=None, step=1, sorter=sorter, viz=vis_sort)
    # concat_lab = concat_lab[np.argsort(order)]

    perms = mat['perms']
    tests = rgb2nlab(mat['tests']) # TESTING DATA
    # tests = mat['tests']
    #tests[tests < 0] = 0
    n_trains = mat['n_trains'][0].astype(int)
    
    if test_author is not None:
        # TESTING WITH DIFFIERENT DATA
        test_data = np.load(join(Common.PATH, 'data_old', test_author, 'dpaintings%d.npz' % n_colors))
        for tr in range(len(n_trains)):
            np.random.seed(tr)
            perms[tr][n_trains[tr]:] = np.random.permutation( len(tests) - n_trains[tr])
            
        np.random.seed()
        tests = test_data['lab'][:len(tests)]
        
    print 'number of training samples, ', n_trains
    mses = []
    for tr in range(len(n_trains)):
        print 'test ', tr
        _, n_cols, n_chas = concat_lab.shape
        n_train = n_trains[tr]
        
        perm = perms[tr][: min(n_train + max_tests, len(tests))].copy()
        Xtrain = concat_lab[perm[:n_train]].copy()
        Xtrain, _, _, _, _ = sort_palettes(Xtrain, n_colors, sort_palettes=sorter in ['binary', 'tsp'], n_themes=None,
                                                        labels=None, step=1, sorter=sorter, viz=vis_sort)

        Xtest = tests[perm[n_train:]].copy()
        # Xtest = concat_lab[perm[n_train:]].copy()
        for xtx, xt in enumerate(Xtest):
            removed_ids = missing_ids[perm[n_train:]][xtx][:n_missings]
            xt[removed_ids] = np.nan        
        
        Xtrain_ = Xtrain.copy()
        if method != 'mean':
            Xtest, Xtest_indices = align_colors_naive(Xtest, Xtrain_, clustering=0,
                                                      dupl=False, match_method='munkres', n_examples=3)

        Xtrain = Xtrain.reshape((-1, n_cols * n_chas))
        Xtest = Xtest.reshape((-1, n_cols * n_chas))
        if method == 'gplvm' or method == 'bgplvm' or method == 'mrd_bgplvm':
            re = test_gplvm_palette_prediction(Xtrain, Xtest, tr, artist, Q=4,
                                               method=method, optimize=optimize, forced_train=True)
        elif method == 'gmm':
            re = test_gmm_palette_prediction(Xtrain.reshape((-1, n_cols, n_chas)), Xtest, init='')
        elif method == 'mean':
            re = test_mean_palette_prediction(Xtest, n_cols, n_chas)
        elif method == 'retrieval':
            re = test_retrieval_palette_prediction(Xtrain, Xtest, n_cols, n_chas)
            re = re.reshape((-1, n_cols * n_chas))
            
        groundtruth = tests[perm[n_train:]]
        # groundtruth = concat_lab[perm[n_train:]].copy()
        prediction = rgb2nlab(np.clip(nlab2rgb(re.reshape((-1, n_cols, n_chas))), 0, 1))
        # prediction = re.reshape((-1, n_cols, n_chas))

        # tmpmask = np.isnan(Xtest)
        # tmpindices = np.nonzero(tmpmask)[1].reshape((-1, n_missings * n_chas))
        # prediction = re[np.arange(len(Xtest))[:, None], tmpindices].reshape((-1, n_missings, n_chas))

        mse = []
        for pal in range(len(re)):
            # removed_ids = missing_ids[perm[n_train:]][pal][:n_missings]
            xtest = Xtest[pal].reshape((n_cols, n_chas))
            mask = np.isnan(xtest[:, 0])
            truth = groundtruth[pal]
            # mse.append(hausdorff(truth[mask], prediction[pal][mask], mode=1))
            mse.append(hausdorff(truth, prediction[pal], mode=1))
            # mse.append(mean_squared_error(truth.ravel(), prediction[pal].ravel()))

        mses.append(mse)
        print 'mse ', sum(mse) /len(re)
        if vis != 0:
            # colorconv = lambda x: x
            colorconv = nlab2rgb
            fig = plt.figure('compare suggestion')
            ax1 = fig.add_subplot(141,title='prediction')
            ax1.axis('off')

            tmpmask = np.isnan(Xtest)
            # tmpindices = np.nonzero(tmpmask)[1].reshape((-1, n_missings * n_chas))
            # re1 = re[np.arange(len(Xtest))[:, None], tmpindices]
            # ax1.imshow(colorconv(re1.reshape((-1, n_missings, n_chas)))[:40], interpolation='nearest')
            ax1.imshow(colorconv(re.reshape((-1, n_cols, n_chas)))[:40], interpolation='nearest')
        
            ax2 = fig.add_subplot(143, title='input')
            ax2.axis('off')
            ax2.imshow(colorconv(Xtest.reshape((-1, n_cols, n_chas)))[:40], interpolation='nearest')
            
            ax3 = fig.add_subplot(142, title='groundtruth')
            ax3.axis('off')

            groundtruth1 = groundtruth.reshape((-1, n_cols, n_chas))
            # groundtruth1 = groundtruth1[np.arange(len(Xtest))[:, None], missing_ids[perm[n_train:], :n_missings]]
            # groundtruth1, _, _, _, _ = sort_palettes(groundtruth1, n_colors, sort_palettes=False, n_themes=None,
            #                                                     labels=None, step=1, sorter='lightness', viz='none')
            ax3.imshow(colorconv(groundtruth1[:40]), interpolation='nearest')
            # ax3.imshow(colorconv(groundtruth.reshape((-1, n_cols, n_chas)))[:20], interpolation='nearest')
            # plt.tight_layout()

            ax4 = fig.add_subplot(144, title='training data')
            ax4.imshow(colorconv(Xtrain.reshape((-1, n_cols, n_chas))), interpolation='nearest')
            plt.show()
            raw_input('press something dude')
        
        del Xtrain, Xtrain_, Xtest, perm
    
    mses = np.array(mses)
    print 'mean mse over datasets', mses.mean(axis=1).mean(axis=0)
    np.save(join(Common.PATH, 'data','%s_%s_pred_results.npy' % (artist, method)), mses)


def test_gplvm_palette_prediction(Xtrain, Xtest, train_id, artist, Q = 2,
                                  method= 'gplvm', optimize=True, forced_train=True):
    n_colors = Xtrain.shape[1] / 3
    if 'gplvm' in method:
        modelfile = join(Common.PATH, 'data', artist, 'model_%d.npy' % train_id)
        if os.path.isfile(modelfile) and not forced_train:
            m = np.load(modelfile).tolist()
        else:
            if method == 'gplvm':
                # kernel = GPy.kern.RBF(Q, ARD=True) + GPy.kern.Bias(Q)
                # kernel = GPy.kern.Linear(Q) + GPy.kern.Bias(Q) #+ GPy.kern.White(Q, 0.0001)
                # kernel = GPy.kern.Exponential(Q, ARD=True)
                # kernel = GPy.kern.TruncLinear(Q, ARD=True)
                kernel = None
                m = GPy.models.GPLVM(Xtrain[::1], Q, kernel=kernel)
                m.optimize('lbfgs', messages=1, max_iters=2000, gtol=1e-4)
                
            elif method == 'bgplvm':
                # kernel = GPy.kern.RBF(Q, 1., 1. / np.random.uniform(0, 1, (Q,)), ARD=True)
                # + GPy.kern.Bias(Q, np.exp(-2))
                kernel = GPy.kern.Linear(Q, ARD=True) # + GPy.kern.White(Q, 0.01)
                # kernel = GPy.kern.RBF(Q, ARD=False) + GPy.kern.White(Q, 0.00001)
                m = GPy.models.BayesianGPLVM(Xtrain[::1], Q, num_inducing=20, kernel=None)
                m.optimize('bfgs', messages=1, max_iters=2000, gtol=1e-4)
            elif method == 'mrd_bgplvm':
                kernel = GPy.kern.Linear(Q, ARD=True) + GPy.kern.White(Q, 1e-4)
                Xlist = [Xtrain[:, i:i+3] for i in range(0, Xtrain.shape[1] - 1, 3)]
                m = GPy.models.MRD(Ylist=Xlist, input_dim=Q, num_inducing=20, kernel=kernel)
                m.optimize(optimizer='bfgs', messages=True, max_iters=8e3, gtol=.1)

            np.save(modelfile, m)
            
    re = Xtest.copy()
    for fx, feat in enumerate(Xtest):
        if method == 'bgplvm':
            xnew, newm = m.infer_newX(feat[None, :], optimize=optimize)
            new_palette, _ = m.predict(np.array(xnew.mean))
        elif method == 'mrd_bgplvm':
            xnew, _ = m.infer_newX([feat[None, i:i+3] for i in range(0, feat.shape[0] - 1, 3)], optimize=True)
            new_palette = np.concatenate([m.predict(np.array(xnew.mean)[0], Yindex=i) for i in range(n_colors)])
        elif method == 'gplvm':
            xnew, newm = m.infer_newX(feat[None, :], optimize=optimize)
            new_palette, _ = m.predict(np.array(xnew))
        
        new_palette = new_palette[0]
        if not np.isfinite(new_palette).all():
            print '@'
            new_palette[np.isnan(new_palette)] = 0

        if sum(new_palette > 1) + sum(new_palette < 0) > 0:
            print '#'
        # new_palette[new_palette > 1] = 1
        # new_palette[new_palette <= 0] = 0
        
        nanid = np.isnan(feat)
        re[fx, nanid] = new_palette[nanid]
    
    del m
    
    return re


def test_gmm_palette_prediction(training_data, Xtest, init='kkmeans'):
    from sklearn.mixture import GMM
    from Regression import gmm_predict
    from smartpalette import FeatureAlignment
    from smartpalette.Kernels import hausdorff_kernel_matrix
    n_trains, n_colors, n_chas = training_data.shape
    if init == 'kkmeans':
        kernel_ = hausdorff_kernel_matrix(training_data)
        _, _, labels = FeatureAlignment.find_best_n_clusters(kernel_)
        unique_labels = np.unique(labels)
        n_clusters = len(unique_labels)
        gmm = GMM(n_components=n_clusters, covariance_type='full', init_params='c', params='cwJ')
        gmm.weights_ = np.array([(labels==l).sum() for l in unique_labels]) / float(n_trains)
        gmm.means_ = np.array([training_data[labels==l].mean(axis=0) for l in unique_labels]).reshape((-1,n_colors * n_chas))
        gmm.fit(training_data.reshape((-1, n_colors * n_chas)))
        re = gmm_predict(Xtest, gmm)
    else:

        gmm = find_best_params_gmm(training_data.reshape((-1, n_colors * n_chas)),
                                   np.arange(5, 40, 2), ['diag'], criterion='aic')
        # GMM(n_components = 45, covariance_type='spherical')

        # sgmm = GMM(n_components=8, covariance_type='diag')
        gmm.fit(training_data.reshape((-1, n_colors * n_chas)))
        re = gmm_predict(Xtest, gmm)

    # '''
    # labels = gmm.predict(training_data.reshape((-1, n_colors * n_chas)))
    # for i in range(gmm.n_components):
    #     plt.subplot(1, gmm.n_components, i + 1)
    #     plt.imshow(Common.nlab2rgb(training_data[labels == i]), interpolation='nearest')
    # plt.show()
    # #'''
    return re


def test_mean_palette_prediction(Xtest, n_cols, n_chas):
    re = Xtest.copy().reshape((-1, n_cols, n_chas))
    for pal in range(re.shape[0]):
        xtest = Xtest[pal].reshape((n_cols, n_chas))
        mask = np.logical_not(np.sum(np.isnan(xtest), axis=1))
        meancolor = xtest[mask].mean(axis=0)
        re[pal][np.logical_not(mask)] = meancolor
    
    return re


def test_retrieval_palette_prediction(Xtrain, Xtest, n_cols, n_chas):
    re = np.zeros_like(Xtest).reshape((-1, n_cols, n_chas)) # Xtest.copy().reshape((-1, n_cols, n_chas))
    for pal in range(re.shape[0]):
        # xtest = Xtest[pal].reshape((n_cols, n_chas))
        # mask = np.logical_not(np.sum(np.isnan(xtest), axis=1))
        mask = np.bitwise_not(np.isnan(Xtest[pal]))
        min_d, best_tpal = 1e10, 0
        for tpal in range(Xtrain.shape[0]):
            d = euc(Xtest[pal][mask].ravel(), Xtrain[tpal][mask].ravel())
            if d < min_d:
                min_d = d
                best_tpal = tpal

        re[pal, :, :] = Xtrain[best_tpal].reshape((n_cols, n_chas))
    return re


def generate_tests(artists, step=1, train_percents=0.5):
    from Common import PATH
    from sklearn.cluster import KMeans
    # from skimage.color import rgb2lab
    # from sklearn.mixture import GMM
    # from scipy.io import savemat, loadmat
    
    for artist in artists:
        DPATH = join(PATH, 'data', artist)
        # data = np.load(join(DPATH, 'dpaintings.npz'))
        themes = loadmat(join(DPATH, 'themes.mat'))
        mat = loadmat(join(DPATH, 'themesData.mat'))
        features = mat['datapoints'][0,0]['features'][::step]
    
        X = themes['data'][::step] #mat['datapoints'][0,0]['rgb'][::5][:10]
        allcolors = X.reshape((-1, 3))
        
        print 'preparing color list'
        km = KMeans(n_clusters=2000, verbose=1,n_jobs=3, n_init=6)
        km.fit(allcolors)
        colordict = km.cluster_centers_
    
        np.random.seed()
        missing_ids = []
        ncols = X.shape[1]
        
        for pal in X:
            removed_ids = np.random.permutation(ncols)[:ncols] # np.random.randint(0, 5, 4)
            # pal[removed_ids[0]] = np.nan
            missing_ids.append(removed_ids)
    
        n_models = 5
        perms = []
        n_trains = []
        
        covars = []
        means = []
        weights = []
        
        for m in range(n_models):
            print 'training model', m
            perm = np.random.permutation(len(X))
            n_train = train_percents * len(X)
            
            perms.append(perm)
            n_trains.append(n_train)
            
            Xtrain = features[perm[:n_train]]
            
            gmm = find_best_params_gmm(Xtrain, np.arange(10, 30, 2), ['diag'])
            # GMM(n_components=60, covariance_type='full').fit(Xtrain)
            covars.append(gmm.covars_)
            means.append(gmm.means_)
            weights.append(gmm.weights_)
        
        savemat(join(DPATH, 'tests.mat'), {'tests': X, 'colordict': colordict, \
        'missing_ids': missing_ids, 'perms': perms, 'n_trains': n_trains, \
        'covars': covars, 'means': means, 'weights': weights})


def prepare_color_by_numbers(artists, n_colors=5, path=PATH):
    import os, sys
    # from Common import PATH
    import shutil
    from sklearn.cluster import KMeans
    from skimage.io import imread, imsave
    from skimage import img_as_float
    from skimage.color import lab2rgb
    from Common import nlab2rgb
    from numpy.random import multivariate_normal
    from scipy.io import loadmat
    
    if isinstance(artists, str):
        artists = [artists]
    
    for artist in artists:
        print artist
        IPATH = join(path, join('img', artist ))
        OPATH = join(path, join('data', artist))
        if not os.path.isdir(OPATH):
            os.makedirs(OPATH)
            
        WIN_SIZE = 200
        STEP_FACTOR = 2
    
        # files = sorted([f for f in os.listdir(IPATH) \
        # if os.path.isfile(join(IPATH, f)) and f.endswith('.jpg')])
    
        testdata = loadmat(join(OPATH, 'tests.mat'))
        tests = testdata['tests']
        n_trains = testdata['n_trains'][0].astype(int)
        perms = testdata['perms']
        missing_ids = testdata['missing_ids']
        
        n_tests = len(n_trains)
        
        data = np.load(join(OPATH, 'dpaintings%d.npz' % n_colors))
        files = data['fnames']
        clustered_lab = data['clustered_lab']
        lab = data['lab']
        
        # must be sure that these arrays are the same
        assert(np.all(tests.shape == lab.shape))
        
        all_patches = []
        all_centers = []
        count = 0
        for f in files:
            img = img_as_float(imread(join(IPATH,f)))
            rows, cols, _ = img.shape
            for i in np.arange(0, cols, WIN_SIZE / STEP_FACTOR):
                for j in np.arange(0, rows, WIN_SIZE / STEP_FACTOR):
                    # if count % 2 != 0:
                    #     count += 1
                    #     continue
                    
                    if i + WIN_SIZE / STEP_FACTOR > cols or j + WIN_SIZE / STEP_FACTOR > rows:
                        break
                    
                    all_centers.append( (lab2rgb(clustered_lab[count][None, ...]) * 255).astype(np.uint8)[0] )
                    all_patches.append(img[j:min(j + WIN_SIZE, rows - 1), i: min(i + WIN_SIZE, cols - 1)])
                    count += 1
        
        all_centers = all_centers[::2]
        all_patches = all_patches[::2]
        assert(len(all_centers) == len(lab))
        all_centers = np.array(all_centers)
        all_patches = np.array(all_patches)
        
        for t in range(n_tests):
            print 'test ', t
            n_train = n_trains[t]
            n_test = 200
            
            perm = perms[t]
            
            missing_ids_ = missing_ids[perm[n_train:n_train + n_test]]
            test_centers = all_centers[perm[n_train:n_train + n_test]]
            test_patches = all_patches[perm[n_train:n_train + n_test]]
            train_centers = all_centers[perm[:n_train]]
            train_patches = all_patches[perm[:n_train]]
            
            if not os.path.isdir(join(OPATH, 'test_%d' % t)):
                os.makedirs(join(OPATH, 'test_%d' % t, 'test'))
                os.makedirs(join(OPATH, 'test_%d' % t, 'train'))
                
            savemat(join(OPATH, 'test_%d' % t, 'test', 'tests.mat'), {'missing_ids' : missing_ids_})
            np.savetxt(join(OPATH, 'test_%d' % t, 'test', 'tests.txt'), missing_ids_,fmt='%d')
            
            for acenters, apatches, mode in [(test_centers, test_patches, 'test'), \
            (train_centers, train_patches, 'train')]:
                fo = open(join(OPATH, 'test_%d' % t, mode, 'palettes.tsv'), 'w')
                fo.write('pid	id	image	colors	numColors	log\n')
                for i in range(len(acenters)):
                    centers = acenters[i]
                    fo.write('%d\t0\t%s_%s_%d_%d.png\t' % (i, artist, mode, t, i))
                    for cx, co in enumerate(centers):
                        fo.write('%d,%d,%d' % tuple(co) )
                        
                        if cx != len(centers) - 1:
                            fo.write(' ')
                            
                    fo.write('\t5\t\n')
                    imsave(join(OPATH, 'test_%d' % t , mode, '%s_%s_%d_%d.png' % (artist, mode, t, i)), \
                    apatches[i])
                
                fo.close()
            print    


def prepare_themes(authors=None,n_colors=5,level=0):
    """
    :param authors:
    :param n_colors:
    :param level:
    :return:
    """
    from scipy.io import savemat
    from oct2py import octave
    from Common import PATH, nlab2rgb, rgb2nlab, lab2rgb

    codeRoot = '/Users/phanquochuy/Projects/smartpalette/lib/ColorCompatibility/colorCode/'
    data_path = '/Users/phanquochuy/Projects/smartpalette/data/data/'

    octave.addpath(codeRoot)
    octave.addpath(join(codeRoot, 'data'))
    octave.addpath(join(codeRoot, 'circstat'))
    octave.addpath(join(codeRoot, 'glmnet_matlab'))

    if authors is None:
        authors = os.listdir(join(PATH, 'data'))
        
    if isinstance(authors, str):
        authors = [authors]
        
    print authors
    for aux, author in enumerate(authors):
        print author
        OPATH = join(PATH, 'data', author)

        data = np.load(join(OPATH, 'dpaintings%d.npz' % n_colors))
        lab = data['clustered_lab'][level]
        themes = lab2rgb(lab)
 
        savemat(join(OPATH, 'themes.mat'), {'data': themes, 'ids' : np.atleast_2d(np.arange(len(themes))).T,
                                            'names' : np.array([['theme_' + str(i)] for i in range(len(themes))]).T,
                                            'targets': np.atleast_2d(np.repeat(5.0, len(themes))).T})

        datapoints = octave.extract_compat_features(themes)
        Common.modify_npz(join(OPATH, 'clusters_nc%d_l%d.npz' % (n_colors, 0)),
                          [('compat_features', datapoints['features'])])


def test_kmeans(artists, methods, n_colors=5,n_clusters=10,step=1):
    from sklearn.cluster import KMeans
    from KernelKMeans import KernelKMeans
    from Kernels import hausdorff_kernel_matrix
    from Utils import vis_artist
    
    for artist in artists:
        data = np.load(join(PATH, 'data', artist, 'dpaintings%d.npz' % n_colors))
        themesData = loadmat(join(PATH, 'data', artist, 'themesData.mat'))
        
        X = data['lab'] # .reshape((-1, n_colors * 3))
        Y = themesData['datapoints'][0,0]['features'][::step]
        
        kernels = ['hausdorff', 'rbf', 'pmk']
        
        for method in methods:
            if method in kernels:
                if method == 'hausdorff':
                    method = hausdorff_kernel_matrix
                else:
                    X = X.reshape((-1, n_colors * 3))
                    
                km = KernelKMeans(n_clusters=n_clusters, kernel=method, max_iter=2000,  verbose=0, random_state=None, gamma=4, tol=1e-5)
                labels = km.fit_predict(X)
            else:
                km = KMeans(n_clusters=n_clusters)
                labels = km.fit_predict(Y)
            
            '''
            fig = plt.figure('compare clustering methods')
            axs = []
            for i in range(1,5):
                axs.append(fig.add_subplot(410 + i))
                axs[-1].imshow()   
            '''
            vis_artist(artist, step=1, show='orig', labels=labels)
            raw_input('press to continue')


def test_kde():
    PATH = '/media/phan/BIGDATA/DPAINTINGS'
    from sklearn import svm, neighbors
    from sklearn.neighbors.kde import KernelDensity
    from matplotlib import pyplot as plt
    from sklearn.naive_bayes import GaussianNB
    from sklearn.mixture import GMM
    from sklearn.decomposition import PCA
    
    #authors = ['monet', 'luce'] #os.listdir(join(PATH, 'data'))[:5]
    authors = ['gogh', 'inria_holiday', 'urbino', 'metcalf']
    
    print authors
    
    X = []
    for aux, author in enumerate(authors):
        print author
        OPATH = join(PATH, 'data', author)
        data = loadmat(join(OPATH, 'themesData.mat'))
        data = data['datapoints'][0,0]['features']#[::4]
        
        X.append(data)
    
    Xpos = X[0]
    Xneg = np.concatenate(X[1:])
    
    #kde = KernelDensity(kernel='gaussian', bandwidth=0.1).fit(Xtrain[0])
    gmm = GMM(n_components=20)
    gmm.fit(Xpos)
    
    pca = PCA(n_components=4)
    pca.fit(Xpos)
    
    #scores1 = kde.score_samples(Xtest[0])
    #scores2 = kde.score_samples(Xtest[1])
    
    #scores1 = gmm.score(Xpos)
    scores1 = pca.score_samples(Xpos)
    #scores1 = gmm.predict_proba(Xpos)
    #print scores1[:20]
    
    #np.clip(scores1, 0, 1000, scores1)
    #step = (scores1.max() - scores1.min()) / 3
    pos_ratings = 1 - scores1 / scores1.max()
    #pos_ratings = scores1
    #print pos_ratings
    
    #scores2 = gmm.score(Xneg)
    scores2 = pca.score_samples(Xneg)
    #scores2 = gmm.predict_proba(Xneg)
    #Xneg = Xneg[scores2 < 0.1]
    #scores2 = scores2[scores2 < 0.1]
    
    #step = (scores2.max() - scores2.min()) / 3
    neg_ratings = 1 - scores2 / scores1.max()
    #neg_ratings = scores2
    #print neg_ratings
    
    print "done rating, starts regressing"
    svr = SVR(kernel='linear', C=0.5, gamma=5)
    lasso = Lasso(alpha=0.00016,fit_intercept=False, max_iter=2000, tol=1e-4)
    
    X = np.concatenate([Xpos, Xneg])
    y = np.concatenate([pos_ratings, neg_ratings])
    
    perm = np.random.permutation(len(y))
    n_train = 0.6 * len(y)
    
    Xtrain = X[perm[:n_train]]
    ytrain = y[perm[:n_train]]
    
    Xtest = X[perm[n_train:]]
    ytest = y[perm[n_train:]]
    
    pred_ratings = svr.fit(Xtrain, ytrain).predict(Xtest)
    #pred_ratings = lasso.fit(Xtrain, ytrain).predict(Xtest)
    
    print pred_ratings[:50]
    print ytest[:50]
    print 'svm rbf', mean_squared_error(pred_ratings, ytest), mean_absolute_error(pred_ratings, ytest)
    print 'mean ', mean_squared_error(np.repeat(ytrain.mean(), len(ytest)), ytest)


def test_classification_1():
    PATH = '/media/phan/BIGDATA/DPAINTINGS'
    from sklearn import svm, neighbors
    #from sklearn.neighbors.kde import KernelDensity
    from sklearn.metrics import confusion_matrix
    from matplotlib import pyplot as plt
    from sklearn.multiclass import OneVsRestClassifier, OneVsOneClassifier
    from sklearn.svm import LinearSVC
    from sklearn.naive_bayes import GaussianNB
    from Kernels import hausdorff_kernel_matrix1
    
    data = loadmat(join(PATH, 'data', 'renoir_gogh_cezanne_monet_themesthemesData.mat'))
    
    X = data['datapoints'][0,0]['features']
    y = data['datapoints'][0,0]['targets']
    
    perm = np.random.permutation(len(X))
    n_train = 0.7 * len(X)
    
    Xtrain = X[perm[:n_train]]
    ytrain = y[perm[:n_train]]
    
    Xtest = X[perm[n_train:]]
    ytest = y[perm[n_train:]]
    
    print 'n_train=', len(Xtrain), ' n_test=', len(Xtest)
    #labels1 = svm.SVC(kernel=hausdorff_kernel_matrix1, C = 0.1).fit(Xtrain, ytrain).predict(Xtest)
    
    #gnb = GaussianNB()
    #gnb.fit(Xtrain, ytrain)
    #labels1 = gnb.predict(Xtest)
    #prob = gnb.predict_proba(Xtest)
    
    #labels1 = OneVsRestClassifier(LinearSVC()).fit(Xtrain, ytrain).predict(Xtest)  
    labels1 = OneVsRestClassifier(LinearSVC()).fit(Xtrain, ytrain).predict(Xtest)  
    
    #n_neighbors = 10
    #weights = 'distance' #'distances'
    #clf1 = neighbors.KNeighborsClassifier(n_neighbors, weights=weights)
    #clf1.fit(Xtrain, ytrain)
    
    
    #labels2 = clf1.predict(Xtest)
    #print zip(labels, ytest)
    print mean_squared_error(labels1, ytest) #0.0993479942535
    #print mean_squared_error(labels2, ytest)
    #'''
    cm = confusion_matrix(ytest, labels1)
    cm_normalized = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    print(cm_normalized)
    #print prob
    
    # Show confusion matrix in a separate window
    #'''
    plt.matshow(cm_normalized)
    plt.title('Confusion matrix')
    plt.colorbar()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.show()
    plt.savefig('.tmp/confusionmatrix.png')
    #'''    


def test_classification():
    PATH = '/media/phan/BIGDATA/DPAINTINGS'
    from sklearn import svm, neighbors
    #from sklearn.neighbors.kde import KernelDensity
    from sklearn.metrics import confusion_matrix
    from matplotlib import pyplot as plt
    from sklearn.multiclass import OneVsRestClassifier, OneVsOneClassifier
    from sklearn.svm import LinearSVC
    from sklearn.naive_bayes import GaussianNB
    
    authors = ['monet', 'inria_holiday'] #os.listdir(join(PATH, 'data'))[:5]
    #authors = ['renoir', 'inria_holiday', 'urbino', 'metcalf']
    
    print authors
    X = []
    Xtrain, ytrain, Xtest, ytest = [], [], [], []
    for aux, author in enumerate(authors):
        print author
        OPATH = join(PATH, 'data', author)
        data = loadmat(join(OPATH, 'themesData.mat'))
        data = data['datapoints'][0,0]['features'][::4]
        
        X.append(data)
        
        n_train = int(0.5 * len(data))
        n_test = len(data) - n_train
        
        perm = permutation(len(data))
        Xtrain.append( data[perm[:n_train]] )
        ytrain.append( [aux] * n_train )
        
        Xtest.append( data[perm[n_train:]] )
        ytest.append( [aux]  * n_test )
    
    Xtrain = np.concatenate(Xtrain)
    Xtest = np.concatenate(Xtest)
    ytrain = np.concatenate(ytrain)
    ytest = np.concatenate(ytest)
    
    print 'n_train=', len(Xtrain), ' n_test=', len(Xtest)
    svc = svm.SVC(kernel='linear').fit(Xtrain, ytrain)
    labels1 = svc.predict(Xtest)
    
    #gnb = GaussianNB()
    #gnb.fit(Xtrain, ytrain)
    #labels1 = gnb.predict(Xtest)
    #prob = gnb.predict_proba(Xtest)
    
    #labels1 = OneVsOneClassifier(LinearSVC()).fit(Xtrain, ytrain).predict(Xtest)  
    
    #n_neighbors = 10
    #weights = 'distance' #'distances'
    #clf1 = neighbors.KNeighborsClassifier(n_neighbors, weights=weights)
    #clf1.fit(Xtrain, ytrain)
    
    
    #labels2 = clf1.predict(Xtest)
    #print zip(labels, ytest)
    print mean_squared_error(labels1, ytest) #0.0993479942535
    #print mean_squared_error(labels2, ytest)
    #'''
    cm = confusion_matrix(ytest, labels1)
    
    print(cm)
    #print prob
    
    # Show confusion matrix in a separate window
    #'''
    plt.matshow(cm)
    plt.title('Confusion matrix')
    plt.colorbar()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.show()
    plt.savefig('.tmp/confusionmatrix.png')
    #'''


def test_gmm_density(artist, n_colors=5):
    from scipy.io import loadmat
    from sklearn.cluster import KMeans
    from sklearn.neighbors.kde import KernelDensity
    from sklearn.mixture import GMM
    from Regression import gmm_predict
    from Common import PATH, nlab2rgb, rgb2nlab
    from ColorManifolds import align_colors
    #PATH = '/media/phan/BIGDATA/DPAINTINGS'
    #artist = 'turk'
    
    data = np.load(join(PATH, 'data/%s/dpaintings%d.npz' % (artist, n_colors)))
    mat = loadmat(join(PATH, 'data/%s/themesData.mat' % artist))
    test_data = loadmat(join(PATH, 'data/%s/test_themesData.mat' % artist))
    offsets = mat['datapoints'][0, 0]['offsets']
    scales = mat['datapoints'][0, 0]['scales']
    X1 = mat['datapoints'][0,0]['features'][::5]
    
    #order = data['concat_order']
    #labels = data['labels']
    #labels = labels[order]
    
    X = data['concat_lab'].reshape((-1, 15))
    X = X[np.argsort(data['concat_order'])]
    
    X2 = data['orig_lab'].reshape((-1, 15))
    
    assert(len(X1) == len(X2) == len(X))
    
    n_train = 0.6 * len(X1)
    perm = np.random.permutation(len(X1))

    Xtrain2 = X2[perm[:n_train]]
    Xtest2 = X2[perm[n_train:]]    
    
    Xtrain1 = X1[perm[:n_train]]
    Xtest1 = X1[perm[n_train:]]

    Xtrain = X[perm[:n_train]]
    Xtest = X[perm[n_train:]]
    
    '''
    gmm = GMM(n_components=100).fit(Xtrain2)
    print gmm._n_parameters()
    print 'color orig lab, BIC', gmm.bic(Xtest2)
    print 'color orig lab, AIC', gmm.aic(Xtest2)
    
    gmm = GMM(n_components=100).fit(Xtrain1)
    print gmm._n_parameters()
    print 'color compat features BIC', gmm.bic(Xtest1)
    print 'color compat features AIC', gmm.aic(Xtest1)
    
    gmm = GMM(n_components=100).fit(Xtrain)
    print gmm._n_parameters()
    print 'color concat lab, BIC', gmm.bic(Xtest)    
    print 'color concat lab, AIC', gmm.aic(Xtest)    
    '''
    from scipy.signal import argrelextrema, argrelmax
    
    tests = test_data['datapoints'][0,0]['features']
    tests = (tests - offsets) / scales
    #tests_orig = loadmat(join(PATH, 'data/%s/test_themes.mat' % artist))['tests']
    tests_orig = X[:10]
    
    #tests[np.isnan(tests)] = 0
    orig_rgb = test_data['datapoints'][0,0]['rgb']
    truth_rgb = mat['datapoints'][0,0]['rgb'][::5][:10]
    
    colordict = loadmat(join(PATH, 'data/%s/tests.mat' % artist))['colordict']
    cd_size = len(colordict)
    gmm1 = GMM(n_components=80).fit(Xtrain1)
    gmm2 = GMM(n_components=80).fit(Xtrain)
    #kde = KernelDensity(bandwidth=0.2, kernel='gaussian').fit(Xtrain)
    
    exts1 = [] #np.zeros(10, dtype='int32')
    exts2 = []
    for t in range(len(tests) / cd_size):
        scores1, _ = gmm1.score_samples(tests[t * cd_size: t * cd_size + cd_size])
        #scores1[np.isnan(scores1)] = 0
        
        scores2, _ = gmm2.score_samples(
        rgb2nlab(orig_rgb[t * cd_size: t * cd_size + cd_size].reshape((-1, 5, 3))).reshape((-1, 15)))
        
        #scores2 = kde.score_samples(\
        #rgb2nlab(orig_rgb[t * cd_size: t * cd_size + cd_size].reshape((-1, 5, 3))).reshape((-1, 15)))  
        
        #exts1.extend( argrelmax(scores1)[0] + t * cd_size )
        exts1.append( np.argmax(scores1) + t * cd_size)
        exts2.append( np.argmax(scores2) + t * cd_size)
    
    #tests_orig_copy = align_colors(tests_orig, Xtrain.reshape((-1, 5, 3)), \
    #clustering=True, dupl=True).reshape((-1, 5, 3))
    
    #predictions = gmm_predict(rgb2nlab(tests_orig).reshape((-1,15)), gmm2)
    
    #tests_orig[0, 9:11] = np.nan
    #tests_orig[1, 6:9] = np.nan
    
    #predictions = gmm_predict(tests_orig.reshape((-1,15)), gmm2)
    #predictions = nlab2rgb(predictions.reshape((-1, 5,3)))
    
    fig = plt.figure('compare suggestion')
    ax1 = fig.add_subplot(131)
    ax1.imshow(orig_rgb[exts1].reshape((-1, 5,3)), interpolation='none')

    ax2 = fig.add_subplot(132)
    ax2.imshow(orig_rgb[exts2].reshape((-1, 5,3)), interpolation='none')
    #ax2.imshow(predictions, interpolation='none')
    
    ax3 = fig.add_subplot(133)
    ax3.imshow(truth_rgb.reshape((-1, 5,3)), interpolation='none')
    
    plt.show()
    #raw_input('space to continue')


def test_kernel_pca(authors, n_colors):
    #PATH = '/media/phan/BIGDATA/DPAINTINGS'
    from Kernels import hausdorff_kernel_matrix
    from sklearn.decomposition import KernelPCA, PCA
    from sklearn.cluster import KMeans
    from sklearn import decomposition
    from sklearn import manifold
    from mpl_toolkits.mplot3d import Axes3D   
    from Common import nlab2rgb
    from matplotlib.offsetbox import OffsetImage, AnnotationBbox    
    
    #from scipy.io import loadmat
    #authors = ['turk'] #os.listdir(join(PATH, 'data'))
    if isinstance(authors, str):
        authors = [authors]
        
    step = 1
    fig = plt.figure(11)
    for aux, author in enumerate(authors):
        print author
        OPATH = join(PATH, 'data', author)
        
        themesData = loadmat(join(OPATH, 'themesData.mat'))
        
        data3 = themesData['datapoints'][0,0]['features'][::step]
        #data3 = data3[::max(1, (len(data3) / 500))]
        
        #PREPARING THE DATA
        dpaintings = np.load(join(OPATH, 'dpaintings%d.npz' % n_colors))
        data1 = dpaintings['lab'][::step]
        data2 = dpaintings['concat_lab'].reshape((-1, n_colors * 3))
        
        #order = dpaintings['concat_order']
        labels1 = dpaintings['labels'][::step]
        labels2 = labels1
        labels3 = labels1
        #nc = len(np.unique(labels1)) #- 5
        #print 'n clusters=', nc
        #labels2 = KernelKMeans(n_clusters=nc, kernel='rbf').fit_predict(data1.reshape((-1, n_colors * 3)))
        #labels3 = KMeans(n_clusters=nc).fit_predict(data3)
        #data[np.isnan(data)] = 1e-8
        
        #data = loadmat(join(OPATH, 'themesData.mat'))
        #data = data['datapoints'][0,0]['features']

        #COMPUTING KERNEL
        #kernel = hausdorff_kernel_matrix(data1.reshape((-1, n_colors * 3)))
        kernel = np.load(join(OPATH, 'kernel.npz'))['kernel']
        #km = KMeans(n_clusters=40, verbose=1, n_jobs=3)
        #labels = km.fit_predict(data)
        
        print 'kernel size =', len(kernel)
        newX1 = KernelPCA(n_components=2, kernel='precomputed').fit_transform(kernel)
        newX1 *= 100
        #newX2 = manifold.LocallyLinearEmbedding(n_neighbors=40, n_components=4,  method='modified').fit_transform(data2)   
        #newX2 = manifold.Isomap(10, n_components=4).fit_transform(data2)
        #newX2 = decomposition.TruncatedSVD(n_components=4).fit_transform(data2)
        #newX1 = KernelPCA(n_components=4, kernel='rbf').fit_transform(data1)
        newX2 = KernelPCA(n_components=2, kernel='rbf').fit_transform(data1.reshape((-1, n_colors * 3)))
        newX3 = PCA(n_components=2).fit_transform(data3)
        
        ax1 = fig.add_subplot(1, len(authors), aux)
        ax1.set_xlim(-10, 10)
        ax1.set_ylim(-5,5)
        ax1.scatter(newX1[:, 0], newX1[:, 1],  c=labels1, s=10,lw=0)   
        
        #'''
        vis_labels = np.random.permutation(len(np.unique(labels1)))[:10]
        for lbl in vis_labels:
            vis_ids = np.random.randint(0, len(labels1[labels1==lbl]), 7)
            vis_ids = np.nonzero(labels1 == lbl)[0][vis_ids]
            #print vis_ids
            for i in vis_ids:
                rgb = nlab2rgb(data1[i][None, ...])
                im = OffsetImage(rgb, zoom=7, interpolation='none')
                xy = newX1[i]
                ab = AnnotationBbox(im, xy,
                                    xybox=(-0.5, 0.5),
                                    xycoords='data',
                                    boxcoords="offset points",
                                    pad=0.05,
                                    arrowprops=dict(arrowstyle="->"))
                                    #arrowprops=None)            
                ax1.add_artist(ab)
        '''
        ax2 = fig.add_subplot('132')
        ax2.scatter(newX2[:, 0], newX2[:, 1], c=labels2, s=10,lw=0)
        
        ax3 = fig.add_subplot('133') #, projection='3d')
        ax3.scatter(newX3[:, 0], newX3[:, 1],  c=labels3, s=10,lw=0)
        '''
    plt.tight_layout()
    plt.show()
    raw_input('press to close')


def test_regressors(artists, n_colors=5):
    #from glmnet import ElasticNet
    from sklearn.decomposition import KernelPCA, PCA
    #from glmnet.cv import CVGlmNet
    from Kernels import hausdorff_kernel_matrix
    #from sklearn import gaussian_process
    from sklearn import linear_model
    from Common import rgb2nlab
    from sklearn.mixture import GMM
    #from skimage.color import rgb2lab
    #from KernelKMeans import KernelKMeans
    
    author = artists[0] #'turk'
    fname = 'themesData.mat'
    step = 5
    
    OPATH = join(PATH, 'data', author)
    data = loadmat(join(OPATH, fname))
    
    dpaintings = np.load(join(OPATH, 'dpaintings%d.npz' % n_colors))
    #labels = dpaintings['labels']
    order = dpaintings['concat_order']
    Xlab = dpaintings['concat_lab'].reshape((-1, n_colors * 3))[np.argsort(order)]
    gmm = GMM(n_components=40).fit(Xlab)
    gmm.score_samples()
    #Xlab = data['datapoints'][0,0]['rgb'][::step] #[order]
    #Xlab = rgb2nlab(Xlab.reshape((-1, n_colors, 3))).reshape((-1, n_colors * 3))
    
    X = data['datapoints'][0,0]['features'][::step] #[order]
    y = data['datapoints'][0,0]['targets'][::step].ravel() #[order]
    
    ylab = y #[order]
    
    assert(len(Xlab) == len(X))
    print "n points =", len(Xlab)
    np.random.seed()
    n_tests = 2
    mses = np.zeros(5, dtype='double')
    
    for test in range(n_tests):
        print '.'
        n_train = 0.4 * len(X)
        perm = permutation(len(X))
        
        Xtrain = X[perm[:n_train]]
        ytrain = y[perm[:n_train]]
        
        ytrain_lab = ylab[perm[:n_train]]
        Xtrain_lab = Xlab[perm[:n_train]]
        
        Xtest = X[perm[n_train:]]
        ytest = y[perm[n_train:]]
        
        Xtest_lab = Xlab[perm[n_train:]]
        ytest_lab = ylab[perm[n_train:]]
        
        #kernel = hausdorff_kernel_matrix(Xlab)
        #kernel = pmk.compute_pmk_uniform(list(X), FINEST_SIDE_LENGTH, SIDE_LENGTH_FACTOR, DISCRETIZE_ORDER, 0, True, True)
        # C = 0.1, 0.05
        svr = SVR(kernel='rbf', C=0.05, gamma=4) #C=0.5, gamma=4) #C=1e3, gamma=0.1)
        #gp = gaussian_process.GaussianProcess(regr='constant', corr='cubic', theta0=1e-2, thetaL=1e-4, thetaU=1e-1)
        #svr1 = SVR(kernel=hausdorff_kernel_matrix, C=1, gamma=2, tol=1e-5, epsilon=0.001)
        #enet = ElasticNet(alpha=1)
        #svr1 = SVR(kernel='', C=1, gamma=10)
        
        clf = Lasso(alpha=0.00016,tol=1e-4) #fit_intercept=True,
        y1 = clf.fit(Xtrain, ytrain).predict(Xtest)
        
        y2 = svr.fit(Xtrain_lab, ytrain_lab).predict(Xtest_lab)
        #y3 = SVR(kernel='linear', C=0.5, gamma=5).fit(Xtrain, ytrain).predict(Xtest)
        #y4 = gp.fit(Xtrain_lab, ytrain_lab).predict(Xtest_lab)
        #y4 = SVR(kernel='linear', C=0.5, gamma=5).fit(Xtrain_lab, ytrain_lab).predict(Xtest_lab)
        
        #y4 = svr1.fit(Xtrain_lab, ytrain_lab).predict(Xtest_lab)
        #enet.fit(Xtrain, ytrain)
        #enet_cv = CVGlmNet(enet, n_folds=10, n_jobs=10)
        #enet_cv.fit(Xtrain, ytrain)
        #y4 = enet_cv.predict(Xtest)
        y5 = np.repeat(ytrain.mean(), len(ytest))
        
        mses[0] += mean_absolute_error(y1, ytest) #mean_squared_error(y1, ytest)
        mses[1] += mean_absolute_error(y2, ytest_lab) #mean_squared_error(y2, ytest_lab)
        #mses[2] += mean_squared_error(y3, ytest)
        #mses[3] += mean_squared_error(y4, ytest_lab)
        mses[4] += mean_squared_error(y5, ytest)
        
        
    print 'sklearn lasso', mses[0] / n_tests
    print 'svm sorted rbf', mses[1] / n_tests 
    print 'svm rbf',  mses[2] / n_tests
    print 'svm hausdorff', mses[3] / n_tests
    print 'baseline',  mses[4] / n_tests


def test_tsp_rearrange():
    from scipy.spatial.distance import euclidean as euc
    from sklearn.decomposition import KernelPCA
    from sklearn.cluster import KMeans
    from sklearn import decomposition
    from sklearn import manifold
    from openopt import *
    
    import networkx as nx    
    from matplotlib import pyplot as plt
    from matplotlib import cm as cmap
    from matplotlib.colors import ListedColormap
    import Common

    PATH = '/media/phan/BIGDATA/DPAINTINGS'
    authors = ['renoir'] #os.listdir(join(PATH, 'data'))
    for aux, author in enumerate(authors):
        print author
        OPATH = join(PATH, 'data', author)
        #files = [f for f in os.listdir(OPATH) if f.endswith('jpg.npz')]

        kernel = np.load(join(OPATH, 'kernel.npz'))['kernel']
        #labels = np.load(join(OPATH, 'dpaintings.npz'))['labels']
        dpaintings = np.load(join(OPATH, 'dpaintings.npz'))
        
        #kkm = dpaintings['kkm'].tolist()
        #kms = dpaintings['kms'].tolist()
        re_data = dpaintings['data']
        data = np.clip(dpaintings['clustered_lab'] / 255., 0, 1)
        
        for i in range(re_data.shape[0]):
            re_data[i][np.isnan(re_data[i])] = 1e-8
            
        #re_data = re_data.reshape((re_data.shape[0], re_data.shape[1], -1))
        #centers = kkm.centers_#[:80]
        #data = np.load(join(OPATH, 'dpaintings.npz'))['orig_data']
        #center_data = np.array([pa.mean(axis=0) for pa in re_data])#[:80] #data[centers]
        #np.clip(center_data, 0, 1, center_data)
        
        #project the data to a 1-D manifold for sorting purpose.
        kpca = KernelPCA(n_components=1, kernel='precomputed')
        newX = kpca.fit_transform(kernel)
        
        #center_order = newX[centers].ravel().argsort()
        #center_data = center_data[center_order]
        center_order = newX.ravel().argsort()
        center_data = data[center_order]
        
        #creat graph
        G = nx.DiGraph()
        n_centers = center_data.shape[0]
        n_colors = center_data.shape[1]
        nodeids = np.arange(n_centers * n_colors).reshape((n_centers, n_colors))
        for center in range(nodeids.shape[0] - 1):
            for color1 in range(nodeids.shape[1]):
                for color2 in range(nodeids.shape[1]):
                    w = euc(center_data[center, color1], center_data[center + 1, color2])
                    if w > 0.0:
                        G.add_edge(nodeids[center, color1], nodeids[center + 1, color2], \
                        {'weight': w})
                        
        #the edges that let the "saleman" goes back to the first cluster
        for color1 in range(nodeids.shape[1]):
            for color2 in range(nodeids.shape[1]):
                w = euc(center_data[-1, color1], center_data[0, color2])
                G.add_edge(nodeids[n_centers-1, color1], nodeids[0, color2], \
                {'weight': w})
                
        p = TSP(G, objective = 'weight', start = 0)  
        r = p.solve('glpk')
        #print(r.nodes)
        #print(r.edges)
        
        #new Indices matrix
        tmp = np.tile(range(n_colors), n_centers).reshape((n_centers, -1))
        newindices = tmp.ravel()[r.nodes[:-1]].reshape((-1, n_centers)).T
        old_center_data = center_data.copy()
        for row in range(center_data.shape[0]):
            center_data[row, :] = center_data[row, newindices[row]]
        
        #resort data to the new order
        '''
        for i in range(re_data.shape[0]):
            order = newindices[i]
            for rx, row in enumerate(re_data[i]):
                re_data[i][rx, :] = row[order]
        '''
                
        #concat_data = re_data 
        #Common.modify_npz(join(OPATH, 'dpaintings.npz'), \
        #[('concat_data', concat_data), ('concat_indices', newindices)])
   
        pos = []
        for row in range(nodeids.shape[0]):
            for col in range(nodeids.shape[1]):
                pos.append((row * nodeids.shape[1] + col, [row * 10, col * 10]))
        pos = dict(pos)
        
        colorlist = np.clip(Common.nlab2rgb(center_data), 0, 1)
        old_colorlist = np.clip(Common.nlab2rgb(old_center_data), 0, 1)
        
        lcmap = ListedColormap(colors=colorlist.reshape((colorlist.shape[0] * colorlist.shape[1], 3)))
        old_lcmap = ListedColormap(colors=old_colorlist.reshape((old_colorlist.shape[0] * old_colorlist.shape[1], 3)))
        
        #nx.draw(G, pos, node_color=np.arange(n_centers * n_colors)/float(n_centers * n_colors), cmap=lcmap, \
        #edge_color=[G.edge[e[0]][e[1]]['weight'] for e in r.edges], \
        #edge_cmap = cmap.get_cmap('RdBu'), edgelist=r.edges)
        nx.draw_networkx_nodes(G, pos, node_color=np.arange(n_centers * n_colors)/float(n_centers * n_colors), \
        cmap=lcmap,linewidths=0.1)   

        plt.show()

        nx.draw_networkx_nodes(G, pos, node_color=np.arange(n_centers * n_colors) / float(n_centers * n_colors), \
        cmap=old_lcmap,linewidths=0.1)
        
        plt.show()
        
if __name__=='__main__':
    import sys
    import argparse
    # sys.argv = [sys.argv[0], 'gmm_pred', 'renoir']
    
    # from KernelKMeans import KernelKMeans
    # from sklearn.cluster import KMeans
    # test_kernel_pca()
    
    # test_tsp_rearrange()
    # test_kernel_pca()
    # test_classification()
    # test_classification_1()
    # test_regressors()
    # prepare_themes(['renoir','gogh','cezanne','monet'])
    # test_clustering()
    # test_kde()
    
    # prepare_themes(['renoir'])
    #
    
    #
    # test_gmm_density()
    parser = argparse.ArgumentParser(description='Test performance of different palette prediction models')
    parser.add_argument('action', type=str, help='action')
    parser.add_argument('artists', type=str, nargs='*', help='artist name')
    parser.add_argument('--prefix', type=str, default='.')
    parser.add_argument('--train_per', type=float, help='training percents', default=0.6)
    parser.add_argument('--n_colors', type=int, help='number of colors in a palette', default=5)  
    parser.add_argument('--method', type=str, help='method name', default='gmm')    
    parser.add_argument('--max_tests', type=int, help='max number of testing points', default=200)
    parser.add_argument('--test_artist', type=str, help='name of the testing artist', default=None)
    parser.add_argument('--n_missings', type=int, help='n missings', default=4)
    parser.add_argument('--n_clusters', type=int, help='n clusters', default=10)
    parser.add_argument('--sorter', type=str, help='binary, lightness, none, local', default='binary')
    parser.add_argument('--vis', type=int, help='visualization', default=1)
    args = parser.parse_args()

    methods =['gmm', 'gplvm', 'bgplvm', 'mrd_bgplvm', 'mean', 'retrieval']
    # methods =['gmm', 'bgplvm']
    if args.action == 'test_pred':
        if args.method in methods:
            test_palette_prediction(args.prefix, args.artists[0], method=args.method, test_author=args.test_artist,
                                    max_tests=args.max_tests, n_missings=args.n_missings, vis=args.vis,
                                    vis_sort='none', sorter=str(args.sorter), optimize=True)
    elif args.action == 'gen_tests':
        generate_tests(args.artists, train_percents=args.train_per)
    elif args.action == 'prep_themes':
        prepare_themes(args.artists, args.n_colors)
    elif args.action == 'prep_cbn':
        prepare_color_by_numbers(args.artists)
    elif args.action == 'test_cluster':
        test_kmeans(args.artists, [args.method], n_colors=args.n_colors, n_clusters=args.n_clusters)
    elif args.action == 'train_model':
        from ColorManifolds import run
        run(args.artists, args.n_colors, method=args.method, step=1)
    else:
        locals()[args.action](args.artists, args.n_colors)


# ~/anaconda/bin/python Main.py test_pred chase --n_colors=5 --n_missings=4 --vis=0 --sorter=lightness --method=bgplvm --prefix=/Users/phanquochuy/Projects/smartpalette/data
#