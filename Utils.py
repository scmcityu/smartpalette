import numpy as np
from os.path import join
from Common import nlab2rgb, PATH
from matplotlib import pyplot as plt
from scipy.io import loadmat, savemat
    
from sklearn.mixture import GMM
from sklearn.grid_search import GridSearchCV
from sklearn.decomposition import PCA
from skimage import draw


def vis_cmeans(artist, step=1, n_colors=5, path=PATH):
    data = np.load(join(path, 'data',artist,'dpaintings%d.npz' % n_colors ))
    # im = data['orig_lab']
    order = data['concat_order']
    # im0 = data['clustered_nlab'][::4] #/ 255.
    im = data['concat_lab']
    labels = data['labels']
    labels1 = labels[::step][order] 
    means = []
    for m in np.unique(labels):
        means.append(im[labels1 == m].mean(axis=0))
        
    means = nlab2rgb(np.array(means))
    fig = plt.figure('vis_cmeans')
    plt.imshow(means, interpolation='none')
    raw_input('press to close')


def vis_artist_level(artist, step=1, show='orig', ordering=True, ids=None, n_colors=5, path=PATH, labels=None, level=0):
    return _vis_artist(join(path, 'data_resized', artist ,'clusters_nc%d_l%d.npz' % (n_colors, level) ), step, show, ordering, ids, n_colors, labels)


def vis_artist(artist, step=1, show='orig', ordering=True, ids=None, n_colors=5, path=PATH, labels=None):
    return _vis_artist(join(path, 'data', artist ,'dpaintings%d.npz' % n_colors ), step, show, ordering, ids, n_colors, labels)


def _vis_artist(fname, step=1, show='orig', ordering=True, ids=None, n_colors=5, labels=None):
    #data = np.load(join(path, 'data', artist ,'dpaintings%d.npz' % n_colors ))
    data = np.load(fname)
    im = data['lab']
    if labels is None:
        labels = data['labels']
        
    if 'concat_order' in data:
        order = data['concat_order']
        if show=='pca':
            labels1 = labels[::step][order] 
        else:
            labels1 = labels[::step]
        
    #im0 = data['clustered_nlab'][::4] #/ 255.
    if 'concat_lab' in data:
        im3 = data['concat_lab']

    #labels2 = np.sort(labels[::step])
    if 'concat_labels' in data:
        labels2 = data['concat_labels']
    else:
        labels2 = np.sort(labels[::step])
    
    axs = []
    if ids is None:
        ncols =labels.max() + 1
        #ids = reduce(lambda x, y: x + [y] if not y in x else x, [[]] + list(labels)) #range(ncols)
        if show == 'pca':
            l = list(labels1)
            ids = [l[i] for i in range(0, len(l)) if l[i] != l[i-1]]
        else:
            ids = range(ncols)
        print ids
    else:
        ncols = len(ids)
    
    fig = plt.figure(fname)
    for ax in range(ncols):
        axe = fig.add_subplot(1, ncols, ax+1)
        axe.xaxis.set_ticklabels([])
        axe.yaxis.set_ticklabels([])
        axs.append(axe)
        

    for ax, idd in enumerate(ids):
        axs[ax].axis('off')
        if show == 'pca':
            axs[ax].imshow(nlab2rgb(im3[labels1==idd]), interpolation='none')
        elif show == 'orig':
            axs[ax].imshow(nlab2rgb(im[::step][labels[::step]==idd]), interpolation='none')
        elif show == 'cluster':
            axs[ax].imshow(nlab2rgb(im3[labels2==idd]), interpolation='none')
            
    plt.tight_layout()        
    plt.show()


def convert_oah11data():
    data = np.load(join(path, 'data', 'turk', 'dpaintings.npz'))
    themesData = loadmat(join(path, 'data', 'turk', 'turkData.mat'))
    
    themes = np.clip(nlab2rgb(data['concat_lab']), 0, 1)
    order = data['concat_order']
    
    targets = themesData['datapoints'][0,0]['targets'][::20][order]
    
    savemat(join(path, 'data', 'turk', 'themes.mat'), {'data': themes, \
    'ids' : np.atleast_2d(np.arange(len(themes))).T, \
    'names' : np.array([['theme_' + str(i)] for i in range(len(themes))]).T, \
    'targets': targets})


def score_samples(artist, path=PATH):
    mat = loadmat(join(path, 'data/%s/tests.mat' % artist))
    ameans = mat['means']
    acovars = mat['covars']
    aweights = mat['weights']
    perms = mat['perms']
    tests = mat['tests']
    n_trains = mat['n_trains'][0].astype(int)
    missing_ids = mat['missing_ids']
    colordict = mat['colordict']
    logprobs = []
    print n_trains
    for tr in range(len(n_trains)):
        gmm = GMM(n_components = ameans[tr].shape[1])
        gmm.means_ = ameans[tr]
        gmm.covars_ = acovars[tr]
        gmm.weights_ = aweights[tr]
        
        logprobs.append( gmm.score(tests[perms[:n_trains[tr]]]) )
    
    savemat(join(path, 'data/%s/tests_.mat' % artist), \
    { 'tests' : X, 'colordict' : colordict, \
    'missing_ids' : missing_ids, 'perms' : perms, 'n_trains' : n_trains, \
    'covars' : covars, 'means' : means, 'weights' : weights, 'logprobs' : logprobs})    


def sample_gmm(artist, cids=[0],n_colors=5,path=PATH):
    data = np.load(join(path, 'data/%s/dpaintings%d.npz' % (artist, n_colors) ))
    order = data['concat_order']
    labels = data['labels']
    labels = labels[order]
    
    fig = plt.figure('vis GMM modes')
    for cx, cid in enumerate(cids):
        X = data['concat_lab'][labels==cid]
        n_pals, n_cols, n_channels = X.shape
        X = X.reshape((-1, n_cols * n_channels ))
    
        pca = PCA(n_components=6, whiten=False)
        newX = pca.fit_transform(X)
    
        lowest_bic = np.infty
        bic = []
        n_components_range = range(2,25)
        cv_types = ['spherical', 'tied', 'diag', 'full']
        #'''
        for cv_type in cv_types:
            for n_components in n_components_range:
                # Fit a mixture of Gaussians with EM
                gmm = GMM(n_components=n_components, covariance_type=cv_type)
                gmm.fit(newX)
                bic.append(gmm.aic(newX))
                if bic[-1] < lowest_bic:
                    lowest_bic = bic[-1]
                    best_gmm = gmm
        #'''            
        #gmm = GMM(n_components=10, covariance_type='full', random_state=None, n_init=5, n_iter=2000, thresh=1e-8)
        #gmm.fit(newX)
        newpoints = pca.inverse_transform(best_gmm.sample(40))
        #newpoints = best_gmm.sample(40)
        #np.clip(newpoints, 0, 1, newpoints)
        
        print 'best gmm ', best_gmm.n_components, best_gmm.covariance_type
        #newpoints = gmm.sample(40)
        rgb = nlab2rgb(newpoints.reshape((-1, n_cols, n_channels)))
        np.clip(rgb, 0, 1, rgb)
        im = np.transpose(rgb, [1, 0, 2])
        #print (im * 255).astype(int)
        ax = fig.add_subplot( len(cids), 1, cx)
        ax.axis('off')
        ax.imshow(im, interpolation='none')
    #plt.axis('off')    
    plt.tight_layout()
    plt.show()


def sample_kde(artist, cid=0):
    data = np.load(join(path, 'data/%s/dpaintings.npz' % artist))
    order = data['concat_order']
    labels = data['labels']
    labels = labels[order]
    X = data['concat_lab'][labels==cid].reshape((-1, 30))

    pca = PCA(n_components=8, whiten=False) #KernelPCA(n_components=4, kernel='sigmoid', fit_inverse_transform=True) #
    newX = pca.fit_transform(X)
    params = {'bandwidth': np.logspace(-1, 1, 20)}
    grid = GridSearchCV(KernelDensity(), params)
    grid.fit(newX)

    print("best bandwidth: {0}".format(grid.best_estimator_.bandwidth))
    kde = grid.best_estimator_

    # sample 40 new points from the data
    newpoints = kde.sample(40, random_state=None)
    newpoints = pca.inverse_transform(newpoints)

    fig = plt.figure('vis KDE modes')
    plt.imshow(nlab2rgb(newpoints.reshape((-1, 10, 3))), interpolation='none')
    plt.show()


def disp_color_by_numbers(artist, test, path=PATH):
    from fast.Distance import hausdorff
    from scipy.spatial.distance import euclidean
    
    missing_ids = loadmat(join(path, 'data', artist, 'test_' + str(test), 'test', 'tests.mat'))['missing_ids']
    
    def read_palette(text, col_id):
        pal = []
        toks = line.split('\t')
        colortext = toks[col_id].split(' ')
        for threecom in colortext:
            coms = threecom.split(',')
            pal.append([int(com) for com in coms])
        return pal
    
    for n_missings in [4]:#[4,3,2,1]:
        fi = open(join(path, 'data', artist, 'test_' + str(test), 'test', 'palettes.tsv'),'r')
        lines = fi.readlines()    
        true_palettes = []
        for line in lines[1:]:
            palette = read_palette(line, 3)
            true_palettes.append(palette)
        true_palettes = np.array(true_palettes).astype(float) / 255.
        fi.close()
    
        fi = open(join(path, 'data', artist, 'test_' + str(test), 'predictions_%d' % n_missings, 'suggestions.tsv'),'r')
        lines = fi.readlines() 
        palettes = np.zeros_like(true_palettes)
        for lx, line in enumerate(lines[1:]):
            imageid = int(line.split('\t')[0][:-4].split('_')[-1])
            palettes[imageid] = read_palette(line, 2)
            
        palettes = np.array(palettes).astype(float) / 255.
        
        dists = []
        for p in range(len(palettes)):
            if (palettes[p] == 0).all():
                continue
            
            #dists.append(euclidean(palettes[p][missing_ids[p, :n_missings]], true_palettes[p][missing_ids[p, :3]]))
            #dists.append( hausdorff(palettes[p][missing_ids[p, :n_missings]], true_palettes[p][missing_ids[p, :n_missings]], mode=1) )
            dists.append( hausdorff(palettes[p], true_palettes[p], mode=1) )
            
        print 'average distance = ', sum(dists) / len(dists)
        fig = plt.figure(artist + '_test_' + str(test))
        ax1 = fig.add_subplot(121)
        ax1.axis('off')
        ax1.imshow(palettes[0:40], interpolation='nearest')
        
        ax2 = fig.add_subplot(122)
        ax2.axis('off')
        ax2.imshow(true_palettes[0:40], interpolation='nearest')
        plt.tight_layout()
        plt.show()
        fi.close()
    
# ----------------------------------
# SHOW PREDICTED RESULTS
def show_predicts(artist, path=PATH,n_colors=5):
    from fast.Distance import hausdorff
    
    from sklearn.metrics import mean_squared_error
    mses = 0
    tests = [0,1,2,3,4]
    for TEST in tests:
        mat1 = loadmat(join(path, 'data/%s/tests.mat' % artist))
        perms = mat1['perms']
        ntrain = mat1['n_trains'][0]
        mat2 = loadmat(join(path, 'data/%s/themesData.mat' % artist))
        rgbs = mat2['datapoints'][0,0]['rgb']
        
        mat = loadmat(join(path, 'data/%s/tests_results.mat' % artist))
        re = mat['bests'][TEST]
        im1 = np.transpose(re[:200].reshape((-1, 3, n_colors)), [0, 2 , 1])
        
        testrgbs = rgbs[::1][perms[TEST, ntrain[TEST]:]]
        im2 = testrgbs[1:201].reshape((-1, n_colors, 3))
        
        mse = 0.0
        for pal in range(len(im1)):
            #mse += mean_squared_error( im1[pal].ravel(), im2[pal].ravel() )
            mse += hausdorff(im1[pal], im2[pal], mode=1)
            
        mse /= len(im1)
        mses += mse
        print 'mse = ', mse
        #'''    
        fig = plt.figure('pred vs groundtruth')
        ax1 = fig.add_subplot(121)
        ax1.axis('off')
        ax2 = fig.add_subplot(122)
        ax2.axis('off')
        ax1.imshow(im1, interpolation='none')
        ax2.imshow(im2, interpolation='none')
        plt.tight_layout()
        plt.show()
        raw_input('press to continue')
        #'''    
        
    print 'ave mse = ', mses / len(tests)
    

def render_palettes_circles(palettes, n_clusters, km=None, radius=100, gap=0):
    # hsv_palettes = rgb2hsv(palettes)
    n_pals, n_cols, n_chas = palettes.shape
    colors = palettes.reshape((n_pals * n_cols, n_chas))
    if km is None:
        km = KMeans(n_clusters=n_clusters)
        labels = km.fit_predict(colors)
    else:
        labels = km.predict(colors)

    iw, ih = radius * 2 * n_clusters, radius * 2
    img = np.zeros((ih, iw, n_chas))
    for c in range(n_clusters):
        ids = np.where(labels == c)[0]
        # idxs = np.argsort([euc(c_, km.cluster_centers_[c]) for c_ in colors[labels == c]])
        # ids = ids[idxs]
        if len(ids) == 0:
            rr, cc = draw.ellipse(radius, c * (radius * 2) + radius, radius, radius, shape=(ih, iw))
            img[rr, cc] = [1, 1, 1]
        else:
            # r = radius / float(len(ids))
            a = np.pi * 2 / float(len(ids))
            for idx in range(len(ids)):
                # rr, cc = draw.ellipse(radius, c * (radius * 2) +
                # radius, radius - idx * r, radius - idx * r, shape=(ih, iw))
                r_ = radius * 2
                y1, x1, y2, x2 = radius - np.sin(idx * a) * r_, \
                                 np.cos(idx * a) * r_ + c * (radius * 2) + radius, \
                                 radius - np.sin((idx + 1) * a) * r_, \
                                 np.cos((idx + 1) * a) * r_ + c * (radius * 2) + radius
                rr1, cc1 = draw.polygon(np.array([radius, y1, y2]),
                                        np.array([c * (radius * 2) + radius, x1, x2]), shape=(ih, iw))

                r2 = radius
                rr2, cc2 = draw.ellipse(radius, c * (radius * 2) + radius, r2, r2, shape=(ih, iw))
                mask1 = np.zeros(img.shape[:2], dtype=bool)
                mask1[rr2, cc2] = True

                mask2 = np.zeros(img.shape[:2], dtype=bool)
                mask2[rr1, cc1] = True

                mask = np.bitwise_and(mask1, mask2)
                img[mask] = colors[ids[idx]]

    return img, km


def render_palettes(palettes, box_size=50, vgap=0, hgap=5, shape='square', background='black'):
    n_pals, n_cols, n_chas = palettes.shape
    iw, ih = n_pals * (box_size + hgap), n_cols * (box_size + vgap)
    if background == 'black':
        img = np.zeros((iw, ih, n_chas))
    elif background == 'white':
        img = np.ones((iw, ih, n_chas))
    for px in range(n_pals):
        for cx in range(n_cols):
            if shape == 'square':
                img[px * (box_size + hgap): px * (box_size + hgap) + box_size,
                cx * (box_size + vgap): cx * (box_size + vgap) + box_size] = palettes[px, cx]
            elif shape == 'circle':
                rr, cc = draw.ellipse(px * (box_size + hgap) + box_size / 2, cx * (box_size + vgap) + box_size / 2,  box_size / 2,  box_size / 2, shape = img.shape)
                img[rr, cc] = palettes[px, cx]
    img = np.swapaxes(img, 0, 1)
    return img

    
if __name__ == '__main__':
    #prepare_color_by_numbers(['renoir'], path)
    vis_artist_level('cezanne', 1, 'cluster', level=3)
    #disp_color_by_numbers('gas', 0)
    #sample_gmm('renoir', [1, 5, 10, 17, 20])
    #vis_artist('luce', step=1, show='pca', ordering=False) #, ids=[1, 5, 10, 17, 20])
    #show_predicts('gas')
