'''
Created on 29 Oct, 2014

@author: phan
'''
from matplotlib import pyplot as plt
from skimage.io import imread, imsave
from skimage.color import lab2rgb, rgb2lab
import Common as cm
#import KernelKMeans
from KernelKMeans import KernelKMeans
import KernelKMeans as km
from sklearn.cluster import KMeans
import GPy
from os.path import join
import os
from scipy.spatial.distance import euclidean as euc
import numpy as np
from fast.Distance import hausdorff, parallel_hausdorff
from numpy.random import multivariate_normal
from LLE import LocallyLinearEmbedding, barycenter_weights
from sklearn.utils import array2d
from skimage.segmentation import slic
from skimage.color import rgb2gray, gray2rgb


class Colorizer(LocallyLinearEmbedding):
    """
    Colorizer, the original version
    """
    def __init__(self, models, kkm , kms, n_channels=3, n_colors=400, patch_size=200,
                 n_components=2, n_neighbors=12, reg=4e-1):
        """
        Constructor

        works best with method = 'modified'
        :param models:
        :param kkm:
        :param kms:
        :param n_channels:
        :param n_colors:
        :param patch_size:
        :param n_components:
        :param n_neighbors:
        :param reg:
        """
        super(Colorizer, self).__init__(eigen_solver='auto', n_components=n_components,
                                        n_neighbors=n_neighbors, reg=reg, method='standard')
        
        self.models = models
        self.patch_size = patch_size
        self.kms = kms
        self.kkm = kkm
        self.n_channels = n_channels
        self.n_colors = n_colors # number of colors for querrying task
        
    def get_weights(self, X):
        X = array2d(X)
        ind = self.nbrs_.kneighbors(X, n_neighbors=self.n_neighbors,
                                    return_distance=False)
        weights = barycenter_weights(X, self.nbrs_._fit_X[ind],
                                     reg=self.reg)

        return weights
    
    def colorize_lle(self, image, colorized_image, lmda = 0.4, sparse=True):
        """
        Xiaowu Chen, Dongqing Zou, Qinping Zhao, and Ping Tan. 2012. Manifold preserving edit 
        propagation. ACM Trans. Graph. 31, 6, Article 132 (November 2012), 7 pages. 
        DOI=10.1145/2366145.2366151 http://doi.acm.org/10.1145/2366145.2366151
        """
        
        # from scipy.linalg import solve
        if sparse:
            from scipy.sparse import csr_matrix, eye, issparse, diags
            from scipy.sparse.linalg import spsolve as solve
        else:
            from scipy.linalg import solve
            from numpy import eye
        
        if len(image.shape) == 3:
            w, h, d = image.shape
        else:
            w, d = image.shape
            h = 1
            
        # newimage = np.zeros_like(image)
        pixels = image.reshape((w * h, d))
        try:
            self.fit(pixels)
        except Exception as e:
            print 'Error!'
            return colorized_image
        
        # return colorized_image
        # Find LLE weights (barycenter weights)
        # W = self.get_weights(pixels)
        M = self.M # M = (I - W).T.dot(I - W)
        I = eye(len(pixels))
        
        # List of target colors
        G = colorized_image.reshape((w * h, d))
        
        # Set weights to non-zero rows in G (annotated image)
        # Lambda = I * lmda
        Lambda = np.array([np.any(row != 0) for row in G]).astype(np.int32) * lmda
        if sparse:
            Lambda = diags([Lambda], [0])
        else:
            Lambda = Lambda * I

        # prepare the linear program
        # A = ((I - W).T.dot(I - W) + Lambda)
        A = M + Lambda
        
        if sparse:
            G = csr_matrix(G)
        b = Lambda.dot(G)
        
        # print A.shape, b.shape, A.getformat(), b.getformat()
        # assert(issparse(A) and issparse(b))
        # solve the linear system and find new image
        if sparse:
            A = A.asformat('csc')
            b = b.asformat('csc')
        
        newimage = solve(A, b)
        
        if sparse:
            newimage = newimage.toarray().reshape((w, h, d))
        else:
            newimage = newimage.reshape((w, h, d))
        
        return newimage
    
    def _weighted_euclidean(self, x, y, ws):
        return np.sqrt((ws[0] * x[0] - y[0])**2 + ws[1] * (x[1] - y[1])**2 + ws[2] * (x[2] - y[2])**2)
        
    def colorize_with_segments(self, image, segments, neighbors=None, clustering=True):
        '''
        segmentation-based colorization
        '''
        original_image = image.copy()
        
        image = cm.rgb2nlab(image)
        newimage = np.zeros_like(image)
        w, h, d = image.shape
        n_colors = 1000 # self.n_colors
        n_segments = np.unique(segments)
        
        print "No segments = %d" % len(n_segments)
        # if os.path.isfile('.tmp/tmp.npy'):
        #     newimage = np.load('.tmp/tmp.npy')
        # else:
        for sx in n_segments:
            segment = segments == sx
            print '.',
            # now we use neigboring segments
            # could it be just the current segment?
            if neighbors is not None:
                nbs = np.nonzero(neighbors[sx])
                pixels = []
                for nb in nbs:
                    pixels = np.concatenate([pixels, image[ segments[nb] ]])
                pixels = pixels[::10]
            else:
                pixels = image[segment][::8]

            n_channels = self.n_channels
            centers = np.zeros((len(self.kkm.centers_), n_colors, n_channels), dtype='double')
            for cx, center in enumerate(self.kkm.centers_):
                # step = len(self.kkm.X_fit_[center]) / n_colors
                # if step == 0:
                #     continue
                centers[cx, :, :] = self.kkm.X_fit_[center] #n-dims = 1000

            dists = parallel_hausdorff(np.ascontiguousarray(pixels), centers, 1)
            cluster_id = np.argmin(dists)

            # extract feature (rearrange colors)
            feat, _ = km.rearrange_colors([pixels], self.kms[cluster_id])
            feat = feat.reshape((feat.shape[0], feat.shape[1] * feat.shape[2]))

            m = self.models[cluster_id]
            xnew, newm = m.infer_newX(feat)
            new_palette, _ = m.predict(np.array(xnew.mean))

            # now that we got the new palette,
            # the next step is to map original colors to palette colors
            new_palette = new_palette.reshape((-1, 3))
            # new_palette = np.clip(new_palette, 0, 1)
            new_palette[new_palette < 0] += -1e8
            new_palette[new_palette > 1] += 1e8

            indices = zip(*np.nonzero(segment))
            colors = np.zeros((len(indices), 3), dtype='double')
            for idx, ids in enumerate(indices):
                i, j = ids
                # dists = [euc(image[i, j], color) for color in new_palette]
                # dists = [abs(image[i, j][0] - color[0]) for color in new_palette]
                dists = [self._weighted_euclidean(image[i, j] , color, [1, 0.2, 0.2]) for color in new_palette]
                # only annotate a few pixels to prepare for colorize_lle
                if np.min(dists) < 5e-2:
                    colors[idx] = new_palette[np.argmin(dists)]
                # newimage[i, j] = new_palette[np.argmin(dists)]

            perm = np.random.permutation(len(indices))
            # take = perm[:min(50, len(indices))]
            colors[perm[min(20, len(indices)):]] = 0
            # indices = np.asarray(indices)[perm[:min(50, len(indices))]]
            newimage[segment] = colors
            # np.save('.tmp/tmp.npy', newimage)
            
        # '''
        # ps = 50
        # for i in np.arange(0, 200, ps):
        #     for j in np.arange(0, 300, ps):
        #         print '-',
        #         rect = [i, i + ps, j, j + ps]
        #         newimage[rect[0]:rect[1], rect[2]:rect[3]] = \
        # self.colorize_lle(image[rect[0]:rect[1], rect[2]:rect[3]], \
        #         newimage[rect[0]:rect[1], rect[2]:rect[3]])
        # '''

        if clustering:
            from sklearn.cluster import KMeans
            n_clusters = 140
            km_ = KMeans(n_clusters=n_clusters, n_jobs=4,n_init=4)
            X = image.reshape((w * h, d))
            print 'clustering ', len(X[::12])
            km_.fit(X[::12])
            labels = km_.predict(X)
            centers = km_.cluster_centers_
            anns = np.zeros_like(centers)
            for i in range(n_clusters):
                tmp = newimage.reshape((w * h, d))[labels == i]
                if np.all(tmp == 0):
                    anns[i] = 0
                else:
                    anns[i] = tmp[np.any(tmp, axis=1)].mean(axis=0)
            
            print 'colorizing'
            newcenters = self.colorize_lle(centers, anns, sparse=False).reshape((n_clusters, d))
            newimage = image.copy()
            for i in range(n_clusters):
                newimage.reshape((w * h, d))[labels == i] = newcenters[i]
        else:    
            newimage = self.colorize_lle(image, newimage)
            
        return cm.nlab2rgb(newimage)
        
        # '''
        # rgb_im = cm.nlab2rgb(newimage.copy())
        # gray_im = gray2rgb(rgb2gray(original_image))
        #
        # for i in range(w):
        #     for j in range(h):
        #         if np.any(newimage[i, j] != 0):
        #             gray_im[i, j] = rgb_im[i, j]
        # return gray_im
        # '''

    # DONT USE THIS
    # def colorize(self, image):
    #     image = cm.rgb2nlab(image)
    #     newimage = np.zeros_like(image)
    #     ps = self.patch_size
    #     w, h, _ = image.shape
    #     patches = []
    #
    #     #Consider the distance from each patch to a theme's center
    #     #alternatively, we can consider distance to a merged set (all patches in a theme merged)
    #     n_channels = self.n_channels
    #     n_colors = self.n_colors
    #     centers = np.zeros((len(self.kkm.centers_), n_colors, n_channels), dtype='double')
    #     for cx, center in enumerate(self.kkm.centers_):
    #         step = len(self.kkm.X_fit_[center]) / n_colors
    #         if step == 0:
    #             continue
    #         centers[cx, :, :] = self.kkm.X_fit_[center][::step][:n_colors]
    #
    #     #sampling the patches. Prepare for patch matching
    #     for i in range(0, w, ps):
    #         for j in range(0, h, ps):
    #             hx = min(w, i + ps)
    #             hy = min(h, j + ps)
    #             patch = image[i:hx, j:hy]
    #
    #             rx, ry = (hx - i) / 2, (hy - j) / 2
    #             X, Y = multivariate_normal( \
    #             mean=[rx, ry], \
    #             cov=np.eye(2) * min(rx, ry), \
    #             size=400
    #             ).T
    #             X = X.astype(int)
    #             Y = Y.astype(int)
    #
    #             X[X >= patch.shape[0]] = patch.shape[0] - 1
    #             Y[Y >= patch.shape[1]] = patch.shape[1] - 1
    #             patch = patch[X, Y]
    #
    #             patches.append((patch, (i, j, hx, hy)))
    #
    #     #Perform patch matching (hausdorff)
    #     cluster_ids = []
    #     for patch, _ in patches:
    #         dists = parallel_hausdorff(np.ascontiguousarray(patch), centers, 1)
    #         cluster_id = np.argmin(dists)
    #         cluster_ids.append(cluster_id)
    #         #sorted_clusters = sorted(zip(dists, range(len(dists))), key=lambda x: x[0])
    #
    #     #Color transfer given matched patches
    #     print 'colorizing %d patches' % len(cluster_ids)
    #     for cx, cluster_id in enumerate(cluster_ids):
    #         print '.',
    #         #extract feature (rearrange colors)
    #         feat, _ = km.rearrange_colors([patch], self.kms[cluster_id])
    #         feat = feat.reshape((feat.shape[0], feat.shape[1] * feat.shape[2]))
    #
    #         m = self.models[cluster_id]
    #         xnew, newm = m.infer_newX(feat) #do_test_latents(Y[:2])
    #         new_palette, _ = m.predict(np.array(xnew.mean))
    #
    #         _, rect = patches[cx]
    #
    #         #now that we got the new palette,
    #         #the next step is to map original colors to palette colors
    #         #new_palette = new_palette.reshape((-1, 3))
    #         new_palette = new_palette.reshape((-1, 6))
    #         means = new_palette[:, :3]
    #         covars = new_palette[:, 3:]
    #
    #         for i in range(rect[0], rect[2]):
    #             for j in range(rect[1], rect[3]):
    #                 dists = [euc(image[i, j],color) for color in new_palette]
    #                 newimage[i, j] = new_palette[np.argmin(dists)]
    #
    #     return cm.nlab2rgb(newimage)

# from skimage.segmentation import find_boundaries


def find_all_neighbors(labelled_img):
    '''
    find the neighbors of the segments (seg_ids)
    '''
    # boundaries = find_boundaries(labelled_img)
    labels = np.unique(labelled_img)
    pos = np.zeros((labelled_img.shape[0], labelled_img.shape[1], 2))
    for row in range(pos.shape[0]):
        for col in range(pos.shape[1]):
            pos[row, col, :] = np.array([row, col])
    
    adj_mat = np.zeros((len(labels), len(labels)), dtype='int32')
    adj_mat += -1
    np.fill_diagonal(adj_mat, 0)
       
    for i in labels:
        print '.',
        set1 = pos[labelled_img == i]
        for j in labels:
            if j != i and adj_mat[i, j] == -1:
                set2 = pos[labelled_img == j]
                done = False
                if not done:
                    for p1 in set1:
                        if not done:
                            for p2 in set2:
                                if abs(p1[0] - p2[0]) <=5 and abs(p1[1] - p2[1]) <= 5:
                                    adj_mat[i, j] = 1
                                    adj_mat[j, i] = 1
                                    done = True
                                    break
                if not done:
                    adj_mat[i, j] = 0
                    adj_mat[j, i] = 0
                    
    return adj_mat
    
# def segmentation(img):
#     segments_slic = slic(img, n_segments=16, compactness=10, sigma=1)
#     return segments_slic


def clustering_based_colorization(im):
    from sklearn.cluster import KMeans
    km = KMeans(n_clusters=1000)
    X = im.reshape((im.shape[0] * im.shape[1], -1))
    labels = km.fit_predict(X)
    centers = km.cluster_centers_
    
    
if __name__ == '__main__':
    from skimage.transform import rescale
    from skimage import img_as_float
    PATH = '/media/phan/BIGDATA/DPAINTINGS'
    #DO COLOR TRANSFER FROM (1) to (2)
    author2 = 'nature'
    author1 = 'rafael'
    
    IPATH1 = join(PATH, 'img', author1)
    OPATH1 = join(PATH, 'data', author1)
    IPATH2 = join(PATH, 'img', author2)
    OPATH2 = join(PATH, 'data', author2)   
    
    files = sorted([f for f in os.listdir(IPATH2) \
    if os.path.isfile(join(IPATH2, f)) and f.endswith('.jpg')])
     
    kkm, kms, models  = None, None, None
    if os.path.isfile(join(OPATH1, 'dpaintings.npz')):
        data = np.load(join(OPATH1, 'dpaintings.npz'))
        kkm = data['kkm'].tolist()
        kms = data['kms'].tolist()
        #clusters = data['merged_data']
        
    if os.path.isfile(join(OPATH1, 'models.npz')):
        data = np.load(join(OPATH1, 'models.npz'))
        models = data['models']
    
    #CACHE
    clz = Colorizer(models, kkm, kms)
    
    f = '0.jpg' #files[3]
    
    im = imread(join(IPATH2, f))
    im = rescale(im, 0.5) #[80:180, 50:220]
    im = img_as_float(im)
    '''
    im1 = np.zeros_like(im)
    print 'processing file %s, size(%d,%d)' % (f, im.shape[0], im.shape[1])
    for si in [0, 1]:
        for sj in [0, 1]:
            segments = slic(im[si::2, sj::2], n_segments=8, compactness=10, sigma=1) #segmentation(im[si::2, sj::2])
            #neighbors = find_all_neighbors(segments)
            neighbors = None
            newim = clz.colorize_with_segments(im[si::2, sj::2], segments, neighbors)
            im1[si::2, sj::2] = newim
    '''
    print 'processing file %s, size(%d,%d)' % (f, im.shape[0], im.shape[1])
    segments = slic(im, n_segments=8, compactness=10, sigma=1)
    neighbors = None
    im1 = clz.colorize_with_segments(im, segments, neighbors)
    #'''
    plt.imshow(im1)
    #plt.show()
    #imsave('.tmp/%s_%s_gray_.png' % (os.path.splitext(f)[0], author1), gray2rgb(rgb2gray(im)))
    imsave('.tmp/%s_%s_.png' % (os.path.splitext(f)[0], author1), im1)
