from distutils.core import setup
from os.path import join
import numpy as np
import warnings


def configuration(parent_package='smartpalette', top_path=None):
    from numpy.distutils.misc_util import Configuration

    config = Configuration('coloropt', parent_package, top_path)

    sources = ['mg.cpp', 'tensor2d.cpp', 'tensor3d.cpp', 'fmg.cpp']
    coloropt_sources = ['coloropt.cpp'] + [join('src', x) for x in sources]
    coloropt_depends = []

    config.add_extension('coloropt',
                         sources=coloropt_sources,
                         libraries=[],
                         #define_macros=[('VL_DISABLE_AVX', None)],
                         include_dirs=[join('src'), np.get_include()],
                         depends=coloropt_depends)

    return config

if __name__ == '__main__':
    from numpy.distutils.core import setup
    print configuration(top_path='').todict()
    setup(**configuration(top_path='').todict())