import numpy as np
from scipy.spatial.distance import euclidean as euc
from smartpalette import Common
import coloropt
from matplotlib import pyplot as plt
from skimage.color import rgb2gray, gray2rgb


class ColorOptColorizer(object):
    """
    This colorizer use segments and their corresponding palettes for colorization
    """
    def __init__(self, in_itr=10, out_itr=10, ann_per=0.1):
        """

        :param in_itr:
        :param out_itr:
        :param ann_per:
        """
        self.in_itr = in_itr
        self.out_itr = out_itr
        self.ann_per = ann_per

    def colorize(self, image_rgb, seg_masks, new_palettes):
        """

        :param image_rgb: image to colorize, must be in rgb
        :param seg_masks: segment masks
        :param new_palettes: contains the colors for each segment
        :return:
        """
        n_channels = new_palettes.shape[-1]
        gray = gray2rgb(rgb2gray(image_rgb))
        labeled_gray = gray.copy()
        if isinstance(seg_masks, list):
            n_segments = len(seg_masks)
        else:
            n_segments = len(np.unique(seg_masks))

        new_palettes = new_palettes.reshape((-1, n_channels))
        # assert n_segments == len(new_palettes)

        for sid in range(n_segments):
            if isinstance(seg_masks, list):
                seg_mask = seg_masks[sid]
            else:
                seg_mask = seg_masks == sid

            _xx, _yy = np.where(seg_mask)
            # n_pixs = seg_mask.sum()
            # random_ids = np.random.permutation(n_pixs)[:int(n_pixs * self.ann_per)]
            # labeled_gray[_xx[random_ids], _yy[random_ids]] = new_palettes[sid]

            n_pixs = int(seg_mask.sum() * self.ann_per)
            pixs = image_rgb[_xx, _yy]
            min_ids = np.argsort([euc(pix, new_palettes[sid]) for pix in pixs])[:n_pixs]
            labeled_gray[_xx[min_ids], _yy[min_ids]] = new_palettes[sid]

        new_image = colorize(gray, labeled_gray, self.in_itr, self.out_itr)
        return new_image
        # return labeled_gray


def colorize(gI, cI, in_itr, out_itr):
    """

    :param gI: image without color labels
    :param cI: image with color labels
    :param in_itr:
    :param out_itr:
    :return:
    """
    h, w, d = cI.shape
    solver = 2
    colorIm = np.asarray(np.sum(np.abs(gI - cI), axis=2) > 0.01, dtype=np.double)
    # colorIm = double(colorIm)

    sgI = Common.rgb2ntsc(gI)
    scI = Common.rgb2ntsc(cI)

    ntscIm = np.zeros((h, w, d), dtype=np.double)
    ntscIm[:, :, 0] = sgI[:, :, 0]
    ntscIm[:, :, 1] = scI[:, :, 1]
    ntscIm[:, :, 2] = scI[:, :, 2]

    max_d = np.floor(np.log( min(h, w) ) / np.log(2) - 2)
    iu = np.floor(h / (2 ** (max_d - 1))) * (2 ** (max_d - 1))
    ju = np.floor(w / (2 ** (max_d - 1))) * (2 ** (max_d - 1))
    id = 0
    jd = 0
    colorIm = colorIm[id:iu, jd:ju]
    ntscIm = ntscIm[id:iu, jd:ju, :]

    nI = coloropt.colorize(np.asfortranarray(colorIm), np.asfortranarray(ntscIm), in_itr, out_itr)
    nI = Common.ntsc2rgb(np.squeeze(nI))

    return nI

if __name__ == '__main__':
    from skimage.io import imread
    from skimage import img_as_float
    from os.path import join

    path = '/Users/phanquochuy/Projects/smartpalette/lib/colorization'
    gI = img_as_float(imread(join(path, 'example.bmp')) )
    cI = img_as_float(imread(join(path, 'example_marked.bmp')) )
    colorize(gI, cI)
