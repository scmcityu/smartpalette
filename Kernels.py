# -*- coding: utf-8 -*-
"""
Created on Mon Oct 20 14:10:08 2014

@author: phan
"""
from fast.Distance import hausdorff, hausdorff_with_dists
import numpy as np
from munkres import Munkres
from scipy.spatial.distance import euclidean as euc


def hausdorff_kernel(x1, x2, dims=(5, 3), alpha=0.05, mode=1, eps=np.finfo(float).eps):
    x1 = x1.reshape((dims[0], dims[1]))
    x2 = x2.reshape((dims[0], dims[1]))

    m1 = x1[:, 0] != eps
    m2 = x2[:, 0] != eps

    x1 = x1[m1]
    x2 = x2[m2]
    return np.e ** (-alpha * hausdorff(x1, x2, mode=mode) ** 2)


def hausdorff_dist(x1, x2, mode=1, dims=(5, 3), eps=np.finfo(float).eps):
    x1 = x1.reshape((dims[0], dims[1]))
    x2 = x2.reshape((dims[0], dims[1]))

    m1 = x1[:, 0] != eps
    m2 = x2[:, 0] != eps

    x1 = x1[m1]
    x2 = x2[m2]
    return hausdorff(x1, x2, mode=mode)


def hausdorff_cw(arr1, arr2, mode=1):
    """
    column-wise hausdorff kernel matrix
    :param arr1: N1 * M * D
    :param arr2: N2 * M * D
    :return:
    """
    w1 = arr1.shape[1]
    w2 = arr2.shape[1]
    dists = np.zeros((w1, w2))
    for i in range(w1):
        for j in range(w2):
            dists[i, j] = hausdorff(arr1[:, i, :], arr2[:, j, :], mode=mode)

    return hausdorff_with_dists(dists)


def hausdorff_kernel_matrix_cw(X, Y, alpha=0.05, mode=1):
    N = len(X)
    kernel = np.zeros((N, N), dtype='float64')
    for i in range(N):
        for j in range(i + 1):
            kernel[i, j] = np.e ** (-alpha * hausdorff_cw(X[i], Y[j], mode=mode) ** 2)
            kernel[j, i] = kernel[i, j]
    return kernel


def hausdorff_kernel_matrix(X, Y=None, alpha=0.05, mode=1):
    nX = X.shape[0]
    
    if len(X.shape) == 3:
        X = X.reshape((X.shape[0], X.shape[1] * X.shape[2]))
        
    if Y is not None:
        nY = Y.shape[0]
        if len(Y.shape) == 3:
            Y = Y.reshape((Y.shape[0], Y.shape[1] * Y.shape[2]))
    else:
        nY = nX
        
    kernel = np.zeros((nX, nY), dtype='float64')
    if Y is not None:
        for i in range(nX):
            for j in range(nY):
                x = X[i][np.logical_not(np.isnan(X[i]))]
                y = Y[j][np.logical_not(np.isnan(Y[j]))]
                
                x = np.ascontiguousarray(x.reshape((-1, 3)))
                y = np.ascontiguousarray(y.reshape((-1, 3)))
                
                kernel[i, j] = np.e**(-alpha * hausdorff(x, y, mode=mode) ** 2)
    else:
        for i in range(nX):
            for j in range(i + 1):
                x = X[i][np.logical_not(np.isnan(X[i]))]
                y = X[j][np.logical_not(np.isnan(X[j]))]
                
                x = np.ascontiguousarray(x.reshape((-1, 3)))
                y = np.ascontiguousarray(y.reshape((-1, 3)))
                
                kernel[i, j] = np.e**(-alpha * hausdorff(x, y, mode=mode) ** 2)
                kernel[j, i] = kernel[i, j]
                
    return kernel