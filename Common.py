# -*- coding: utf-8 -*-
"""
Created on Sat Sep 27 09:33:44 2014

@author: phan
"""
import numpy as np
import matplotlib
from skimage.color import rgb2lab, lab2rgb, rgb2hsv, hsv2rgb
from skimage import img_as_float
import colorsys

color_names = matplotlib.colors.cnames.keys()

import platform

OS = platform.system()
PATH = '.'
if OS == 'Windows':
    PATH = 'F:\\DPAINTINGS'
elif OS == "Linux":
    #PATH = '/media/phan/BIGDATA/DPAINTINGS'
    PATH = '/media/phan/SMALLDATA1/DPAINTINGS'
elif OS == "Darwin":
    PATH = '/Users/phanquochuy/Projects/smartpalette/data'

import six
from matplotlib import colors

colors_ = list(six.iteritems(colors.cnames))
hex_ = [color[1] for color in colors_]
mpl_colors = [colors.hex2color(color) for color in hex_]


def rgb2ntsc(img):
    h, w, d = img.shape
    ntscimg = np.zeros_like(img)
    for row in range(h):
        for col in range(w):
            ntscimg[row, col] = colorsys.rgb_to_yiq(img[row, col][0], img[row, col][1],img[row, col][2])
    return ntscimg


def ntsc2rgb(img):
    h, w, d = img.shape
    rgbimg = np.zeros_like(img)
    for row in range(h):
        for col in range(w):
            rgbimg[row, col] = colorsys.yiq_to_rgb(img[row, col][0], img[row, col][1], img[row, col][2])
    return rgbimg


def rgb2nlab(img):
    # if np.issubdtype(img.dtype, np.int):
    #     img = np.float_(img) / 255.
        
    lab_img = rgb2lab(img)
    lab_img[:,:, 0] /= 100.
    lab_img[:,:,1] = (lab_img[:,:,1] + 128) / 255.
    lab_img[:,:,2] = (lab_img[:,:,2] + 128) / 255.
    return lab_img


def nlab2rgb(img):
    rgbimg = img.copy()
    rgbimg[:, :, 0] *= 100
    rgbimg[:, :, 1] = rgbimg[:, :, 1] * 255 - 128
    rgbimg[:, :, 2] = rgbimg[:, :, 2] * 255 - 128
    rgbimg = lab2rgb(rgbimg)
    return rgbimg


def nlab2rgb_p(pixel):
    lab = np.array([[list(pixel)]])
    return nlab2rgb(lab)[0, 0]


def rgb2nlab_p(pixel):
    lab = np.array([[list(pixel)]])
    return rgb2nlab(lab)[0, 0]


def nlab2hsv_p(pixel):
    lab = np.array([[list(pixel)]])
    rgb = nlab2rgb(lab)
    return rgb2hsv(rgb)[0, 0]


def hsv2nlab_p(pixel):
    hsv = np.array([[list(pixel)]])
    rgb = hsv2rgb(hsv)
    return rgb2nlab(rgb)[0, 0]


def normalize_lab(img):
    lab_img = img.copy().astype(np.float64)
    lab_img[:,:,0] /= 100.
    lab_img[:,:,1] =  (lab_img[:,:,1] + 128) / 255.
    lab_img[:,:,2] =  (lab_img[:,:,2] + 128) / 255.    
    
    return np.clip(lab_img, 0, 1)


def save_inc(fname, **kwargs):
    '''
    add number to the file name if it exists
    '''
    import os, re
    if os.path.isfile(fname):
        stem, ext = os.path.splitext(os.path.basename(fname))
        newstem = re.sub(r'\(+[0-9]\)', '(%d)', stem)
        if newstem == stem:
            newstem = stem + '(%d)'
            idd = 1
        else:
            idd = int(re.findall(r'\(+[0-9]\)', stem)[-1][1:-1]) + 1
        
        newname = newstem % idd + ext
    np.save(newname, **kwargs)


def modify_npz(original_file, replacements):
    '''
    replacements [('fieldname', newvalue)]
    '''
    data = np.load(original_file)
    kwargs = {}
    for f in data.files:
        kwargs[f] = data[f]
        
    for field, rep in replacements:
        kwargs[field] = rep
        
    np.savez(original_file, **kwargs)


if __name__ == '__main__':
    x = np.array([[45, 42, 36, 39, 51, 44],[50,42,41,35,55,49],[55,45,43,40,59,56]]).astype(float).T
    print ANOVA(x)