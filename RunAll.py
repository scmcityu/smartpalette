
"""
export PYTHONPATH=/Users/phanquochuy/Projects/smartpalette; python RunAll.py auto_segment 7 binary gogh monet guillaumin
"""

# import matplotlib
# matplotlib.use('AGG')
import DataSource as ds
import KernelKMeans as km
import FeatureAlignment as fa
import ColorManifolds as cm


if __name__ == '__main__':
    import sys
    import Common
    from os.path import join

    # root = Common.PATH

    n_colors = int(sys.argv[2])
    mode = sys.argv[1] # auto_segment or sliding
    sorter = sys.argv[3]
    collections = sys.argv[4:]

    fp = open(join(Common.PATH, 'RunAll.log'), 'a')
    fp.write(reduce(lambda x, y: x + '\t' + y, sys.argv[1:]) + '\n')
    fp.close()
    # prendergast, chase --> sliding 7
    # luce, monet, guillaumin, urbino, gogh, cezanne, renoir --> 7 auto_segment
    # ['renoir' 'prendergast' 'luce', 'urbino', 'gogh', 'gas'] --> 10 auto_segment
    # [ 'monet' 'gerome' 'metcalf'] --> 10 sliding
    step = 1
    print 'preparing datasource'
    if mode == 'auto_segment':
        ds.prepare_auto_segment_based_palettes(collections, n_segments=10, n_colors=n_colors, cap=40, use_slic=False)
    elif mode == 'sliding':
        ds.prepare_sliding_palettes(n_colors, collections=collections, isizes=[200], steps=[1])
    else:
        exit()

    # ds.prepare_segment_based_palettes(n_colors, collections)

    print 'clustering'
    km.main(n_colors=n_colors, collections=collections)

    print 'aligning features'
    fa.run_tsp_coordinator(n_colors=n_colors, collections=collections, sorter=sorter, save=True, viz='none')

    print 'learning palette manifold'
    cm.run(collections, n_colors, 'gplvm', step=1, mode='train')
