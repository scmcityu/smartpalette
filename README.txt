scikits-image
scikits-learn
numpy
scipy
GPy
OpenOpt
CVXOPT #see http://cvxopt.org/install/ if you are on Darwin (need to install glpk and set CVXOPT_BUILD_GLPK & CVXOPT_GLPK_LIB_DIR ...)
networkx
matplotlib
openmp #only GCC has -fopenmp on MacOS

----- Steps to prepare the data -------

+Run DataSource to extract the palettes
+Run KernelKMeans to group similar palettes together
+Run FeatureAlignment to align the palletes
+Run ColorManifolds to build palette manifolds

----- Explanation for MyPaint code ------
TestMode, PaletteRequestMode --> Mode is a mechanism to integrate our functionalities into MyPaint

----- Two colorization methods -----
1> ColorTransfer.py - Based on Locally Linear Embedding. "Manifold Preserving Edit Propagation" SIGGRAPH 2012
2> coloropt/Colorizer.py - "Colorization Using Optimization" (SIGGRAPH 2014). Need to remove original colors before colorization.

----- Experiments in the paper -----
experiments/test_crossdataset.py