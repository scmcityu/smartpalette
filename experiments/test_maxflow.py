
"""
This file contains a list of examples with different layouts that can be
obtained using the ``add_grid_edges`` method.
"""

import numpy as np
import maxflow
import matplotlib.pyplot as plt
import matplotlib as mpl
#from matplotlib import pyplot as ppl
from graph_utils import plot_graph_2d
from smartpalette.pygco import cut_simple, cut_from_graph

def example_pymaxflow():
    # Standard 4-connected grid
    g = maxflow.Graph[int]()
    img = np.zeros((2, 10), dtype=int)
    intensities = np.random.randint(0, 256, 10)
    img[0, :] = intensities
    img[1, :] = 255 - intensities

    nodeids = g.add_grid_nodes((2, 10))

    # 8-connected grid
    g = maxflow.Graph[int]()
    nodeids = g.add_grid_nodes((2, 10))
    structure = np.array([[0, 0, 1],
                          [0, 0, 1],
                          [0, 0, 1]])
    # Also structure = maxflow.moore_structure(ndim=2, directed=True)
    g.add_grid_edges(nodeids, 1, structure=structure, symmetric=False)
    g.add_grid_tedges(nodeids, img, 255-img)
    # Find the maximum flow.
    g.maxflow()
    # Get the segments of the nodes in the grid.
    sgm = g.get_grid_segments(nodeids)
    # The labels should be 1 where sgm is False and 0 otherwise.
    img2 = np.int_(np.logical_not(sgm))
    # Show the result.

    ax1 = plt.subplot(121)
    ax1.imshow(img2, interpolation='nearest', cmap=mpl.cm.gray)

    ax2 = plt.subplot(122)
    ax2.imshow(img, interpolation='nearest', cmap=mpl.cm.gray)
    plt.show()

    plot_graph_2d(g, nodeids.shape, plot_terminals=True)


def example_color_sorting(sigma=100):
    img = np.zeros((10, 3), dtype=np.int32)
    h, w = img.shape
    #intensities = np.random.randint(0, 256, 10)
    img[:, 0] = np.random.randint(0, 85, 10)
    img[:, 1] = np.random.randint(85, 170, 10)
    img[:, 2] = np.random.randint(170, 256, 10)

    ids = np.arange(img.size).reshape(img.shape)
    edges = []
    for row1 in range(h - 1):
        for col1 in range(w):
            for col2 in range(w):
                edges.append(
                    [ids[row1, col1], ids[row1 + 1, col2],
                     np.int_(10 * np.e ** (-(np.abs(img[row1, col1] - img[row1 + 1, col2]) ** 2) / (sigma ** 2)) )])

    unaries = (np.dstack([np.abs(42 - img.ravel()), np.abs(125 - img.ravel()), np.abs(210 - img.ravel())]).copy("C")).astype(np.int32)
    pairwise = np.array([-1, -1, -1], dtype=np.int32) * np.eye(w, dtype=np.int32)

    result = cut_from_graph(np.array(edges, dtype=np.int32), unaries.reshape(-1, w), pairwise)

    plt.subplot(121, title='img')
    plt.imshow(img, interpolation='nearest', cmap=mpl.cm.gray)
    plt.subplot(122, title="cut_from_graph")
    plt.imshow(result.reshape(img.shape), interpolation='nearest', cmap=mpl.cm.gray)
    plt.show()


def example_multinomial():
    # generate dataset with three stripes
    np.random.seed(15)
    x = np.zeros((10, 12, 3))
    x[:, :4, 0] = -1
    x[:, 4:8, 1] = -1
    x[:, 8:, 2] = -1
    unaries = x + 1.5 * np.random.normal(size=x.shape)
    x = np.argmin(x, axis=2)
    unaries = (unaries * 10).astype(np.int32)
    x_thresh = np.argmin(unaries, axis=2)

    # potts potential
    pairwise_potts = -2 * np.eye(3, dtype=np.int32)
    result = cut_simple(unaries, 10 * pairwise_potts)
    # potential that penalizes 0-1 and 1-2 less thann 0-2
    pairwise_1d = -15 * np.eye(3, dtype=np.int32) - 8
    pairwise_1d[-1, 0] = 0
    pairwise_1d[0, -1] = 0
    print(pairwise_1d)
    result_1d = cut_simple(unaries, pairwise_1d)
    plt.subplot(141, title="original")
    plt.imshow(x, interpolation="nearest")
    plt.subplot(142, title="thresholded unaries")
    plt.imshow(x_thresh, interpolation="nearest")
    plt.subplot(143, title="potts potentials")
    plt.imshow(result, interpolation="nearest")
    plt.subplot(144, title="1d topology potentials")
    plt.imshow(result_1d, interpolation="nearest")
    plt.show()


def example_binary():
    # generate trivial data
    x = np.ones((10, 10))
    x[:, 5:] = -1
    x_noisy = x + np.random.normal(0, 0.8, size=x.shape)
    x_thresh = x_noisy > .0

    # create unaries
    unaries = x_noisy
    # as we convert to int, we need to multipy to get sensible values
    unaries = (10 * np.dstack([unaries, -unaries]).copy("C")).astype(np.int32)
    # create potts pairwise
    pairwise = -10 * np.eye(2, dtype=np.int32)

    # do simple cut
    result = cut_simple(unaries, pairwise)

    # use the gerneral graph algorithm
    # first, we construct the grid graph
    inds = np.arange(x.size).reshape(x.shape)
    horz = np.c_[inds[:, :-1].ravel(), inds[:, 1:].ravel()]
    vert = np.c_[inds[:-1, :].ravel(), inds[1:, :].ravel()]
    edges = np.vstack([horz, vert]).astype(np.int32)

    # we flatten the unaries
    result_graph = cut_from_graph(edges, unaries.reshape(-1, 2), pairwise)

    # plot results
    plt.subplot(231, title="original")
    plt.imshow(x, interpolation='nearest')
    plt.subplot(232, title="noisy version")
    plt.imshow(x_noisy, interpolation='nearest')
    plt.subplot(233, title="rounded to integers")
    plt.imshow(unaries[:, :, 0], interpolation='nearest')
    plt.subplot(234, title="thresholding result")
    plt.imshow(x_thresh, interpolation='nearest')
    plt.subplot(235, title="cut_simple")
    plt.imshow(result, interpolation='nearest')
    plt.subplot(236, title="cut_from_graph")
    plt.imshow(result_graph.reshape(x.shape), interpolation='nearest')

    plt.show()


if __name__ == '__main__':
    #example_binary()
    example_color_sorting()
    #example_multinomial()