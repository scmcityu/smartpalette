from smartpalette import ColorManifolds, Common, FeatureAlignment, KernelKMeans
from smartpalette.fast.Distance import hausdorff
import numpy as np
from matplotlib import pyplot as plt
import matplotlib as mpl
from os.path import join
import GPy
from sklearn.metrics import mean_squared_error, mean_absolute_error
from sklearn.svm import SVR
from sklearn.linear_model import Lasso
from scipy.spatial.distance import euclidean as euc
from sklearn.mixture import GMM, DPGMM
from smartpalette.Regression import gmm_predict
from smartpalette.Kernels import hausdorff_kernel_matrix
from scipy.io import loadmat


def find_best_params_gmm(Xtrain, n_components_range=None, cv_types=None, criterion='aic'):
    lowest_bic = np.infty
    bic = []
    if n_components_range is None:
        n_components_range = np.arange(20, 60, 2)

    if cv_types is None:
        cv_types = ['diag', 'full'] #'spherical', 'tied',

    best_gmm = None
    for cv_type in cv_types:
        #print 'cv_type ', cv_type
        for n_components in n_components_range:
            #print '.',
            # Fit a mixture of Gaussians with EM
            gmm = GMM(n_components=n_components, covariance_type=cv_type)
            gmm.fit(Xtrain)
            if criterion == 'bic':
                bic.append(gmm.bic(Xtrain))
            else:
                bic.append(gmm.aic(Xtrain))

            if bic[-1] < lowest_bic:
                lowest_bic = bic[-1]
                best_gmm = gmm

    print 'best gmm ', best_gmm.n_components, best_gmm.covariance_type
    if best_gmm is None:
        return gmm
    else:
        return best_gmm


def test_lightness_vs_spf(collection, methods, sort_methods, n_colors, level, n_trials=1, missing_ratio=0.5,
                          training_ratio=0.2, step=100, viz=True, test='test_rating', viz_sorting='none'):
    path = Common.PATH
    import os

    # prepare to load matlab code
    from oct2py import octave
    codeRoot = '/Users/phanquochuy/Projects/smartpalette/lib/ColorCompatibility/colorCode/'
    octave.addpath(codeRoot)
    octave.addpath(join(codeRoot, 'data'))
    octave.addpath(join(codeRoot, 'circstat'))
    octave.addpath(join(codeRoot, 'glmnet_matlab'))

    from sklearn.preprocessing import MinMaxScaler
    mms = MinMaxScaler()
    datafile = join(path, 'data', collection, 'clusters_nc%d_l%d.npz' % (n_colors, level))
    # datafile = join(path, 'data', collection, 'dpaintings%d.npz' % (n_colors))
    data = np.load(datafile)
    # sort_methods =  #['tsp', 'lightness', 'pca'] #pca none lightness tsp
    # orig_lab = np.squeeze(data['clustered_lab'])[::step]
    norm_lab = np.squeeze(data['normalized_clustered_lab'])[::step]
    orig_lab = norm_lab
    n_pals, n_cols, n_chas = orig_lab.shape
    n_trains = int(n_pals * training_ratio)
    n_tests = n_pals - n_trains
    n_missings = (n_colors * missing_ratio)

    dists = {}
    for m in methods:
        dists[m] = {}
        for sm in sort_methods:
            dists[m][sm] = np.zeros(n_trials)

    if 'compat_features' in data:
        compat_features = data['compat_features'][::step]
    else:
        rgb = Common.nlab2rgb(orig_lab)
        compat_features = octave.extract_compat_features(rgb)['features']

    if test == 'test_predictive':
        svrfile = join(path, 'data', 'turk', 'svr.npy')
        matfile = join(path, 'data', 'turk', 'themesData.mat')
        mat = loadmat(matfile)
        mms.min_ = mat['datapoints'][0,0]['offsets']
        mms.scale_ = mat['datapoints'][0,0]['scales']

        if (compat_features < 0).any() or (compat_features > 1).any():
            compat_features = np.clip(mms.transform(compat_features), 0, 1)

        if os.path.isfile(svrfile):
            svr = np.load(svrfile).tolist()
        else:
            #svr = SVR(kernel='rbf', C=0.05, gamma=4, verbose=True)
            svr = Lasso(alpha=0.00016,tol=1e-4, copy_X=False)
            turk_compat_features = mat['datapoints'][0,0]['features']
            ratings = np.squeeze(mat['datapoints'][0,0]['targets'])
            pred_ratings = svr.fit(turk_compat_features, ratings).predict(turk_compat_features)
            np.save(svrfile, svr)

            print 'turk rating prediction: ', mean_squared_error(ratings, pred_ratings)
            #svr.fit(data['clustered_lab'][level].reshape((-1, n_colors * n_chas)), data['rating'][level].ravel())

    rs = np.random.RandomState()
    tmpdata, _, labels, order, _ = FeatureAlignment.sort_palettes(orig_lab, n_colors, sort_palettes=True,
                                                                  n_themes=None, labels=None, step=1, sorter='none')
    # tmpdata, order = orig_lab, np.arange(n_pals)
    for tr in range(n_trials):
        perms = rs.permutation(n_pals)
        #print perms
        c_perms = np.array([np.random.permutation(n_colors) for _ in range(n_pals)])
        testids = perms[n_trains:n_trains + n_tests]
        trainids = perms[:n_trains]
        #
        count = 3
        if viz:
            n_plots = len(methods) * len(sort_methods) + 2
            plt.subplot(1, n_plots, 1)
            true_rgb = Common.nlab2rgb(orig_lab)
            plt.imshow(true_rgb[order][testids], interpolation='nearest')

            plt.subplot(1, n_plots, 2)
            true_rgb[np.arange(len(true_rgb))[:, None], c_perms[:, :n_missings]] = 0
            plt.imshow(true_rgb[order][testids], interpolation='nearest')

        for method in methods:
            print 'testing ', method
            for sort_method in sort_methods:
                # rearrange color palettes
                newdata, newindices, _, _, _ =\
                    FeatureAlignment.sort_palettes(tmpdata, n_colors, sort_palettes=False, n_themes=None,
                                                   labels=None, step=1, sorter=sort_method, viz=viz_sorting)

                training_data = newdata[trainids].copy()
                testing_data = newdata[testids].copy()

                if test == 'test_predictive' or test == 'test_sampling':
                    # train the model
                    if method == 'gplvm':
                        Q = 4
                        kernel = GPy.kern .RBF(Q, ARD=True) + GPy.kern.Bias(Q)
                        m = GPy.models.GPLVM(training_data.reshape((-1, n_colors * n_chas)), Q, kernel=kernel)
                        m.optimize('scg', messages=True, max_iters=2000, gtol=1e-4)
                    elif method == 'bgplvm':
                        Q = 4
                        kernel = GPy.kern.RBF(Q, 1., 1. / np.random.uniform(0, 1, (Q,)), ARD=True) + GPy.kern.Bias(Q, np.exp(-2))
                        m = GPy.models.BayesianGPLVM(training_data.reshape((-1, n_colors * n_chas)), Q, kernel=kernel, num_inducing=40)
                        m.optimize('bfgs', messages=True, max_iters=2000, gtol=1e-4)
                    elif 'gmm' in method:
                        if method == 'gmm_1':
                            kernel_ = hausdorff_kernel_matrix(training_data)
                            _, _, labels = FeatureAlignment.find_best_n_clusters(kernel_)
                            unique_labels = np.unique(labels)
                            n_clusters = len(unique_labels)
                            m = GMM(n_components=n_clusters, covariance_type='diag', init_params='c', params='cw')
                            m.weights_ = np.array([(labels==l).sum() for l in unique_labels]) / float(n_trains)
                            m.means_ = np.array([training_data[labels==l].mean(axis=0) for l in unique_labels]).reshape((-1,n_colors * n_chas))
                            m.fit(training_data.reshape((-1, n_colors * n_chas)))
                        if method == 'gmm_2':
                            m = GMM(n_components=8)
                            # m = find_best_params_gmm(training_data.reshape((-1, n_colors * n_chas)),
                            #                          n_components_range=range(2, n_trains / 2, 2), criterion='bic')
                            #m = DPGMM(alpha=5, covariance_type='full')
                            m.fit(training_data.reshape((-1, n_colors * n_chas)))

                if test == 'test_predictive':
                    tmp_c_perms = c_perms[order][testids].copy()
                    for i in range(n_tests):
                        for j in range(n_cols):
                            tmp_c_perms[i, j] = list(newindices[testids][i]).index(c_perms[order][testids][i, j])
                    testing_data[np.arange(n_tests)[:, None], tmp_c_perms[:, :n_missings]] = np.nan
                    pred_data = np.zeros_like(testing_data)

                    # test predictive performance
                    for fx, feat in enumerate(testing_data):
                        tfeat = feat.ravel()

                        if method == 'bgplvm':
                            xnew, newm = m.infer_newX(tfeat[None, :], optimize=True)
                            new_palette, _ = m.predict(np.array(xnew.mean), full_cov=True)
                        elif method == 'gplvm':
                            xnew, newm = m.infer_newX(tfeat[None, :], optimize=True)
                            new_palette, _ = m.predict(np.array(xnew))
                        elif 'gmm' in method:
                            new_palette = gmm_predict(tfeat[None, :], m)

                        pred_data[fx, :] = new_palette[0].reshape((-1, n_chas))

                    # bring pred_data back to the old order
                    pred_data[np.arange(n_tests)[:, None], tmp_c_perms[:, n_missings:]] = \
                        testing_data[np.arange(n_tests)[:, None], tmp_c_perms[:, n_missings:]]
                    reversed_indices = np.argsort(newindices[testids], axis=1)
                    pred_data = pred_data[np.arange(len(testids))[:, None], reversed_indices]
                    #pred_data = Common.nlab2rgb(pred_data)
                    #pred_features = np.clip(mms.transform(octave.extract_compat_features(pred_data)['features']), 0, 1)
                    if viz:
                        plt.subplot(1, n_plots, count, title=method + sort_method)
                        #testing_data[np.isnan(testing_data)] = 0
                        #plt.imshow(Common.nlab2rgb(testing_data.reshape((-1, n_cols, n_chas))), interpolation='nearest')
                        plt.imshow(pred_data.reshape((-1, n_cols, n_chas)), interpolation='nearest')
                        #plt.imshow(Common.nlab2rgb(training_data.reshape((-1, n_cols, n_chas))), interpolation='nearest')

                    total_dist = 0
                    # total_dist = mean_squared_error(compat_features[order][testids], pred_features)
                    # total_dist = mean_absolute_error(svr.predict(compat_features[order][testids]), svr.predict(pred_features))

                    for i, y_pred, y_test in zip(range(n_tests), pred_data, orig_lab[order][testids]):
                        # newperm = [list(newindices[testids[i]]).index(c) for c in c_perms[testids[i]][:n_missings]]
                        total_dist += hausdorff(y_pred, y_test, mode=1)
                        # sample_weight = np.tile([1, 1, 1], len(newperm))
                        # total_dist += mean_squared_error(y_pred[newperm].ravel(), y_test[newperm].ravel(), sample_weight)

                    dists[method][sort_method][tr] = total_dist

                elif test == 'test_rating':
                    interpolalions = []
                    ratings = []
                    for i in range(len(testids)):
                        for j in range(1, i):
                            print '.',
                            t1, t2 = testids[i], testids[j]
                            if t1 != t2:
                                vec = newdata[t2].ravel() - newdata[t1].ravel()
                                vec /= 10.
                                move = newdata[t1].ravel()
                                for _ in range(10):
                                    move += vec
                                    interpolalions.append(move.reshape((-1, n_chas)))
                                    ratings.append( svr.predict(move) )

                    print np.mean(ratings), np.max(ratings), np.min(ratings), np.std(ratings)
                elif test == 'test_sampling':
                    samples = m.sample(100)
                    samples = samples.reshape((-1, n_cols, n_chas))

                    for td in testing_data:
                        dists[method][sort_method][tr] += np.min([hausdorff(td, s, mode=1) for s in samples])
                    dists[method][sort_method][tr] /= n_tests

                count += 1

        if viz:
            plt.show()

    for m in methods:
        for sm in sort_methods:
            print m, sm, dists[m][sm].mean(), dists[m][sm].std()


if __name__ == "__main__":
    # guillaumin pissarro prendergast --> tsp
    # renoir, gogh, gas --> lightness
    test_lightness_vs_spf('renoir', methods=['gmm_1'], sort_methods=['binary', 'lightness'],
                          n_colors=5, level=0, n_trials=10, missing_ratio=0.6,
                          training_ratio=0.6, step=1, viz=False, test='test_predictive', viz_sorting='none')

    #test_lightness_vs_spf_with_kkm('pissarro', method='gmm_2', n_colors=5, level=0, n_trials=4, missing_ratio=0.4,
    #                      training_ratio=0.5, step=1, viz=True, test='test_predictive')
