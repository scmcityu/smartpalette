'''
Created on 20 Nov, 2014

@author: phan
'''
import pymatlab
import numpy as np

if __name__ == '__main__':
    session = pymatlab.session_factory()
    session.run("addpath('/home/phan/workspace/smartpalette/smartpalette/matlab')")
    session.run('B=[1 3 4]')
    session.run('A=dummy(B)')
    b = session.getvalue('B')
    print b