# -*- coding: utf-8 -*-
"""Main experiments for TVCG happen here.

sort_colorize - simply sort the palettes and then colorize them for comparison without interpolatino
interpolate_palettes - sort & interpolation new palettes via manifolds

"""

import matplotlib
# matplotlib.use('AGG')

from smartpalette import Common, FeatureAlignment, ColorManifolds
from smartpalette.fast import Distance
import numpy as np
from matplotlib import pyplot as plt
from os.path import join
from smartpalette.coloropt import Colorizer
from skimage.color import rgb2gray, gray2rgb, rgb2hsv, hsv2rgb
from skimage.draw import ellipse
from sklearn.svm import SVC
from sklearn.metrics import mean_squared_error

from sklearn.metrics import mean_absolute_error, mean_squared_error
from sklearn.mixture import GMM
from scipy.spatial.distance import euclidean as euc
import itertools
from skimage.io import imsave
from skimage import draw
import os
import GPy


def render_palettes_circles(palettes, n_clusters, km=None, radius=100, gap=0):
    # hsv_palettes = rgb2hsv(palettes)
    n_pals, n_cols, n_chas = palettes.shape
    colors = palettes.reshape((n_pals * n_cols, n_chas))
    if km is None:
        km = KMeans(n_clusters=n_clusters)
        labels = km.fit_predict(colors)
    else:
        labels = km.predict(colors)

    iw, ih = radius * 2 * n_clusters, radius * 2
    img = np.zeros((ih, iw, n_chas))
    for c in range(n_clusters):
        ids = np.where(labels == c)[0]
        # idxs = np.argsort([euc(c_, km.cluster_centers_[c]) for c_ in colors[labels == c]])
        # ids = ids[idxs]
        if len(ids) == 0:
            rr, cc = draw.ellipse(radius, c * (radius * 2) + radius, radius, radius, shape=(ih, iw))
            img[rr, cc] = [1, 1, 1]
        else:
            # r = radius / float(len(ids))
            a = np.pi * 2 / float(len(ids))
            for idx in range(len(ids)):
                # rr, cc = draw.ellipse(radius, c * (radius * 2) +
                # radius, radius - idx * r, radius - idx * r, shape=(ih, iw))
                r_ = radius * 2
                y1, x1, y2, x2 = radius - np.sin(idx * a) * r_, \
                                 np.cos(idx * a) * r_ + c * (radius * 2) + radius, \
                                 radius - np.sin((idx + 1) * a) * r_, \
                                 np.cos((idx + 1) * a) * r_ + c * (radius * 2) + radius
                rr1, cc1 = draw.polygon(np.array([radius, y1, y2]),
                                        np.array([c * (radius * 2) + radius, x1, x2]), shape=(ih, iw))

                r2 = radius
                rr2, cc2 = draw.ellipse(radius, c * (radius * 2) + radius, r2, r2, shape=(ih, iw))
                mask1 = np.zeros(img.shape[:2], dtype=bool)
                mask1[rr2, cc2] = True

                mask2 = np.zeros(img.shape[:2], dtype=bool)
                mask2[rr1, cc1] = True

                mask = np.bitwise_and(mask1, mask2)
                img[mask] = colors[ids[idx]]

    return img, km


def render_palettes(palettes, box_size=50, vgap=0, hgap=5, shape='square', background='black'):
    n_pals, n_cols, n_chas = palettes.shape
    iw, ih = n_pals * (box_size + hgap), n_cols * (box_size + vgap)
    if background == 'black':
        img = np.zeros((iw, ih, n_chas))
    elif background == 'white':
        img = np.ones((iw, ih, n_chas))
    for px in range(n_pals):
        for cx in range(n_cols):
            if shape == 'square':
                img[px * (box_size + hgap): px * (box_size + hgap) + box_size,
                cx * (box_size + vgap): cx * (box_size + vgap) + box_size] = palettes[px, cx]
            elif shape == 'circle':
                rr, cc = draw.ellipse(px * (box_size + hgap) + box_size / 2, cx * (box_size + vgap) + box_size / 2,  box_size / 2,  box_size / 2, shape = img.shape)
                img[rr, cc] = palettes[px, cx]
    img = np.swapaxes(img, 0, 1)
    return img


def mark_image(img, colors, yy, xx, sizes):
    h, w, d = img.shape
    marked_img = img.copy()

    if isinstance(sizes, int):
        sizes = [sizes] * len(colors)

    for cx, color in enumerate(colors):
        rr, cc = ellipse(yy[cx], xx[cx], sizes[cx], sizes[cx], (h, w))
        marked_img[rr, cc] = color

    return marked_img


def test_classification(artists, queries, n_reps=5, train_ratio=0.6, n_colors=5, step=1,
                        n_missings=6, n_pals_per_artist=10, sort_methods=['binary', 'none']):
    from smartpalette.Regression import gmm_predict, find_best_params_gmm
    path = Common.PATH
    n_pals, n_cols, n_chas = 0, n_colors, 3
    data = []
    labels = []
    data_lengths = []

    if isinstance(queries, str):
        filename = queries
        query_image = img_as_float(imread(filename))
        km = KMeans(n_clusters=n_colors, verbose=0)
        km.fit(Common.rgb2nlab(query_image).reshape((-1, 3)))
        queries = km.cluster_centers_[None, :]

        # pixels = query_image.reshape((-1, 3))
        # mask = [(p < 0.9).any() for p in pixels]
        # km.fit(Common.rgb2nlab(pixels[mask][None, ...])[0])
        # queries = km.cluster_centers_[None, :]

    for ax, artist in enumerate(artists):
        datafile = join(path, 'data', artist, 'clusters_nc%d_l%d.npz' % (n_colors, 0))
        tmpdata = np.load(datafile)
        norm_lab = np.squeeze(tmpdata['normalized_clustered_lab'])[::step]
        data_lengths.append(len(norm_lab))
        labels.extend(np.repeat(ax, len(norm_lab)))

        for qx, query in enumerate(queries):
            # dists = Distance.parallel_hausdorff(query, norm_lab, mode=1)
            # best_ids = np.argsort(dists)
            # data.append(norm_lab[best_ids[:n_pals_per_artist]])
            perm = np.random.permutation(len(norm_lab))[:n_pals_per_artist]
            data.append(norm_lab[perm])

    labels = np.array(labels)
    data = np.concatenate(data)
    error = {}
    for smethod in sort_methods:
        error[smethod] = []

    data_length = len(data)
    n_trains = int(data_length * train_ratio)
    n_tests = data_length - n_trains

    for t in range(n_reps):
        print 'test:', t
        perms = np.random.permutation(data_length)
        trainids = perms[:n_trains]
        testids = perms[n_trains:]
        c_perms = np.array([np.random.permutation(n_colors) for _ in range(data_length)])
        # svc = SVC(C=100.0, kernel='rbf')

        for sort_method in sort_methods:
            sorted_lab, newindices_, _, order, _ = \
                FeatureAlignment.sort_palettes(np.concatenate([queries, data]), n_colors,
                                               sort_palettes= sort_method in ['binary'],
                                               n_themes=None, labels=None, step=1, sorter=sort_method, viz='none')
            sorted_lab = sorted_lab[np.argsort(order)]
            newindices = newindices_[np.argsort(order)][1:]

            qr = sorted_lab[:1] #[0]
            # print qr[np.argsort(newindices_[list(order).index(0)])]

            training_data = sorted_lab[1:][trainids]
            testing_data = sorted_lab[1:][testids]

            # ----- TEST DIST -----
            # err = 0
            # for pal in sorted_lab[1:]:
            #     err += euc(qr.ravel(), pal.ravel())
            # error[sort_method].append(err)

            # ----- TEST SAMPLING ------
            '''
            m = find_best_params_gmm(training_data.reshape((-1, n_colors * n_chas)), cv_types=['diag'],
                                     n_components_range=range(1, 20, 2), criterion='aic')
            m.fit(training_data.reshape((-1, n_colors * n_chas)))
            samples = m.sample(1000)
            samples = samples.reshape((-1, n_cols, n_chas))
            err = 0
            for td in qr:
                err += np.min([mean_absolute_error(td.ravel(), s.ravel()) for s in training_data])
            error[sort_method].append(err / float(n_trains))

            # ------ TEST PREDICTION -----
            '''
            # training the model
            m = GPy.models.BayesianGPLVM(training_data.reshape((-1, n_colors * n_chas)), 4, num_inducing=8)
            m.optimize('bfgs', messages=True, max_iters=2000, gtol=1e-4)

            # remomving some colors
            input_data = testing_data.copy()
            tmp_c_perms = c_perms[testids]
            for cx, cp in enumerate(tmp_c_perms):
                for ix in cp[:n_missings]:
                    ixx = list(newindices[testids][cx]).index(ix)
                    input_data[cx][ixx] = np.nan

            # predicting the missing colors & record the error
            err = 0
            for fx, feat in enumerate(input_data):
                tfeat = feat.ravel()
                xnew, newm = m.infer_newX(tfeat[None, :], optimize=True)
                new_palette, _ = m.predict(np.array(xnew.mean), full_cov=True)
                err += mean_absolute_error(testing_data[fx].ravel(), new_palette.ravel())

            error[sort_method].append(err / float(n_tests))
            # '''
            # ------ TEST CLASSFICIATION -----
            # testing_data, _ = \
            # FeatureAlignment.align_colors_naive(data[testids], , match_method='munkres', n_examples=100)
            # svc.fit(trainind_data.reshape((-1, n_cols * n_chas)), labels[trainids])
            # plabels = svc.predict(testing_data.reshape((-1, n_cols * n_chas)))
            # e = mean_squared_error(labels[testids], plabels)
            # error[sort_method].append(e)

    for sm in sort_methods:
        print sm, error[sm], np.mean(error[sm])


def palette_from_image(filename, n_colors):
    query_image = img_as_float(imread(filename))
    km = KMeans(n_clusters=n_colors, verbose=0)
    km.fit(Common.rgb2nlab(query_image).reshape((-1, 3)))
    query = km.cluster_centers_ #[None, :]
    return query


def generate_user_study(artists, query_file, n_colors=7, step=1, sort_methods=None, viz=False, n_trains=10, n_samples=10, seed=None):
    from sklearn.decomposition import PCA
    # from smartpalette.Regression import find_best_params_gmm
    path = Common.PATH
    testpath = join(Common.PATH, 'user_study')

    n_pals_per_artist = n_trains / len(artists)
    query_palette = palette_from_image(query_file, n_colors=n_colors)

    training_data = []
    current_n_trains = 0
    for ax, artist in enumerate(artists):
        datafile = join(path, 'data', artist, 'clusters_nc%d_l%d.npz' % (n_colors, 0))
        tmpdata = np.load(datafile)
        norm_lab = np.squeeze(tmpdata['normalized_clustered_lab'])[::step]

        dists = Distance.parallel_hausdorff(query_palette, norm_lab, mode=1)
        best_ids = np.argsort(dists)[::1]
        # best_ids = np.random.permutation(len(norm_lab))
        training_data.append(norm_lab[best_ids[0:n_pals_per_artist]])
        current_n_trains += n_pals_per_artist
        if len(training_data) < n_trains and ax == len(artists) - 1:
            training_data.append(norm_lab[best_ids[n_pals_per_artist:n_pals_per_artist + (n_trains - current_n_trains)]])

    training_data = np.concatenate(training_data).reshape((n_trains, n_colors, -1))
    # training_data = Common.nlab2rgb(training_data)
    colorconv = Common.nlab2rgb # lambda x: x

    assert len(training_data) == n_trains
    training_data, _, _, _, _ = \
        FeatureAlignment.sort_palettes(training_data, n_colors,
                                       sort_palettes=True,
                                       n_themes=None, labels=None, step=1, sorter='none', viz=False)

    n_pals, n_cols, n_chas = training_data.shape
    # train_img, km = render_palettes_circles(colorconv(training_data), n_colors)
    train_img = render_palettes(colorconv(training_data))
    imsave(join(testpath, 'training.png'), train_img)
    for sort_method in sort_methods:
        rs = np.random.RandomState(seed)

        training_data_ = np.array(training_data)
        training_data_, newindices, _, order, _ = \
            FeatureAlignment.sort_palettes(training_data_, n_colors,
                                           sort_palettes=False,
                                           n_themes=None, labels=None, step=1, sorter=sort_method, viz=False)

        pca = PCA(n_components=2)
        reduced_training_data = pca.fit_transform(training_data_.reshape((-1, n_colors * n_chas)))
        # gmm = find_best_params_gmm(reduced_training_data, n_components_range=range(1, 10, 1), cv_types=['full'])

        tmp_X = reduced_training_data
        minx, miny = tuple(np.min(tmp_X, axis=0))
        maxx, maxy = tuple(np.max(tmp_X, axis=0))

        xpos = np.linspace(minx, maxx, n_samples)
        ypos = np.linspace(miny, maxy, n_samples)

        sampling_X = np.zeros((n_samples * n_samples, 2))
        sampling_X[:] = list(itertools.product(xpos, ypos))
        sampling_Y = np.clip(pca.inverse_transform(sampling_X), 0, 1)

        # densities, _ = gmm.score_samples(sampling_X)
        # sampling_ids = list(itertools.product(range(n_samples), range(n_samples)))
        samples = colorconv(sampling_Y.reshape((-1, n_colors, n_chas)))
        surf_indices = np.array([rs.permutation(n_colors) for _ in range(n_samples * n_samples)])
        samples = samples[np.arange(n_samples * n_samples)[:, None], surf_indices]

        img = render_palettes(samples)
        # imsave(join(testpath, 'training_' + sort_method + '.jpg'), render_palettes(Common.nlab2rgb(training_data_)))

        # img, _ = render_palettes_circles(samples, n_colors, km=km)
        imsave(join(testpath, 'samples_' + sort_method + '.png'), img)

    # imsave(join(testpath, 'training.jpg'), render_palettes_circles(Common.nlab2rgb(training_data)), n_colors)


def interpolate_palettes(artists, modes, queries=None, n_colors=5, step=1, sort_methods=['none'], viz=False, viz_sorting='none',
         n_trains=5, n_samples=5, n_pals_per_artist=2, sampling=False):
    """
    The main function that generates interpolation examples for the TVCG paper
    :param artists:
    :param modes gmm_manifold, manifold, image ,palteee
    :param queries: querying palettes
    :param n_colors:
    :param step:
    :param sort_methods: binary, lightness, pca, none
    :param viz:
    :param viz_sorting:
    :param n_trains:
    :param n_samples:
    :return:
    """
    if isinstance(queries, str):
        filename = queries
        query_image = img_as_float(imread(queries))[:, :, :3]
        # infofile = join(os.path.dirname(filename), 'info.npy')
        # if os.path.isfile(infofile):
        #     queries = np.load(infofile)
        #     km = KMeans(n_clusters=n_colors)
        #     km.cluster_centers_ = queries[0]
        # else:
        km = KMeans(n_clusters=n_colors, verbose=0)
        km.fit(Common.rgb2nlab(query_image).reshape((-1, 3)))
        queries = km.cluster_centers_[None, :]
        # np.save(infofile, queries)

        if modes is None:
            modes = ['image']

    if not isinstance(modes, list) and modes is not None:
        modes = [modes]

    path = Common.PATH
    n_pals, n_cols, n_chas = 0, 5, 3
    data = [[] for _ in range(len(queries))]
    artist_labels = np.ravel([np.repeat(a, n_pals_per_artist) for a in artists])
    for artist in artists:
        datafile = join(path, 'data', artist, 'clusters_nc%d_l%d.npz' % (n_colors, 0))
        tmpdata = np.load(datafile)
        norm_lab = np.squeeze(tmpdata['normalized_clustered_lab'])[::step]
        n_pals, n_cols, n_chas = norm_lab.shape
        rs = np.random.RandomState(1999) #1999
        # rs = np.random.RandomState(None)
        for qx, query in enumerate(queries):
            dists = Distance.parallel_hausdorff(query, norm_lab, mode=1)
            best_ids = np.argsort(dists)
            data[qx].append(norm_lab[best_ids[0:n_pals_per_artist]])
            # perm = rs.permutation(len(norm_lab))[:n_pals_per_artist]
            # data[qx].append(norm_lab[perm])

    concat_labs = [np.concatenate(data[qx]) for qx in range(len(queries))]
    for mode in modes:
        qx = 0

        for sort_method in sort_methods:
            # queries1 = queries.copy()
            # queries1[:, :, 0] -= 0.15
            # sort_method in ['binary', 'lightness', 'local', 'none']
            training_data, newindices, _, order, _ = \
                FeatureAlignment.sort_palettes(np.concatenate([queries[None, qx],  concat_labs[qx]]), n_colors,
                                               sort_palettes=True,
                                               n_themes=None, labels=None, step=1, sorter=sort_method, viz='none')

            training_data = training_data[np.argsort(order)]
            newindices = newindices[np.argsort(order)]
            query_newindices = newindices[0]

            query = training_data[0]
            training_data = training_data[1:]
            training_img = render_palettes(Common.nlab2rgb(training_data[:, query_newindices.argsort(), :]),
                                           background='white', shape='circle', hgap=10)

            if viz_sorting == 'clean':
                plt.subplot(111)
                plt.imshow(training_img, interpolation='nearest')
                plt.axis('off')
                plt.show()

            n_trains = len(training_data)

            if mode == 'palette' or mode == 'image':
                interpolalions = np.zeros((n_trains, n_samples, n_cols, n_chas))
                query = query[np.argsort(query_newindices)]
                training_data = training_data[:, np.argsort(query_newindices), :]
                for i in range(n_trains):
                    vec = training_data[i].ravel() - query.ravel()
                    vec /= float(n_samples - 1)
                    move = query.ravel().copy()
                    for k in range(n_samples):
                        interpolalions[i][k] = move.reshape((-1, n_chas)).copy()
                        move += vec

            if viz:
                matplotlib.rcParams.update({'font.size': 22})
                if mode == 'palette':
                    fig = plt.figure(sort_method + '_' + mode)
                    for i in range(n_trains):
                        ax = fig.add_subplot(len(artists), n_pals_per_artist, i + 1, title=artist_labels[i])
                        ax.axis('off')
                        ax.imshow(Common.nlab2rgb(np.swapaxes(interpolalions[i], 0, 1)), interpolation='nearest')

                elif mode == 'image':
                    from smartpalette.coloropt import Colorizer
                    h, w, d = query_image.shape
                    gray = gray2rgb(rgb2gray(query_image))
                    pix_labels = km.predict(Common.rgb2nlab(query_image).reshape((-1, d))).reshape((h, w))
                    it = 1
                    fig = plt.figure(sort_method + '_' + mode)
                    for i in range(n_trains):
                        interpolalions[i] = Common.nlab2rgb(interpolalions[i])
                        for j in range(n_samples):
                            ann_image = gray.copy()
                            pal = interpolalions[i][j] #[np.argsort(query_newindices)]

                            xx = np.random.randint(0, w, 500)
                            yy = np.random.randint(0, h, 500)

                            ann_image[yy, xx, :] = pal[pix_labels[yy, xx]]
                            colorized_image = Colorizer.colorize(gray, ann_image, 5, 2)

                            th, tw, td = colorized_image.shape
                            colorized_image = np.pad(colorized_image, ((0, 50), (0, 0), (0, 0)), mode='constant', constant_values=1)
                            rr, cc = draw.polygon(np.array([th-1, th-1, th + 50, th + 50]),
                                                  np.array([0, n_colors * 50, n_colors * 50, 0]), shape=colorized_image.shape)
                            colorized_image[rr, cc] = 0
                            for c in range(n_colors):
                                colorized_image[th:th+49, c*50:c*50 + 49, :] = pal[c]

                            # ann_image = mark_image(ann_image, pal[pix_labels[yy, xx]], yy, xx, 5)
                            ax = fig.add_subplot(n_trains, n_samples, it, title=artist_labels[i])
                            ax.axis('off')
                            ax.imshow(colorized_image, interpolation='nearest')

                            it += 1
                elif mode == 'diversify':
                    import GPy
                    from smartpalette.coloropt import Colorizer
                    h, w, d = query_image.shape
                    gray = gray2rgb(rgb2gray(query_image))
                    pix_labels = km.predict(Common.rgb2nlab(query_image).reshape((-1, d))).reshape((h, w))
                    it = 1
                    fig = plt.figure(sort_method + '_' + mode)
                    Q = 4

                    for artist in artists:
                        X_train = training_data[artist_labels == artist]
                        m = GPy.models.GPLVM(X_train.reshape((-1, n_cols * n_chas)), Q, kernel=None)
                        m.optimize('lbfgs', messages=1, max_iters=4000, gtol=1e-4)

                        ax = fig.add_subplot(len(artists), n_samples + 1, it, title='original')
                        ax.imshow(query_image)
                        it += 1
                        for j in range(n_samples):
                            xnew, _ = m.infer_newX(query.reshape((1, n_cols * n_chas)), optimize=True)
                            pal, _ = m.predict(np.array(xnew))
                            pal = pal[0].reshape((-1, n_chas))[np.argsort(query_newindices)]
                            pal = Common.nlab2rgb(pal.reshape((-1, n_cols, n_chas)))[0]

                            ann_image = gray.copy()

                            xx = np.random.randint(0, w, 500)
                            yy = np.random.randint(0, h, 500)

                            ann_image[yy, xx, :] = pal[pix_labels[yy, xx]]
                            colorized_image = Colorizer.colorize(gray, ann_image, 5, 2)

                            th, tw, td = colorized_image.shape
                            colorized_image = np.pad(colorized_image, ((0, 40), (0, 0), (0, 0)),
                                                     mode='constant', constant_values=1)
                            for c in range(n_colors):
                                colorized_image[th:th+40, c*40:c*40 + 40, :] = pal[c]

                            # ann_image = mark_image(ann_image, pal[pix_labels[yy, xx]], yy, xx, 5)
                            ax = fig.add_subplot(len(artists), n_samples + 1, it, title=artist)
                            ax.axis('off')
                            ax.imshow(colorized_image, interpolation='nearest')

                            it += 1

                elif mode == 'gmm_manifold':
                    from sklearn.decomposition import PCA
                    from smartpalette.Regression import find_best_params_gmm
                    from smartpalette.ColorTransfer import Colorizer, prepare_colorizer
                    from matplotlib.colors import LogNorm
                    from matplotlib.offsetbox import OffsetImage, AnnotationBbox
                    fname = os.path.basename(filename)
                    # from sklearn.neighbors import KernelDensity

                    # training_data = training_data[:, np.argsort(query_newindices), :]
                    pca = PCA(n_components=2)
                    reduced_training_data = pca.fit_transform(training_data.reshape((-1, n_colors * n_chas)))

                    gmm = find_best_params_gmm(reduced_training_data, n_components_range=range(1, 10, 1), cv_types=['full'])
                    if True:
                        fig_gmm = plt.figure('gmm visualization')
                        ax = fig_gmm.add_subplot(111)

                        # visualize the reduced space
                        X_train = reduced_training_data
                        mnx, mny = tuple(X_train.min(axis=0))
                        mxx, mxy = tuple(X_train.max(axis=0))

                        x = np.linspace(mnx - .2, mxx + .2)
                        y = np.linspace(mny - .2, mxy + .2)
                        X, Y = np.meshgrid(x, y)
                        XX = np.array([X.ravel(), Y.ravel()]).T
                        Z = -gmm.score_samples(XX)[0]
                        Z = Z.reshape(X.shape) - Z.min() + 1e-5

                        CS = ax.contourf(X, Y, Z, norm=LogNorm(vmin=1, vmax=50), levels=np.logspace(0, 3), cmap=plt.cm.Greys)
                        # CS = ax.contourf(X, Y, Z, norm=LogNorm(vmin=1.0, vmax=1000.0), levels=np.logspace(0, 10, 10))
                        # CB = plt.colorbar(CS, extend='both') #shrink=0.8,

                        ax.scatter(X_train[:, 0], X_train[:, 1], .8)
                        for i in range(n_trains):
                            y = training_data[i]
                            rgb = Common.nlab2rgb(y.reshape((1, n_colors, 3)))
                            im = OffsetImage(rgb, zoom=5, interpolation='nearest')
                            xy = np.array(reduced_training_data[i, (0, 1)])
                            ab = AnnotationBbox(im, xy,
                                                xybox=(-0.5, 0.5),
                                                xycoords='data',
                                                boxcoords="offset points",
                                                pad=0.00,
                                                arrowprops=dict(arrowstyle="->"))
                            ax.add_artist(ab)

                        # ax.title('Negative log-likelihood predicted by a GMM')
                        ax.axis('tight')
                        fig_gmm.savefig(join(Common.PATH, 'results', 'manifold_sampling', sort_method, fname[:-4] + '_gmm.jpg'))

                        # plt.show()

                    if sampling:
                        tmp_X = reduced_training_data
                        minx, miny = tuple(np.min(tmp_X, axis=0))
                        maxx, maxy = tuple(np.max(tmp_X, axis=0))

                        xpos = np.linspace(minx, maxx, n_samples)
                        ypos = np.linspace(miny, maxy, n_samples)

                        sampling_X = np.zeros((n_samples * n_samples, 2))
                        sampling_X[:] = list(itertools.product(xpos, ypos))

                        sampling_Y = np.clip(pca.inverse_transform(sampling_X), 0, 1)
                        densities, _ = gmm.score_samples(sampling_X)
                        sampling_ids = list(itertools.product(range(n_samples), range(n_samples)))

                        aligner = FeatureAlignment.PaletteAligner(method='data',
                                                                  data=training_data,
                                                                  match_method='munkres',
                                                                  n_examples=-1)
                        colorizer, prm = \
                            prepare_colorizer(filename, aligner=aligner, n_segments=40, n_colors=n_colors)

                        for sy in range(len(sampling_Y)):
                            new_palette = sampling_Y[sy].reshape((n_colors, n_chas))[prm['alignment_ids']]
                            new_image, lle_params = \
                                colorizer.colorize_quick(prm['image_nlab'], prm['seg_masks'], prm['seg_mean_colors'],
                                                         prm['seg_color_labels'], prm['orig_palette'],
                                                         new_palette, prm['lle_params'], prm['lmda'])

                            pal_img = render_palettes(Common.nlab2rgb(new_palette[None, :]))
                            imsave(join(Common.PATH, 'results', 'manifold_sampling', sort_method, fname[:-4] +
                                        '_%d_%d.jpg') % sampling_ids[sy], new_image)

                            imsave(join(Common.PATH, 'results', 'manifold_sampling', sort_method, fname[:-4] +
                                        '_pal_%d_%d.jpg') % sampling_ids[sy], pal_img)
                            prm['lle_params'] = lle_params

                        np.savez(join(Common.PATH, 'results', 'manifold_sampling', sort_method, fname[:-4] + '.npz'),
                                 sampling_X=sampling_X, sampling_Y=sampling_Y, sampling_ids=sampling_ids,
                                 gmm=gmm, densities=densities)

                elif mode == 'gplvm_manifold':
                    import GPy
                    from matplotlib.offsetbox import OffsetImage, AnnotationBbox
                    from GPy.plotting.matplot_dep.dim_reduction_plots import most_significant_input_dimensions
                    from smartpalette.Visualization import colorizer_show

                    h, w, d = query_image.shape
                    method = 'gplvm'
                    Q = 4
                    training_data = training_data[:, np.argsort(query_newindices), :]
                    if method == 'bgplvm':
                        m = GPy.models.BayesianGPLVM(
                            training_data.reshape((-1, n_cols * n_chas)), Q, kernel=None, num_inducing=20)
                        m.optimize('bfgs', messages=1, max_iters=2000, gtol=1e-4)
                    else:
                        m = GPy.models.GPLVM(training_data.reshape((-1, n_cols * n_chas)), Q, kernel=None)
                        m.optimize('lbfgs', messages=1, max_iters=4000, gtol=1e-4)

                    which_indices = most_significant_input_dimensions(m, None)
                    y = np.asfarray(m.Y[0, :])
                    ax = m.plot_latent()

                    ax.axis('off')

                    if method == 'bgplvm':
                        Y = training_data #np.array(m.Y)
                        X = np.array(m.X.mean)
                    else:
                        Y = np.array(m.Y)
                        X = np.array(m.X)

                    for i in range(n_trains):
                        y = Y[i]
                        rgb = Common.nlab2rgb(y.reshape((1, n_colors, 3)))
                        im = OffsetImage(rgb, zoom=7, interpolation='nearest')
                        xy = np.array(X[i, which_indices])
                        ab = AnnotationBbox(im, xy,
                                            xybox=(-0.5, 0.5),
                                            xycoords='data',
                                            boxcoords="offset points",
                                            pad=0.00,
                                            arrowprops=dict(arrowstyle="->"))
                        ax.add_artist(ab)

                    data_show = colorizer_show(y[None, :], model=m, image_file=filename,
                                               n_segments=40, method='none', dimensions=(1, n_cols, n_chas),
                                               transpose=True, order='C', invert=False, scale=False, n_colors=n_colors)

                    if sampling:
                        fname = os.path.basename(filename)

                        tmp_X = X[:, which_indices]
                        minx, miny = tuple(np.min(tmp_X, axis=0))
                        maxx, maxy = tuple(np.max(tmp_X, axis=0))

                        xpos = np.linspace(minx, maxx, n_samples)
                        ypos = np.linspace(miny, maxy, n_samples)

                        sampling_X = np.zeros((n_samples * n_samples, Q))
                        sampling_X[:, which_indices] = list(itertools.product(xpos, ypos))
                        sampling_Y, _ = m.predict(Xnew=sampling_X, full_cov=True)

                        sampling_ids = list(itertools.product(range(n_samples), range(n_samples)))
                        err = 0.
                        for sy in range(len(sampling_Y)):
                            # m.posterior_samples_f()
                            err += euc(sampling_Y[sy].ravel(), query.ravel())
                            data_show.set_image(sampling_Y[sy][None, :])

                            pal = Common.nlab2rgb(sampling_Y[sy].reshape((1, n_cols, n_chas)))
                            pal_img = render_palettes(pal, hgap=1)
                            imsave(join(Common.PATH, 'results', 'manifold_sampling', sort_method, fname[:-4] +
                                        '_%d_%d.jpg') % sampling_ids[sy], data_show.vals)
                            imsave(join(Common.PATH, 'results', 'manifold_sampling', sort_method, fname[:-4] +
                                        '_pal_%d_%d.jpg') % sampling_ids[sy], pal_img)

                        print sort_method, 'error: ', err
                    if method == "bgplvm":
                        lvm = GPy.plotting.matplot_dep.visualize.lvm(X.copy(), m, data_show, latent_axes=ax, latent_index=which_indices)
                    else:
                        lvm = GPy.plotting.matplot_dep.visualize.lvm(X.copy(), m, data_show, latent_axes=ax, latent_index=which_indices)
                    # raw_input('Press enter to finish')

                plt.show()


def sort_colorize(query_files, sort_methods, artists, n_colors=7, step=1, n_pals_per_artist=5, vis_sorting='none',
                  save_data=False):
    """ Comparing sorting methods by direct colorization

    :param query_files:
    :param sort_methods:
    :param artists:
    :param n_colors:
    :param step:
    :param n_pals_per_artist:
    :param vis_sorting: whether to visualize the sorted palettes
    :return:
    """
    # from smartpalette.ColorTransfer import Colorizer, prepare_colorizer
    # from skimage.restoration import denoise_tv_chambolle, denoise_bilateral
    # from skimage.segmentation import slic
    from skimage.transform import rescale
    from smartpalette.coloropt.Colorizer import ColorOptColorizer

    path = Common.PATH
    query_palettes = []
    image_data = []
    n_queries = len(query_files)

    # for query_file in query_files:
    #     query_palettes.append(palette_from_image(query_file, n_colors))

    # prepare the target images
    source_images = []
    source_palettes = []
    source_image_segmasks = []
    for qx in range(n_queries):
        _im = rescale(img_as_float(imread(query_files[qx])), 0.5)
        _h, _w, _d = _im.shape
        _lab = Common.rgb2nlab(_im)
        source_images.append(_im)
        _km = KMeans(n_clusters=n_colors, n_init=5)
        _labels = _km.fit_predict(_lab.reshape((-1, 3)))
        source_image_segmasks.append(_labels.reshape((_h, _w)))
        source_palettes.append(_km.cluster_centers_.reshape((n_colors, 3)))
    # end

    query_palettes = np.array(source_palettes)
    # n_queries = len(query_palettes)
    data = []
    query_labels = []
    n_trains = len(artists) * n_queries * n_pals_per_artist

    # for each artist, retrieve n_pals_per_artist palettes and use them to colorize the input images later
    for artist in artists:
        datafile = join(path, 'data', artist, 'clusters_nc%d_l%d.npz' % (n_colors, 0))
        tmpdata = np.load(datafile)
        norm_lab = np.squeeze(tmpdata['normalized_clustered_lab'])[::step]
        for qx, query in enumerate(query_palettes):
            dists = Distance.parallel_hausdorff(query, norm_lab, mode=1)
            best_ids = np.argsort(dists)
            data.append(norm_lab[best_ids[0:n_pals_per_artist]])
            query_labels.extend([qx] * n_pals_per_artist)

    if save_data:
        set_name = reduce(lambda x,y: x + '_' + y, artists)
        if not os.path.isdir(join(path, 'data', set_name)):
            os.makedirs(join(path, 'data', set_name))

        set_file_name = join(path, 'data', set_name, 'clusters_nc%d_l%d.npz' % (n_colors, 0))
        np.savez(set_file_name, normalized_clustered_lab=np.vstack(data))
        return

    query_labels = np.array(query_labels)
    all_data = np.vstack(data + [np.array(source_palettes)])
    for sort_method in sort_methods:
        savepath = join(Common.PATH, 'results', 'sorting_comparison', sort_method)
        if not os.path.isdir(join(savepath)):
            os.makedirs(savepath)

        sorted_all_data, all_newindices, _, order, _ = \
            FeatureAlignment.sort_palettes(all_data, n_colors, sort_palettes=True,
                                           n_themes=None, labels=None, step=1,
                                           sorter=sort_method, viz=vis_sorting)

        training_data = sorted_all_data[np.argsort(order)][:n_trains].copy()
        test_data = sorted_all_data[np.argsort(order)][n_trains:].copy()

        # training_indices = all_newindices[np.argsort(order)][:n_trains].copy()
        test_indices = all_newindices[np.argsort(order)][n_trains:].copy()

        # aligner = FeatureAlignment.PaletteAligner(method='data', data=training_data, match_method='munkres',
        #                                           n_examples=3)

        # for each input palette (extracted from an image), colorize the image with the reordered palettes
        for qx in range(len(query_palettes)):
            target_palettes = training_data[query_labels == qx]

            # target_palettes = training_data[query_labels[order] == qx]
            # colorizer, prm = \
            #     prepare_colorizer(query_files[qx], aligner=aligner, n_segments=80, n_colors=n_colors, mode='single')

            for tx, target_palette in enumerate(target_palettes):
                # LLE
                # new_image, lle_params = \
                #     colorizer.colorize_quick(prm['image_nlab'], prm['seg_masks'], prm['seg_mean_colors'],
                #                              prm['seg_color_labels'], prm['orig_palette'],
                #                              target_palette[prm['alignment_ids']], prm['lle_params'], prm['lmda'],
                #                              ann_per=1.0)
                # prm['lle_params'] = lle_params
                # pal = Common.nlab2rgb(target_palette[None, :])
                # END

                # COLOROPT
                ann_per_coloropt = 0.1

                # Find the correct color order for the target palette before optimization
                # _, new_indices = aligner.align(source_palettes[qx][None, ...])
                # target_palette_rgb = Common.nlab2rgb(target_palette[np.argsort(new_indices[0])][None, ...])

                target_palette_rgb = Common.nlab2rgb(target_palette[np.argsort(test_indices[qx])][None, ...])
                colorizer = ColorOptColorizer(5, 5, ann_per=ann_per_coloropt)
                new_image = colorizer.colorize(source_images[qx], source_image_segmasks[qx],
                                               target_palette_rgb)
                # END

                pal_img = render_palettes(Common.nlab2rgb(target_palette[None, ...]))
                imsave(join(savepath, os.path.basename(query_files[qx])[:-4] +
                            '_%d.jpg') % (tx + 1), new_image)
                imsave(join(savepath, os.path.basename(query_files[qx])[:-4] +
                            '_pal_%d.jpg') % (tx + 1), pal_img)

            # Save the original image & palette
            # query_pal_img = render_palettes(Common.nlab2rgb(query_palettes[qx:qx+1]))
            query_pal_img = render_palettes(Common.nlab2rgb(test_data[qx:qx+1].reshape((1, -1, 3))))
            query_img = source_images[qx]
            imsave(join(savepath, os.path.basename(query_files[qx])[:-4] +
                        '_0.jpg'), query_img)

            imsave(join(savepath, os.path.basename(query_files[qx])[:-4] +
                        '_pal_0.jpg') , query_pal_img)


if __name__ == '__main__':
    # from skimage.color import hsv2rgb
    import argparse
    from skimage import img_as_float
    from skimage.io import imread
    from sklearn.cluster import KMeans

    parser = argparse.ArgumentParser(description='Test performance of different palette prediction models')
    parser.add_argument('artists', type=str, nargs='+', default='gogh luce')
    parser.add_argument('--modes', type=str, nargs='+', default=['gplvm_manifold'])
    # ['pairwise', 'binary', 'local', 'lightness', 'none']
    parser.add_argument('--sort_methods', type=str, nargs='+', default=['binary'])
    parser.add_argument('--n_ppc', type=int, default=5) # no. palettes per artist
    parser.add_argument('--n_samples', type=int, default=5)
    parser.add_argument('--n_colors', type=int, default=7)
    args = parser.parse_args()

    '''
    r = np.random.RandomState(None)
    r1 = np.random.RandomState()
    bias = r.rand(5)
    print bias
    queries = Common.rgb2nlab(hsv2rgb(np.array([[[bias[i] * 250, 100., 90 + 100 * r1.rand()] for i in range(5)]]) / 255.))
    #'''

    collection = 'distinctive' # 'distinctive' # 'benchmark_unstylized' # decorative
    query_files = ['image_%d.jpg' % i for i in [2, 3, 5, 6, 7, 9, 10, 14, 16, 19]] # [2, 3, 5, 6, 7, 9, 10, 14, 16, 19]]

    query_files = [join(Common.PATH, 'images', collection, f) for f in query_files]
    # query_file2 = join(Common.PATH, 'images', collection, 'image_17.jpg')
    # query_file1 = join(Common.PATH, 'images', collection, 'image_3.jpg')

    query_file = join(Common.PATH, 'images', collection, 'image_3.jpg')

    # for sorting & colorization with query image
    # for i in range(0, 6, 2):

    # luce prendergast
    sort_colorize(query_files, sort_methods=['binary', 'lightness'], # 'lightness', 'binary',
                  artists=['luce', 'prendergast'], n_colors=15, step=5,
                  n_pals_per_artist=2, save_data=False)

    # for interpolation
    # interpolate_palettes(args.artists, queries=query_file, n_colors=args.n_colors, viz=True,
    # sort_methods=args.sort_methods,n_samples=args.n_samples, n_pals_per_artist=args.n_ppc,
    # modes=args.modes, sampling=True, viz_sorting='none')

    # test_classification(args.artists, query_file, n_reps=20, step=1, n_colors=10, train_ratio=0.6,
    #                     n_missings=8, n_pals_per_artist=20, sort_methods=['binary', 'lightness'])luc

    # luce, monet, guillaumin, prendergast, urbino, gogh --> 7
    # artists = ['gogh', 'cezanne', 'luce']
    # generate_user_study(artists, query_file=query_file, n_colors=7, step=1, sort_methods=['binary', 'lightness',
    # 'none'], viz=False, n_trains=9, n_samples=3, seed=12345678)
