import numpy as np
from matplotlib import pyplot as plt
from skimage import draw
from sklearn.metrics import mean_squared_error


import sys

sys.path.append('/Users/phanquochuy/Projects/smartpalette')

from smartpalette.fast.Distance import hausdorff
from smartpalette.Common import rgb2nlab, nlab2rgb

def render_palettes(palettes, box_size=50, vgap=0, hgap=5, shape='square', background='black'):
    n_pals, n_cols, n_chas = palettes.shape
    iw, ih = n_pals * (box_size + hgap), n_cols * (box_size + vgap)
    if background == 'black':
        img = np.zeros((iw, ih, n_chas))
    elif background == 'white':
        img = np.ones((iw, ih, n_chas))
    for px in range(n_pals):
        for cx in range(n_cols):
            if shape == 'square':
                img[px * (box_size + hgap): px * (box_size + hgap) + box_size,
                cx * (box_size + vgap): cx * (box_size + vgap) + box_size] = palettes[px, cx]
            elif shape == 'circle':
                rr, cc = draw.ellipse(px * (box_size + hgap) + box_size / 2, cx * (box_size + vgap) + box_size / 2,  box_size / 2,  box_size / 2, shape = img.shape)
                img[rr, cc] = palettes[px, cx]
    # img = np.swapaxes(img, 0, 1)
    return img
    
vec = np.array([0.5, 0.0, 0.0])
vec /= (np.linalg.norm(vec) * 20)

print(np.linalg.norm(vec))
init_color = np.array([0.3, 0.8, 0.3])

pal = np.array([[list(init_color + vec * i) for i in range(-4, 5, 1)]])
plt.imshow(render_palettes(nlab2rgb(pal)), interpolation='nearest')
plt.axis('off')
plt.show()

# pal_a = np.array([[[0.5, 0.2, 0.4],[0.5, 0.7, 0.1],[0.5, 0.2, 0.2],[0.1, 0.2, 0.5], [0.7, 0.7, 0.1]]])
# pal_b = pal_a.copy()
# pal_b[:, 0, :] += np.random.rand(3) * 0.1
# 
# # print( mean_squared_error(pal_a.flatten(), pal_b.flatten()) )
# print( hausdorff(rgb2nlab(pal_a)[0], rgb2nlab(pal_b)[0],mode=1) )
# 
# ax = plt.subplot(211)
# ax.imshow(render_palettes(pal_a), interpolation='nearest')
# 
# ax = plt.subplot(212)
# ax.imshow(render_palettes(pal_b), interpolation='nearest')
# 
# plt.show()