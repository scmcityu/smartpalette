import numpy as np
from os.path import join
from matplotlib import pyplot as plt
from sklearn.metrics import mean_squared_error
from smartpalette.FeatureAlignment import sort_palettes
from smartpalette.Common import PATH
from scipy.spatial.distance import euclidean as euc


def _average_color_dist(pal1, pal2):
    assert(len(pal1) == len(pal2))
    d = np.mean([euc(pal1[cx], pal2[cx])for cx in range(len(pal1))])
    return d


def average_consecutive_dist(data, neighbor=2):
    n_pals = len(data)
    ds = []
    for i in range(neighbor, n_pals - neighbor, 1):
        _d = 0.
        for j in range(-neighbor, neighbor + 1):
            if j != 0 :
                # _d += mean_squared_error(data[i], data[i + j])
                _d += _average_color_dist(data[i], data[i + j])
        ds.append(_d / (neighbor * 2.))
    return np.mean(ds)


def test_vary_palette_size(collections, palette_sizes, n_palettes=40, max_palette_size=20,
                           n_trials=10, sort_methods=['binary'], random_state=None, ax=None):
    rs = np.random.RandomState(random_state)
    n_colls = len(collections)

    # fig = plt.figure('vary palette size')
    f, axes = plt.subplots(2, 1, sharex=True)
    axes = [axes[0], axes[1], axes[1]]
    colors = ['r', 'g', 'b', 'y']
    for sx, sort_method in enumerate(sort_methods):
        print(sort_method)
        results = {}
        for ps in palette_sizes:
            results[ps] = np.zeros((n_colls))

        for cx, collection in enumerate(collections):
            datafile = join(PATH, 'data', collection, 'clusters_nc%d_l%d.npz' % (max_palette_size, 0))
            tmpdata = np.load(datafile)
            n_all_palettes = len(tmpdata['normalized_clustered_lab'])
            norm_lab = np.squeeze(tmpdata['normalized_clustered_lab'])[::n_all_palettes / n_palettes]

            for px, ps in enumerate(palette_sizes):
                d = 0.
                for tx in range(n_trials):
                    perm = rs.permutation(max_palette_size)[:ps]
                    test_data = norm_lab[:, list(perm)]
                    sorted_test_data, test_data_indices, _, order, _ = sort_palettes(test_data, ps, sort_palettes=True,
                                                                                     n_themes=None, labels=None, step=1,
                                                                                     sorter=sort_method, viz='none')
                    d += average_consecutive_dist(sorted_test_data)

                results[ps][cx] = d / float(n_trials)

        # print(results)
        for ps in palette_sizes:
            results[ps] = np.mean(results[ps])

        print(results)

        axes[sx].plot(palette_sizes, [results[k] for k in palette_sizes], c=colors[sx])

    # axes[1].set_ylim(0.175, 0.2)
    # axes[0].set_ylim(0.09, 0.125)
    axes[0].spines['bottom'].set_visible(False)
    axes[1].spines['top'].set_visible(False)
    axes[0].xaxis.tick_top()
    axes[1].tick_params(labeltop='off')  # don't put tick labels at the top
    axes[1].xaxis.tick_bottom()

    plt.show()

if __name__ == '__main__':
    test_vary_palette_size(['chase', 'gas', 'renoir', 'luce', 'prendergast', 'gogh', 'guillaumin', 'monet'],
                           palette_sizes=[5, 10, 15, 20, 25, 30, 35, 40], n_palettes=20, max_palette_size=40,
                           n_trials=5, sort_methods=['hue', 'binary', 'lightness'], random_state=None)
    exit()
