"""
Sample command

 ~/anaconda/bin/python experiments/test_interpolations_2.py prendergast test_predictive --sort_methods binary lightness
 --training_methods gmm_2 --missing_ratio 0.8  --n_trials 20 --vis_sorting none --vis 0
"""

from smartpalette import ColorManifolds, Common, FeatureAlignment, KernelKMeans
from smartpalette.fast.Distance import hausdorff, parallel_hausdorff
import numpy as np
from matplotlib import pyplot as plt
import matplotlib as mpl
from os.path import join
import GPy
from sklearn.metrics import mean_squared_error, mean_absolute_error
from sklearn.svm import SVR
from sklearn.linear_model import Lasso
from scipy.spatial.distance import euclidean as euc
from sklearn.mixture import GMM, DPGMM
from smartpalette.Regression import gmm_predict
from smartpalette.Kernels import hausdorff_kernel_matrix
from scipy.io import loadmat


def find_best_params_gmm(Xtrain, n_components_range=None, cv_types=None, criterion='aic'):

    lowest_bic = np.infty
    bic = []
    if n_components_range is None:
        n_components_range = np.arange(20, 60, 2)

    if cv_types is None:
        cv_types = ['diag', 'full'] #'spherical', 'tied',

    best_gmm = None
    for cv_type in cv_types:
        # print 'cv_type ', cv_type
        for n_components in n_components_range:
            # print '.',
            if n_components < Xtrain.shape[0] / 2:
                # Fit a mixture of Gaussians with EM
                gmm = GMM(n_components=n_components, covariance_type=cv_type)
                gmm.fit(Xtrain)
                if criterion == 'bic':
                    bic.append(gmm.bic(Xtrain))
                else:
                    bic.append(gmm.aic(Xtrain))

                if bic[-1] < lowest_bic:
                    lowest_bic = bic[-1]
                    best_gmm = gmm

    print 'best gmm ', best_gmm.n_components, best_gmm.covariance_type
    if best_gmm is None:
        return gmm
    else:
        return best_gmm


def test_lightness_vs_spf(collection, methods, sort_methods, n_colors, level, n_trials=1, missing_ratio=0.5,
                          training_ratio=0.2, step=100, viz=True, test='test_rating', viz_sorting='none',
                          metric='mhd', max_n_trains=1000, random_state=None):
    path = Common.PATH
    import os

    # prepare to load matlab code
    from oct2py import octave
    codeRoot = '/Users/phanquochuy/Projects/smartpalette/lib/ColorCompatibility/colorCode/'
    # octave.addpath(codeRoot)
    # octave.addpath(join(codeRoot, 'data'))
    # octave.addpath(join(codeRoot, 'circstat'))
    # octave.addpath(join(codeRoot, 'glmnet_matlab'))

    from sklearn.preprocessing import MinMaxScaler
    mms = MinMaxScaler()
    datafile = join(path, 'data', collection, 'clusters_nc%d_l%d.npz' % (n_colors, level))
    # datafile = join(path, 'data', collection, 'dpaintings%d.npz' % (n_colors))
    data = np.load(datafile)
    # sort_methods =  #['tsp', 'lightness', 'pca'] #pca none lightness tsp
    # orig_lab = np.squeeze(data['clustered_lab'])[::step]
    norm_lab = np.squeeze(data['normalized_clustered_lab'])[::step]
    orig_lab = norm_lab

    n_pals, n_cols, n_chas = orig_lab.shape
    n_trains = min(int(n_pals * training_ratio), max_n_trains)
    n_tests = n_pals - n_trains
    n_missings = int(n_colors * missing_ratio)

    dists = {}
    for m in methods:
        dists[m] = {}
        for sm in sort_methods:
            dists[m][sm] = np.zeros(n_trials)

    if test == 'test_rating':
        if 'compat_features' in data:
            compat_features = data['compat_features'][::step]
        else:
            rgb = Common.nlab2rgb(orig_lab)
            compat_features = octave.extract_compat_features(rgb)['features']

        svrfile = join(path, 'data', 'turk', 'svr.npy')
        matfile = join(path, 'data', 'turk', 'themesData.mat')
        mat = loadmat(matfile)
        mms.min_ = mat['datapoints'][0,0]['offsets']
        mms.scale_ = mat['datapoints'][0,0]['scales']

        if (compat_features < 0).any() or (compat_features > 1).any():
            compat_features = np.clip(mms.transform(compat_features), 0, 1)

        if os.path.isfile(svrfile):
            svr = np.load(svrfile).tolist()
        else:
            # svr = SVR(kernel='rbf', C=0.05, gamma=4, verbose=True)
            svr = Lasso(alpha=0.00016, tol=1e-4, copy_X=False)
            turk_compat_features = mat['datapoints'][0,0]['features']
            ratings = np.squeeze(mat['datapoints'][0,0]['targets'])
            pred_ratings = svr.fit(turk_compat_features, ratings).predict(turk_compat_features)
            np.save(svrfile, svr)

            print 'turk rating prediction: ', mean_squared_error(ratings, pred_ratings)
            # svr.fit(data['clustered_lab'][level].reshape((-1, n_colors * n_chas)), data['rating'][level].ravel())

    rs = np.random.RandomState(random_state)
    for tr in range(n_trials):
        perms = rs.permutation(n_pals)
        c_perms = np.array([np.random.permutation(n_colors) for _ in range(n_pals)])
        testids = perms[n_trains:n_trains + n_tests]
        trainids = perms[:n_trains]

        # orig_lab, _, labels, order, _ = FeatureAlignment.sort_palettes(orig_lab, n_colors, sort_palettes=True,
        # n_themes=None, labels=None, step=1, sorter=sort_method)
        # tmpdata, order = orig_lab, np.arange(n_pals)

        count = 3
        if viz:
            n_plots = len(methods) * len(sort_methods) * 3 + 2
            plt.subplot(1, n_plots, 1)
            true_rgb = Common.nlab2rgb(orig_lab)
            plt.imshow(true_rgb[testids][:20], interpolation='nearest')

            plt.subplot(1, n_plots, 2)
            true_rgb[np.arange(len(true_rgb))[:, None], c_perms[:, :n_missings]] = 0
            plt.imshow(true_rgb[testids][:20], interpolation='nearest')

        for method in methods:
            print 'testing ', method
            for sort_method in sort_methods:
                # rearrange color palettes
                if test != 'test_rating':
                    tmplab, tmpindices, labels, order, _ = \
                        FeatureAlignment.sort_palettes(orig_lab, n_colors, sort_palettes=sort_method in ['binary', 'lightness', 'hue'],
                                                       n_themes=None, labels=None, step=1, sorter=sort_method, viz=viz_sorting)
                    tmplab = tmplab[np.argsort(order)]
                    tmpindices = tmpindices[np.argsort(order)]

                    training_data = tmplab[trainids].copy()
                    testing_data = tmplab[testids].copy()
                else:
                    training_data = orig_lab[trainids].copy()
                    testing_data = orig_lab[testids].copy()

                # training_data, _, _, _, _ =\
                #     FeatureAlignment.sort_palettes(training_data, n_colors,
                #                                    sort_palettes=sort_method in ['binary', 'tsp'], n_themes=None,
                #                                    labels=None, step=1, sorter=sort_method, viz=viz_sorting)

                if test == 'test_predictive' or test == 'test_sampling':
                    # train the model
                    if method == 'gplvm':
                        Q = 4
                        kernel = GPy.kern.RBF(Q, ARD=True) + GPy.kern.Bias(Q)
                        # kernel = GPy.kern.Linear(Q) + GPy.kern.White(Q, 0.0001)
                        m = GPy.models.GPLVM(training_data.reshape((-1, n_colors * n_chas)), Q, kernel=None)
                        m.optimize('lbfgs', messages=True, max_iters=1800, gtol=1e-4)
                    elif method == 'bgplvm':
                        Q = 4
                        kernel = GPy.kern.RBF(Q, 1., 1. / np.random.uniform(0, 1, (Q,)), ARD=True)
                        # + GPy.kern.Bias(Q, np.exp(-2))
                        # kernel = GPy.kern.RBF(Q, ARD = False) #+ GPy.kern.White(Q, 0.0001)
                        # kernel = GPy.kern.Linear(Q) # + GPy.kern.Bias(Q) + GPy.kern.White(Q, 0.00001)

                        m = GPy.models.BayesianGPLVM(training_data.reshape((-1, n_colors * n_chas)), Q, num_inducing=8,
                                                     kernel=kernel)
                        m.optimize('bfgs', messages=True, max_iters=2000, gtol=1e-4)
                    elif 'gmm' in method:
                        if method == 'gmm_1':
                            kernel_ = hausdorff_kernel_matrix(training_data)
                            _, _, labels = FeatureAlignment.find_best_n_clusters(kernel_)
                            unique_labels = np.unique(labels)
                            n_clusters = len(unique_labels)
                            m = GMM(n_components=n_clusters, covariance_type='diag', init_params='c', params='cw')
                            m.weights_ = np.array([(labels==l).sum() for l in unique_labels]) / float(n_trains)
                            m.means_ = np.array([training_data[labels==l].mean(axis=0) for l in unique_labels]).reshape((-1,n_colors * n_chas))
                            m.fit(training_data.reshape((-1, n_colors * n_chas)))
                        if method == 'gmm_2':
                            # m = GMM(n_components=1)
                            # m = DPGMM(alpha=5, covariance_type='full')
                            m = find_best_params_gmm(training_data.reshape((-1, n_colors * n_chas)), cv_types=['diag'],
                                                     n_components_range=range(2, 40, 4), criterion='aic')
                            m.fit(training_data.reshape((-1, n_colors * n_chas)))

                        # '''
                        # labels = m.predict(training_data.reshape((-1, n_colors * n_chas)))
                        # for i in range(m.n_components):
                        #     plt.subplot(1, m.n_components, i + 1)
                        #     plt.imshow(Common.nlab2rgb(training_data[labels == i]), interpolation='nearest')
                        # plt.show()
                        # #'''

                if test == 'test_predictive':
                    tmp_c_perms = c_perms[testids].copy()
                    for cx, cp in enumerate(tmp_c_perms):
                        for ix in cp[:n_missings]:
                            ixx = list(tmpindices[testids][cx]).index(ix)
                            testing_data[cx][ixx] = np.nan

                    # tmp_c_perms = tmp_c_perms[np.arange(n_tests)[:, None], tmpindices[testids]]
                    # testing_data[np.arange(n_tests)[:, None], tmp_c_perms[:, :n_missings]] = np.nan

                    # aligned_testing_data, aligned_testing_indices = \
                    #     FeatureAlignment.align_colors_naive(testing_data, training_data.copy(),
                    #                                         clustering=0, dupl=True, match_method='munkres',
                    #                                         n_examples=10)

                    aligned_testing_data = testing_data
                    aligned_testing_indices = np.tile(range(n_colors), n_tests).reshape((n_tests, n_colors))

                    pred_data = aligned_testing_data.copy()
                    # test predictive performance
                    for fx, feat in enumerate(aligned_testing_data):
                        tfeat = feat.ravel()

                        if method == 'bgplvm':
                            xnew, newm = m.infer_newX(tfeat[None, :], optimize=True)
                            new_palette, _ = m.predict(np.array(xnew.mean), full_cov=True)
                        elif method == 'gplvm':
                            xnew, newm = m.infer_newX(tfeat[None, :], optimize=True)
                            new_palette, _ = m.predict(np.array(xnew))
                        elif 'gmm' in method:
                            new_palette = gmm_predict(tfeat[None, :], m)
                        elif method == 'retrieval':
                            mask = np.bitwise_not(np.isnan(feat[:, 0]))
                            min_d, best_tpal = 1e10, 0
                            for tpal in range(training_data.shape[0]):
                                d = euc(testing_data[fx][mask].ravel(), training_data[tpal][mask].ravel())
                                if d < min_d:
                                    min_d = d
                                    best_tpal = tpal

                            new_palette = [training_data[best_tpal].reshape((n_cols, n_chas))]

                        pred_data[fx, :] = new_palette[0].reshape((-1, n_chas))

                    # bring pred_data back to the old order
                    # reversed_indices = np.argsort(aligned_testing_indices, axis=1)
                    # pred_data = pred_data[np.arange(len(testids))[:, None], reversed_indices]
                    # pred_data[np.arange(n_tests)[:, None], tmp_c_perms[:, n_missings:]] = \
                    #     testing_data[np.arange(n_tests)[:, None], tmp_c_perms[:, n_missings:]]

                    if metric == 'compat':
                        pred_data = Common.nlab2rgb(pred_data)
                        pred_features = np.clip(mms.transform(octave.extract_compat_features(pred_data)['features']), 0, 1)
                        total_dist = mean_squared_error(compat_features[testids], pred_features)
                        # total_dist = mean_absolute_error(svr.predict(compat_features[order][testids]), \
                        # svr.predict(pred_features))
                        dists[method][sort_method][tr] = total_dist
                    elif metric == 'mhd':
                        total_dist = 0
                        for i, y_pred, y_test in zip(range(n_tests), pred_data, tmplab[testids]):
                            # newperm = [list(newindices[testids[i]]).index(c) for c in c_perms[testids[i]][:n_missings]]
                            # missing_ids = c_perms[testids][i][:n_missings]

                            missing_ids = np.where(np.isnan(testing_data[i][:, 0]))[0]
                            # for mid in missing_ids:
                            #     total_dist += euc(y_pred[mid].ravel() ,y_test[mid].ravel())
                            total_dist += mean_absolute_error(y_test[missing_ids].ravel(), y_pred[missing_ids].ravel())
                            # total_dist += hausdorff(y_pred[missing_ids], y_test[missing_ids], mode=1)

                        print method, sort_method, total_dist / float(n_tests)
                        dists[method][sort_method][tr] = total_dist / float(n_tests)
                    if viz:
                        plt.subplot(1, n_plots, count, title=method + sort_method)
                        # aligned_testing_data[np.isnan(aligned_testing_data)] = 0
                        # plt.imshow(Common.nlab2rgb(aligned_testing_data.reshape((-1, n_cols, n_chas))),
                        # interpolation='nearest')
                        # plt.imshow(pred_data.reshape((-1, n_cols, n_chas)), interpolation='nearest')
                        plt.imshow(Common.nlab2rgb(pred_data.reshape((-1, n_cols, n_chas)))[:20], interpolation='nearest')

                        count += 1
                        plt.subplot(1, n_plots, count, title='truth')
                        plt.imshow(Common.nlab2rgb(tmplab[testids][:20].reshape((-1, n_cols, n_chas))), interpolation='nearest')

                        count += 1
                        plt.subplot(1, n_plots, count, title='input')
                        testing_data[np.isnan(testing_data)] = 0
                        plt.imshow(Common.nlab2rgb(testing_data[:20].reshape((-1, n_cols, n_chas))), interpolation='nearest')
                        # plt.imshow(Common.nlab2rgb(training_data.reshape((-1, n_cols, n_chas))),
                        # interpolation='nearest')

                elif test == 'test_sampling':
                    samples = m.sample(100)
                    samples = samples.reshape((-1, n_cols, n_chas))
                    # parallel_hausdorff(samples, testing_data)
                    for td in testing_data:
                        dists[method][sort_method][tr] \
                            += np.min([euc(td.ravel(), s.ravel()) for s in samples])
                    dists[method][sort_method][tr] /= n_tests

                elif test == 'pwd':
                    total_dist = 0
                    # for k in range(n_colors):
                    #     for i in range(n_trains):
                    #         for j in range(i + 1):
                    #             total_dist += euc(training_data[i, k, :], training_data[j, k, :])
                    for i in range(n_trains - 1):
                        # for j in range(i + 1):
                        total_dist += euc(training_data[i].ravel(), training_data[i + 1].ravel())
                    dists[method][sort_method][tr] = total_dist

                elif test == 'test_rating':
                    import itertools
                    training_data, _, _, _, _ = \
                        FeatureAlignment.sort_palettes(training_data, n_colors, sort_palettes=False,
                                                       n_themes=None, labels=None, step=1, sorter=sort_method, viz=viz_sorting)
                    n_samples = 5

                    pairs = list(itertools.combinations(range(n_trains), 2))
                    ratings = {}
                    interpolalions = {}

                    it = 1
                    for i, j in pairs:
                        ratings[(i, j)] = np.zeros(n_samples, dtype=float)
                        interpolalions[(i, j)] = np.zeros((n_samples, n_cols, n_chas))
                        vec = training_data[j].ravel() - training_data[i].ravel()
                        vec /= float(n_samples)
                        move = training_data[i].ravel().copy()

                        # interpolalions[(i, j)][0] = training_data[i].copy()
                        # interpolalions[(i, j)][-1] = training_data[j].copy()
                        for k in range(n_samples):
                            interpolalions[(i, j)][k] = move.reshape((-1, n_chas)).copy()
                            # ratings[(i, j)][k] = svr.predict(move)
                            move += vec

                    for i, j in pairs:
                        palettes = interpolalions[(i, j)][1:-1].reshape((-1, n_cols, n_chas))
                        for pal in palettes:
                            min_dist = 1e10
                            for tpal in testing_data:
                                d = hausdorff(pal, tpal, mode=1)
                                if d < min_dist:
                                    min_dist = d
                            dists[method][sort_method][tr] += min_dist

                    dists[method][sort_method][tr] /= float(len(pairs) * n_samples)

                    if viz:
                        for i, j in pairs:
                            rgb = np.clip( Common.nlab2rgb(interpolalions[(i,j)]), 0, 1)
                            ratings[(i, j)] = svr.predict(np.clip( mms.transform(octave.extract_compat_features(rgb)['features']), 0, 1))

                            plt.subplot(len(pairs), 2, it, title=str((i , j)))
                            plt.axis('off')
                            plt.imshow(Common.nlab2rgb(interpolalions[(i, j)]), interpolation='nearest')
                            it += 1
                            plt.subplot(len(pairs), 2, it)
                            plt.axis('off')
                            plt.imshow(ratings[(i, j)][None, :], interpolation='nearest')

                            for x in range(0, n_samples):
                                plt.text(x, 0, "{:10.1f}".format(ratings[(i, j)][x]), va='center', ha='center')

                            it += 1

                        plt.show()

                count += 1
        if viz:
            plt.show()

    for m in methods:
        for sm in sort_methods:
            print m, sm, dists[m][sm], dists[m][sm].mean()


def test_lightness_vs_spf_with_kkm(collection, method, n_colors, level, n_trials=1, missing_ratio=0.5,
                          training_ratio=0.2, step=100, viz=True, test='test_rating'):
    path = Common.PATH
    import os

    # prepare to load matlab code
    from oct2py import octave
    from sklearn.preprocessing import MinMaxScaler
    from smartpalette.FeatureAlignment import align_colors_naive

    codeRoot = '/Users/phanquochuy/Projects/smartpalette/lib/ColorCompatibility/colorCode/'
    octave.addpath(codeRoot)
    octave.addpath(join(codeRoot, 'data'))
    octave.addpath(join(codeRoot, 'circstat'))
    octave.addpath(join(codeRoot, 'glmnet_matlab'))
    # Prepare for rating prediction
    matfile = join(path, 'data', 'turk', 'themesData.mat')
    mat = loadmat(matfile)
    mms = MinMaxScaler()
    mms.min_ = mat['datapoints'][0, 0]['offsets']
    mms.scale_ = mat['datapoints'][0, 0]['scales']

    datafile = join(path, 'data', collection, 'clusters_nc%d_l%d.npz' % (n_colors, level))
    # datafile = join(path, 'data', collection, 'dpaintings%d.npz' % (n_colors))
    data = np.load(datafile)
    sort_methods = ['none', 'tsp', 'lightness'] #['tsp', 'lightness', 'pca'] #pca none lightness tsp
    norm_lab = np.squeeze(data['normalized_clustered_lab'])[::step]

    kernel_ = hausdorff_kernel_matrix(norm_lab)
    kkm, n_clusters, labels = KernelKMeans.find_best_n_clusters(kernel_)
    cluster_centers = kkm.centers_

    orig_lab = norm_lab
    n_pals, n_cols, n_chas = orig_lab.shape
    n_trains = len(cluster_centers)
    n_tests = n_pals - n_trains
    n_missings = (n_colors * missing_ratio)

    # randomize missing colors
    c_perms = np.array([np.random.permutation(n_colors) for _ in range(n_tests)])
    testids = np.array([i for i in range(n_pals) if i not in cluster_centers])
    trainids = cluster_centers
    dists = {}
    for sm in sort_methods:
        dists[sm] = 0

    if 'compat_features' in data:
        compat_features = data['compat_features'][::step]
    else:
        rgb = Common.nlab2rgb(orig_lab)
        compat_features = octave.extract_compat_features(rgb)['features']

    if (compat_features < 0).any() or (compat_features > 1).any():
        compat_features = np.clip(mms.transform(compat_features), 0, 1)

    count = 3
    if viz:
        n_plots = len(sort_methods) + 2
        plt.subplot(1, n_plots, 1)
        true_rgb = Common.nlab2rgb(orig_lab)[testids]
        plt.imshow(true_rgb[:20], interpolation='nearest')

        plt.subplot(1, n_plots, 2)
        true_rgb = true_rgb.copy()
        true_rgb[np.arange(len(true_rgb))[:, None], c_perms[:, :n_missings]] = 0
        plt.imshow(true_rgb[:20], interpolation='nearest')

    for sort_method in sort_methods:
        training_data = orig_lab[trainids].copy()
        testing_data = orig_lab[testids].copy()
        # rearrange color palettes
        new_training_data, new_training_indices, _, new_training_order, _ =\
            FeatureAlignment.sort_palettes(training_data, n_colors, sort_palettes=(sort_method == 'tsp'), n_themes=None,
                                           labels=None, step=1, sorter=sort_method, viz='none')

        testing_data[np.arange(n_tests)[:, None], c_perms[:, :n_missings]] = np.nan
        new_testing_data, new_testing_indices = align_colors_naive(testing_data, new_training_data,dupl=False)
        pred_data = testing_data.copy()

        # train the model
        if method == 'gplvm':
            Q = 4
            kernel = GPy.kern.RBF(Q, ARD=True) + GPy.kern.Bias(Q)
            m = GPy.models.GPLVM(new_training_data.reshape((-1, n_colors * n_chas)), Q, kernel=kernel)
            m.optimize('scg', messages=True, max_iters=2000, gtol=1e-4)
        elif method == 'bgplvm':
            Q = 4
            kernel = GPy.kern.RBF(Q, 1., 1. / np.random.uniform(0, 1, (Q,)), ARD=True) + GPy.kern.Bias(Q, np.exp(-2))
            m = GPy.models.BayesianGPLVM(new_training_data.reshape((-1, n_colors * n_chas)), Q, kernel=kernel, num_inducing=40)
            m.optimize('bfgs', messages=True, max_iters=2000, gtol=1e-4)
        elif 'gmm' in method:
            if method == 'gmm_2':
                # m = GMM(n_components=16)
                m = find_best_params_gmm(new_training_data.reshape((-1, n_colors * n_chas)), n_components_range=range(1, n_trains, 4))
                m.fit(training_data.reshape((-1, n_colors * n_chas)))

        # test predictive performance
        for fx, feat in enumerate(new_testing_data):
            tfeat = feat.ravel()
            if method == 'bgplvm':
                xnew, newm = m.infer_newX(tfeat[None, :], optimize=True)
                new_palette, _ = m.predict(np.array(xnew.mean), full_cov=True)
            elif method == 'gplvm':
                xnew, newm = m.infer_newX(tfeat[None, :], optimize=True)
                new_palette, _ = m.predict(np.array(xnew))
            elif 'gmm' in method:
                new_palette = gmm_predict(tfeat[None, :], m)

            pred_data[fx, :] = new_palette[0].reshape((-1, n_chas))

        # bring pred_data back to the old order
        reversed_indices = np.argsort(new_testing_indices, axis=1)
        pred_data = pred_data[np.arange(n_tests)[:, None], reversed_indices]
        pred_data[np.arange(n_tests)[:, None], c_perms[:, n_missings:]] = \
            testing_data[np.arange(n_tests)[:, None], c_perms[:, n_missings:]]

        pred_data = Common.nlab2rgb(pred_data)
        pred_features = np.clip(mms.transform(octave.extract_compat_features(pred_data)['features']), 0, 1)
        total_dist = mean_squared_error(compat_features[testids], pred_features)
        # total_dist = mean_absolute_error(svr.predict(compat_features[order][testids]), svr.predict(pred_features))
        dists[sort_method] = total_dist

        # samples = m.sample(100)
        # samples = samples.reshape((-1, n_cols, n_chas))
        # for td in orig_lab[testids]:
        #     dists[sort_method] += np.min([hausdorff(td, s, mode=1) for s in samples])
        # dists[sort_method] /= n_tests

        if viz:
            plt.subplot(1, n_plots, count, title=sort_method)
            plt.imshow(pred_data[:20].reshape((-1, n_cols, n_chas)), interpolation='nearest')

        count += 1

    if viz:
        plt.show()

    for sm in sort_methods:
        print sm, dists[sm]


if __name__ == "__main__":
    # 0.0082 0.0077
    import sys
    import argparse
    parser = argparse.ArgumentParser(description='Test performance of different palette prediction models')
    parser.add_argument('artist', type=str, default='gogh')
    parser.add_argument('test', type=str, default='test_predictive')
    parser.add_argument('--sort_methods', type=str, nargs='*', default=['binary'])
    parser.add_argument('--training_ratio', type=float, help='training percents', default=0.6)
    parser.add_argument('--missing_ratio', type=float, help='number of missing colors', default=0.8)
    parser.add_argument('--training_methods', type=str, nargs='*', default=['gmm_2'])
    parser.add_argument('--vis_sorting', type=str, default='none')
    parser.add_argument('--vis', type=int, default=0)
    parser.add_argument('--n_trials', type=int, default=5)
    parser.add_argument('--max_n_trains', type=int, default=1000)
    parser.add_argument('--n_colors', type=int, default=10)
    parser.add_argument('--step', type =int, default=1)
    args = parser.parse_args()

    test_lightness_vs_spf(args.artist, methods=args.training_methods, sort_methods=args.sort_methods,
                          n_colors=args.n_colors, level=0, n_trials=args.n_trials, missing_ratio=args.missing_ratio,
                          training_ratio=args.training_ratio, step=args.step, viz=bool(args.vis), test=args.test,
                          viz_sorting=args.vis_sorting, metric='mhd', max_n_trains=args.max_n_trains, random_state=None)

    # test_lightness_vs_spf_with_kkm('pissarro', method='gmm_2', n_colors=5, level=0, n_trials=4, missing_ratio=0.4,
    #                       training_ratio=0.5, step=1, viz=True, test='test_predictive')


