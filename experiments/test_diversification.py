import matplotlib
#matplotlib.use('pdf')
from smartpalette.ColorDiversifier import interpolate, find_variations, sample_gmm
from smartpalette import Common
from smartpalette.coloropt import Colorizer
import numpy as np
from os.path import join
from smartpalette import HGMM
from scipy.spatial.distance import  euclidean as euc

from skimage.io import imread
from skimage import img_as_float
from Distances import ChiSquareDistance, ChiSquareBRD, NormalizedCorrelation, BinRatioDistance, EuclideanDistance
#import cv2
from pyemd import emd
import os
from matplotlib import pyplot as plt
from sklearn.mixture import GMM
from skimage.draw import ellipse
from skimage.transform import rescale


def mark_image(img, colors, yy, xx, sizes):
    h, w, d = img.shape
    marked_img = img.copy()

    if isinstance(sizes, int):
        sizes = [sizes] * len(colors)

    for cx, color in enumerate(colors):
        rr, cc = ellipse(yy[cx], xx[cx], sizes[cx], sizes[cx], (h, w))
        marked_img[rr, cc] = color

    return marked_img


def prepare_dist_mat(data, steps, n_bins=200):
    from sklearn.metrics import pairwise
    from sklearn.cluster import KMeans
    km = KMeans(n_clusters=n_bins, verbose=True, n_jobs=1, n_init=3)
    km.fit(data[::steps])
    mat = pairwise.pairwise_distances(km.cluster_centers_, km.cluster_centers_, metric='euclidean')
    return mat, km


def plot_histograms(artist, methods, img_range=(0, 20)):
    path = Common.PATH
    data_path = join(path, 'data', artist)
    result_path = join(path, 'results', 'shade_reconstruction', artist)
    distmat = np.load(join(data_path, 'distmat.npz'))
    km = distmat['km'].tolist()

    fig = plt.figure(artist + '_' + str(img_range[0]) + '_' + str(img_range[1]))
    fig.set_tight_layout(True)
    n_imgs = img_range[1] - img_range[0]
    n_methods = len(methods)
    c = 0
    for i in range(img_range[0], img_range[1]):
        for method in methods:
            if method == 'original':
                shaded = imread(join(result_path,  'original_' + str(i) + '.jpg'))
            else:
                shaded = imread(join(result_path, method + '_shaded_' + str(i) + '.jpg'))
            lbls = km.predict(Common.rgb2nlab(shaded).reshape((-1, 3)))
            ax = fig.add_subplot(n_imgs, n_methods * 2, c + 1, title=method)
            ax.axis('off')
            ax.imshow(shaded, interpolation='nearest')
            ax = fig.add_subplot(n_imgs, n_methods * 2, c + 2)
            ax.axis('off')
            ax.hist(lbls, bins=range(0, km.n_clusters + 1))

            c += 2

    #plt.tight_layout()
    plt.show()


def test_shade_reconstruction(artist, inputfiles, method='riemann', n_training_files=20, n_coms=80,
                              annotation_ratio=0.1, n_sample_colors=40, use_cache=False, viz=False):
    """

    :param artist:
    :param inputfiles:
    :param method: riemann, random
    :param n_training_files:
    :param n_coms:
    :param annotation_ratio:
    :return:
    """
    from skimage.color import rgb2gray, gray2rgb
    from skimage.io import imsave
    path = Common.PATH

    image_path = join(path, 'images', artist)
    output_path = join(path, 'results', 'shade_reconstruction', artist)
    if not os.path.isdir(output_path):
        os.makedirs(output_path)

    data = np.load(join(path, 'data', artist, 'hgmm.npz'))
    segments = data['segments']
    params = data['params'].tolist()
    images = data['images'].tolist()[0]
    files = list(data['fnames'])
    covars = params['covars'].reshape((-1, 3, 3))
    means = params['means'].reshape((-1, 3))
    weights = params['weights'].ravel()

    labels = np.unique(segments[0])
    n_labels = len(labels)
    n_files = len(files)
    n_segments = n_labels * n_files
    n_test_files = len(inputfiles)

    f_test_ids = [files.index(ifile) for ifile in inputfiles]
    f_train_ids = [i for i in range(n_files) if i not in f_test_ids][:n_training_files]

    img_ids = np.repeat(range(n_files), n_labels)
    seg_ids = np.tile(range(n_labels), n_files)
    train_ids = np.concatenate([np.where(img_ids == fi)[0] for fi in f_train_ids])

    if method in ['riemann', 'euclid', 'closest_mean']:
        #print 'training HGMM, n_components=', n_coms, ',data_size=', len(train_ids), ',n_training_files', n_training_files
        hgmm_cache = join(output_path, 'hgmm.npz')
        if os.path.isfile(hgmm_cache) and use_cache:
            print 'using cached HGMM-----'
            hgmm = np.load(hgmm_cache)['hgmm'].tolist()
        else:
            hgmm = HGMM.HGMM(n_components=n_coms, covariance_type='full')
            hgmm.fit([weights[train_ids] / float(len(f_train_ids)), means[train_ids], covars[train_ids]])
            np.savez(join(output_path, 'hgmm.npz'), hgmm=hgmm)
    elif method in ['sample_retrieval']:
        dist_mat_file = join(path, 'data', artist, 'distmat.npz')
        if not os.path.isfile(dist_mat_file):
            color_data = np.vstack([im.reshape((-1, 3))[::10] for im in images])
            mat, km = prepare_dist_mat(color_data, steps=1)
            np.savez(dist_mat_file, mat=mat, km=km)
        else:
            tmp = np.load(dist_mat_file)
            mat = tmp['mat']
            km = tmp['km'].tolist()

    #print 'colorizing image'
    for fx, inputfile in enumerate(inputfiles):
        fid = files.index(inputfile)
        image = images[fid]
        seg = segments[fid]
        labels = np.unique(seg)

        original_image = img_as_float(imread(join(image_path, inputfile)))
        gray = gray2rgb(rgb2gray(original_image))
        marked_image = gray.copy()
        scale = gray.shape[0] / float(image.shape[0])

        #print 'colorizing image'
        for l in labels:
            print '.',
            pixels = image[seg == l]
            query = pixels.mean(axis=0)

            if method in ['riemann', 'euclid', 'closest_mean']:
                synth_pixels = np.clip(find_variations(
                    hgmm, query, n_refcoms=100, n_colors=n_sample_colors, method=method, sort_palette=False), 0, 1)
            elif method == 'random':
                synth_pixels = np.clip(sample_gmm(query, np.eye(3) * 0.01, n_colors=n_sample_colors, method='random'), 0, 1)
            elif method == 'covar_retrieval':
                best_com = np.argmin([euc(query, s) for s in means[train_ids]])
                synth_pixels = sample_gmm(query, covars[train_ids[best_com]], n_colors=n_sample_colors, method='random')
            elif method == 'sample_retrieval':
                best_com = np.argmin([euc(query, s) for s in means[train_ids]])
                seg_ = segments[img_ids[train_ids[best_com]]]
                synth_pixels = images[img_ids[train_ids[best_com]]][seg_ == seg_ids[train_ids[best_com]]]
                labels_ = km.predict(synth_pixels)
                idx_ = np.argsort(np.histogram(labels, bins=range(0, km.n_clusters + 1))[0])[-n_sample_colors:]
                synth_pixels = synth_pixels[idx_]

            yy, xx = np.where(seg == l)
            n_yy = len(yy)
            perm = np.random.permutation((n_yy))
            for y, x in zip(yy[perm[:int(n_yy * annotation_ratio)]], xx[perm[:int(n_yy * annotation_ratio)]]):
                best_id = np.argmin([euc(image[y, x], pix) for pix in synth_pixels])
                marked_image[int(y * scale), (x * scale)] = Common.nlab2rgb_p(synth_pixels[best_id])

        colorized_image = Colorizer.colorize(gray, marked_image, 5, 2)

        imsave(join(output_path, 'original_' + inputfile), original_image)
        mask = np.sum(np.abs(gray - marked_image), axis=2) > 0.01
        colors = marked_image[mask]
        yy, xx = np.where(mask)
        marked_image_viz = mark_image(gray, colors, yy, xx, 4)
        imsave(join(output_path, method + '_marked_' + inputfile), marked_image_viz)
        imsave(join(output_path, method + '_shaded_' + inputfile), colorized_image)

        segment_image = image.copy()
        simple_image = image.copy()
        for l in labels:
            tm_ = seg == l
            c_ = segment_image[tm_].copy()
            c_[:, 1:3] = image[seg == l].mean(axis=0)[1:3]
            segment_image[tm_] = image[seg == l].mean(axis=0)
            simple_image[tm_] = c_

        imsave(join(output_path, 'segments_' + inputfile), rescale(Common.nlab2rgb(segment_image), scale))
        imsave(join(output_path, 'simple_' + inputfile), rescale(Common.nlab2rgb(simple_image), scale))
        if viz:
            plt.subplot(n_test_files, 3, fx * 3 + 1)
            plt.imshow(gray, interpolation='nearest')
            plt.subplot(n_test_files, 3, fx * 3 + 2)
            plt.imshow(Common.nlab2rgb(image), interpolation='nearest')
            plt.subplot(n_test_files, 3, fx * 3 + 3)
            plt.imshow(colorized_image, interpolation='nearest')

    if viz:
        plt.show()


def test_interpolation(artist, n_coms, n_trials=1, train_ratio=0.6, max_n_tests=100, viz=False, use_cache=False):
    path = Common.PATH

    data = np.load(join(path, 'data', artist, 'hgmm.npz'))
    segments = data['segments']
    params = data['params'].tolist()
    covars = params['covars'].reshape((-1, 3, 3))
    means = params['means'].reshape((-1, 3))
    weights = params['weights'].ravel()

    images = data['images'].tolist()[0]
    files = data['fnames']

    # prepare the dist matrix for EMD
    dist_mat_file = join(path, 'data', artist, 'distmat.npz')
    if not os.path.isfile(dist_mat_file):
        color_data = np.vstack([im.reshape((-1, 3))[::10] for im in images])
        mat, km = prepare_dist_mat(color_data, steps=1)
        np.savez(dist_mat_file, mat=mat, km=km)
    else:
        tmp = np.load(dist_mat_file)
        mat = tmp['mat']
        km = tmp['km'].tolist()

    bins = np.arange(0, 1.01, 0.01)
    labels = np.unique(segments[0])
    n_labels = len(labels)
    n_files = len(files)
    n_segments = n_labels * n_files

    n_itrains = int(n_files * train_ratio)
    n_trains = n_itrains * n_labels
    n_tests = min(max_n_tests, n_segments - n_trains)
    n_coms = min(n_coms, n_trains)

    img_ids = np.repeat(range(n_files), n_labels)
    seg_ids = np.tile(range(n_labels), n_files)

    # distance = ChiSquareBRD() #BinRatioDistance() #NormalizedCorrelation()
    # ChiSquareBRD() #cv2.compareHist #ChiSquareDistance()
    distance = 'emd'
    methods = ['riemann', 'euclid', 'closest_mean', 'covar_retrieval', 'sample_retrieval']
    #methods = ['retrieval']
    distances = {'emd': emd, 'chisquare_brd': ChiSquareBRD(), 'norm_correlation': NormalizedCorrelation()}

    print 'testing with parameters: n_segments=', n_segments, 'n_trains=', n_trains, 'n_tests', n_tests ,'n_labels=', n_labels

    Dists = dict(zip(methods, [np.zeros((n_trials, n_tests)) for _ in range(len(methods))]))
    for t in range(n_trials):
        print 'trial ', t
        iperm = np.random.permutation(n_files)
        perm = np.concatenate([np.where(img_ids == ip)[0] for ip in iperm])
        #perm = np.random.permutation(n_labels * n_files)

        train_ids = perm[:n_trains]
        test_ids = perm[n_trains:n_trains + n_tests]

        #if n_trains > n_coms * 2:
        #print 'training hgmm..'
        hgmm = HGMM.HGMM(n_components=n_coms, covariance_type='full')
        # if the weights have not been normalized
        if abs(1 - weights.sum()) < 0.1:
            hgmm.fit([weights[train_ids] * n_files / float(n_itrains), means[train_ids], covars[train_ids]])
        else:
            #print (weights[train_ids] / float(n_itrains)).sum()
            hgmm.fit([weights[train_ids] / float(n_itrains), means[train_ids], covars[train_ids]])

        #else:
        #print 'using image gmm..'
        #hgmm = HGMM.HGMM(n_components=n_trains, covariance_type='full')
        #hgmm.covars_ = covars[train_ids]
        #hgmm.means_ = means[train_ids]
        #hgmm.weights_ = weights[train_ids] / sum(weights[train_ids])

        for i in range(n_tests):
            for method in methods:
                tid = test_ids[i]
                seg = segments[img_ids[tid]]
                truth_pixels = images[img_ids[tid]][seg == seg_ids[tid]]

                #labels_ = km.predict(truth_pixels)
                #dom_id = np.argmax(np.histogram(labels_, bins=range(km.n_clusters + 1))[0])
                #query = truth_pixels[labels_ == dom_id].mean(axis=0)

                query = truth_pixels.mean(axis=0) #means[tid]
                #query += noise
                if method == 'covar_retrieval':
                    best_com = np.argmin([euc(query, s) for s in means[train_ids]])
                    synth_pixels = sample_gmm(query, covars[train_ids[best_com]], n_colors=len(truth_pixels), method='random')
                elif method == 'sample_retrieval':
                    best_com = np.argmin([euc(query, s) for s in means[train_ids]])
                    seg = segments[img_ids[train_ids[best_com]]]
                    synth_pixels = images[img_ids[train_ids[best_com]]][seg == seg_ids[train_ids[best_com]]]
                else:
                    synth_pixels = find_variations(hgmm, query, n_refcoms=100,
                                                   n_colors=len(truth_pixels), method=method, sort_palette=False)

                if viz:
                    plt.subplot(121)
                    labels_ = km.predict(truth_pixels)
                    plt.hist(labels_, bins=range(km.n_clusters + 1))
                    plt.subplot(122)
                    labels_ = km.predict(synth_pixels)
                    plt.hist(labels_, bins=range(km.n_clusters + 1))
                    plt.show()

                d = 0
                if distance == 'emd':
                    synth_labels = km.predict(synth_pixels)
                    truth_labels = km.predict(truth_pixels)
                    h2 = np.histogram(synth_labels, bins=range(0, len(km.cluster_centers_) + 1), density=True)[0]
                    h1 = np.histogram(truth_labels, bins=range(0, len(km.cluster_centers_) + 1), density=True)[0]
                    d += distances[distance](h1, h2, mat)
                else:
                    for ch in range(3):
                        h1 = np.histogram(truth_pixels[:, ch], bins=bins, density=True)[0]
                        h2 = np.histogram(synth_pixels[:, ch], bins=bins, density=True)[0]

                        # cv2.cv.CV_COMP_CORREL (bigger better)
                        # cv2.cv.CV_COMP_CHISQR
                        # cv2.cv.CV_COMP_INTERSECT (bigger better)
                        # cv2.cv.CV_COMP_BHATTACHARYYA
                        # d += distance(np.asarray(h1, dtype=np.float32), np.asarray(h2, dtype=np.float32), method=cv2.cv.CV_COMP_BHATTACHARYYA)
                        d += distances[distance](np.asarray(h1, dtype=np.float32), np.asarray(h2, dtype=np.float32))

                Dists[method][t, i] = d

    for method in methods:
        # n_trials * n_tests
        print method, Dists[method].mean(), Dists[method].sum(), Dists[method].std()


if __name__ =='__main__':
    import sys
    if len(sys.argv) > 2:
        action = sys.argv[1]
        artist = sys.argv[2]
    else:
        action = 'plot_hists'
        artist = 'gogh'
        ratio = 0.2

    if action == 'test_interpolation':
        ratio = float(sys.argv[3])
        test_interpolation(artist, 40, n_trials=2, train_ratio=ratio, max_n_tests=80, viz=False, use_cache=False)
    elif action == 'test_shading':
        # method = random, riemann, euclid, closest_mean, covars_retrieval, sample_retrieval
        # methods = ['random', 'riemann', 'euclid', 'covar_retrieval', 'sample_retrieval']
        # methods = ['riemann', 'covar_retrieval', 'sample_retrieval', 'random', 'euclid']

        methods = ['riemann', 'covar_retrieval']
        #artists = ['renoir', 'monet', 'cezanne', 'luce', 'guillaumin', 'metcalf']
        artists = sys.argv[2:]
        for artist in artists:
            print 'artist ', artist
            for method in methods:
                print method,
                test_shade_reconstruction(artist, ['%d.jpg' % d for d in range(0, 20)], method=method, n_coms=20,
                                          n_training_files=4, annotation_ratio=0.05, n_sample_colors=20, use_cache=False)

            print
    elif action == 'plot_hists':
        frm = int(sys.argv[3])
        to = int(sys.argv[4])
        plot_histograms(artist, ['original', 'riemann', 'covar_retrieval'],(frm, to))
