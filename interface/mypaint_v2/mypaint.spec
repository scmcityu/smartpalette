# -*- mode: python -*-
a = Analysis(['mypaint.py'],
             pathex=['F:\\Downloads\\mypaint-master\\mypaint-master','gi'],
             hiddenimports=['sklearn.utils.lgamma', 'sklearn.utils.weight_vector','sklearn.neighbors.typedefs', 'skimage.filter.rank.core_cy', 'skimage._shared.geometry'],
             hookspath=['hooks'],
             runtime_hooks=None)
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='mypaint.exe',
          debug=False,
          strip=None,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=None,
               upx=True,
               name='mypaint')
