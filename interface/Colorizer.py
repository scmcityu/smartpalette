# -*- coding: utf-8 -*-
"""
Created on Tue Sep  8 07:17:00 2015

@author: phanquochuy
"""
import sys, os
from os.path import join
import numpy as np
from PyQt4 import QtGui

from PyQt4.QtCore import (QByteArray, QDataStream, QFile, QFileInfo,
        QIODevice, QPoint, QPointF, QRectF, Qt, SIGNAL, QRect, QTimer, QSize)
from PyQt4.QtGui import (QApplication, QImage, QCursor, QFont, QFontComboBox, QCheckBox, QSlider,
        QFontMetrics, QGraphicsItem, QGraphicsPixmapItem, QMainWindow, QDockWidget, QWidget,
        QGraphicsScene, QGraphicsTextItem, QGraphicsView, QGridLayout, QTableWidget,
        QHBoxLayout, QLabel, QMatrix, QMenu, QMessageBox, QPainter, QTableWidgetItem,
        QPen, QPixmap, QPrintDialog, QPrinter, QPushButton, QSpinBox, QGraphicsGridLayout,
        QStyle, QTextEdit, QVBoxLayout, QListWidget,QBrush, QColor, QGraphicsWidget,
        QGraphicsPolygonItem, QPolygonF, QHBoxLayout, QListWidget, QListWidgetItem, 
        QIcon, QTransform, QColorDialog, QPalette)
        
from pywidgets.qt4.GraphicsItems import HandleItem, InteractiveScene, \
CircleItem, RectItem, GroupItem, PolylineItem, _QC, _NC, _QP, _NP
from pygeometry.Geometry import make_mask
from smartpalette import Common as cm
from smartpalette.MyColorList import colist


class FillInteractiveScene(InteractiveScene):
    def __init__(self, parent):
        super(FillInteractiveScene, self).__init__(parent)

    def mousePressEvent(self, e):
        par = self.parent()
        if hasattr(par, 'currentColor'):
            if par.currentColor is not None:
                pos = _NP(e.scenePos())
                for mx, mc in par.attrs.iteritems():
                    color, mask = mc
                    if mask[pos[1], pos[0]] == 1:
                        par.attrs[mx] = (par.currentColor, mask)
                        par.refresh()
                        #par.loadToCurrent(par.currentFile)
                        break

        return super(FillInteractiveScene, self).mousePressEvent(e)


class MainGUI(QtGui.QMainWindow):
    '''
    mode: segment or colorize
    '''

    def __init__(self, root, mode='segment', src='BSDS500_Animal', target='BSDS500G', hint='BSDS500'):
        super(MainGUI, self).__init__()
        self.root = root
        self.source = src
        self.target = target
        self.hint = hint

        self.imageDir = join(root, join('images', target))
        self.dataDir = join(root, join('data', target))

        self.data = {}
        self.mode = mode
        self.image = None
        self.anns = {}
        self.attrs = {}
        self.prepareData(self.imageDir)
        self.currentFile = ''
        self.currentColor = None
        self.initUI()
        self.n_items_per_group = 100
        
    def prepareData(self, root):
        '''
        if self.mode == 'segment':
            files = sorted([f for f in os.listdir(root)
            if os.path.isfile(join(root, f)) and not f.endswith('gray.jpg')])
        elif self.mode == 'colorize':
            files = sorted([f for f in os.listdir(root)
            if os.path.isfile(join(root, f)) and f.endswith('gray.jpg')])
        '''
        files = sorted([f for f in os.listdir(root)
            if os.path.isfile(join(root, f)) and f.endswith('.jpg')])
        self.imageFiles = files

    def keyPressEvent(self, e):
        if (e.modifiers() == Qt.ControlModifier  or self.scene.activeItem is None) \
        and (e.key() == Qt.Key_R or e.key() == Qt.Key_P or e.key() == Qt.Key_C):
            g = GroupItem([], idd=self._getGroupId())
            gid = self.addItemToScene(g, idd=g.idd)
        else:
            g = self.scene.activeItem
            gid = self.scene.activeItemId

        if gid is not None:
            idd = self._getItemId(gid)

        if e.key() == Qt.Key_R:
            g.addItem(self.createRect(color=idd, idd=idd))
        elif e.key() == Qt.Key_P:
            g.addItem(self.createPolyline(color=idd, idd=idd))
        elif e.key() == Qt.Key_C:
            c = self.createCircle(color=idd, idd=idd)
            g.addItem(c)
        elif e.key() == Qt.Key_Minus:
            self.removeLastItem()
        elif e.key() == Qt.Key_E and e.modifiers() == Qt.ControlModifier:
            self.segment()
        elif e.key() == Qt.Key_W and e.modifiers() == Qt.ControlModifier:
            self.segment_from_annotation()
        elif e.key() >= Qt.Key_0 and e.key() <= Qt.Key_9:

            if e.modifiers() == Qt.ControlModifier:
                idd = self.bringToFront(e.key() - Qt.Key_0)
            else:
                idd = self.bringToFront(e.key() - Qt.Key_0 , group_id=self.scene.activeItemId)

            self.statusBar().showMessage('Selected Item: ' + str(idd) + ' | ' )#+ str(self.attrs[int(e.text())]))
        elif e.key() == Qt.Key_G:
            color = self.chooseColor()
            self.scene.activeItem.color = color #.setColor(color)
              
    def addItem(self, item, idd=None, attr = None):
        if idd is None and isinstance(item, GroupItem):
            idd = self._getGroupId()
            
        self.anns[idd] = item
        #if attr is None:
        #    self.attrs[idd] = self.defaultAttributes().copy()
        #else:
        #    self.attrs[idd] = attr
        return idd
        
    def addItemToScene(self, item, idd):
        idd = self.addItem(item, idd=idd)
        self.scene.addItem(item)
        self.bringToFront(idd)
        return idd
    
    def _getGroupId(self):
        if len(self.anns) == 0:
            idd = 1
        else:
            idds = []
            for k, v in self.anns.iteritems():
                idds.append(v.idd)
            idd = max(idds) + 1
            
        return idd
        
    def _getItemId(self, group_id):
        if len(self.anns[group_id].childItems()) == 0:
            return group_id * self.n_items_per_group
            
        return max([ch.idd for ch in self.anns[group_id].childItems()]) + 1
        
        
    def createCircle(self, color, idd=None, parent=None):
        circle = CircleItem([0,0], 200, color=_QC(list(colist[color]) + [0.3]) , parent=parent,
                            idd= idd, main=self)
        return circle
                
    def createRect(self, color, idd=None, parent=None):
        return RectItem([0,0], [100, 100], color=_QC(list(colist[color]) + [0.3]) , parent=None, 
                        idd= idd, main=self)
    
    def createPolyline(self, color, idd=None, parent=None):
        return PolylineItem([QPointF(0,0)] , color=_QC(list(colist[color]) + [0.3] ), parent=None, idd=idd, main=self)
    
    def addPixmap(self, pixmap):
        pixitem = self.scene.addPixmap(pixmap)
        pixitem.setOpacity(0.5)
        return pixitem
        
    def removeLastItem(self):
        rid = max(self.anns.keys())
        self.scene.removeItem(self.anns[rid])
        
        del self.anns[rid]
        del self.attrs[rid]
        
        if len(self.anns) == 0:
            self.scene.activeItem = None
            self.scene.activeItemId = None
            
        if len(self.anns) >0 :        
            self.bringToFront(0)
     
    def bringToFront(self, idd, group_id=None):
        for ann in self.anns.values():
            ann.setZValue(0)
            ann.setVisible(False)

        items = self.scene.items()
        if group_id is not None:

            group = self.anns[group_id]
            group.setVisible(True)
            for child in group.childItems():
                if child.idd == idd + group_id * self.n_items_per_group:
                    child.setZValue(1)
                    #child.setVisible(True)
                    self.scene.activeSubItem = child
                    return child.idd
        else:
            group_id = idd
            item = self.anns[idd]
            item.setZValue(1)
            item.setVisible(True)
            self.scene.activeItem = item
            self.scene.activeItemId = idd
            self.scene.activeSubItem = None
            return idd
                
    def initUI(self):
        #right panel
        rightLayout = QVBoxLayout()
        gridLayout = QGridLayout()
        
        rightPanel = QWidget()
        rightPanel.setLayout(rightLayout)

        btnSave = QPushButton('&Save')
        self.connect(btnSave, SIGNAL("clicked()"), self.save)
        gridLayout.addWidget(btnSave, 0, 1)

        btnSeg = QPushButton('&Segment')
        self.connect(btnSeg, SIGNAL("clicked()"), self.segment)
        gridLayout.addWidget(btnSeg, 1, 1)

        btnSegU = QPushButton('Segment&U')
        self.connect(btnSegU, SIGNAL("clicked()"), self.segment_unsupervised)
        gridLayout.addWidget(btnSegU, 1, 2)

        btnSetColor = QPushButton('&Choose Color')
        lblSetColor = QLabel()
        self.connect(btnSetColor, SIGNAL('clicked()'), self.setColor)
        gridLayout.addWidget(btnSetColor, 2, 2)
        gridLayout.addWidget(lblSetColor, 2, 1)
        self.lblSetColor = lblSetColor

        btnColorize = QPushButton('&Colorize')
        self.connect(btnColorize, SIGNAL("clicked()"), self.colorize)
        gridLayout.addWidget(btnColorize, 3, 1)
        rightLayout.addLayout(gridLayout)
        
        #add the list of images
        lstImages = QListWidget()
        lstImages.setViewMode(QListWidget.IconMode)
        lstImages.setIconSize(QSize(200,200))
        lstImages.setResizeMode(QListWidget.Adjust)
        for f in self.imageFiles:
            lstImages.addItem( QListWidgetItem(QIcon(join(self.imageDir, f)), f))
        self.lstImages = lstImages
        rightLayout.addWidget(lstImages)
        self.connect(self.lstImages, SIGNAL("itemSelectionChanged()"), self.listChanged)

        #Add the panel to a dock for flexibility
        rightDock = QDockWidget("Fonts and Characters", self)
        rightDock.setAllowedAreas(Qt.LeftDockWidgetArea | Qt.RightDockWidgetArea)
        rightDock.setWidget(rightPanel)
        self.addDockWidget(Qt.RightDockWidgetArea, rightDock)
        
        #The main widget which displays the character being edited
        self.view = QGraphicsView()
        #self.view.scale(1,-1)
        self.view.centerOn(0,0)
        
        self.scene = FillInteractiveScene(self) #QGraphicsScene(self)
        self.scene.setBackgroundBrush(QBrush(Qt.white,Qt.SolidPattern))
        
        #Add the axes
        self.scene.addLine(0,0,500,0)       
        self.scene.addLine(0,0,0,500)
        self.activeItem = None
        #self.activeItemID = 0
        
        #add the first image
        if not os.path.isfile(join(self.dataDir, 'data.npy')):
            pxFirst = QPixmap(join(self.imageDir, self.imageFiles[0]))
            self.anns = {}
            self.image = self.scene.addPixmap(pxFirst)
            self.image.setOpacity(0.5)
        else:
            self.load()

        self.currentFile = self.imageFiles[0]
        self.view.setScene(self.scene)
        self.setCentralWidget(self.view)

        #Menus, status bar, tool bar and stuff
        exitAction = QtGui.QAction(QtGui.QIcon('exit24.png'), 'Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Exit application')
        exitAction.triggered.connect(self.close)
        self.statusBar().showMessage('Welcome to SmartPalette!')
        
        self.setGeometry(0, 0, 1200, 800)
        self.setWindowTitle('SmartPalette 1.0')    
        self.showMaximized()        
        
    def refresh(self):
        self.loadToCurrent(self.currentFile)
        self.scene.addItem(self.image)
        hint = self.scene.addPixmap(QPixmap(join(self.root, 'images', self.hint, self.currentFile)))
        hint.setPos(hint.pixmap().width(), 0)

        for item in self.anns.values():
            self.scene.addItem(item)

    def listChanged(self):
        if not len(self.anns) == 0 or not len(self.attrs) == 0:
            self.saveCurrent()
            
        self.scene.activeItem = None
        self.scene.activeItemId = None
        self.scene.activeSubItem = None
        currentFile = str(self.lstImages.currentItem().text())
        self.scene.clear()

        if not currentFile in self.data.keys():
            px = QPixmap(join(self.imageDir, currentFile))
            self.anns = {}
            self.attrs = {}
            self.image = self.addPixmap(px)
        else:
            self.loadToCurrent(currentFile)
            self.scene.addItem(self.image)

            hint = self.scene.addPixmap(QPixmap(join(self.root, 'images', self.hint, currentFile)))
            hint.setPos(hint.pixmap().width(), 0)

            for item in self.anns.values():
                self.scene.addItem(item)

            if len(self.anns) > 0:
                self.scene.activeItem = self.anns.values()[-1]
                self.scene.activeItemId = self.anns.keys()[-1]
                self.scene.activeSubItem = None
            
        self.currentFile = currentFile
            
    def loadToCurrent(self, fname):
        # import copy
        item = self.data[fname]
        groups = item['anns'].tolist()
        attrs = item['attrs'].tolist()
        self.scene.clear()
        self.anns = {}
        self.attrs = attrs

        for gx, group in groups.iteritems():
            anns_type = group['shape']
            anns = group['cplist']
            colors = group['color']
            ids = group['id']

            idd = self.addItem(GroupItem([], idd=gx), idd=gx)
            for px  in range(len(anns)):
                color = _QC(list(colors[px]) + [0.3])
                #for each contour
                if anns_type[px] == 'P':
                    plist = [_QP(p) for p in anns[px][0]]
                    item = self.anns[idd].addItem(PolylineItem(plist,
                    color=color , idd=ids[px],
                    parent=None, main=self))
                    
                    for ax, ann in enumerate(anns[px][1:]):
                        item.addBreak(len(item))
                        item.addHandles([_QP(p) for p in ann])

                    
                elif anns_type[px] == 'R':
                    self.anns[idd].addItem(RectItem(anns[px][0],
                    anns[px][1], color=color , parent=None,
                    idd=ids[px], main=self))
                    
                elif anns_type[px] == 'C':
                    self.anns[idd].addItem(CircleItem(anns[px][0],
                    anns[px][1][0], color=color , parent=None,
                    idd=ids[px], main=self))

        for mx, mc in attrs.iteritems():
            color, mask = mc
            if mx in self.anns.keys():
                self.anns[mx].setVisible(False)
            pix = self._mask2boundary(mask, _QC(color).rgb())
            self.scene.addPixmap(pix)
                    
        self.image = QGraphicsPixmapItem(QPixmap(join(self.imageDir, fname)))
        self.image.setOpacity(0.5)

    def saveCurrent(self):
        anntype = np.dtype([('id','uint'), ('type', 'a4'), ('shape', 'a4'), 
                            ('color', 'f8', 3), ('cplist', 'O'), ('polies', 'O')])
        dt = np.dtype([('fname','a10'),('anns', 'O'),('attrs', 'O')])
        
        data = self.data
        anns = {}
        attrs = self.attrs

        #for each ring
        for gx, group in self.anns.iteritems():
            groupItems = []
            for rx, region in enumerate(group.childItems()):
                if rx == 0:
                    atype = 'BOUNDING_BOX' #segment bounding box
                elif rx == 2:
                    atype = 'BACKGROUND' #background
                elif rx == 1:
                    atype = 'FOREGROUND' #foreground
                    
                ps = region.points()    
                if isinstance(region, PolylineItem):
                    groupItems.append((region.idd, atype, 'P', _NC(region.color), region.points(),  ps))
                elif isinstance(region, RectItem):
                    groupItems.append((region.idd, atype, 'R', _NC(region.color), region.points(), region.points()))
                elif isinstance(region, CircleItem):
                    groupItems.append((region.idd, atype, 'C', _NC(region.color), region.points(), region.polies()))
                    
            anns[gx] = np.array(groupItems, dtype=anntype)
        
        data[self.currentFile] = np.array((self.currentFile, anns, attrs), dtype=dt)
                
    def save(self):
        self.saveCurrent()
        np.save(join(self.dataDir, 'data.npy'), self.data)
        
    def load(self):
        if os.path.isfile(join(self.dataDir, 'data.npy')):
            self.data = np.load(join(self.dataDir, 'data.npy')).tolist()
            
    def setColor(self):
        color = QColorDialog.getColor(Qt.green, self)
        if color.isValid():
            self.lblSetColor.setPalette(QPalette(color))
            self.lblSetColor.setAutoFillBackground(True)
            self.currentColor = _NC(color)

    def segment_unsupervised(self):
        from skimage.segmentation import felzenszwalb, slic
        from skimage.transform import rescale
        from skimage.io import imread
        from skimage import img_as_float

        img = img_as_float(imread(join(self.imageDir, self.currentFile)))
        img = rescale(img, 0.5)
        masks = felzenszwalb(img, scale=100, sigma=0.6, min_size=11)
        #masks = slic(img, n_segments=8, compactness=20.0, max_iter=100, sigma=0, spacing=None, multichannel=True, convert2lab=None,
        #     enforce_connectivity=False, min_size_factor=0.5, max_size_factor=3, slic_zero=False)

        labels = np.unique(masks)
        print 'segmented into ' + str(labels)
        for l in labels:
            npcolor = colist[l] #anns.childItems()[0].color
            color = _QC(npcolor)
            mask = np.zeros_like(masks)
            mask[masks == l] = 1
            self.attrs[l] = (npcolor, mask)
            pix = self._mask2boundary(mask, color.rgb())
            self.scene.addPixmap(pix)

    def segment_from_annotation(self):

        anns = self.anns[self.scene.activeItem.idd]
        color = anns.childItems()[0].color
        npcolor = _NC(color)

        shape = (self.image.pixmap().width(), self.image.pixmap().height())
        mask2 = make_mask(anns.polies(), shape)
        self.attrs[self.scene.activeItem.idd] = (npcolor, mask2)
        pix = self._mask2boundary(mask2, color.rgb())
        anns.setVisible(False)
        self.scene.addPixmap(pix)

    def segment(self):
        import cv2
        from skimage.draw import line

        anns = self.anns[self.scene.activeItem.idd]
        bgdModel = np.zeros((1, 65),np.float64)
        fgdModel = np.zeros((1, 65),np.float64)
        
        img = cv2.imread(join(self.imageDir, self.currentFile))

        if len(anns.childItems()) == 1:
            #---- bounding box based segment ----
            mask = np.zeros(img.shape[:2],np.uint8)
            rect = np.array(anns.childItems()[0].points(), dtype=int)
            rect = (rect[0, 0], rect[0, 1], rect[1, 0] - rect[0, 0], rect[1, 1] - rect[0, 1])

            # rect = (50,50,450,290)
            cv2.grabCut(img,mask,rect,bgdModel,fgdModel,5,cv2.GC_INIT_WITH_RECT)
            mask2 = np.where((mask==2)|(mask==0),0,1).astype('uint8')
            img = img*mask2[:,:,np.newaxis]
            # cv2.imwrite(join(self.dataDir, self.currentFile), img)
        elif len(anns.childItems()) == 2:
            # ----- mask based segment ----
            if anns.childItems()[0].idd > anns.childItems()[1].idd:
                fid = 1
                bid = 0
            else:
                fid = 0
                bid = 1
            # fid, bid = 0, 1
            foreground_poly = anns.childItems()[fid].points()
            background_poly = anns.childItems()[bid].points()

            mask = np.zeros(img.shape[:2], dtype='uint8') + cv2.GC_PR_BGD

            for ix, item in self.attrs.iteritems():
                _, mask_ = item
                mask[mask_.astype(bool)] = cv2.GC_BGD

            for points in foreground_poly:
                points = np.array(points, dtype=int)
                for px in range(len(points)-1):
                    rr,cc  = line(points[px][1], points[px][0],
                                  points[px+1][1], points[px+1][0])
                    rr = np.clip(rr, 0, mask.shape[0] - 1)
                    cc = np.clip(cc, 0, mask.shape[1] - 1)
                    mask[rr, cc] = cv2.GC_FGD
            for points in background_poly:
                points = np.array(points, dtype=int)
                for px in range(len(points)-1):
                    rr,cc  = line(points[px][1], points[px][0],
                                  points[px+1][1], points[px+1][0])
                    rr = np.clip(rr, 0, mask.shape[0] - 1)
                    cc = np.clip(cc, 0, mask.shape[1] - 1)
                    mask[rr, cc] = cv2.GC_BGD

            mask, bgdModel, fgdModel = cv2.grabCut(img,mask,None,bgdModel,fgdModel,5,cv2.GC_INIT_WITH_MASK)
            bbox = anns.boundingRect()

            mask2 = np.where((mask==cv2.GC_PR_BGD)|(mask==cv2.GC_BGD),0,1).astype('uint8')

            mask2[:, 0:int(bbox.x())] = 0
            mask2[0:int(bbox.y()),:] = 0
            mask2[:, int(bbox.right()):] = 0
            mask2[int(bbox.bottom()):, :] = 0

            img = img * mask2[:,:, np.newaxis]

        if mask2.sum() < 100:
            print 'invalid annotation, do nothing'
            return

        cv2.imwrite(join(self.dataDir, self.currentFile), img)
        color = anns.childItems()[0].color
        npcolor = _NC(color)
        self.attrs[self.scene.activeItem.idd] = (npcolor, mask2)
        pix = self._mask2boundary(mask2, color.rgb())
        anns.setVisible(False)

        #self.scene.clear()
        #self.anns = {}

        #self.image = QGraphicsPixmapItem(QPixmap(join(self.imageDir, self.currentFile)))
        #self.image.setOpacity(0.5)

        #self.scene.addItem(self.image)
        self.scene.addPixmap(pix)

    def _mask2boundary(self, mask2, color, get_boundary=False):
        from skimage.segmentation import find_boundaries

        overlay = QImage(mask2.shape[1], mask2.shape[0], QImage.Format_ARGB32)
        overlay.fill(QColor(255, 255, 255, 0))
        if get_boundary:
            boundary = find_boundaries(mask2)
            yy, xx = np.where(boundary)
        else:
            yy, xx = np.where(mask2)

        for i in range(len(yy)):
            overlay.setPixel(xx[i], yy[i], color)

        pix = QPixmap()
        pix.convertFromImage(overlay)
        return pix

    def colorize(self):
        from skimage.io import imread, imsave
        from skimage.color import rgb2gray, rgb2hsv, gray2rgb, hsv2rgb
        from smartpalette import ColorTransfer as ct
        from skimage import img_as_ubyte
        import shutil
        from matplotlib import pyplot as plt
        from skimage import img_as_float

        N_COLORS = 7
        LEVEL = 0

        srcDataDir = join(self.root, 'data', self.source)
        models = np.load(join(srcDataDir, 'models_nc%d_l%d.npz' % (N_COLORS, LEVEL)))
        model = models['concat_models'].tolist()

        imgfile = self.currentFile
        gray = rgb2gray(imread(join(self.imageDir, imgfile)))
        hsv = rgb2hsv(gray2rgb(gray))

        attrs = self.attrs
        combined_mask = np.ones(gray.shape[:2], dtype='uint8')
        for mx, mc in attrs.iteritems():
            color, mask = mc
            if mask.sum() < 50:
                continue
            combined_mask += mask * mx
            yy, xx = np.where(mask)
            hsv[yy, xx, 0:2] = rgb2hsv([[color]])[0, 0, 0:2]

        rgb = hsv2rgb(hsv)
        cl = ct.Colorizer(model, n_channels=3, n_query_colors=400, patch_size=200, n_components=2, n_neighbors=12, reg=4e-1)
        newrgb = cl.colorize_with_segments(rgb, combined_mask, None, n_clusters=160, use_lle=False, show_palette=True)

        if not os.path.isdir(join(self.root, 'results', self.target)):
            os.makedirs(join(self.root, 'results', self.target))

        imsave(join(self.root, 'results', self.target, 'hue__' + imgfile), hsv2rgb(hsv))
        imsave(join(self.root, 'results', self.target, 'int__' + imgfile), img_as_ubyte(newrgb))
        shutil.copy(join(self.imageDir, imgfile), join(self.root, 'results', self.target, 'ori__' + imgfile))

        #imsave(join(self.root, 'results', self.target, 'ori__' + imgfile), img_as_ubyte())
                        
def main():
    app = QtGui.QApplication(sys.argv)
    ex = MainGUI(cm.PATH, mode=sys.argv[1], src=sys.argv[2], target=sys.argv[3])
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()            
    #PYTHONPATH=/Users/huphan-osx/perforce/patternista python Main.py