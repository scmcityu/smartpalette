from smartpalette.HGMM import HGMM
from sklearn.mixture import GMM
from skimage.io import imread
from os.path import join
from smartpalette import Common
from Common import PATH
import os
import numpy as np
from numpy.random import multivariate_normal
from matplotlib import pyplot as plt
from scipy.spatial.distance import  euclidean as euc
import mpl_toolkits.mplot3d.axes3d as p3
import matplotlib.animation as animation
from sklearn.cluster import KMeans
from smartpalette.coloropt import Colorizer
from skimage import img_as_float, img_as_int, img_as_ubyte
from skimage.draw import ellipse
from skimage.color import rgb2gray, gray2rgb
from skimage.transform import rescale
from pyriemann.utils import mean as prmean
from pyriemann.clustering import Kmeans as RKMeans
from smartpalette import MyColorList
import matplotlib


def visualize_variations_3d(covars, means, labels=None, n_coms=20):
    tips = np.zeros((n_coms, 3, 3))
    origs = means[:n_coms]
    lengths = np.zeros((n_coms, 3))
    if labels is None:
        ax_colors = np.tile(np.array([[200, 0, 0], [0, 200, 0], [0, 0, 200]], dtype=float).T / 255., n_coms).T
    else:
        ax_colors = np.zeros((n_coms * 3, 3))
        for l in range(len(labels)):
            for i in range(3):
                ax_colors[l * 3 + i, :] = MyColorList.colist[labels[l]]

        print ax_colors

    points = np.zeros((n_coms, 20, 3))
    for c in range(n_coms):
        evals, evecs = np.linalg.eigh(covars[c])
        points[c, :] = multivariate_normal(means[c], covars[c], 20)
        for d in range(-1, -4, -1):
            lengths[c, abs(d + 1)] = evals[d]
            tips[c, abs(d + 1), :] = evecs[d]

    fig = plt.figure(figsize=(16,9), dpi=80)

    ax = fig.add_subplot(111, projection='3d')
    ax.axis([0, 1, 0, 1])
    ax.set_zlim3d(0, 1)
    ax.set_xlabel('L', fontsize=25)
    ax.set_ylabel('a', fontsize=25)
    ax.set_zlabel('b', fontsize=25)
    plt.axis('scaled')

    tips = tips.reshape((-1, 3))
    origs = np.hstack(map(lambda x: np.tile(x[np.newaxis, :].T, 3), list(origs) )).T

    for c in range(n_coms):
        for i in range(3):
            length = lengths[c, i]
            if matplotlib.__version__ != '1.5.0':
                ax.quiver(origs[:, 0][c * 3 + i], origs[:, 1][c * 3 + i], origs[:, 2][c * 3 + i], tips[:, 0][c * 3 + i],
                          tips[:, 1][c * 3 + i], tips[:, 2][c * 3 + i], length=length,
                          arrow_length_ratio=0.0, color=ax_colors[i::3], linewidth=2.0)
                # length=1.0 / ((i+1) * 10),
            elif matplotlib.__version__ == '1.5.0':
                #ax.quiver(origs[:, 0][i::3], origs[:, 1][i::3], origs[:, 2][i::3], tips[:, 0][i::3],
                #          tips[:, 1][i::3], tips[:, 2][i::3], length=length * 10,
                #          arrow_length_ratio=0.2, pivot='tail', color=ax_colors[i::3], linewidth=2.0)
                ax.quiver(origs[:, 0][c * 3 + i], origs[:, 1][c * 3 + i], origs[:, 2][c * 3 + i], tips[:, 0][c * 3 + i],
                          tips[:, 1][c * 3 + i], tips[:, 2][c * 3 + i], length=length * 5,
                          arrow_length_ratio=0.2, pivot='tail', color=ax_colors[c * 3 + i], linewidth=2.0)

    # scatter the color points
    if labels is None:
        colors = Common.nlab2rgb(points).reshape((-1, 3))
        points = points.reshape((-1, 3))
        ax.scatter(points[:, 0], points[:, 1], points[:, 2], c=colors, linewidth=0.0)

    plt.show()


def cluster_covariances(covars, n_clusters, means=None, viz=True):
    km = RKMeans(n_clusters)
    km.fit(covars)
    labels = km.predict(covars)
    centers = km.centroids()
    if viz and means is not None:
        visualize_variations_3d(covars, means, labels=labels, n_coms=means.shape[0])
        '''
        pos = np.zeros((n_clusters, 3))
        for i in range(n_clusters):
            pos[i, :] = np.array([i / float(n_clusters), 0.5, 0.5])
        visualize_variations_3d(centers, pos, labels=range(n_clusters), n_coms=n_clusters)
        '''

    print labels


def _full_covariance(covar):
    if len(covar.shape) == 1:
        return covar * np.eye(covar.shape[0])
    elif len(covar.shape) == 2:
        return covar


def find_variations(hgmm, color, n_vars, n_colors=5, method='random', sort_palette=True):
    posteriors = hgmm.predict_proba(color[np.newaxis,:])
    order = np.argsort(np.squeeze(posteriors))[::-1]

    n_vars = min(n_vars, hgmm.n_components)
    variations = np.zeros((n_vars + 2, n_colors, hgmm.means_.shape[1] ))

    mean_covar = prmean.mean_riemann(hgmm.covars_[order[:n_vars]], sample_weight=np.squeeze(posteriors)[order[:n_vars]])
    variations[-1, :] = sample_gmm(color, _full_covariance(mean_covar), n_colors=n_colors, method=method)

    mean_covar = np.average(hgmm.covars_[order[:n_vars]], axis=0, weights=np.squeeze(posteriors)[order[:n_vars]])
    variations[-2, :] = sample_gmm(color, _full_covariance(mean_covar), n_colors=n_colors, method=method)
    if sort_palette:
        variations[-1] = variations[-1][np.argsort(variations[-1, :, 0])]
        variations[-2] = variations[-2][np.argsort(variations[-2, :, 0])]

    print mean_covar
    for i in range(n_vars):
        closest_mean = order[i]
        palette = sample_gmm(color, _full_covariance(hgmm.covars_[closest_mean]), n_colors=n_colors, method=method)
        if sort_palette:
            palette = palette[np.argsort(palette[:, 0])]
        variations[i, :] = palette

    return variations


def find_variation(hgmm, color, n_refcoms=100, n_colors=5, method='riemann', sampling_method='random', sort_palette=True):
    """
    :param hgmm:
    :param color:
    :param n_vars:
    :param n_colors:
    :param method: riemann, euclid, retrieval
    :param sampling_method:
    :param sort_palette:
    :return:
    """
    posteriors = hgmm.predict_proba(color[np.newaxis,:])
    n_refcoms = min(n_refcoms, hgmm.n_components)
    order = np.argsort(np.squeeze(posteriors))[::-1]

    if method == 'riemann':
        mean_covar = prmean.mean_riemann(hgmm.covars_[order[:n_refcoms]], sample_weight=np.squeeze(posteriors)[order[:n_refcoms]])
        variation = sample_gmm(color, _full_covariance(mean_covar), n_colors=n_colors, method=sampling_method)
    elif method == 'euclid':
        mean_covar = np.average(hgmm.covars_[order[:n_refcoms]], axis=0, weights=np.squeeze(posteriors)[order[:n_refcoms]])
        variation = sample_gmm(color, _full_covariance(mean_covar), n_colors=n_colors, method=sampling_method)
    elif method == 'closest_mean':
        best_com = order[0]
        variation = sample_gmm(color, _full_covariance(hgmm.covars_[best_com]), n_colors=n_colors, method=sampling_method)

    if sort_palette:
        variation = variation[np.argsort(variation[:, 0])]

    '''
    print mean_covar
    for i in range(n_vars):
        closest_mean = order[i]
        palette = sample_gmm(color, _full_covariance(hgmm.covars_[closest_mean]), n_colors=n_colors, method=sampling_method)
        if sort_palette:
            palette = palette[np.argsort(palette[:, 0])]
        variations[i, :] = palette
    '''
    return variation


def diversify(img, hgmm, n_colors=5, mode='shading', inter_steps=10, method='random', viz=False):
    """
    :param img: RGBA image, A is used as a mask
    :param hgmm Hierarchical GMM
    :param mode: shading or random
    :return: diversified image
    """
    h, w, d = img.shape
    mask = img[:, :, 3]
    yy, xx = np.nonzero(mask)
    lab = Common.rgb2nlab(img[:, :, :3])
    mean_color = lab[mask != 0].reshape((-1, 3)).mean(axis=0)
    #new_colors, _ = interpolate(hgmm, mean_color[np.newaxis, :], inter_steps)
    #new_color = new_colors[0]
    new_color = interpolate_with_animation(hgmm, mean_color[np.newaxis, :], n_steps=inter_steps)[0]
    palette = find_variations(hgmm, new_color, n_colors=1, method='random')
    palette = find_variations(hgmm, new_color, n_colors=1, method=method)

    # desaturate img before optimization
    orig_img = img.copy()
    img = img_as_ubyte(gray2rgb(rgb2gray(img)) )

    if mode == 'shading':
        s_palette_ = palette[np.argsort(palette[:, 0])]
        s_palette = np.clip(np.int_( Common.nlab2rgb(s_palette_[np.newaxis, :, :]) * 255 ), 0, 255)[0]

        X = lab[yy, xx] #.reshape((-1, 3))
        km = KMeans(n_clusters=n_colors)
        labels = km.fit_predict(X)
        label_map = np.zeros((h, w), dtype=int)
        label_map[yy, xx] = labels
        label_map[mask == 0] = -1

        marked_img = img[:, :, :3].copy()
        if viz:
            marked_img_viz = marked_img.copy()

        means = km.cluster_centers_
        c_order = list(np.argsort(means[:, 0]))
        for m in range(n_colors):
            tmp = label_map == m
            n_cols_ = tmp.sum()
            perm = np.random.permutation(n_cols_)[:n_cols_ * 0.003]
            _yy, _xx = np.nonzero(tmp)
            marked_img[_yy[perm], _xx[perm]] = s_palette[c_order.index(m)]

            if viz:
                for p in range(len(perm)):
                    rr, cc = ellipse(_yy[perm[p]], _xx[perm[p]], 5, 5, (img.shape[0], img.shape[1]))
                    marked_img_viz[rr, cc] = s_palette[c_order.index(m)]

        marked_img[mask == 0] = 0
        new_img = Colorizer.colorize(img_as_float(img[:, :, :3]), img_as_float(marked_img), 10, 10)
        new_img[mask[:new_img.shape[0], :new_img.shape[1]] == 0] = 0

        if viz:
            plt.subplot(231)
            plt.imshow(Common.nlab2rgb(s_palette_[np.newaxis, :, :]), interpolation='nearest')

            plt.subplot(232)
            plt.imshow(marked_img_viz, interpolation='nearest')

            plt.subplot(233)
            plt.imshow(new_img)

            plt.subplot(234)
            plt.imshow(orig_img, interpolation='nearest')

            plt.show()
    if mode == 'random':
        #new_img[mask == 0] = 0

        pass

    return new_img


def interpolate_with_animation(hgmm, colors, n_steps, step_size=0.01):
    means = hgmm.means_
    color_depth = colors.shape[1]

    def update(num, lines, dataLines):
        for line, data in zip(lines, dataLines) :
            # NOTE: there is no .set_data() for 3 dim data...
            line.set_data(data[0:2, :num])
            line.set_3d_properties(data[2, :num])
            line.set_color(Common.nlab2rgb_p(data[0:color_depth, num-1].ravel()))

        return lines

    newcolors, cdata = interpolate(hgmm, colors,n_steps,step_size)
    #data = np.array(cdata[0]).T
    fig = plt.figure()
    ax = p3.Axes3D(fig)
    lines = [ax.plot(dat[0, 0:1], dat[1, 0:1], dat[2, 0:1], 'o', lw=10)[0] for dat in cdata]

    # Setting the axes properties
    ax.set_xlim3d([0.0, 1.0])
    ax.set_xlabel('X')

    ax.set_ylim3d([0.0, 1.0])
    ax.set_ylabel('Y')

    ax.set_zlim3d([0.0, 1.0])
    ax.set_zlabel('Z')

    ax.set_title('3D Test')

    ss = hgmm.weights_
    cols = Common.nlab2rgb(hgmm.means_[np.newaxis, :, :])[0]
    ax.scatter(hgmm.means_[:, 0], hgmm.means_[:, 1], hgmm.means_[:, 2], s= ss * 10000, c=cols)

    ani = animation.FuncAnimation(fig, update, frames=n_steps, interval=100, fargs=(lines, cdata), blit=False)
    plt.show()
    raw_input('press')

    return newcolors


def interpolate(hgmm, colors, n_steps, step_size=0.01):
    means = hgmm.means_
    color_depth = len(colors[0])

    newcolors = np.zeros_like(colors)
    cdata = []
    for c in range(len(colors)):
        cdata.append([])
        pos = colors[c]
        for step in range(n_steps):
            vec = np.zeros(color_depth)
            for m in range(hgmm.n_components):
                d = euc(means[m], pos)
                tmp = (means[m] - pos) / d
                mass = hgmm.weights_[m]
                tmp = tmp * (mass / d ** 2)
                vec += tmp

            pos += vec / np.linalg.norm(vec) * step_size
            cdata[-1].append(list(pos))

        cdata[-1] = np.array(cdata[-1]).T
        newcolors[c] = pos

    return newcolors, cdata


def sample_gmm(mean, covar, n_colors=5, method='random', viz=False):
    if method == 'random':
        samples = multivariate_normal(mean, covar, n_colors)
    elif method == 'grid':
        evals, evecs = np.linalg.eigh(covar)
        #'''
        samples = np.zeros((n_colors, mean.shape[0]))
        val = evals[-1]
        vec = evecs[-1] * val * 4
        pos = mean - vec
        for i in range(n_colors):
            #print pos
            samples[i] = pos
            pos += vec

        '''
        mid = 7 / 2
        samples = np.zeros((7, 3))
        samples[mid] = mean.copy()
        for d in [-1, -2, -3]:
            val = evals[d]
            vec = evecs[d] * val * 10

            samples[mid + d, :] = mean + vec
            samples[mid - d, :] = mean - vec

        tmp = n_colors / 2
        samples = samples[mid-tmp:mid+tmp+1]
        '''

    samples = np.clip(samples, 0, 1)
    if viz:
        img = samples[np.newaxis, :, :]
        rgb = Common.nlab2rgb(img)
        plt.imshow(rgb, interpolation='nearest')
        plt.show()

    return samples


def sample_hgmm(hgmm, components=None, n_colors=5, method='random', viz=False):
    n_dims = hgmm.means_.shape[1]

    if components is None:
        ms = range(hgmm.n_components)
    elif isinstance(components, list):
        ms = components
    else:
        ms = [0]

    img = np.zeros((len(ms), n_colors, 3))
    for m in range(len(ms)):
        if hgmm.covariance_type == 'diag':
            samples = sample_gmm(hgmm.means_[ms[m]], hgmm.covars_[ms[m]] * np.eye(n_dims), n_colors, method)
        elif hgmm.covariance_type == 'full':
            samples = sample_gmm(hgmm.means_[ms[m]], hgmm.covars_[ms[m]], n_colors, method)

        img[m, :] = samples

    img = np.clip(img, 0, 1)
    if viz:
        rgb = Common.nlab2rgb(img)
        plt.imshow(rgb, interpolation='nearest')
        plt.show()

    return img


def prepare_auto_segment_based_palettes(collections, data=None, n_sub_coms=20, n_coms=100, step=5, cap=5, covariance_type='diag'):
    """
    learn a Hierachical Gaussian Mixture Model
    :param collections:
    :param n_sub_coms: number of gmm components
    :param n_coms: number of hgmm components
    :param step:
    :param cap: max number of images
    :return:
    """
    #cap = 5
    for collection in collections:
        image_path = join(PATH, 'images', collection)
        data_path = join(PATH, 'data', collection)

        imagefiles = sorted([f for f in os.listdir(image_path) if f.endswith('.jpg')])
        n_files = len(imagefiles)
        # weights, means, covars
        params = [[], [], []]
        if data is not None:
            params[0] = data['weights']
            params[1] = data['means']
            params[2] = data['covars']

        hgmm = HGMM(n_components=n_coms, covariance_type=covariance_type)
        segments = []
        lab_images = []
        for fname in imagefiles[:cap]:
            print fname,
            gmm = GMM(n_components=n_sub_coms, covariance_type=covariance_type)
            img = imread(join(image_path, fname))
            img = rescale(img, 0.4)

            h, w, d = img.shape
            nlab = Common.rgb2nlab(img)
            locations = np.array([(x / float(w), y / float(h)) for y in range(h) for x in range(w)])

            X = np.hstack((nlab.reshape((-1, d)), locations))
            # X = nlab.reshape((-1, d))

            gmm.fit(X[::step])
            labels = gmm.predict(X[::step])

            segments.append(np.asarray(labels, np.uint8).reshape((h, w)))
            lab_images.append(nlab)

            params[0].append(gmm.weights_) # * (1. / float(n_files))
            params[1].append(gmm.means_[:, :d])
            if covariance_type == 'diag':
                params[2].append(gmm.covars_[:, :d])
            elif covariance_type == 'full':
                params[2].append(gmm.covars_[:, :d, :d])

            # labels = gmm.predict(X)
            # plt.imshow(labels.reshape((h, w)), interpolation='nearest')
            # plt.show()

        hgmm.fit([np.concatenate(params[0]) * (1. / float(n_files)), np.vstack(params[1]), np.vstack(params[2])])
        # hgmm = GMM(n_components=n_coms, covariance_type='full')
        # hgmm.fit(np.vstack(params[1]))
        if not os.path.isdir(data_path):
            os.mkdir(data_path)

        np.savez(join(data_path, 'hgmm.npz'), hgmm=hgmm,
                 params={'weights': np.array(params[0]), 'means': np.array(params[1]), 'covars': np.array(params[2])},
                 fnames=imagefiles[:cap],
                 segments=segments,
                 images=[lab_images])

        return hgmm, params


if __name__ == '__main__':
    import sys
    action = sys.argv[1]
    if action == 'train':
        # monet cezanne gogh renoir luce guillaumin gerome metcalf gas
        artist = sys.argv[2]
        hgmm, params = prepare_auto_segment_based_palettes([artist], n_sub_coms=10, n_coms=40, cap=10, step=1, covariance_type='full')
        sample_hgmm(hgmm, viz=True)
    elif action == 'diversify':
        image_name = sys.argv[3]
        imgfile = join(PATH, 'images', 'decorative', image_name)
        img = imread(imgfile)
        artist = sys.argv[2]
        inter_steps = int(sys.argv[4])
        colors = Common.rgb2nlab(np.array([[[0.0, 0.0, 1.0], [1., 0.5, 0.], [0, 1.0, 0.]]]))[0]
        data = np.load(join(PATH, 'data', artist, 'hgmm.npz'))
        hgmm = data['hgmm'].tolist()
        diversify(img, hgmm, inter_steps=inter_steps, method='random', viz=True)
    elif action == 'sample':
        artist = sys.argv[2]
        data = np.load(join(PATH, 'data', artist, 'hgmm.npz'))
        hgmm = data['hgmm'].tolist()
        sample_hgmm(hgmm, viz=True, method='random')
    elif action == 'interpolate':
        pass
    elif action == 'vary':
        artist = sys.argv[2]
        data = np.load(join(PATH, 'data', artist, 'hgmm.npz'))
        hgmm = data['hgmm'].tolist()
        color = Common.rgb2nlab(np.array([[[int(c) for c in sys.argv[3:6]]]]) / 255.)
        variations = find_variations(hgmm, color[0, 0], n_refcoms=10, n_colors=5, sort_palette=True)
        plt.subplot(111)
        plt.imshow(Common.nlab2rgb(variations), interpolation='nearest')
        plt.show()
    elif action == 'vis_var':
        artist = sys.argv[2]
        data = np.load(join(PATH, 'data', artist, 'hgmm.npz'))
        hgmm = data['hgmm'].tolist()
        visualize_variations_3d(hgmm.covars_, means=hgmm.means_, n_coms=20)
    elif action == 'cluster':
        artists = sys.argv[2:]

        covars = []
        means = []
        for artist in artists:
            data = np.load(join(PATH, 'data', artist, 'hgmm.npz'))
            hgmm = data['hgmm'].tolist()
            prms = data['params'].tolist()
            covars.append(hgmm.covars_)
            means.append(hgmm.means_)

        covars = np.vstack(covars )
        means = np.vstack(means)
        #cluster_covariances(params['covars'].reshape((-1, 3, 3)), n_clusters=7, means=params['means'].reshape((-1, 3)), viz=True)
        cluster_covariances(covars, n_clusters=8, means=means, viz=True)
    else:
        # Interpolate colors
        #'''
        steps = 20
        newcolors = interpolate(hgmm, colors.copy(), n_steps=steps)
        plt.subplot(121)
        plt.imshow(Common.nlab2rgb( newcolors[np.newaxis, :, :] ), interpolation='nearest')

        plt.subplot(122)
        plt.imshow(Common.nlab2rgb( colors[np.newaxis, :, :] ), interpolation='nearest')

        #plt.show()
        interpolate_with_animation(hgmm, colors.copy(), n_steps=steps)
        #'''