function handle = imageColorModify(handle, imageVals, imageSize, colorspace)

% IMAGEMODIFY Helper code for visualisation of image data.
% FORMAT
% DESC is a helper function for visualising image data using latent
% variable models.
% ARG handle : the handle of the image data.
% ARG imageValues : the values to set the image data to.
% ARG imageSize : the size of the image.
% ARG transpose : whether the resized image needs to be transposed
% (default 1, which is yes).
% ARG negative : whether to display the negative of the image
% (default 0, which is no).
% ARG scale : dummy input, to maintain compatability with
% IMAGEVISUALISE.
% RETURN handle : a the handle to the image data.
%
% COPYRIGHT : Neil D. Lawrence, 2003, 2004, 2006
%
% SEEALSO : imageVisualise, fgplvmResultsDynamic

% GPMAT

if strcmp(colorspace , 'lab')
    nChannels = 3;
end

imageData = reshape(imageVals, imageSize(1), imageSize(2)/nChannels, nChannels);

if strcmp(colorspace, 'lab')
    imageData(:,:,1) = imageData(:,:,1) * 100;
    imageData(:,:,2) = imageData(:,:,2) * 220 - 110;
    imageData(:,:,3) = imageData(:,:,3) * 220 - 110;
    imageData = lab2RGB(imageData);
end

set(handle, 'CData', imageData);

