function [Varmu Varsigma] = predictPalette(model_id, Ytrs, Yts, models)
%tic

iters = 100; %Default: 100
display = 0;
dims = size(Yts, 2);
N = size(Yts, 1);

if ~exist('models')
    fprintf(1, 'load models first!');
    return;
end

model = models{model_id};
%model.optimiser = 'optimiMinimize';
Ytr = Ytrs{model_id};
%fprintf(1, '# Removing some outputs randomly from test data...\n');
% randomly choose which outputs are present.
% Here we assume 50% missing outputs from each test point
%numIndPresent = round(0.5*dims);
%indicesPresent = zeros(N, numIndPresent);
%indicesMissing = zeros(N, dims-numIndPresent);

indicesPresent = {};
indicesMissing = {};
for i=1:N
%    permutat = randperm(dims);
    indicesMissing{i} = find(isnan(Yts(i,:))); %permutat(1:numIndPresent);
    indicesPresent{i} = setdiff(1:dims, indicesMissing{i});
end
% Missing data are indicated with NaN values. This demo creates randomly
% such data.

%for i=1:size(Yts,1)
%    Yts(i,indicesMissing(i,:))=NaN;
%end

indexP = [];
Init = [];
Testmeans = [];
Testcovars = [];
Varmu = [];
Varsigma = [];
fprintf(1, '# Partial reconstruction of test points...\n');
pb = myProgressBar(size(Yts,1), size(Yts,1)/10); % This will print 10 times
% patrial reconstruction of test points
for i=1:size(Yts,1)
    indexPresent = indicesPresent{i};
    indexP(i,:) = indexPresent;
    pb = myProgressBar(pb,i);
    % initialize the latent point using the nearest neighbour
    % from he training data
    dst = dist2(Yts(i,indexPresent), Ytr(:,indexPresent));
    [mind, mini] = min(dst);
    Init(i,:) = model.vardist.means(mini,:);
    % create the variational distribtion for the test latent point
    vardistx = vardistCreate(model.vardist.means(mini,:), model.q, 'gaussian');
    vardistx.covars = 0.2*ones(size(vardistx.covars));
    % optimize mean and vars of the latent point
    model.vardistx = vardistx;
    [x, varx] = vargplvmOptimisePoint(model, vardistx, Yts(i, :), display, iters);
    Testmeans(i,:) = x;
    Testcovars(i,:) = varx;
    % reconstruct the missing outputs
    [mu, sigma] = vargplvmPosteriorMeanVar(model, x, varx);
    Varmu(i,:) = mu;
    Varsigma(i,:) = sigma;
end

%toc
%palettes = [Varmu Varsigma];
