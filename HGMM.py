import numpy as np
from sklearn import mixture as mx
import sklearn as skl
import logging as lgg


class HGMM(mx.GMM):
    def __init__(self, n_components=1, covariance_type='diag', random_state=None, thresh=1e-2, min_covar=1e-3, \
    n_iter=1000, n_init=1, params='wmc', init_params='wmc', N=1000):
        # TODO: Need to assign init_params here
        super(HGMM, self).__init__(n_components=n_components,covariance_type=covariance_type, \
                 random_state=random_state, tol=thresh, min_covar=min_covar, \
                 n_iter=n_iter, n_init=n_init, params=params, init_params=init_params)
        self.N = N
        self.means_ = []
        self.covars_ = []
        self.weights_ = []
        self.sweights_ = []
        self.smeans_ = []
        self.scovars_ = []

    def _do_mstep(self, X, responsibilities, params, min_covar=0):
        """
        Perform the Mstep of the EM algorithm and return the class weights.

        :param X:
        :param responsibilities:
        :param params:
        :param min_covar:
        :return:
        """
        weights = responsibilities.sum(axis=0)
        (xw, xh) = X.shape
        ww = (responsibilities * self.sweights_[:,np.newaxis]) \
        / (np.dot(responsibilities.T,self.sweights_[:, np.newaxis]).flatten() + mx.gmm.EPS) #maybe with np.newaxis ?
        if 'w' in params:
            self.weights_ = (weights / xw)
        if 'm' in params:
            self.means_ = np.dot(ww.T, self.smeans_)
        if 'c' in params:
             for m in range(self.n_components):
                xmu = self.smeans_ - self.means_[m]
                wj = ww[:, m]

                if self.covariance_type == 'diag':
                    cv = np.sum(wj[:, np.newaxis] * (self.scovars_ + xmu ** 2), axis=0)
                    cv = np.maximum(cv, np.repeat(1e-6, xh))
                elif self.covariance_type == 'full':
                    cv = np.dot((wj[:, np.newaxis] * xmu).T, xmu) + np.sum(wj[:, np.newaxis, np.newaxis] * self.scovars_, axis=0)
                    #D, V = np.linalg.eig(cv)
                    #D = np.maximum(D, np.repeat(1e-6, len(D)))
                    #cv = V * (D * np.eye(len(D))) * V.T

                self.covars_[m] = cv

        return weights

    def e_step(self, X):
        X = np.asarray(X)
        if X.ndim == 1:
            X = X[:, np.newaxis]
        if X.size == 0:
            raise ValueError("X is empty")
        if X.shape[1] != self.means_.shape[1]:
            raise ValueError('The shape of X  is not compatible with self')

        lgauss = mx.gmm.log_multivariate_normal_density( \
        X, self.means_, self.covars_, self.covariance_type)

        if self.covariance_type=='diag':
            tr = np.dot(1. / self.covars_ , self.scovars_.T).T
            lpr = (( lgauss - 0.5 * tr) * self.N * self.sweights_[:, np.newaxis]) + np.log(self.weights_)
        elif self.covariance_type == 'spherical':
            pass
        elif self.covariance_type == 'full':

            tr = np.zeros((len(self.scovars_), self.n_components))
            for j in range(self.n_components):
                tmp = np.linalg.inv(self.covars_[j])
                for i in range(len(self.scovars_)):
                    tr[i, j] = np.trace(np.dot(tmp , self.scovars_[i]))

            lpr = ((lgauss - 0.5 * tr) * self.N * self.sweights_[:, np.newaxis]) + np.log(self.weights_)

        logprob = self.logtrick2(lpr, axis=1)

        if X.shape[0] != 1:
            lgg.info("sum lob probability: %s" % str(logprob.sum()))

        responsibilities = np.exp(lpr - logprob[:, np.newaxis])
        return logprob, responsibilities

    def logtrick2(self, X, axis=1):
        v, i = X.max(axis), X.argmax(axis)
        cterm = np.sum(np.exp(X - v[:,np.newaxis]),axis=axis)
        return v + np.log(cterm)

    def set_data(self, data):
        assert(abs(data[0].sum() - 1.0) < 1e-8)
        self.sweights_ = data[0]
        self.smeans_ = data[1]
        self.scovars_ = data[2]

    def fit(self, XX):
        self.set_data(XX)
        #X <- smeans_ (centers of all components of the lower layer)
        X = self.smeans_
        if X.ndim == 1:
            X = X[:, np.newaxis]

        if X.shape[0] < self.n_components:
            raise ValueError(
                'GMM estimation with %s components, but got only %s samples' %
                (self.n_components, X.shape[0]))

        max_log_prob = -np.infty

        for _ in range(self.n_init):
            if 'm' in self.init_params or not hasattr(self, 'means_'):
                self.means_ = skl.cluster.KMeans(
                    n_clusters=self.n_components,
                    random_state=self.random_state).fit(X).cluster_centers_

            if 'w' in self.init_params or not hasattr(self, 'weights_'):
                self.weights_ = np.tile(1.0 / self.n_components,
                                        self.n_components)

            if 'c' in self.init_params or not hasattr(self, 'covars_'):
                cv = np.cov(X.T) + self.min_covar * np.eye(X.shape[1])
                if not cv.shape:
                    cv.shape = (1, 1)
                self.covars_ = \
                    mx.gmm.distribute_covar_matrix_to_match_covariance_type(
                        cv, self.covariance_type, self.n_components)

            # EM algorithms
            log_likelihood = []
            # reset self.converged_ to False
            self.converged_ = False
            for i in range(self.n_iter):
                # Expectation step
                curr_log_likelihood, responsibilities = self.e_step(X)
                log_likelihood.append(curr_log_likelihood.sum())

                # Check for convergence.
                if i > 0 and abs(log_likelihood[-1] - log_likelihood[-2]) < \
                        self.thresh:
                    self.converged_ = True
                    break

                # Maximization step
                self._do_mstep(X, responsibilities, self.params,
                               self.min_covar)

            # if the results are better, keep it
            if self.n_iter:
                if log_likelihood[-1] > max_log_prob:
                    max_log_prob = log_likelihood[-1]
                    best_params = {'weights': self.weights_,
                                   'means': self.means_,
                                   'covars': self.covars_}

        # check the existence of an init param that was not subject to
        # likelihood computation issue.
        if np.isneginf(max_log_prob) and self.n_iter:
            raise RuntimeError(
                "EM algorithm was never able to compute a valid likelihood " +
                "given initial parameters. Try different init parameters " +
                "(or increasing n_init) or check for degenerate data.")

        # self.n_iter == 0 occurs when using GMM within HMM
        if self.n_iter:
            lgg.info("max log prob: %s" % max_log_prob)
            self.covars_ = best_params['covars']
            self.means_ = best_params['means']
            self.weights_ = best_params['weights']
        return self

    def dosplit(self):
        """
        Component splitting
        :return:
        """