# -*- coding: utf-8 -*-
"""
Created on Sat Oct 19 23:03:33 2013

@author: phan
"""

__all__ = ['fast','features', 'KernelKMeans','ColorManifolds']