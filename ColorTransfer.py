'''
Created on 29 Oct, 2014

@author: phan
'''
from skimage.io import imread, imsave
import Common as cm
from os.path import join
import os
import numpy as np
from LLE import LocallyLinearEmbedding, barycenter_weights
from sklearn.utils import check_array
from skimage.segmentation import slic
from skimage.color import rgb2hsv, hsv2rgb
from FeatureAlignment import align_colors_naive
from sklearn.cluster import KMeans
from matplotlib import pyplot as plt
from scipy.spatial.distance import euclidean as euc
from smartpalette.fast import Distance


def prepare_colorizer(image_file, aligner, n_segments=40, n_colors=7, mode='single', use_spatial=False, scale=1.0,
                      slic_compactness=10., vis=False):
    """
    pre-segment the the source and target palettes

    :param image_file:
    :param aligner:
    :param n_segments:
    :param n_colors:
    :param mode: prepare colorizer with 'single' palette or 'multi' palettes
    :param use_spatial: whether to use spatial information for the initial segmentation
    :param scale:
    :param slic_compactness: compactness
    :param vis:
    :return:
    """
    modes = ['single', 'multi']
    assert  mode in modes

    from sklearn.cluster import KMeans
    from skimage.io import imread
    from skimage.transform import rescale
    # from smartpalette import FeatureAlignment
    # import itertools

    params = dict()
    params['lmda'] = 0.4
    params['image_file'] = image_file
    params['aligner'] = aligner

    N_SEGS = n_segments
    N_COLS = n_colors

    colorizer = Colorizer(n_components=2, n_neighbors=10, reg=4e-1, method='standard')

    params['image'] = rescale(imread(params['image_file'])[:, :, :3], scale)
    # h, w, d = params['image'].shape
    # dimensions = params['image'].shape
    image_nlab = cm.rgb2nlab(params['image'])
    params['image_nlab'] = image_nlab

    if mode == 'single':
        km = KMeans(n_clusters=N_SEGS)
        labels = km.fit_predict(image_nlab.reshape((-1, 3)))
        labels = labels.reshape(image_nlab.shape[:2])
        seg_masks = []
        for sid in range(N_SEGS):
            seg_masks.append(labels == sid)

        params['seg_masks'] = seg_masks
        params['seg_mean_colors'] = km.cluster_centers_
        km1 = KMeans(N_COLS)
        params['seg_color_labels'] = km1.fit_predict(km.cluster_centers_)
        params['orig_palette'] = km1.cluster_centers_
        params['lle_params'] = None

        if aligner is not None:
            _, newids = aligner.align(km1.cluster_centers_[None, ...])
        else:
            newids = np.arange(n_colors)[None, ...]

        params['alignment_ids'] = np.argsort(newids[0])
    elif mode == 'multi':
        if use_spatial:
            # locs = np.array(list(itertools.product(np.arange(h) / float(h), np.arange(w) / float(w))))
            # X = np.hstack([image_nlab.reshape((-1, 3)), locs])
            from skimage.segmentation import slic
            labels = slic(params['image'], n_segments=N_SEGS, compactness=slic_compactness,
                          multichannel=True, slic_zero=False, enforce_connectivity=False)
        else:
            km = KMeans(n_clusters=N_SEGS)
            X = image_nlab.reshape((-1, 3))
            km.fit(X)
            labels = km.predict(X)
            labels = labels.reshape(image_nlab.shape[:2])

        if vis:
            fig = plt.figure('segmentation')
            ax1 = fig.add_subplot(111)
            ax1.imshow(labels, interpolation='nearest')
            # plt.show()

        # newclusters = []
        # for sx in range(N_SEGS):
        #     dists = [euc(km.cluster_centers_[sx, :3], newclusters[i][:3 ]) for i in range(len(newclusters))]
        #     if len(dists) > 0 and min(dists) < 0.2:
        #         continue
        #
        #     newclusters.append(km.cluster_centers_[sx])

        # km.cluster_centers_ = np.array(newclusters)
        # km.n_clusters = len(newclusters)
        # N_SEGS = len(newclusters)

        # The actual number of segments, might != N_SEGS
        n_segs = len(np.unique(labels))
        seg_masks = []
        for sid in range(n_segs):
            seg_masks.append(labels == sid)

        orig_palette = []
        seg_color_labels = []
        alignment_ids = []
        sub_seg_masks = []
        sub_seg_mean_colors = []
        km1 = KMeans(N_COLS)
        offset = 0

        # plt.subplot(111)
        # plt.imshow(labels.reshape((h, w)))
        # plt.show()

        # Find palette for each segment (sub_seg_mean_colors) and then align them with the data
        for sx in range(n_segs):
            seg_labels = km1.fit_predict(image_nlab[seg_masks[sx]])
            _seg_mask = np.int_(seg_masks[sx])
            _seg_mask[_seg_mask == 1] = seg_labels + 1
            sub_seg_mean_colors.append(km1.cluster_centers_.copy())

            # plt.subplot(1, N_SEGS, sx + 1)
            # plt.imshow(_seg_mask, interpolation='nearest')

            for j in range(N_COLS):
                sub_seg_masks.append(_seg_mask == j + 1)

            orig_palette.append(km1.cluster_centers_.copy())
            seg_color_labels.append(np.arange(N_COLS) + offset)
            _, _tmp_ids = aligner.align(km1.cluster_centers_[None, ...])
            alignment_ids.append(np.argsort(_tmp_ids[0]))
            offset += N_COLS

        # plt.show()

        params['seg_color_labels'] = np.concatenate(seg_color_labels)
        params['orig_palette'] = np.concatenate(orig_palette)
        params['alignment_ids'] = np.concatenate(alignment_ids)
        params['seg_masks'] = sub_seg_masks
        params['seg_mean_colors'] = np.concatenate(sub_seg_mean_colors)
        params['lle_params'] = None

    return colorizer, params


class Colorizer(LocallyLinearEmbedding):

    def __init__(self, n_components=2, n_neighbors=12, reg=4e-1, method='standard'):
        """
        Constructor
        
        works best with method = 'modified'
        """
        super(Colorizer, self).__init__(eigen_solver='auto', n_components=n_components,
                                        n_neighbors=n_neighbors, reg=reg, method=method)
        
    def get_weights(self, X):
        X = check_array(X)
        ind = self.nbrs_.kneighbors(X, n_neighbors=self.n_neighbors,
                                    return_distance=False)
        weights = barycenter_weights(X, self.nbrs_._fit_X[ind],
                                     reg=self.reg)

        return weights

    def precompute_params(self, image, colorized_image, lmda=0.4, sparse=True):
        if sparse:
            from scipy.sparse import csr_matrix, eye, issparse, diags
        else:
            from numpy import eye

        if len(image.shape) == 3:
            w, h, d = image.shape
        else:
            w, d = image.shape
            h = 1

        pixels = image.reshape((w * h, d))
        try:
            self.fit(pixels)
        except Exception as e:
            print 'Error!'
            return colorized_image

        M = self.M
        I = eye(len(pixels))

        # List of target colors
        G = colorized_image.reshape((w * h, d))

        Lambda = np.array([np.any(row != 0) for row in G]).astype(np.int32) * lmda
        if sparse:
            Lambda = diags([Lambda], [0])
        else:
            Lambda = Lambda * I

        A = M + Lambda

        if sparse:
            G = csr_matrix(G)
        b = Lambda.dot(G)

        if sparse:
            A = A.asformat('csc')
            b = b.asformat('csc')

        return A, b, M, Lambda, (w, h, d)

    def colorize_lle(self, image, colorized_image, lmda=0.4, sparse=True, params=None):
        '''
        Xiaowu Chen, Dongqing Zou, Qinping Zhao, and Ping Tan. 2012. Manifold preserving edit 
        propagation. ACM Trans. Graph. 31, 6, Article 132 (November 2012), 7 pages. 
        DOI=10.1145/2366145.2366151 http://doi.acm.org/10.1145/2366145.2366151
        '''
        if sparse:
            from scipy.sparse import csr_matrix, eye, diags
            from scipy.sparse.linalg import spsolve as solve
        else:
            from scipy.linalg import solve

        if params is None:
            A, b, M, Lambda, img_dims = self.precompute_params(image, colorized_image, lmda, sparse)
        else:
            A, b, M, Lambda, img_dims = params

        w, h, d = img_dims
        G = colorized_image.reshape((w * h, d))
        if sparse:
            G = csr_matrix(G)

        b = Lambda.dot(G)
        newimage = solve(A, b)
        if sparse:
            newimage = newimage.toarray().reshape((w, h, d))
        else:
            newimage = newimage.reshape((w, h, d))
        
        return np.squeeze(newimage)

    @staticmethod
    def _weighted_euclidean(self, x, y, ws):
        return np.sqrt((ws[0] * x[0] - y[0])**2 + ws[1] * (x[1] - y[1])**2 + ws[2] * (x[2] - y[2])**2)

    def colorize(self, image_nlab, seg_masks, seg_mean_colors, seg_color_labels, orig_palette, new_palette,
                 params=None, lmda=0.4):
        self.colorize_quick(image_nlab, seg_masks, seg_mean_colors, seg_color_labels, orig_palette, new_palette,
                            params, lmda)

    def colorize_quick(self, image_nlab, seg_masks, seg_mean_colors, seg_color_labels, orig_palette, new_palette,
                       params=None, lmda=0.4, ann_per=0.2):
        """
        quickly colorize entire image with pre-segmented images. used for real-time visualization
        :param image_nlab: (N x M x 3) image
        :param seg_masks: segments
        :param seg_mean_colors: mean colors in each segment
        :param seg_color_labels: color labels for pixels in image_nlab (1 to len(orig_palette))
        :param orig_palette: palette extracted from image_nlab
        :param new_palette: new palette
        :param params: lle parameters
        :param lmda:
        :param ann_per: percentage of annotated pixels (for LLE)
        :return: colorized image
        """
        annotation = np.zeros_like(seg_mean_colors)
        labels = np.unique(seg_color_labels)
        n_labels = len(labels)
        perm = np.random.permutation(n_labels)
        for l in perm[: int(ann_per * n_labels)]:
            annotation[seg_color_labels == l] = new_palette[l]

        if params is None:
            params = self.precompute_params(seg_mean_colors, annotation, lmda=lmda, sparse=False)

        new_colors = self.colorize_lle(seg_mean_colors, annotation, lmda=lmda, sparse=False, params=params)
        newimage = np.zeros_like(image_nlab)
        # newimage_raw = np.zeros_like(image_nlab)

        new_colors[:, 0] = seg_mean_colors[:, 0]
        for sx in range(len(seg_masks)):
            _yy, _xx = np.where(seg_masks[sx])
            newimage[_yy, _xx] = new_colors[sx]
            # newimage_raw[_yy, _xx] = new_palette[sx]
            # newimage[seg_masks[sx]] = new_colors[sx]

        # plt.subplot(121)
        # plt.imshow(cm.nlab2rgb(newimage_raw), interpolation='nearest')
        # plt.subplot(122)
        # plt.imshow(cm.nlab2rgb(newimage), interpolation='nearest')
        # plt.show()

        return cm.nlab2rgb(newimage), params


def colorize_with_manifold(imagefile, artists, inputs=None, n_colors=7, n_segments=10, space_color_ratio=10.,
                           mode='multi', latent_opt=True, ann_per_lle=0.5, vis=False, scale=1.0,
                           palette_sorter='binary'):
    """

    :param imagefile:
    :param artists:
    :param inputs: target palette. only used in 'compl' mode.
    :param n_colors:
    :param n_segments: number of initial regions to extract palettes
    :param space_color_ratio: parameter for image segmentation
    :param mode: 'single' - use a single palette for an entire image. 'multi' - multiple palettes.
    'compl' - use a global palette to guide multiple local palettes. 'coloropt' apply color optimization to produce
    rough results.
    :param latent_opt: whether to optimize latent point
    :param ann_per_lle: percentage of annotated pixels for lle optimization
    :param vis:
    :param scale: image scale
    :param palette_sorter
    :return:
    """
    modes = ['multi', 'single', 'compl', 'coloropt']
    from smartpalette import Common, FeatureAlignment
    import GPy
    from smartpalette.gplvm.MyInferenceX import MyInferenceX, my_infer_newX
    # from GPy.inference.latent_function_inference.inferenceX import infer_newX
    path = Common.PATH
    fn, fex = tuple(os.path.splitext(os.path.basename(imagefile)))
    # ipath, iname = os.path.splitext(imagefile)

    for aid, artist in enumerate(artists):
        rpath = join(path, 'results', 'style_comparison', artist)
        if not os.path.isdir(rpath):
            os.makedirs(rpath)

        datafile = join(path, 'data', artist, 'clusters_nc%d_l%d.npz' % (n_colors, 0))
        modelfile = join(path, 'data', artist, 'models_nc%d_l%d.npy' % (n_colors, 0))

        tmpdata = np.load(datafile)
        training_data = np.squeeze(tmpdata['normalized_clustered_lab']) #[::2]
        n_pals, n_cols, n_chas = training_data.shape
        training_data, newindices, _, order, _ = \
            FeatureAlignment.sort_palettes(training_data, n_colors, sort_palettes=True,
                                           n_themes=None, labels=None, step=1, sorter=palette_sorter, viz='none')

        Q = 4
        kern = GPy.kern.RBF(Q, ARD=True)
        m = GPy.models.BayesianGPLVM(training_data.reshape((-1, n_cols * n_chas)), Q, kernel = kern)
        m.optimize('bfgs', messages=1, max_iters=2000)

        # m = GPy.models.GPLVM(training_data.reshape((-1, n_cols * n_chas)), Q)
        # m.optimize('lbfgs', messages=0, max_iters=2000, gtol=1e-4)
        # np.save(modelfile, m)

        aligner = FeatureAlignment.PaletteAligner(method='data',
                                                  data=training_data,
                                                  match_method='munkres',
                                                  n_examples=7)

        if mode == 'multi':
            colorizer, prm = \
                prepare_colorizer(imagefile, aligner=aligner, n_segments=n_segments, n_colors=n_colors,
                                  mode='multi', use_spatial=False, vis=vis)

            orig_palettes = prm['orig_palette'].reshape((-1, n_colors, n_chas))
            new_palettes = []
            for ox in range(len(orig_palettes)):
                Xnew, _ = m.infer_newX(orig_palettes[ox].reshape((1, -1)), optimize=latent_opt)
                Ynew, _ = m.predict(Xnew, full_cov=True)
                new_palettes.append(Ynew.reshape((n_colors, n_chas)))

            new_palette = np.concatenate(new_palettes)

            # plt.subplot(121)
            # plt.imshow(Common.nlab2rgb(new_palette.reshape((-1, n_colors, n_chas))), interpolation='nearest')
            # plt.subplot(122)
            # plt.imshow(Common.nlab2rgb(orig_palettes), interpolation='nearest')
            # plt.show()

            new_image, lle_params = \
                colorizer.colorize_quick(prm['image_nlab'], prm['seg_masks'], prm['seg_mean_colors'],
                                         prm['seg_color_labels'], prm['orig_palette'],
                                         new_palette, prm['lle_params'], prm['lmda'], ann_per=0.2)
        elif mode == 'coloropt':
            from smartpalette.coloropt.Colorizer import ColorOptColorizer

            _, prm = \
                prepare_colorizer(imagefile, aligner=aligner, n_segments=n_segments, n_colors=n_colors, mode='multi',
                                  use_spatial=True, scale=scale, slic_compactness=space_color_ratio, vis=vis)

            orig_palettes = prm['orig_palette'].reshape((-1, n_colors, n_chas))
            new_palettes = []
            for ox in range(len(orig_palettes)):

                # Xnew, _ = m.infer_newX(orig_palettes[ox].reshape((1, -1)), optimize=latent_opt)
                # Ynew = m.posterior_samples(Xnew, size=1, full_cov=False)

                Xnew, _ = my_infer_newX(m, orig_palettes[ox].reshape((1, -1)),
                                        optimize=latent_opt, init='L2', randomize_x=False, max_iters=1000)
                Ynew, _ = m.predict(Xnew.mean, full_cov=True)
                new_palettes.append(Ynew.reshape((n_colors, n_chas)))

            new_palette = np.concatenate(new_palettes)

            if vis:
                figname = '%s_%s_%d_%.2f_%s_palettes' % (fn, mode, n_segments, ann_per_lle, artist)

                fig1 = plt.figure(figname)
                ax1 = fig1.add_subplot(121)
                ax1.imshow(Common.nlab2rgb(new_palette.reshape((-1, n_colors, n_chas))), interpolation='nearest')
                ax2 = fig1.add_subplot(122)
                ax2.imshow(Common.nlab2rgb(orig_palettes), interpolation='nearest')

                fig1.savefig(join(rpath, figname + '.pdf'))
                # plt.show()

            # "smoothen" the new palette before applying colotopt, change ann_per(s), n_neighbors for different results
            # # ann_per_lle = 0.2
            ann_per_coloropt = 0.1
            n_neighbors = n_segments * n_colors / 3

            # n_pals = len(new_palette)
            # new_palette[np.random.permutation(n_pals)[:n_pals - int(n_pals * ann_per_lle)]] = 0
            # orig_palette = orig_palettes.reshape((-1, n_chas))
            # colorizer = Colorizer(n_neighbors=n_neighbors)
            # new_palette = colorizer.colorize_lle(orig_palette, new_palette, lmda=0.4, params=None, sparse=False)

            # # new_palette[:, 0] = orig_palette[:, 0]

            # Now apply coloropt
            image_rgb = Common.nlab2rgb(prm['image_nlab'])
            colorizer = ColorOptColorizer(10, 10, ann_per=ann_per_coloropt)
            new_image = colorizer.colorize(image_rgb, prm['seg_masks'], Common.nlab2rgb(new_palette[None, ...]))
        elif mode == 'compl':
            # NOT USED
            # colorizer, prm = \
            #     prepare_colorizer(imagefile, aligner=aligner, n_segments=n_segments, n_colors=n_colors, mode='single')
            #
            # input_pal = np.empty((1, n_colors, n_chas))
            # input_pal[...] = np.NaN
            # input_pal[0, :len(inputs), :] = inputs
            # new_inputpal, newids = FeatureAlignment.align_colors_naive(input_pal, prm['orig_palette'][None, :],
            #                                                            match_method='munkres', n_examples=1
            #                                                            )
            # Xnew, _ = m.infer_newX(new_inputpal.reshape((1, -1)), optimize=False)
            # Ynew, _ = m.predict(Xnew, full_cov=True)
            # new_palette = Ynew.reshape((n_colors, n_chas))
            # NOT USED

            colorizer, prm = \
                prepare_colorizer(imagefile, aligner=aligner, n_segments=n_segments, n_colors=n_colors, mode='multi',
                                  use_spatial=True, scale=scale, slic_compactness=space_color_ratio, vis=vis)

            # orig_palettes = prm['orig_palette'].reshape((-1, n_colors, n_chas))
            annotation = prm['orig_palette'].copy()

            for j in range(len(annotation)):
                dists = [euc(inputs[i], prm['orig_palette'][j]) for i in range(len(inputs))]
                if np.min(dists) < 0.4:
                    annotation[j] = inputs[np.argmin(dists)]

            colorizer1 = Colorizer(n_components=2, n_neighbors=12, reg=4e-1, method='standard')
            orig_palettes = \
                colorizer1.colorize_lle(prm['orig_palette'], annotation, lmda=0.4, sparse=False, params=None).\
                    reshape((-1, n_colors, n_chas))

            new_palettes = []
            for ox in range(len(orig_palettes)):
                Xnew, _ = m.infer_newX(orig_palettes[ox].reshape((1, -1)), optimize=latent_opt)
                Ynew, _ = m.predict(Xnew, full_cov=True)
                new_palettes.append(Ynew.reshape((n_colors, n_chas)))
            new_palette = np.concatenate(new_palettes)

            fig = plt.figure('palettes')
            ax = fig.add_subplot(121)
            ax.imshow(Common.nlab2rgb(new_palette.reshape((-1, n_colors, n_chas))), interpolation='nearest')
            ax = fig.add_subplot(122)
            ax.imshow(Common.nlab2rgb(orig_palettes), interpolation='nearest')
            # plt.show()

            new_image, lle_params = \
                colorizer.colorize_quick(prm['image_nlab'], prm['seg_masks'], prm['seg_mean_colors'],
                                         prm['seg_color_labels'], prm['orig_palette'],
                                         new_palette, prm['lle_params'], prm['lmda'])
        elif mode == 'single':
            colorizer, prm = \
                prepare_colorizer(imagefile, aligner=aligner, n_segments=n_segments, n_colors=n_colors, mode='single')

        figname = '%s_%s_%d_%.2f_%s' % (fn, mode, n_segments, ann_per_lle, artist)
        fig2 = plt.figure(figname)
        ax = fig2.add_subplot(121)
        ax.imshow(new_image, interpolation='nearest')

        ax = fig2.add_subplot(122)
        ax.imshow(prm['image'], interpolation='nearest')

        fig2.savefig(join(rpath, figname + '.pdf'))

        if vis:
            plt.show()


if __name__ == '__main__':
    import Common
    from skimage.transform import rescale

    # imagefile = join(Common.PATH, 'images', 'decorative', 'image_47.png')
    # imagefile = join(Common.PATH, 'images', 'decorative', 'image_25.jpg')
    imagefile = join(Common.PATH, 'images', 'distinctive', 'image_5.jpg')
    # imagefile = join(Common.PATH, 'images', 'benchmark_unstylized', 'image_3.jpg')

    # input_pal = Common.rgb2nlab(np.array([[[255, 215, 157],[167, 117, 71],[122,118,61]]]) / 255.)[0]
    # input_pal = Common.rgb2nlab(np.array([[[101, 215, 0], [193, 233, 0], [255, 254, 30]]]) / 255.)[0]
    colorize_with_manifold(imagefile, ['beach'], inputs=None, n_colors=15, # ['renoir', 'cezanne', 'luce', 'urbino']
                           n_segments=20, mode='coloropt', latent_opt=True,
                           ann_per_lle=0.3, vis=True, scale=0.5,
                           space_color_ratio=10., palette_sorter='binary')

    exit()

    from smartpalette.coloropt.Colorizer import colorize
    from skimage.color import rgb2gray, gray2rgb
    from skimage import img_as_float
    from skimage.transform import resize

    source_image = gray2rgb(rgb2gray(imread(join(Common.PATH, 'images', 'decorative', 'image_24.jpg'))))
    h, w, d = source_image.shape
    label_images = [join(Common.PATH, 'results', 'context_specific', 'figure_2.%d.png' % i) for i in range(1, 8)]
    fig = plt.figure('recolorized images')
    c = 1
    for lif in label_images:
        li = img_as_float(imread(lif)[:, :, :3])
        li = resize(li, source_image.shape)

        # X, Y = list(np.random.randint(0, w, 100)), list(np.random.randint(0, h, 100))
        # il = source_image.copy()
        # il[Y, X, :] = li[Y, X, :]
        il = li

        assert (li.shape == source_image.shape)
        new_img = colorize(source_image, il, 7, 7)

        ax = fig.add_subplot(1, len(label_images), c)
        ax.imshow(new_img, interpolation='nearest')
        ax.axis('off')

        # ax = fig.add_subplot(1, 3, 2)
        # ax.imshow(il, interpolation='nearest')
        #
        # ax = fig.add_subplot(1, 3, 3)
        # ax.imshow(li, interpolation='nearest')
        c += 1

    plt.show()