import GPy
from GPy.inference.latent_function_inference.inferenceX import InferenceX
from GPy.core import Model
from GPy.core.parameterization import variational
from GPy.util.linalg import tdot
from GPy.core.parameterization.variational import VariationalPosterior

import numpy as np


def my_infer_newX(model, Y_new, optimize=True, init='L2', max_iters=1000, randomize_x=False):
    infr_m = MyInferenceX(model, Y_new, init=init, randomize_x=randomize_x)

    if optimize:
        infr_m.optimize(max_iters=max_iters)

    return infr_m.X, infr_m


class MyInferenceX(InferenceX):
    def __init__(self, model, Y, name='inferenceX', init='L2', randomize_x=False):
        self.randomize_x = randomize_x
        super(MyInferenceX, self).__init__(model, Y, name, init)

    def _init_X(self, model, Y_new, init='L2'):
        Y = model.Y
        if self.missing_data:
            Y = Y[:,self.valid_dim]
            Y_new = Y_new[:,self.valid_dim]
            dist = -2.*Y_new.dot(Y.T) + np.square(Y_new).sum(axis=1)[:,None]+ np.square(Y).sum(axis=1)[None,:]
        else:
            if init=='L2':
                dist = -2.*Y_new.dot(Y.T) + np.square(Y_new).sum(axis=1)[:,None]+ np.square(Y).sum(axis=1)[None,:]
            elif init=='NCC':
                dist = Y_new.dot(Y.T)
            elif init=='rand':
                dist = np.random.rand(Y_new.shape[0],Y.shape[0])

        if not self.randomize_x:
            idx = dist.argmin(axis=1)
        else:
            _id = np.random.randint(0, dist.shape[1] / 4)
            idx = np.argsort(dist, axis=1)[:, _id]

        from GPy.models import SSGPLVM
        from GPy.util.misc import param_to_array

        if isinstance(model, SSGPLVM):
            X = variational.SpikeAndSlabPosterior(param_to_array(model.X.mean[idx]), param_to_array(model.X.variance[idx]), param_to_array(model.X.gamma[idx]))
            if model.group_spike:
                X.gamma.fix()
        else:
            if self.uncertain_input and self.sparse_gp:
                X = variational.NormalPosterior(param_to_array(model.X.mean[idx]), param_to_array(model.X.variance[idx]))
            else:
                from GPy.core import Param
                X = Param('latent mean',param_to_array(model.X[idx]).copy())

        return X