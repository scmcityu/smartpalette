'''
Created on 29 Oct, 2014

@author: phan
'''
from skimage.io import imread, imsave
import Common as cm
from os.path import join
import os
import numpy as np
from LLE import LocallyLinearEmbedding, barycenter_weights
from sklearn.utils import check_array
from skimage.segmentation import slic
from skimage.color import rgb2hsv, hsv2rgb
from FeatureAlignment import align_colors_naive
from sklearn.cluster import KMeans
from matplotlib import pyplot as plt


class Colorizer(LocallyLinearEmbedding):
    '''

    "classdocs"

    '''

    def __init__(self, model, n_channels=3, n_query_colors=400, patch_size=200, \
    n_components=2, n_neighbors=12, reg=4e-1):
        '''
        Constructor
        
        works best with method = 'modified'
        '''
        super(Colorizer, self).__init__(eigen_solver='auto', n_components=n_components, \
        n_neighbors=n_neighbors, reg=reg, method='standard')
        
        self.model = model
        self.patch_size = patch_size
        self.n_channels = n_channels
        self.n_query_colors = n_query_colors #number of colors for querrying task
        
    def get_weights(self, X):
        X = check_array(X)
        ind = self.nbrs_.kneighbors(X, n_neighbors=self.n_neighbors,
                                    return_distance=False)
        weights = barycenter_weights(X, self.nbrs_._fit_X[ind],
                                     reg=self.reg)

        return weights

    def precompute_params(self, image, colorized_image, lmda=0.4, sparse=True):
        if sparse:
            from scipy.sparse import csr_matrix, eye, issparse, diags
            #from scipy.sparse.linalg import spsolve as solve
        else:
            #from scipy.linalg import solve
            from numpy import eye

        if len(image.shape) == 3:
            w, h, d = image.shape
        else:
            w, d = image.shape
            h = 1

        #newimage = np.zeros_like(image)
        pixels = image.reshape((w * h, d))
        try:
            self.fit(pixels)
        except:
            print 'Error!'
            return colorized_image

        #return colorized_image
        #Find LLE weights (barycenter weights)
        #W = self.get_weights(pixels)
        M = self.M #M = (I - W).T.dot(I - W)
        I = eye(len(pixels))

        #List of target colors
        G = colorized_image.reshape((w * h, d))

        #Set weights to non-zero rows in G (annotated image)
        #Lambda = I * lmda
        Lambda = np.array([np.any(row != 0) for row in G]).astype(np.int32) * lmda
        if sparse:
            Lambda = diags([Lambda], [0])
        else:
            Lambda = Lambda * I

        #prepare the linear program
        #A = ((I - W).T.dot(I - W) + Lambda)
        A = M + Lambda

        if sparse:
            G = csr_matrix(G)
        b = Lambda.dot(G)

        #print A.shape, b.shape, A.getformat(), b.getformat()
        #assert(issparse(A) and issparse(b))
        #solve the linear system and find new image
        if sparse:
            A = A.asformat('csc')
            b = b.asformat('csc')

        return A, b, M, Lambda, (w, h, d)

    def colorize_lle(self, image, colorized_image, lmda=0.4, sparse=True, params=None):
        '''
        Xiaowu Chen, Dongqing Zou, Qinping Zhao, and Ping Tan. 2012. Manifold preserving edit 
        propagation. ACM Trans. Graph. 31, 6, Article 132 (November 2012), 7 pages. 
        DOI=10.1145/2366145.2366151 http://doi.acm.org/10.1145/2366145.2366151
        '''
        if sparse:
            from scipy.sparse import csr_matrix, eye, diags
            from scipy.sparse.linalg import spsolve as solve
        else:
            from scipy.linalg import solve

        if params is None:
            A, b, M, Lambda, img_dims = self.precompute_params(image, colorized_image, lmda, sparse)
        else:
            A, b, M, Lambda, img_dims = params

        w, h, d = img_dims
        G = colorized_image.reshape((w * h, d))
        if sparse:
            G = csr_matrix(G)

        b = Lambda.dot(G)
        newimage = solve(A, b)
        if sparse:
            newimage = newimage.toarray().reshape((w, h, d))
        else:
            newimage = newimage.reshape((w, h, d))
        
        return newimage

    def _weighted_euclidean(self, x, y, ws):
        return np.sqrt((ws[0] * x[0] - y[0])**2 + ws[1] * (x[1] - y[1])**2 + ws[2] * (x[2] - y[2])**2)

    def colorize_quick(self, image_nlab, seg_masks, seg_mean_colors, seg_color_labels, orig_palette, new_palette,
                       params=None, lmda=0.4, method='sort', alignment_ids=None):
        '''
        quickly colorize entire image without segmentation. used for real-time visualization
        :param image_nlab: (N x M x 3) image
        :param seg_masks: segments
        :param seg_mean_colors: mean colors in each segment
        :param seg_color_labels: color labels for pixels in image_nlab (1 to len(orig_palette))
        :param orig_palette: palette extracted from image_nlab
        :param new_palette: new palette
        :param params: lle parameters
        :return: colorized image
        '''
        ncols = self.model.Y.shape[1] / 3
        n_channels = self.n_channels
        m = self.model
        centers = m.Y.reshape((-1, ncols, n_channels))
        annotation = np.zeros_like(seg_mean_colors)

        if method == 'realign':
            aligned_palette, newindices = align_colors_naive(orig_palette[None, ...], centers, match_method='munkres', n_examples=3)
            nids = np.argsort(newindices[0])
            for l in np.unique(seg_color_labels):
                if not np.isnan(aligned_palette[0][l]).any():
                    annotation[seg_color_labels == l] = new_palette[nids[l]]
        elif method == 'sort':
            sorted_seg_centers_order = [i[1] for i in sorted(zip(orig_palette, range(ncols)), key=lambda x: x[0][0])]
            sorted_palette_order = [i[1] for i in sorted(zip(new_palette, range(ncols)), key=lambda x: x[0][0])]
            for i in range(ncols):
                _sid = sorted_seg_centers_order.index(i)
                _pid = sorted_palette_order[_sid]
                annotation[seg_color_labels == i] = new_palette[_pid]
        elif method == 'none' and alignment_ids is not None:
            for l in np.unique(seg_color_labels):
                annotation[seg_color_labels == l] = new_palette[alignment_ids[l]]

        #original = np.array([image_nlab[seg].mean(axos=0) for seg in segments])
        if params is None:
            params = self.precompute_params(seg_mean_colors, annotation, lmda=lmda, sparse=False)

        new_colors = self.colorize_lle(seg_mean_colors, annotation, lmda=lmda, sparse=False, params=params)
        newimage = np.zeros_like(image_nlab)#image_nlab.copy()
        for sx in range(len(seg_masks)):
            newimage[seg_masks[sx]] = new_colors[sx]

        return cm.nlab2rgb(newimage), params
        
    def colorize_with_segments(self, image, segments, neighbors=None, n_clusters=True, use_lle=True, show_palette=False):
        '''
        segmentation-based colorization
        '''
        # original_image = image.copy()
        #original = image.copy()
        image = cm.rgb2nlab(image)
        newimage = np.zeros_like(image)
        w, h, d = image.shape
        # n_colors = 1000 #self.n_colors
        n_segments = np.unique(segments)
        
        ncols = self.model.Y.shape[1] / 3
        n_channels = self.n_channels

        # , n_jobs=4, n_init=4).\
        # centers = KMeans(n_clusters=40).fit(self.model.Y).cluster_centers_.reshape((-1, ncols, n_channels))
        centers = self.model.Y.reshape((-1, ncols, n_channels))

        print "No segments = %d" % len(n_segments)
        # if os.path.isfile('.tmp/tmp.npy'):
        #     newimage = np.load('.tmp/tmp.npy')
        # else:
        scount = 0
        for sx in n_segments:
            segment = segments == sx
            print '.', 
            # now we use neighboring segments
            # could it be just the current segment?
            if neighbors is not None:
                nbs = np.nonzero(neighbors[sx])
                pixels = []
                for nb in nbs:
                    pixels = np.concatenate([pixels, image[ segments[nb] ]])
                pixels = pixels[::10]
            else:
                pixels = image[segment][::1]

            # n_jobs=4, n_init=4
            if len(pixels) <= ncols:
                px_centers = pixels
            else:
                px_centers = KMeans(n_clusters=ncols).fit(pixels).cluster_centers_
            aligned_px_centers, _ = align_colors_naive(px_centers[None, ...], centers, dupl=True)
            feat = aligned_px_centers[0].ravel()
            
            m = self.model
            xnew, newm = m.infer_newX(feat[None, :], optimize=True)
            
            if 'BayesianGPLVM' in str(m.__class__):
                new_palette, _ = m.predict(np.array(xnew.mean))
            else:
                new_palette, _ = m.predict(np.array(xnew))

            # now that we got the new palette,
            # the next step is to map original colors to palette colors
            new_palette = new_palette.reshape((-1, 3))
            new_palette = np.clip(cm.nlab2rgb(new_palette[None, ...]), 0, 1)
            new_palette = cm.rgb2nlab(new_palette)[0]
            
            indices = zip(*np.nonzero(segment))
            colors = np.zeros((len(indices), 3), dtype='double')

            km10 = KMeans(ncols)
            km10.fit_predict(image[segment])
            seg_centers = km10.cluster_centers_
            seg_labels = km10.labels_

            sorted_seg_centers_order = [i[1] for i in sorted(zip(seg_centers, range(ncols)), key=lambda x: x[0][0])]
            sorted_palette_order = [i[1] for i in sorted(zip(new_palette, range(ncols)), key=lambda x: x[0][0])]
            if show_palette:
                #'''
                ax = plt.subplot( len(n_segments), 2, scount * 2  + 1)
                ax.axis('off')

                ax.imshow(cm.nlab2rgb(new_palette[None, sorted_palette_order, :]), interpolation='nearest')
                ax = plt.subplot(len(n_segments), 2, scount * 2 + 2)
                ax.axis('off')
                ax.imshow(cm.nlab2rgb(seg_centers[None, sorted_seg_centers_order, :]), interpolation='nearest')
                scount += 1
                #'''

            for i in range(ncols):
                _sid = sorted_seg_centers_order.index(i)
                _pid = sorted_palette_order[_sid]
                colors[seg_labels == i] = new_palette[_pid]

            '''
            for color in new_palette:
                for idx, ids in enumerate(indices):
                    i, j = ids
                    dist = self._weighted_euclidean(image[i, j], color, [1.0, 0.0, 0.0])
                    if dist < 5e-2:
                        colors[idx] = color
            '''
            '''
            for idx, ids in enumerate(indices):
                i, j = ids
                dists = [self._weighted_euclidean(image[i, j], color, [1.0, 2.0, 2.0]) for color in new_palette]
                # only annotate a few pixels to prepare for colorize_lle
                if np.min(dists) < 5e-2:
                    colors[idx] = new_palette[np.argmin(dists)]
                    #colors[idx] = new_palette[np.argsort(dists)[np.random.randint(3)]]
            '''
            perm = np.random.permutation(len(indices))
            if use_lle:
                colors[perm[min(100, len(indices)):]] = 0
            newimage[segment] = colors

        if show_palette:
            plt.tight_layout(pad=0.01)
            plt.show()
        if not use_lle:
            newimage[:, :, 0] = image[:, :, 0].copy()
            imsave(join(cm.PATH, 'tmp.png'), np.clip(cm.nlab2rgb(newimage), 0, 1))
            return np.clip(cm.nlab2rgb(newimage), 0, 1)

        elif n_clusters != 0:
            # n_jobs=2, n_init=4
            km_ = KMeans(n_clusters=n_clusters)
            X = image.reshape((w * h, d))
            print 'clustering ', len(X[::2])
            km_.fit(X[::2])
            labels = km_.predict(X)
            centers = km_.cluster_centers_
            anns = np.zeros_like(centers)
            for i in range(n_clusters):
                tmp = newimage.reshape((w * h, d))[labels == i]
                if np.all(tmp == 0):
                    anns[i] = 0
                else:
                    anns[i] = tmp[np.any(tmp, axis=1)].mean(axis=0)
            
            print 'colorizing'
            newcenters = self.colorize_lle(centers, anns, sparse=False).reshape((n_clusters, d))
            newimage = image.copy()
            for i in range(n_clusters):
                newimage.reshape((w * h, d))[labels == i] = newcenters[i]
        else:    
            newimage = self.colorize_lle(image, newimage)
            
        return np.clip(cm.nlab2rgb(newimage), 0, 1)
     
'''
def find_all_neighbors(labelled_img):
    #boundaries = find_boundaries(labelled_img)
    labels = np.unique(labelled_img)
    pos = np.zeros((labelled_img.shape[0], labelled_img.shape[1], 2))
    for row in range(pos.shape[0]):
        for col in range(pos.shape[1]):
            pos[row, col, :] = np.array([row, col])
    
    adj_mat = np.zeros((len(labels), len(labels)), dtype='int32')
    adj_mat += -1
    np.fill_diagonal(adj_mat, 0)
       
    for i in labels:
        print '.',
        set1 = pos[labelled_img == i]
        for j in labels:
            if j != i and adj_mat[i, j] == -1:
                set2 = pos[labelled_img == j]
                done = False
                if not done:
                    for p1 in set1:
                        if not done:
                            for p2 in set2:
                                if abs(p1[0] - p2[0]) <=5 and abs(p1[1] - p2[1]) <= 5:
                                    adj_mat[i, j] = 1
                                    adj_mat[j, i] = 1
                                    done = True
                                    break
                if not done:
                    adj_mat[i, j] = 0
                    adj_mat[j, i] = 0
                    
    return adj_mat
'''
   
    
if __name__ == '__main__':
    from skimage import img_as_float
    from Common import PATH
    
    COLLECTION = 'BSDS500'
    DATADIR = 'data'
    IMGDIR = 'images/decorative'
    LEVEL = 0
    N_COLORS = 7

    modelfile = join(PATH, DATADIR, COLLECTION, 'models_nc%d_l%d.npz' % (N_COLORS, LEVEL))
    datafile = join(PATH, DATADIR, COLLECTION, 'clusters_nc%d_l%d.npz' % (N_COLORS, LEVEL))
    imgpath = join(PATH, IMGDIR)

    files = sorted([f for f in os.listdir(imgpath)
                    if os.path.isfile(join(imgpath, f)) and f.endswith('.jpg')])
     
    model = None
    if os.path.isfile(datafile):
        data = np.load(datafile)

    if os.path.isfile(modelfile):
        models = np.load(modelfile)
        model = models['concat_models'].tolist()

    # CACHE
    clz = Colorizer(model, n_components=2, n_neighbors=12)
    
    f = 'image_21.jpg'
    im = imread(join(imgpath, f))
    im = img_as_float(im)

    print 'processing file %s, size(%d,%d)' % (f, im.shape[0], im.shape[1])
    segments = slic(im, n_segments=8, compactness=10, sigma=1)
    neighbors = None
    im1 = clz.colorize_with_segments(im, segments, neighbors, n_clusters=160)
    imsave(join(PATH, 'colorization_results', COLLECTION + '_' + f.replace('jpg', 'png')), im1)
