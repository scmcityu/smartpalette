# -*- coding: utf-8 -*-
"""
Created on Sat Sep 27 09:13:23 2014

@author: phan
"""
from matplotlib import pyplot as plt
import numpy as np
from skimage.segmentation import mark_boundaries
from scipy.signal import argrelextrema
from skimage.color import lab2rgb
from mpl_toolkits.mplot3d import Axes3D

import matplotlib as mpl
import time

try:
    import GPy
    from GPy.core.parameterization.variational import VariationalPosterior
    from GPy.plotting.matplot_dep.visualize import *
except ImportError:
    pass

import Common as cm
from os.path import join
from smartpalette.Common import nlab2rgb
from smartpalette.ColorTransfer_ver2 import Colorizer
from skimage.io import imsave
from smartpalette import FeatureAlignment
from smartpalette.Utils import render_palettes


class colorizer_show(matplotlib_show):
    def __init__(self, vals, model, image_file, n_segments, method='sort', axes=None, dimensions=(16,16,3), transpose=False, order='C',
                 invert=False, scale=False, palette=[], preset_mean=0., preset_std=1., select_image=0, cmap=None, n_colors=7):
        matplotlib_show.__init__(self, vals, axes)
        self.dimensions = dimensions
        self.transpose = transpose
        self.order = order
        self.invert = invert
        self.scale = scale
        self.palette = palette
        self.preset_mean = preset_mean
        self.preset_std = preset_std
        self.select_image = select_image
        # This is used when the y vector contains multiple images concatenated.
        self.count = 1

        self.colorizer = None
        self.params = dict()
        self.params['lmda'] = 0.4
        self.params['model'] = model
        self.params['image_file'] = image_file
        self.params['method'] = method
        self._prepare_colorizer(n_segments=n_segments, n_colors=n_colors)

        self.set_image(self.vals)
        self.handle = self.axes.imshow(self.vals, interpolation='nearest') # @UndefinedVariable
        self.axes.axis('off')
        plt.show()

    def modify(self, vals):
        #print 'modify'
        self.set_image(vals.copy())
        self.handle.set_array(self.vals)
        self.axes.axis('off')
        self.axes.figure.canvas.draw()

    def set_image(self, vals):
        # print 'set_image'
        prm = self.params
        new_palette = vals[0, :].reshape((-1, 3))
        new_image, lle_params = self.colorizer.colorize_quick(
            prm['image_nlab'], prm['seg_masks'], prm['seg_mean_colors'],
            prm['seg_color_labels'], prm['orig_palette'],
            new_palette, prm['lle_params'], prm['lmda'], method=prm['method'],
            alignment_ids=prm['alignment_ids'])

        self.params['lle_params'] = lle_params
        # pal_img = render_palettes(new_palette[None, ...], )
        print(new_palette)
        self.vals = np.clip(new_image, 0, 1)

    def _prepare_colorizer(self, n_segments=40, n_colors=7):
        from sklearn.cluster import KMeans
        from skimage.io import imread
        from skimage import img_as_float

        N_SEGS = n_segments
        N_COLS = n_colors

        self.colorizer = Colorizer(self.params['model'], n_channels=3, n_query_colors=400,
                                   patch_size=200, n_components=2, n_neighbors=12, reg=4e-1)

        self.params['image'] = imread(self.params['image_file'])[:, :, :3]
        self.dimensions = self.params['image'].shape
        image_nlab = cm.rgb2nlab(self.params['image'])
        self.params['image_nlab'] = image_nlab

        km = KMeans(n_clusters=N_SEGS)
        labels = km.fit_predict(image_nlab.reshape((-1, 3)))
        labels = labels.reshape(image_nlab.shape[:2])
        seg_masks = []
        for sid in range(N_SEGS):
            seg_masks.append(labels == sid)

        self.params['seg_masks'] = seg_masks
        self.params['seg_mean_colors'] = km.cluster_centers_

        km1 = KMeans(N_COLS)
        self.params['seg_color_labels'] = km1.fit_predict(km.cluster_centers_)
        self.params['orig_palette'] = km1.cluster_centers_
        self.params['lle_params'] = None

        _, newids = FeatureAlignment.align_colors_naive(km1.cluster_centers_[None, ...],
                                                        self.params['model'].Y.reshape((-1, n_colors, 3)),
                                                        match_method='munkres', n_examples=3)

        self.params['alignment_ids'] = np.argsort(newids[0])
        # lle_params = self.colorizer.precompute_params(self.params['image_nlab'],
        # colorized_image, lmda=0.4, sparse=True)
        # self.params['lle_params'] = lle_params


class color_image_show(matplotlib_show):
    def __init__(self, vals, axes=None, dimensions=(16,16,3), transpose=False, order='C',
                 invert=False, scale=False, palette=[], preset_mean=0., preset_std=1., select_image=0, cmap=None):
        matplotlib_show.__init__(self, vals, axes)
        # super(color_image_show, self).__init__(vals, axes)
        self.dimensions = dimensions
        self.transpose = transpose
        self.order = order
        self.invert = invert
        self.scale = scale
        self.palette = palette
        self.preset_mean = preset_mean
        self.preset_std = preset_std
        self.select_image = select_image # This is used when the y vector contains multiple images concatenated.
        self.count = 1
        
        self.set_image(self.vals)
        self.handle = self.axes.imshow(self.vals, interpolation='nearest') # @UndefinedVariable
        self.axes.axis('off')
        plt.show()

    def modify(self, vals):
        #print 'modifying'
        self.set_image(vals.copy())
        self.handle.set_array(self.vals)
        self.axes.figure.canvas.draw()

    def set_image(self, vals):
        #print 'setting vals'
        #dim = self.dimensions[0] * self.dimensions[1]
        self.vals = np.reshape(vals[0,:], self.dimensions, order=self.order)
        self.vals = np.clip(cm.nlab2rgb(self.vals), 0, 1)
        
        newimage = np.zeros((50 * self.dimensions[0], 50 * self.dimensions[1], 3))
        for v in range(self.vals.shape[1]):
            newimage[:50, v * 50: v * 50 + 50] = self.vals[0, v]
            
        #imsave(join(Common.PATH, 'tmp', 'tmp_%d.png') % self.count, newimage)
        #fig = plt.figure(tight_layout=True)
        #ax = fig.add_subplot(111)
        #ax.imshow()
        self.count += 1
        #if self.transpose:
        #    self.vals = self.vals.T

        #if self.invert:
        #    self.vals = -self.vals

        # un-normalizing, for visualisation purposes:
        #self.vals = self.vals * self.preset_std + self.preset_mean


def vis_artist_level(artist, clusters=[0], step=1, n_colors=5, level=0, show='orig', path='/media/phan/SMALLDATA1/DPAINTINGS/', data_dir='data'):
    data = np.load(join(path, '%s/%s/clusters_nc%d_l%d.npz' % (data_dir, artist, n_colors, level) ))
    im = None    
    if 'normalized_clustered_lab' in data:
        im = data['normalized_clustered_lab']
    order = data['concat_order']
    #im0 = data['clustered_nlab'][::4] #/ 255.
    im3 = data['concat_lab']
    labels = data['labels']
    labels1 = labels[::step][order] 
    labels2 = np.sort(labels[::step])
    axs = []
    fig = plt.figure(artist)
    ncols = labels.max() + 1
    if clusters is None:
        clusters = range(len(np.unique(labels)))

    n_clusters = len(clusters)
    #for ax in range(len(clusters)):
    #    axs.append(fig.add_subplot(1, ncols, ax+1))

    for i in range(n_clusters):
        ii = clusters[i]
        ax = fig.add_subplot(1, n_clusters + 1, i + 1, title=str(ii))
        if show == 'pca':
            ax.imshow(nlab2rgb(im3[labels1==ii]), interpolation='nearest')
        elif show == 'orig':
            ax.imshow(nlab2rgb(im[labels==ii]), interpolation='nearest')
        elif show == 'cluster':
            ax.imshow(nlab2rgb(im3[labels2==ii]), interpolation='nearest')

    fig.show()


def vis_artist(artist, step=1, show='orig', path='/media/phan/BIGDATA/DPAINTINGS/'):
    data = np.load(join(path, 'data/%s/dpaintings.npz' % artist))
    im = data['orig_lab']
    order = data['concat_order']
    #im0 = data['clustered_nlab'][::4] #/ 255.
    im3 = data['concat_lab']
    labels = data['labels']
    labels1 = labels[::step][order] 
    labels2 = np.sort(labels[::step])
    axs = []
    fig = plt.figure(artist)
    ncols = labels.max() + 1
    for ax in range(ncols):
        axs.append(fig.add_subplot(1, ncols, ax+1))

    for ax in range(ncols):
        if show == 'pca':
            axs[ax].imshow(nlab2rgb(im3[labels1==ax]), interpolation='none')
        elif show == 'orig':
            axs[ax].imshow(nlab2rgb(im[labels==ax]), interpolation='none')
        elif show == 'cluster':
            axs[ax].imshow(nlab2rgb(im3[labels2==ax]), interpolation='none')

    fig.show()


def viz_segments(image, segments):
    fig, ax = plt.subplots(1, 1)
    fig.set_size_inches(8, 3, forward=True)
    #fig.subplots_adjust(0.05, 0.05, 0.95, 0.95, 0.05, 0.05)
    
    ax.imshow(mark_boundaries(image, segments))
    ax.set_title("Felzenszwalbs's method")

    #for a in ax:
    ax.set_xticks(())
    ax.set_yticks(())
    plt.show()


def viz_labcolor(colors, ax):
    img = []
    for cc in colors:
        line = []
        for c in cc:
            line.append([c[0]*100, c[1] * 255 - 128, c[2] * 255 - 128])
        img.append(line)
        
    img = lab2rgb( np.array(img) )
    ax.imshow(img, interpolation='nearest')
    #plt.show()
    

if __name__ == '__main__':
    from Common import PATH
    # vis_artist('cezanne', 2, 'pca', level)
    vis_artist_level('renoir', clusters=range(0, 10, 1), step=1, n_colors=7, level=0, show='pca', path=PATH, data_dir='data')
    raw_input('')