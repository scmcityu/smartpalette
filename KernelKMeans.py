"""Kernel K-means"""

# Author: Mathieu Blondel <mathieu@mblondel.org>
# License: BSD 3 clause

import numpy as np

from sklearn.base import BaseEstimator, ClusterMixin
from sklearn.metrics.pairwise import pairwise_kernels
from sklearn.utils import check_random_state
from sklearn.cluster import KMeans
import os
import math
import Common 
import platform
if platform.system() != 'Windows' and platform.system() != "Darwin":
    from features import PMK

FINEST_SIDE_LENGTH = 0.01
SIDE_LENGTH_FACTOR = 1.5
DISCRETIZE_ORDER = 4


class KernelKMeans(BaseEstimator, ClusterMixin):
    """
    Kernel K-means
    
    Reference
    ---------
    Kernel k-means, Spectral Clustering and Normalized Cuts.
    Inderjit S. Dhillon, Yuqiang Guan, Brian Kulis.
    KDD 2004.
    """

    def __init__(self, n_clusters=3, max_iter=50, tol=1e-5, random_state=None,
                 kernel="linear", gamma=None, degree=3, coef0=1,
                 kernel_params=None, verbose=0):
        self.n_clusters = n_clusters
        self.max_iter = max_iter
        self.tol = tol
        self.random_state = random_state
        self.kernel = kernel
        self.gamma = gamma
        self.degree = degree
        self.coef0 = coef0
        self.kernel_params = kernel_params
        self.verbose = verbose
        #self.centers_ = None
        
    @property
    def _pairwise(self):
        return self.kernel == "precomputed"
        
    def normalize_kernel(self, kernel):
        size = len(kernel)
        diag = np.diag(kernel)
        newkernel = kernel.copy()
        for i in range(size):
            for j in range(i + 1):
                normval = newkernel[i, j] / math.sqrt(diag[i] * diag[j])
                newkernel[i, j] = normval
                newkernel[j, i] = normval
        
        return newkernel

    def _get_kernel(self, X, Y=None):
        if callable(self.kernel):
            params = self.kernel_params or {}
        else:
            params = {"gamma": self.gamma,
                      "degree": self.degree,
                      "coef0": self.coef0}
                      
        return pairwise_kernels(X, Y, metric=self.kernel,
                                filter_params=False, **params)

    def fit(self, X, y=None, sample_weight=None):
        n_samples = len(X) #X.shape[0]

        K = self._get_kernel(X)
        
        sw = sample_weight if sample_weight else np.ones(n_samples)
        self.sample_weight_ = sw

        rs = check_random_state(self.random_state)
        self.labels_ = rs.randint(self.n_clusters, size=n_samples)

        dist = np.zeros((n_samples, self.n_clusters))
        self.within_distances_ = np.zeros(self.n_clusters)

        for it in xrange(self.max_iter):
            dist.fill(0)
            self._compute_dist(K, dist, self.within_distances_, update_within=True)
            labels_old = self.labels_
            self.labels_ = dist.argmin(axis=1)
            
            # Compute the number of samples whose cluster did not change 
            # since last iteration.
            n_same = np.sum((self.labels_ - labels_old) == 0)
            score = 1 - float(n_same) / n_samples
            if self.verbose:
                print 'score = ', score
                
            if score < self.tol:
                if self.verbose:
                    print "Converged at iteration", it + 1
                
                self.centers_ = dist.argmin(axis=0)
                break

        self.X_fit_ = X

        return self

    def _compute_dist(self, K, dist, within_distances, update_within):
        """Compute a n_samples x n_clusters distance matrix using the 
        kernel trick."""
        sw = self.sample_weight_

        for j in xrange(self.n_clusters):
            mask = self.labels_ == j

            if np.sum(mask) == 0:
                raise ValueError("Empty cluster found, try smaller n_cluster.")

            denom = sw[mask].sum()
            denomsq = denom * denom

            if update_within:
                KK = K[mask][:, mask]  # K[mask, mask] does not work.
                dist_j = np.sum(np.outer(sw[mask], sw[mask]) * KK / denomsq)
                within_distances[j] = dist_j
                dist[:, j] += dist_j
            else:
                dist[:, j] += within_distances[j]

            dist[:, j] -= 2 * np.sum(sw[mask] * K[:, mask], axis=1) / denom

    def predict(self, X):
        K = self._get_kernel(X, self.X_fit_)
        n_samples = len(X) #.shape[0]
        dist = np.zeros((n_samples, self.n_clusters))
        self._compute_dist(K, dist, self.within_distances_,
                           update_within=False)
                           
        return dist.argmin(axis=1)
        
    def soft_predict(self, X):
        K = self._get_kernel(X, self.X_fit_)
        n_samples = len(X) #.shape[0]
        dist = np.zeros((n_samples, self.n_clusters))
        self._compute_dist(K, dist, self.within_distances_,
                           update_within=False)
        
        re = []
        for i in range(len(dist)):
            re.append( sorted( zip(range(self.n_clusters), dist[i]), key = lambda x: x[1] )[0] )
        
        return re


def find_best_n_clusters_1(data, kernel, max_themes=100, kernel_params={}):
    """
    :param kernel:
    :param max_themes:
    :return: (kkm, n_themes, labels)
    """
    for n_themes in xrange(max_themes, 2, -1):
        kkm = KernelKMeans(n_clusters=n_themes, kernel=kernel, kernel_params=kernel_params, max_iter=2000,  verbose=0, random_state=None, gamma=4, tol=1e-5)
        OK = False
        for i in range(5):
            try:
                labels = kkm.fit_predict(data)
                OK = True
                break
            except Exception as e:
                pass
                #print '#',

        if OK:
            break

    if not OK:
        print 'Clustering failed!'
        return None

    return kkm, n_themes, labels


def find_best_n_clusters(kernel, max_themes=100):
    """
    :param kernel:
    :param max_themes:
    :return: (kkm, n_themes, labels)
    """
    for n_themes in xrange(max_themes, 2, -1):
        kkm = KernelKMeans(n_clusters=n_themes, kernel="precomputed", max_iter=2000,  verbose=0, random_state=None, gamma=4, tol=1e-5)
        OK = False
        for i in range(5):
            try:
                labels = kkm.fit_predict(kernel)
                OK = True
                break
            except Exception as e:
                pass
                #print '#',
        
        if OK:
            break
        
    if not OK:
        print 'Clustering failed!'
        return None
        
    return kkm, n_themes, labels


def cluster_by_structure(n_colors, collections, step=1, level=0):
    from os.path import join
    from Common import PATH
    import Kernels

    for collection in collections:
        data_path = join(PATH, 'data', collection)
        if not os.path.isfile(join(data_path, 'dpaintings%d.npz' % n_colors)):
            continue

        data = np.load(join(data_path, 'dpaintings%d.npz' % n_colors))
        clustered_lab = data['clustered_lab'][level][::step]
        normalized_clustered_lab = Common.normalize_lab(clustered_lab)
        training_data = normalized_clustered_lab.copy()
        means = training_data.mean(axis=1)
        for i in range(len(training_data)):
            training_data[i] -= means[i]

        kernel = Kernels.hausdorff_kernel_matrix(training_data)
        kkm, n_themes, labels = find_best_n_clusters(kernel, max_themes=100)

        np.savez(join(data_path, 'clusters_nc%d_l%d.npz' % (n_colors, level)),
                 normalized_clustered_lab=normalized_clustered_lab, clustered_lab=clustered_lab,
                 kkm=kkm, labels=labels, n_themes=n_themes)


def main(n_colors, collections, step=1):
    from os.path import join
    from Common import PATH
    import Kernels

    DATADIR = 'data'
    IMGDIR = 'images'
    KERNEL = 'hausdorff' #hausdorff
    COMP_KERNEL = True
    
    for artist in collections:
        OPATH = join(PATH, DATADIR, artist)
        if not os.path.isfile(join(OPATH, 'dpaintings%d.npz' % n_colors)):
            continue
        
        print artist
        max_themes = 50 # number of color themes (a theme = a collection of similar palettes)
        data = np.load(join(OPATH, 'dpaintings%d.npz' % n_colors))
        all_cl_lab = data['clustered_lab']
        
        for level in range(len(all_cl_lab)):
            cl_lab = np.asfarray(all_cl_lab[level])
            if step == 'flexible':
                step = len(cl_lab) / 500
                
            print 'level = ', level, ' step = ', step
            if 'orig_rgb' in data and KERNEL == 'PMK':
                orig_rgb = data['orig_rgb'][::step].astype(float)
                
            trainX = Common.normalize_lab(cl_lab)[::step]
            # clustered_nlab = Common.normalize_lab(data['clustered_lab'])

            kern_file = join(OPATH, 'kernel_nc%d_l%d.npz' % (n_colors, level) )
            if not os.path.isfile(kern_file) or COMP_KERNEL:
                print 'computing kernel'
                if KERNEL == 'hausdorff':
                    kernel = Kernels.hausdorff_kernel_matrix(trainX)
                elif KERNEL == 'pmk':
                    kernel = PMK.compute(orig_rgb, method = 'uniform', finest_side_length=0.01, 
                                         side_length_factor=4, discretize_order=4, normalized=True)
                else:
                    params = {"gamma": 4,
                              "degree": 2,
                              "C": 0.5, 
                              "coef0": 1}                
                    tmpdata = trainX.reshape((-1, trainX.shape[1] * trainX.shape[2]))
                    kernel = pairwise_kernels(tmpdata, tmpdata, KERNEL, params)
                    
                np.savez(kern_file, kernel=kernel, X=trainX)
            else:
                data = np.load(kern_file)
                kernel = data['kernel']
        
            # scan for best number of themes
            for n_themes in xrange(max_themes, 2, -1):
                print '-',
                # kkm = KernelKMeans(n_clusters=n_themes, kernel="rbf", max_iter=2000,  verbose=1,
                # random_state=None, gamma=4)
                kkm = KernelKMeans(n_clusters=n_themes, kernel="precomputed", max_iter=2000,  verbose=0,
                                   random_state=None, gamma=4, tol=1e-5)
                OK = False
                for i in range(20):
                    try:
                        # labels = kkm.fit_predict(trainX.reshape((-1, trainX.shape[1] * trainX.shape[2])))
                        labels = kkm.fit_predict(kernel)
                        OK = True
                        break
                    except Exception as e:
                        print '#',
                
                if OK:
                    print 'best n_themes=', n_themes
                    break
    
            np.savez(join(OPATH, 'clusters_nc%d_l%d.npz' % (n_colors, level)),
                     normalized_clustered_lab=trainX, clustered_lab=cl_lab,
                     kkm=kkm, labels=labels, n_themes=n_themes)

        
if __name__ == '__main__':
    main(n_colors=7, collections=['gogh'])
