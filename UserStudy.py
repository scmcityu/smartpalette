import numpy as np
import subprocess
import os
from os.path import join
from Common import PATH
import re


def gen_ordering(n_items=10, n_samples=20):
    a = np.array([0] * (n_items / 2) + [1] * (n_items / 2))
    B = np.zeros((n_samples, n_items), dtype=np.int64)
    for i in range(n_samples):
        p = np.random.permutation(n_items)
        B[i, :] = a[p]

    return B


def gen_sorting_study(test_id=1, methods=['binary', 'lightness'], n_images=10, n_samples=1):
    labels = ['A', 'B']
    path = join(PATH, 'user_study', 'sorting_study')

    ordering = gen_ordering(n_images, n_samples)[0] # np.int_(np.loadtxt(join(path, '%d.txt' % test_id))[0])
    np.savetxt(join(path, '%d.txt' % test_id), ordering, fmt='%d')

    print('ORDERING: ' + str(ordering))

    mpath = join(path, str(test_id), 'montage')
    if not os.path.isdir(mpath):
        os.makedirs(mpath)

    ids = sorted(np.unique([int(re.findall(r"image_([0-9]+).png", fn)[0])
                 for fn in os.listdir(join(path, str(test_id))) if fn.endswith('.png')]))

    for ix, i in enumerate(ids):
        # images in the pre-defined order
        ims = ['%s_image_%d.png' % (methods[m], i) for m in [ordering[ix], 1 - ordering[ix]]]
        ims_concat = reduce(lambda x, y: join(mpath, x) + ' ' + join(mpath, y), ims)
        # annotating each image first
        for j, im in enumerate(ims):
            args1 = join(path, str(test_id), im) + \
                    " -gravity west -pointsize 60 -annotate 0 '%s' " \
                    % labels[j] + join(mpath, im)

            # args1 = join(path, str(test_id), im) + \
            #         " label:'%s' +swap -gravity Center -append " \
            #         % labels[j] + join(mpath, im)

            # print(args1)
            subprocess.call('convert ' + args1, shell=True)

        # now combine the images
        args2 = ims_concat + " -geometry 1000x -tile 1x2 " + join(mpath, 'combined_image_%d.png' % i)
        # print(args2)
        subprocess.call('montage ' + args2, shell=True)


if __name__ == '__main__':
    gen_sorting_study(test_id=3, methods=['binary', 'lightness'], n_images=10, n_samples=1)