# -*- coding: utf-8 -*-
"""
Created on Thu Dec 25 20:49:52 2014

@author: phan
"""
import networkx as nx
import numpy as np
from scipy.spatial.distance import euclidean as euc
from scipy.spatial.distance import wminkowski
from openopt import TSP
from sklearn.decomposition import KernelPCA
from sklearn.metrics.pairwise import euclidean_distances
from sklearn.cluster import KMeans
from matplotlib import pyplot as plt
from matplotlib import cm as cmap
from matplotlib.colors import ListedColormap
from os.path import join
import Common
import os, sys
from sklearn.decomposition import PCA
from KernelKMeans import KernelKMeans, find_best_n_clusters, find_best_n_clusters_1
from Kernels import hausdorff_kernel_matrix, hausdorff_cw, hausdorff_kernel_matrix_cw, hausdorff_kernel, hausdorff_dist
from smartpalette.pygco import cut_simple, cut_from_graph
from smartpalette.fast.Distance import hausdorff, hausdorff_with_dists
from itertools import product
from munkres import Munkres

try:
    from pyclust import KMedoids
except ImportError as ie:
    pass


def subtract_mean(array):
    array1 = array.copy()
    for a in array1:
        a += a.mean(axis=0)
    return array1


class Coordinator(object):
    def __init__(self):
        pass

    def build_graph(self, data):
        n_features, n_points, _ = data.shape
        G = nx.DiGraph()
        n_features = data.shape[0]
        n_points = data.shape[1]
        nodeids = np.arange(n_features * n_points).reshape((n_features, n_points))
        for center in range(n_features - 1):
            for color1 in range(n_points):
                for color2 in range(n_points):
                    if np.isnan(data[center, color1]).any() or np.isnan(data[center + 1, color2]).any():
                        w = 0
                    else:
                        w = euc(data[center, color1], data[center + 1, color2])  #np.e ** (-1 * (euc(data[center, color1], data[center + 1, color2]) ** 2) / (1 ** 2))

                    G.add_edge(nodeids[center, color1], nodeids[center + 1, color2], {'weight': w })

        return G, nodeids

    def arrange(self, data, viz=False):
        pass

    def visualize(self, old_data, new_data, style='clean'):
        h, w, d = new_data.shape
        G, nodeids = self.build_graph(old_data)
        fig = plt.figure()
        ax1 = fig.add_subplot(211)
        ax2 = fig.add_subplot(212)

        pos = []
        for row in range(nodeids.shape[0]):
            for col in range(nodeids.shape[1]):
                pos.append((row * nodeids.shape[1] + col, [row * 10, col * 10]))

        pos = dict(pos)
        # colorconv = lambda x: x
        # if new_data.max() < 1.01 and new_data.min() > 0:
        colorconv = Common.nlab2rgb
        # else:
        #     colorconv = Common.lab2rgb

        colorlist = np.clip(colorconv(new_data), 0, 1)
        old_colorlist = np.clip(colorconv(old_data), 0, 1)

        lcmap = ListedColormap(colors=colorlist.reshape((colorlist.shape[0] * colorlist.shape[1], 3)))
        old_lcmap = ListedColormap(colors=old_colorlist.reshape((old_colorlist.shape[0] * old_colorlist.shape[1], 3)))

        for _ax, _cmap in [(ax1, lcmap), (ax2, old_lcmap)]:
            if style == 'full':
                nx.draw(G, pos, ax=_ax, node_color=np.arange(h * w) / float(h * w), node_size=500, cmap=_cmap,
                        edge_color=[G.edge[e[0]][e[1]]['weight'] for e in G.edges()],
                        edge_cmap=cmap.get_cmap('jet'), edgelist=G.edges(), arrows=False)
            elif style == 'clean':
                nx.draw_networkx_nodes(G, pos, ax=_ax, node_color=np.arange(h * w)/float(h * w), cmap=_cmap,linewidths=0.1)

        # plt.tight_layout()
        plt.autoscale(True)
        plt.show()


class KMCoordinator(Coordinator):
    def __init__(self):
        super(KMCoordinator, self).__init__()

    def arrange(self, data, viz=False):
        h, w, d = data.shape
        colors = data.reshape((-1, d))
        km = KMeans(n_clusters=w)
        km.fit(colors)

        clusters = km.cluster_centers_[np.argsort(km.cluster_centers_[:, 0]) ]
        newids = np.zeros((h, w), dtype=int)
        tmpdata = data.copy()
        for i in range(w):
            for j in range(h):
                newids[j, i] = np.argmin([euc(clusters[i], tmpdata[j, c]) for c in range(w)])
                tmpdata[j, newids[j, i]] = [10, 10, 10]

        aligned_data = data[np.arange(h)[:, None], newids]
        if viz != 'none':
            self.visualize(data, aligned_data, style=viz)
        return aligned_data, newids


class StableMarriageCoordinator(Coordinator):
    def __init__(self):
        super(StableMarriageCoordinator, self).__init__()

    def arrange(self, data, viz=False):
        from sklearn.metrics.pairwise import euclidean_distances
        r"""
        rankings[(a, n)] = partner that a ranked n^th
        >>> from itertools import product
        >>> A = ['1','2','3','4','5','6']
        >>> B = ['a','b','c','d','e','f']
        >>> rank = dict()
        >>> rank['1'] = (1,4,2,6,5,3)
        >>> rank['2'] = (3,1,2,4,5,6)
        >>> rank['3'] = (1,2,4,3,5,6)
        >>> rank['4'] = (4,1,2,5,3,6)
        >>> rank['5'] = (1,2,3,6,4,5)
        >>> rank['6'] = (2,1,4,3,5,6)
        >>> rank['a'] = (1,2,3,4,5,6)
        >>> rank['b'] = (2,1,4,3,5,6)
        >>> rank['c'] = (5,1,6,3,2,4)
        >>> rank['d'] = (1,3,2,5,4,6)
        >>> rank['e'] = (4,1,3,6,2,5)
        >>> rank['f'] = (2,1,4,3,6,5)
        >>> Arankings = dict(((a, rank[a][b_]), B[b_]) for (a, b_) in product(A, range(0, 6)))
        >>> Brankings = dict(((b, rank[b][a_]), A[a_]) for (b, a_) in product(B, range(0, 6)))
        >>> rankings = Arankings
        >>> rankings.update(Brankings)
        >>> stable(rankings, A, B)
        [('1', 'a'), ('2', 'b'), ('3', 'd'), ('4', 'f'), ('5', 'c'), ('6', 'e')]
        """
        def _stable(rankings, A, B):
            partners = dict((a, (rankings[(a, 1)], 1)) for a in A)
            is_stable = False # whether the current pairing (given by `partners`) is stable
            while is_stable == False:
                is_stable = True
                for b in B:
                    is_paired = False # whether b has a pair which b ranks <= to n
                    for n in range(1, len(B) + 1):
                        a = rankings[(b, n)]
                        a_partner, a_n = partners[a]
                        if a_partner == b:
                            if is_paired:
                                is_stable = False
                                partners[a] = (rankings[(a, a_n + 1)], a_n + 1)
                            else:
                                is_paired = True
            return sorted((a, b) for (a, (b, n)) in partners.items())

        h, w, d = data.shape
        for pal in range(h - 1):
            dists = euclidean_distances(data[pal], data[pal+1])
            rank = np.zeros(w * 2, w)
            rank[:w] = np.argsort(dists, axis=0) + w
            rank[w:] = np.argsort(dists, axis=1)
            A = range(w)
            B = range(w, w * 2)
            Arankings = dict(((a, rank[a][b_]), B[b_]) for (a, b_) in product(A, range(0, w)))
            Brankings = dict(((b, rank[b][a_]), A[a_]) for (b, a_) in product(B, range(0, w)))
            rankings = Arankings
            rankings.update(Brankings)

            matches = _stable(rankings, A, B)
            matches = np.array(matches)
            matches[:, 1] -= w


class PCACoordinator(Coordinator):
    def __init__(self):
        super(PCACoordinator, self).__init__()

    def arrange(self, data, viz=False):
        h, w, d = data.shape
        pca = PCA(n_components=1)
        projected_data = pca.fit_transform(data.reshape((-1, d))).reshape((h, w))
        newids = projected_data.argsort(axis=1)

        aligned_data = data.copy()
        for i in range(h):
            aligned_data[i] = data[i][newids[i], :]

        #tsp = TspCoordinator(start_id=0)
        #aligned_data1, newids1 = tsp.arrange(aligned_data, viz='none')
        #for i in range(h):
        #    newids[i, :] = newids[i, newids1[i]]

        if viz != 'none' and viz is not False:
            self.visualize(data, aligned_data, style=viz)
        return aligned_data, newids


class LightnessCoordinator(Coordinator):
    def __init__(self, light_channel=0):
        super(LightnessCoordinator, self).__init__()
        self.light_channel = light_channel

    def arrange(self, data, viz='none'):
        h, w, d = data.shape
        newids = np.argsort(data[:, :, self.light_channel], axis=1)
        aligned_data = data.copy()
        for i in range(h):
            aligned_data[i] = data[i][newids[i], :]

        if viz != 'none' and viz is not False:
            self.visualize(data, aligned_data, style=viz)
        return aligned_data, newids


class HueCoordinator(Coordinator):
    def __init__(self, hue_channel=0):
        super(HueCoordinator, self).__init__()
        self.hue_channel = hue_channel

    def arrange(self, data, viz='none'):
        h, w, d = data.shape
        data = Common.rgb2hsv( Common.nlab2rgb(data))

        newids = np.argsort(data[:, :, self.hue_channel], axis=1)
        aligned_data = data.copy()
        for i in range(h):
            aligned_data[i] = data[i][newids[i], :]

        if viz != 'none' and viz is not False:
            self.visualize(data, aligned_data, style=viz)

        aligned_data = Common.rgb2nlab(Common.hsv2rgb(aligned_data))
        return aligned_data, newids


class MaxFlowCoordinator(Coordinator):
    def __init__(self):
        super(MaxFlowCoordinator, self  ).__init__()

    def arrange(self, nlab, viz=False, sigma=20):
        h, w, d = nlab.shape
        scale = 100
        data = np.asarray( nlab.copy() * scale, dtype=np.int32)
        rdata = data.reshape((-1, d))
        # n_palettes * n_colors * n_channels
        ids = np.arange(h * w).reshape((h, w))
        edges = []
        for row1 in range(h - 1):
            for col1 in range(w):
                for col2 in range(w):
                    edges.append(
                        [ids[row1, col1], ids[row1 + 1, col2],
                         np.int_((np.e ** (-(euc(data[row1, col1] , data[row1 + 1, col2]) ** 2) / (sigma ** 2)) ) * 10)])

        step = scale / w
        count = step / 2
        unaries = np.zeros((h * w, w), dtype=np.int32)
        for i in range(w):
            unaries[:, i] = np.abs(rdata[:, 0] - count)
            count += step

        pairwise = -1 * np.eye(w, dtype=np.int32)
        result = cut_from_graph(np.array(edges, dtype=np.int32), unaries.reshape(-1, w), pairwise)
        result = result.reshape((h, w))

        if viz:
            rgb = Common.nlab2rgb(nlab)
            plt.subplot(121, title='img')
            plt.imshow(rgb, interpolation='nearest')
            plt.subplot(122, title="cut_from_graph")
            plt.imshow(result.reshape((h, w)), interpolation='nearest', cmap=cmap.jet)
            plt.show()

        return data, result


class TspCoordinator(Coordinator):
    def __init__(self, thresh=1e-4, start_id=4, solver='glpk', return_mode='', distance_func=None):
        self.thresh = thresh
        self.start_id = start_id
        self.solver = solver
        if distance_func is not None:
            self.distance_func = distance_func
        else:
            # self.distance_func = lambda data, r1, c1, r2, c2: euc(data[r1, c1] - data[r1].mean(axis=0), data[r2, c2] - data[r2].mean(axis=0))
            self.distance_func = lambda data, r1, c1, r2, c2: euc(data[r1, c1], data[r2, c2])

    def build_graph(self, data):
        #creat graph
        n_features, n_points, _ = data.shape
        G = nx.DiGraph()
        n_features = data.shape[0]
        n_points = data.shape[1]
        nodeids = np.arange(n_features * n_points).reshape((n_features, n_points))
        for center in range(n_features - 1):
            # m1, m2 = [0, 0, 0], [0, 0, 0]
            #  data[center, :].mean(axis=0), data[center + 1, :].mean(axis=0) ## [0, 0, 0], [0, 0, 0]
            for color1 in range(n_points):
                for color2 in range(n_points):
                    if np.isnan(data[center, color1]).any() or np.isnan(data[center + 1, color2]).any():
                        w = 0
                    else:
                        #w = euc(data[center, color1] - m1, data[center + 1, color2] - m2)
                        w = self.distance_func(data, center, color1, center + 1, color2)
                        #w = wminkowski(data[center, color1], data[center + 1, color2], p=2, w=std)
                        
                    if w >= self.thresh:
                        G.add_edge(nodeids[center, color1], nodeids[center + 1, color2], \
                        {'weight': w})
                        
        # the edges that let the "salesman" goes back to the first cluster
        for color1 in range(nodeids.shape[1]):
            for color2 in range(nodeids.shape[1]):
                G.add_edge(nodeids[n_features-1, color1], nodeids[0, color2], {'weight': 0})
        return G, nodeids

    def solve(self, G):        
        p = TSP(G, objective='weight', start = self.start_id)
        r = p.solve(self.solver) #r.nodes, r.edges
        return r
    
    def arrange(self, data, viz=False):
        '''
        data n_features x n_points x point_size
        '''
        old_data = data
        data = old_data.copy()
        
        n_features = data.shape[0] #number of features vector
        n_points = data.shape[1] #number of points in a feature vector
        #point_size = data.shape[2]
        
        G, nodeids = self.build_graph(data)
        sol = self.solve(G)

        #now reorder the data
        tmp = np.tile(range(n_points), n_features).reshape((n_features, -1))
        newindices = tmp.ravel()[sol.nodes[:-1]].reshape((-1, n_features)).T
        for row in range(n_features):
            data[row, :] = data[row, newindices[row]]
        
        if viz != 'none':
            self.visualize(old_data, data.copy(), viz)
        
        return data, newindices


class LocalCoordinator(Coordinator):
    """
    Similar to BinaryCoordinator but does not rely on KPCA arrangement
    """
    def __init__(self):
        super(LocalCoordinator, self).__init__()
        self.m = Munkres()

    def arrange(self, data, viz='none'):
        h, w, d = data.shape
        data_zm = data.copy()
        for item in data_zm:
            item -= item.mean(axis=0)

        params = {'dims': (w, 3), 'mode': 1, 'alpha': 0.01, 'eps': np.finfo(float).eps}

        def _columnwise_dists(arr1, arr2):
            dists = np.zeros((w, w))
            for i in range(w):
                for j in range(w):
                    _arr1, _arr2 = arr1[:, i, :], arr2[:, j, :]
                    dists[i, j] = hausdorff(_arr1, _arr2, mode=1)
                    # dists[i, j] = hausdorff(arr1[:, i, :], arr2[:, j, :], mode=1)

            return dists

        def _partition(_array):
            if len(_array) == 2:
                return np.array([0]), np.array([1])
            else:
                # _kkm = KernelKMeans(n_clusters=2, kernel=hausdorff_kernel, kernel_params=params)
                # _lbls = _kkm.fit_predict(array.reshape((-1, w * d)))
                hd10 = lambda x, y: hausdorff_dist(x, y, dims=(w, 3), mode=1, eps=np.finfo(float).eps)
                _km = KMedoids(n_clusters=2, distance=hd10, n_trials=4, max_iter=100, tol=0.001)
                _lbls = _km.fit_predict(_array.reshape((-1, w * d)))
                return np.where(_lbls == 0)[0], np.where(_lbls == 1)[0]

        def _sort(array, indices, order):
            _length = len(array)
            if _length >= 2:
                ids1, ids2 = _partition(array)
                m1, id1, ord1 = _sort(array[ids1], indices[ids1], order[ids1])
                m2, id2, ord2 = _sort(array[ids2], indices[ids2], order[ids2])

                dists = _columnwise_dists(m1, m2)
                matches = np.array(self.m.compute(dists))

                # if len(ids1) > len(ids2):
                aligned_pair = np.vstack([m1[:, matches[:, 0]], m2[:, matches[:, 1]]])
                # else:
                # _mch = np.argsort(matches[:, 1])
                # aligned_pair = np.vstack([m1[:, _mch], m2])

                return aligned_pair, np.vstack([id1[:, matches[:, 0]], id2[:, matches[:, 1]]]), np.concatenate([ord1, ord2])
            else:
                return array, np.atleast_2d(np.arange(w)), order

        ids = np.tile(np.arange(w), h).reshape((-1, w))
        # newdata, newindices, order = _sort(data, ids, np.arange(h))

        _, newindices, order = _sort(data_zm, ids, np.arange(h))
        newdata = data[order].copy()
        newdata = newdata[np.arange(h)[:, None], newindices]

        if viz != 'none' and viz is not False:
            self.visualize(data, newdata[np.argsort(order)], style=viz)

        return newdata[np.argsort(order)], newindices[np.argsort(order)]


class LightnessTspCoordinator(TspCoordinator):
    def __init__(self, start_id=4, solver='glpk', return_mode='', light_channel=0, alpha=10):
        def _func(data, r1, c1, r2, c2):
            return euc(data[r1, c1], data[r2, c2]) + abs(c1 - c2) / float(data.shape[1] * alpha)

        self.light_channel = light_channel
        super(LightnessTspCoordinator, self).__init__(thresh=0, start_id=start_id, solver=solver, return_mode=return_mode, distance_func=_func)

    def arrange(self, data, viz='none'):
        h, w, d = data.shape
        newids = np.argsort(data[:, :, self.light_channel], axis=1)
        newdata = data.copy()
        newdata = newdata[np.arange(h)[:, None], newids]
        newdata1, newids1 = super(LightnessTspCoordinator, self).arrange(newdata, viz=viz)
        return newdata1, newids[np.arange(h)[:, None], newids1]


class PCATspCoordinator(TspCoordinator):
    def __init__(self, start_id=0, solver='glpk', return_mode='', alpha=10):
        def _func(data, r1, c1, r2, c2):
            return euc(data[r1, c1], data[r2, c2]) + abs(c1 - c2) / float(data.shape[1] * alpha)

        super(PCATspCoordinator, self).__init__(thresh=0, start_id=start_id, solver=solver, return_mode=return_mode, distance_func=_func)

    def arrange(self, data, viz=False):
        h, w, d = data.shape
        pca = PCA(n_components=1)
        projected_data = pca.fit_transform(data.reshape((-1, d))).reshape((h, w))
        newids = projected_data.argsort(axis=1)
        newdata = np.zeros_like(data)
        for i in range(h):
            newdata[i] = data[i][newids[i], :]

        newdata1, newids1 = super(PCATspCoordinator, self).arrange(newdata, viz=viz)
        return newdata1, newids[np.arange(h)[:, None], newids1]


class PairwiseCoordinator(Coordinator):
    def __init__(self):
        super(PairwiseCoordinator, self).__init__()
        self.m = Munkres()

    def arrange(self, data, viz=False):
        h, w, d = data.shape

        query = data[0]
        newindices = np.zeros(data.shape[:2], dtype=int)
        newindices[0] = np.arange(w)
        newdata = data.copy()
        for k in range(1, h):
            dists = np.zeros((w, w))
            for i in range(w):
                for j in range(w):
                    dists[i, j] = euc(query[i], data[k][j])
                    #dists[j, i] = dists[i, j]

            matches = np.array(self.m.compute(dists))
            newindices[k] = matches[:, 1]
            newdata[k, ...] = data[k, newindices[k], :]

        if viz != 'none' and viz is not False:
            self.visualize(data, newdata, style=viz)

        return newdata, newindices


class RandomCoordinator(Coordinator):
    def __init__(self):
        super(RandomCoordinator, self).__init__()

    def arrange(self, data, viz=False):
        h, w, d = data.shape
        newindices = np.array([list(np.random.permutation(w)) for _ in range(h)])
        return data[np.arange(h)[:, None], newindices], newindices


class BinaryCoordinator(Coordinator):
    def __init__(self):
        super(BinaryCoordinator, self).__init__()
        self.m = Munkres()

    def arrange(self, data, viz=False):
        h, w, d = data.shape
        # data_zm = subtract_mean(data)

        def _columnwise_dists(arr1, arr2):
            """
            :param arr1: N1 * M * D
            :param arr2: N2 * M * D
            :return:
            """
            dists = np.zeros((w, w))
            for i in range(w):
                for j in range(w):
                    dists[i, j] = hausdorff(arr1[:, i, :], arr2[:, j, :], mode=1)

            return dists

        def _sort(array, indices):
            _length = len(array)
            if _length >= 2:
                midpoint = len(array) / 2
                m1, id1 = _sort(array[:midpoint], indices[:midpoint])
                m2, id2 = _sort(array[midpoint:], indices[midpoint:])

                dists = _columnwise_dists(m1, m2)
                matches = np.array(self.m.compute(dists))

                # _mch = np.argsort(matches[:, 1])
                # aligned_pair = np.vstack([m1[:, _mch], m2])

                aligned_pair = np.vstack([m1[:, matches[:, 0]], m2[:, matches[:, 1]]])
                return aligned_pair, np.vstack([id1[:, matches[:, 0]], id2[:, matches[:, 1]]])
            else:
                return array, np.atleast_2d(np.arange(w))

        indices = np.tile(np.arange(w), h).reshape((-1, w))
        newdata, newindices = _sort(data, indices)
        # newdata = data[np.arange(h)[:, None], newindices]
        if viz != 'none' and viz is not False:
            self.visualize(data, newdata, style=viz)

        return newdata, newindices


class PaletteAligner(object):
    def __init__(self, method='data', data=None, match_method='munkres', n_examples=-1):
        self.method = method
        self.data = data
        self.match_method = match_method
        self.n_examples = n_examples

    def align(self, palettes):
        return align_colors_naive(palettes, self.data, match_method=self.match_method, n_examples=self.n_examples)


def align_colors_naive(palettes, data, clustering=0, dupl=True, match_method='simple', n_examples=40):
    """
        align the palette according to the data

    :param palettes:
    :param data:
    :param clustering:
    :param dupl:
    :param match_method:
    :return:
    """
    from fast.Distance import parallel_hausdorff, best_match

    w, h, d = data.shape

    def _columnwise_dists(arr1, arr2, weights=None, mode=1):
        """
        :param arr1: N1 * M * D
        :param arr2: N2 * M * D
        :return:
        """
        w1, w2 = arr1.shape[1], arr2.shape[1]
        dists = np.zeros((w1, w2))
        for i in range(w1):
            for j in range(w2):
                # (1)
                # ds = euclidean_distances(arr1[:, i, :], arr2[:, j, :])
                # dists[i, j] = (ds * weights).sum()

                # (2)
                dists[i, j] = euc(arr1[:, i, :].mean(axis=0), arr2[:, j, :].mean(axis=0))

                # (3)
                # d = euclidean_distances(arr1[:, i, :], arr2[:, j, :])
                # if weights is not None:
                #     d *= weights
                # dists[i, j] = hausdorff_with_dists(d, mode=mode)

                # dists[i, j] = hausdorff(arr1[:, i, :], arr2[:, j, :], mode=1)

        return dists

    m = Munkres()
    _, ncols, nchas = data.shape
    
    if clustering !=0 :
        params = {'dims': (5, 3), 'mode': 1, 'alpha': 0.05, 'eps': np.finfo(float).eps}
        kkm, n_themes, labels = find_best_n_clusters_1(data.reshape((w, h * d)), kernel=hausdorff_kernel,
                                                       kernel_params=params, max_themes=20)
        
    if len(palettes.shape) == 1:
        palettes = [palettes]
    
    new_palettes = []
    newindices = []
    for pal in palettes:
        mask = np.bitwise_not(np.isnan(pal[:, 0]))
        palette = pal[mask] # .reshape((-1, 3))
        dists = parallel_hausdorff(palette, data, mode=0)
        # dists = euclidean_distances(palette.reshape((-1, h * d)), data.reshape())
        if match_method == 'munkres':
            if clustering != 0:
                pal1 = pal.copy()
                pal1[np.isnan(pal)] = np.finfo(float).eps
                lbl = kkm.predict(pal1.reshape((1, h * d)))
                n_examples = len(lbl)
                best_ids = np.where(labels == lbl)[0]
            else:
                best_ids = np.argsort(dists)[:n_examples]

            '''
            plt.subplot(121)
            plt.imshow(Common.nlab2rgb(palette[None, :, :]), interpolation='nearest')
            plt.subplot(122)
            plt.imshow(Common.nlab2rgb(data[best_ids]), interpolation='nearest')
            plt.show()
            #'''

            # best_id = np.argsort(dists)[0]
            # distmat = euclidean_distances(palette, data[best_id])
            distmat = _columnwise_dists(palette[None, :], data[best_ids],
                                        weights=np.e ** - (100 * np.sort(dists)[:n_examples] ** 2), mode=1)
            # distmat = _columnwise_dists(palette[None, :], data[best_ids], weights=None)
            bestpairs = np.array(m.compute(distmat))
            best_order = bestpairs[:, 1]
        elif match_method == 'simple':
            best_id = np.argsort(dists)[0]
            best_order = best_match(palette, data[best_id], dupl=dupl)
       
        orig_indices = np.arange(0, ncols, 1)
        orig_indices[best_order] = np.nonzero(mask)
        orig_indices[[i for i in range(ncols) if i not in best_order]] = np.where(mask==False)[0]
        newindices.append(orig_indices)
        
        new_palette = np.repeat(np.nan, ncols * nchas).reshape((ncols, nchas))
        new_palette[best_order] = palette #[best_order]
        new_palettes.append(new_palette)
        
    return np.array(new_palettes), np.array(newindices)


def run_tsp_coordinator(n_colors, collections, step=1, save=False, sorter='tsp', viz='none'):
    run_coordinator(n_colors, collections, step=step, save=save, sorter=sorter, viz=viz)


def run_coordinator(n_colors, collections, step=1, save=False, sorter=None, viz='none'):
    from Common import PATH
    SORT_FEAT = True
    DATADIR = 'data'

    for aux, collection in enumerate(collections):
        print collection
        OPATH = join(PATH, DATADIR, collection)
        dpaintings = np.load(join(OPATH, 'dpaintings%d.npz' % n_colors))
        n_levels = len(dpaintings['clustered_lab'])
        del dpaintings

        for level in range(n_levels):
            print level,
            fname = join(OPATH, 'clusters_nc%d_l%d.npz' % (n_colors, level))
            if not os.path.isfile(fname):
                continue

            dpaintings = np.load(fname)
            labels = dpaintings['labels']
            cl_data = dpaintings['normalized_clustered_lab']
            n_themes = dpaintings['n_themes']

            concat_data, newindices, newlabels, order, cluster_order = sort_palettes(
                cl_data, n_colors, sort_palettes=True, n_themes=n_themes, labels=labels, step=1, sorter=sorter, viz=viz)

            if save:
                print 'saving to file...'
                Common.modify_npz(join(OPATH, 'clusters_nc%d_l%d.npz' % (n_colors, level)),
                [('concat_lab', concat_data), ('concat_indices', newindices), ('concat_order', order),
                 ('concat_labels', newlabels), ('cluster_order', cluster_order)])

                # concat_lab -- palettes that have been reordered (within a palette and across palettes)
                # concat_indices -- new indices of each color
                # concat_order -- new palette order (PCA projection)
                # concat_labels -- cluster labels assigned by K-KMeans
                # cluster_order -- order of the palette clusters
                # orig_lab --> original palettes data


def sort_palettes(cl_data, n_colors, sort_palettes=True, n_themes=None, labels=None, step=1, sorter=None, viz='none'):
    """
    data ~ kernel ~ projected_data
    project data to a 1-D manifold for reordering purpose
    since we don't know the alignments of features yet, the best solution is to use kernel method
    in this case, Pyramid Match Kernel is used

    :param cl_data data in normalized Lab color palettes
    :param n_colors number of colors in each palette
    :param sort_palettes whether to sort the palettes before color re-arrangment
    :param labels the clustering labels, produced by Kernel-KMeans
    :param step skip some data points
    :param sorter color arrangment methods (TSP, MaxFlow, Lightness)
    :param viz:
    :return (concat_lab, concat_indices, concat_order, concat_labels, cluster_order)
    - concat_lab -- palettes that have been reordered (within a palette and across palettes)
    - concat_indices -- new indices of each color
    - concat_order -- new palette order (PCA projection)
    - concat_labels -- cluster labels assigned by K-KMeans
    - cluster_order -- order of the palette clusters
    - orig_lab --> original palettes data
    """
    cl_data = cl_data[::step]
    h, w, d = cl_data.shape

    newlabels, order, cluster_order, data = None, np.arange(len(cl_data)), None, cl_data
    if sort_palettes is True:
        training_data = cl_data.copy()
        kernel1 = hausdorff_kernel_matrix(training_data, alpha=0.5, mode=1) #0.0001 for clustered_data
        # '''
        means = training_data.mean(axis=1)
        training_data1 = training_data.copy()
        for td in range(len(training_data)):
            training_data1[td] -= means[td]
        kernel2 = hausdorff_kernel_matrix(training_data1, alpha=0.5, mode=1)
        kernel = kernel1 + kernel2
        # '''
        # kernel = kernel1

        # if labels is None or n_themes is None:
        #     kkm, n_themes, labels = find_best_n_clusters(kernel)

        # --------- sort with hausdorff kernel matrix ---------
        kpca = KernelPCA(n_components=4, kernel='precomputed')
        projected_data = kpca.fit_transform(kernel)
        data = cl_data.copy()
        # sort according to the largest eigen values
        order = projected_data[:, 0].ravel().argsort()
        # free some memory
        del kernel
        data = data[order]
        newlabels = None
        # '''
        # # reordering color clusters
        # labels = labels[order]
        # pos = []
        # for idd in np.unique(labels):
        #     pos.append(np.nonzero(labels == idd)[0].mean())
        #
        # cluster_order = sorted(zip(np.unique(labels), pos), key=lambda x: x[1])
        # cluster_order = [co[0] for co in cluster_order]
        #
        # # reorderring data according to cluster_order
        #
        # tmp = np.zeros_like(data)
        # count = 0
        # tmporder = np.zeros_like(order)
        # for lb in cluster_order:
        #     mask = labels == lb
        #     n_elems = np.sum(mask)
        #     if n_elems == 0:
        #         continue
        #
        #     tmp[count: count + n_elems] = data[mask]
        #     tmporder[count: count + n_elems] = np.nonzero(mask)[0]
        #     count += n_elems
        #
        # data = tmp
        # order[:] = np.arange(0, len(data))[order][tmporder]
        # newlabels = np.concatenate([np.repeat(c, np.sum(labels == c)) for c in cluster_order])
        # #'''

    # ----- SORT ENTIRE DATASET WITH TSP / MAXFLOW / LIGHTNESS ----
    if sorter != 'none':
        if sorter == 'tsp':
            tc = TspCoordinator(thresh=0, start_id=0, solver='glpk')
        elif sorter == 'hue':
            tc = HueCoordinator(hue_channel=0)
        elif sorter == 'lightness':
            tc = LightnessCoordinator(light_channel=0)
        elif sorter == 'pca':
            tc = PCACoordinator()
        elif sorter == 'km':
            tc = KMCoordinator()
        elif sorter == 'local':
            tc = LocalCoordinator()
        elif sorter == 'lightness_tsp':
            tc = LightnessTspCoordinator(light_channel=0, start_id=0, alpha=10.0)
        elif sorter == 'pca_tsp':
            tc = PCATspCoordinator(alpha=10.0)
        elif sorter == 'binary':
            tc = BinaryCoordinator()
        elif sorter == 'pairwise':
            tc = PairwiseCoordinator()
        elif sorter == 'random':
            tc = RandomCoordinator()
        else:
            tc = RandomCoordinator()

        newdata, newindices = tc.arrange(data, viz=viz)
    else:
        newdata, newindices = data, np.tile( np.arange(n_colors), len(cl_data)).reshape((len(cl_data), n_colors))

    return newdata, newindices, newlabels, order, cluster_order

if __name__ == '__main__':
    run_tsp_coordinator(n_colors=7, collections=['luce'], step=1, save=False)
