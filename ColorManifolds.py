# -*- coding: utf-8 -*-
"""
Created on Tue Nov 25 11:41:28 2014

@author: phan
"""

import GPy
from os.path import join
import os
import numpy as np
from matplotlib import pyplot as plt
from Common import modify_npz
from smartpalette import KernelKMeans


def learn_color_manifolds(authors, PATH, optimize=True, verbose=1, plot=False, N=200, Q=7,
                          num_inducing=40, max_iters=1000, method='gplvm', step=5, level=0, n_colors=10, **k):

    models = {}
    for author in authors:
        print author
        OPATH = join(PATH, 'data', author)
        datafile = join(OPATH, 'clusters_nc%d_l%d.npz' % (n_colors, level))
        modelfile = join(PATH, 'data', author, 'models_nc%d_l%d.npz' % (n_colors, level))

        if os.path.isfile(datafile):
            data = np.load(datafile)
            Y = data['concat_lab'][::step]
            Y = Y.reshape((Y.shape[0], Y.shape[1] * Y.shape[2]))
            Y[np.isnan(Y)] = 1e-8
            
            if method == 'bgplvm':
                kernel = GPy.kern.RBF(Q, 1., 1. / np.random.uniform(0, 1, (Q,)), ARD=True)  + GPy.kern.Bias(Q, np.exp(-2))
                m = GPy.models.BayesianGPLVM(Y, Q, kernel=kernel, num_inducing=num_inducing, **k)
                m.optimize('bfgs', messages=verbose, max_iters=max_iters, gtol=1e-4)
            else:
                kernel = GPy.kern.RBF(Q, ARD=True) + GPy.kern.Bias(Q)
                m = GPy.models.GPLVM(Y, Q, kernel=kernel)
                m.optimize('scg', messages=verbose, max_iters=max_iters, gtol=1e-4)
            
            if not os.path.isfile(modelfile):
                np.savez(modelfile, concat_models=m)
            else:
                modify_npz(modelfile, [('concat_models', m)])

            models[author] = m
    return models


def run(artists, n_colors, method , step=1, mode='train', level=0, data_dir='data'):
    import Common, scipy
    from Visualization import color_image_show
    from fast.Distance import hausdorff, hausdorff_abs
    from Common import PATH

    blacklist = [] 
    Xs = {}
    Xs_orig = {}
    models = {}
    labels = {}
    for author in artists:
        if author in blacklist:
            continue
        
        print author
        if mode == 'train':
            learn_color_manifolds([author], PATH, Q=4, num_inducing=30, verbose=1,
                                    max_iters=4000, method=method, step=step, n_colors=n_colors)
            
        elif mode == 'test' or mode =='vis' or mode == 'vis_img':
            datafile = join(PATH, data_dir, author, 'clusters_nc%d_l%d.npz' % (n_colors, level))
            modelfile = join(PATH, data_dir, author, 'models_nc%d_l%d.npz' % (n_colors, level))
            if os.path.isfile(modelfile) and \
                    os.path.isfile(datafile):
                data = np.load(modelfile)
                dpaintings = np.load(datafile)

                order = dpaintings['concat_order']
                labels_ = dpaintings['labels'][order][::step]
                
                m = data['concat_models'].tolist()
                labels[author] = labels_
                Xs[author] = dpaintings['concat_lab']
                Xs_orig[author] = dpaintings['normalized_clustered_lab']
                models[author] = m
            else:
                models[author] = learn_color_manifolds([author], PATH, Q=4, num_inducing=20, verbose=1,
                                               max_iters=1000, method=method, step=step, n_colors=n_colors)[0]

    if mode == 'train':
        return
    
    _, n_cols, n_chas = Xs[artists[0]].shape
    m = models[artists[0]]
    lbs = labels[artists[0]]
    trange = (0, -1)
        
    if mode == 'vis':    
        y = np.asfarray(m.Y[0, :])
        if len(lbs) == len(m.Y):
            ax = m.plot_latent(labels=lbs, legend=False) #, which_indices=(0,2))
        else:
            ax = m.plot_latent()
            
        #data_show = GPy.plotting.matplot_dep.visualize.image_show( \
        data_show = color_image_show(y[None, :], dimensions=(1, n_cols, n_chas),
                                     transpose=True, order='C', invert=False, scale=False)
        
        if method == "bgplvm":
            lvm = GPy.plotting.matplot_dep.visualize.lvm(m.X.mean[0, :].copy(), m, data_show, ax)
        else:
            lvm = GPy.plotting.matplot_dep.visualize.lvm(m.X.copy(), m, data_show, ax)
        #plt.savefig('.tmp/tmp.png')
        
        raw_input('Press enter to finish')

    if mode == 'vis_img':
        from matplotlib.offsetbox import OffsetImage, AnnotationBbox
        from GPy.plotting.matplot_dep.dim_reduction_plots import most_significant_input_dimensions
        from Visualization import colorizer_show

        which_indices = most_significant_input_dimensions(m, None)
        y = np.asfarray(m.Y[0, :])
        # ax = plt.subplot(111)
        # ax.axis('off')
        if len(lbs) == len(m.Y):
            ax = m.plot_latent(labels=lbs, which_indices=which_indices, s=0, legend=False)
        else:
            ax = m.plot_latent()

        ax.axis('off')
        Y = np.array(m.Y)
        if method == 'bgplvm':
            X = np.array(m.X.mean)
        else:
            X =  np.array(m.X)

        vis_labels = np.unique(lbs) #np.random.permutation(len(np.unique(lbs)))[:10]
        for lbl in vis_labels:
            n_elems_ = len(lbs[lbs == lbl])
            vis_ids = np.random.permutation(n_elems_)[:min(5, n_elems_)] #np.random.randint(0, len(lbs[lbs == lbl]), n_colors)
            vis_ids = np.nonzero(lbs == lbl)[0][vis_ids]
            for i in vis_ids:
                y = Y[i]
                rgb = Common.nlab2rgb(y.reshape((1, n_colors, 3)))
                im = OffsetImage(rgb, zoom=7, interpolation='nearest')
                xy = np.array(X[i, which_indices])
                ab = AnnotationBbox(im, xy,
                                    xybox=(-0.5, 0.5),
                                    xycoords='data',
                                    boxcoords="offset points",
                                    pad=0.00,
                                    arrowprops=dict(arrowstyle="->"))
                ax.add_artist(ab)

        data_show = colorizer_show(y[None, :], model=m, image_file=join(PATH, 'images', 'decorative', 'image_24.jpg'),
                                   n_segments=120, method='realign', dimensions=(1, n_cols, n_chas),
                                   transpose=True, order='C', invert=False, scale=False, n_colors=n_colors)

        # data_show = color_image_show(y[None, :], dimensions=(1, n_cols, n_chas),
        #                              transpose=True, order='C', invert=False, scale=False)
        if method == "bgplvm":
            lvm = GPy.plotting.matplot_dep.visualize.lvm(X.copy(), m, data_show, latent_axes=ax, latent_index=which_indices)
        else:
            lvm = GPy.plotting.matplot_dep.visualize.lvm(X.copy(), m, data_show, latent_axes=ax, latent_index=which_indices)

        raw_input('Press enter to finish')
        
if __name__ == "__main__":
    run(['beach'], 40, 'bgplvm', step=1, mode='vis_img')