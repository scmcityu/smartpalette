# -*- coding: utf-8 -*-
"""
Created on Sat Oct 19 22:55:31 2013

@author: phan
"""
from Feature import for_all_patches, normalize, FeatureExtractor
import math
import numpy as np

def extract_feature_srp(image, r_dim, patch_size=(17,17), \
    spacing=1,sorting_method="radial",norm_method="weber",padding='none'):
    """
    Extract Sorted Random Projection features from the image.
    Li Liu; Fieguth, P.; Gangyao Kuang; Hongbin Zha, "Sorted Random Projections for robust texture classification," Computer Vision (ICCV), 2011 IEEE International Conference on , vol., no., pp.391,398, 6-13 Nov. 2011
doi: 10.1109/ICCV.2011.6126267
    http://ieeexplore.ieee.org/xpls/abs_all.jsp?arnumber=6126267&tag=1
    """
    
    #precompute circles
    circles = []
    cx = math.trunc(patch_size[0] / 2)
    arc_length = 2 * math.pi / 8.
    for r1 in range(1,cx):
        n = int(round(2 * math.pi * r1 / arc_length))
        arc = 2 * math.pi / n
        circles.append([])
        for i in range(n):
            x1 = int(round(math.sin(i * arc) * r1))
            y1 = int(round(math.cos(i * arc) * r1))                    
            x2 = int(round(math.sin(i * arc) * (r1 - 1)))
            y2 = int(round(math.cos(i * arc) * (r1 - 1)))
            circles[r1-1].append((x1,y1,x2,y2))
    
    #extract SRP features from a single patch
    def _extract_feature_srp_radial(array):
        cx, cy = math.trunc(array.shape[0] / 2) , math.trunc(array.shape[1] / 2)
        arc_length = 2 * math.pi / 8.
        deltas = []
        for r1 in range(1,cx):
            n = int(round(2 * math.pi * r1 / arc_length))
            delta = []
            for i in range(n):
                x1,y1,x2,y2 = circles[r1-1][i]
                delta.append(array[x1 + cx,y1  + cy] - array[x2 + cx,y2  + cy])
            delta.sort()
            deltas.append(delta)
        deltas = np.asfarray(reduce(lambda a,b: a+b,deltas))
        sampling_indices = np.random.random_integers(0,len(deltas)-1,r_dim)
        samples = deltas[sampling_indices]
        normalize(samples,norm_method)
        return samples
        
    def _extract_feature_srp_ordinary(array):
        sampling_indices = np.random.random_integers(0,len(array)-1,r_dim)
        return normalize(array.flatten()[sampling_indices],norm_method)

    if sorting_method == "radial":
        re = np.asfarray(for_all_patches(image,_extract_feature_srp_radial,patch_size,spacing,padding))
    elif sorting_method == "none":
        re = np.asfarray(for_all_patches(image,_extract_feature_srp_ordinary,patch_size,spacing,padding))
    else: re = np.array([]) 

    return re
    
class SRP(FeatureExtractor):
    def __init__(self,patch_size,spacing,r_dim,sorting_method='none',normalizer='weber',padding='none'):
        super(SRP, self).__init__(patch_size,spacing,normalizer,padding)
        self.r_dim = r_dim
        self.sorting_method = sorting_method
        self.norm_method = normalizer
        self.padding = padding
        
    def extract(self,image):
        #extract_feature
        return extract_feature_srp(image, self.r_dim, self.patch_size, \
        self.spacing,self.sorting_method,self.norm_method,self.padding)
        
    def size(self):
        return self.r_dim
