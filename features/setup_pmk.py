from distutils.core import setup, Extension
import numpy
from os.path import join

pmk_dir = '/home/phan/workspace/smartpalette/libs/libpmk-2.5/libpmk2'   
# define the extension module
pmk = Extension('pmk', sources=['PMK.cpp'], \
                          include_dirs=[numpy.get_include(), pmk_dir], \
                          libraries = ['stdc++','m'], \
                          extra_objects=[join(pmk_dir,'libpmk.o'), join(pmk_dir,'libpmk_util.o')], \
                          extra_compile_args = ['-fPIC','-fopenmp'], \
                          extra_link_args = ['-fopenmp']
                          #library_dirs = ['/usr/local/lib']
                          )

# run the setup
setup(ext_modules=[pmk])
