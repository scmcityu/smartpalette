# distutils: language = c++
# distutils: sources = ssdesc.cc
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 16 09:07:39 2013

@author: phan
"""

from libcpp.vector cimport vector
cimport numpy as np
import numpy as np
from cython.operator cimport dereference as deref, preincrement as inc

cdef extern from "ssdesc.h" namespace "ssdesc":
    cdef cppclass rectRegion:
        int xfrom, xto, yfrom, yto
        rectRegion() except +
        rectRegion(int, int, int, int) except +
        
    cdef cppclass coordElem:
        int x
        int y
        coordElem(int, int) except +
      
    cdef cppclass ssdesc_parms[T]:
        ssdesc_parms() except +
        ssdesc_parms(int, int, int, int, T,T,T,T) except +
        unsigned short int patch_size
        unsigned short int cor_size
        unsigned short int nrad
        unsigned short int nang
        T var_noise
        T saliency_thresh
        T homogeneity_thresh
        T snn_thresh

    cdef void calc_ssdescs_d "ssdesc::calc_ssdescs<double>"(double* image, int image_width,
                                                 int image_height, 
                                                 int image_channels, 
                                                 ssdesc_parms[double] &parms, vector[double]* ssdescs, rectRegion calc_rect)
                                                 
    cdef void calc_ssdescs_alt_d "ssdesc::calc_ssdescs_alt<float>"(float* image, int image_width,
		int image_height, int image_channels, ssdesc_parms[float] &parms,
		vector[float]* ssdescs, rectRegion calc_rect)
  
    cdef void calc_ssdescs_alt_d "ssdesc::calc_ssdescs_alt<double>"(double* image, int image_width,
		int image_height, int image_channels, ssdesc_parms[double] &parms,
		vector[double]* ssdescs, rectRegion calc_rect)  

    cdef void prune_normalise_ssdescs_d "ssdesc::prune_normalise_ssdescs<double>"(vector[double]& ssdescs,
		int image_width, int image_height, ssdesc_parms[double] &parms,
		vector[double]* resp, vector[coordElem]* draw_coords, vector[coordElem]* salient_coords,
		vector[coordElem]* homogeneous_coords, vector[coordElem]* snn_coords, rectRegion calc_rect)
    cdef void test_vector(vector[double]* d)
    
    #cdef void calc_ssdescs_d(double* image, int image_width, \
    #		const int image_height, int image_channels, ssdesc_parms[double] &parms, \
    #		vector[double]* ssdescs, rectRegion calc_rect)
def test_v():
    cdef vector[double] *v = new vector[double]()
    test_vector(v)
    nd = vector_to_ndarray(v)
    for n in nd:
        print n

def calc_ssdescs(np.ndarray[np.double_t, ndim=3] image, parms, calc_rect):
    """
    image: numpy array
    calc_rect: tuple4
    ssdescs: a list of doubles
    parms: a dictionary
    """
    cdef int image_width, image_height, image_channels
    image_width = image.shape[0]
    image_height = image.shape[1]
    image_channels = image.shape[2]
    x1,y1,x2,y2 = calc_rect
    cdef rectRegion calc_rect_ = rectRegion(x1,y1,x2,y2)
    
    cdef vector[double] *ssdescs = new vector[double]()
    
    image = np.ascontiguousarray(image)
    #cdef np.ndarray[np.double_t, ndim=1, mode="c"] Y = np.zeros_like(image)
    cdef ssdesc_parms[double] parms_ = ssdesc_parms[double](parms["patch_size"],parms["cor_size"], \
    parms["nrad"],parms["nang"],parms["var_noise"],parms["saliency_thresh"], \
    parms["homogeneity_thresh"],parms["snn_thresh"] )
    
    calc_ssdescs_d(&image[0,0,0], image_width, image_height, image_channels, parms_, ssdescs, calc_rect_)
    return vector_to_ndarray(ssdescs)
    #cdef ssdesc_parms[double]* parms_ = new ssdesc_parms[double]()
    #calc_ssdescs_d(&image[0,0,0], image_width, image_height, image_channels, parms_, ssdescs, calc_rect_)
                                             
def calc_ssdescs_alt(np.ndarray[np.double_t, ndim=3] image, parms, calc_rect):
    """
    image: numpy array
    calc_rect: tuple4
    ssdescs: a list of doubles
    parms: a dictionary
    """
    cdef int image_width, image_height, image_channels
    image_width = image.shape[0]
    image_height = image.shape[1]
    image_channels = image.shape[2]
    x1,y1,x2,y2 = calc_rect
    cdef rectRegion calc_rect_ = rectRegion()
    
    cdef vector[double] *ssdescs = new vector[double]()
    image = np.ascontiguousarray(image)

    #cdef np.ndarray[np.double_t, ndim=1, mode="c"] Y = np.zeros_like(image)
    cdef ssdesc_parms[double] parms_ = ssdesc_parms[double](parms["patch_size"],parms["cor_size"], \
    parms["nrad"],parms["nang"],parms["var_noise"],parms["saliency_thresh"], \
    parms["homogeneity_thresh"],parms["snn_thresh"] )
    
    calc_ssdescs_alt_d(&image[0,0,0], image_width, image_height, image_channels, parms_, ssdescs, calc_rect_)

    return vector_to_ndarray(ssdescs)
    
cdef vector_to_ndarray(vector[double]* v):
    re = np.zeros(v.size())
    
    for i in range(re.shape[0]):
        re[i] = v[0][i]

    return re
    

def prune_normalise_ssdescs(np.ndarray[np.double_t, ndim=1] ssdescs,int image_width, int image_height, parms, calc_rect):
     """
     """
     #cdef int n_elem = ssdescs.shape[0]
     x1,y1,x2,y2 = calc_rect
     cdef rectRegion calc_rect_ = rectRegion(x1,y1,x2,y2)
     #ssdescs = np.ascontiguousarray(ssdescs)
     cdef vector[double]* ssdescs_ = new vector[double]()
     for i in range(ssdescs.shape[0]):
         ssdescs_.push_back(ssdescs[i])
         
     cdef vector[double]* resp = new vector[double]()
     cdef vector[coordElem] *draw_coords = new vector[coordElem]()
     cdef vector[coordElem] *salient_coords = new vector[coordElem]()
     cdef vector[coordElem] *homogeneous_coords = new vector[coordElem]()
     cdef vector[coordElem] *snn_coords = new vector[coordElem]()
     
     cdef ssdesc_parms[double] parms_ = ssdesc_parms[double](parms["patch_size"],parms["cor_size"], \
     parms["nrad"],parms["nang"],parms["var_noise"],parms["saliency_thresh"], \
     parms["homogeneity_thresh"],parms["snn_thresh"] )
     
     prune_normalise_ssdescs_d(ssdescs, image_width, image_height, parms_, \
     resp,draw_coords, salient_coords, homogeneous_coords, snn_coords, calc_rect_)
     
     return (vector_to_ndarray(resp))
     
     
     
    