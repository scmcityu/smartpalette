# -*- coding: utf-8 -*-
"""
Created on Fri Oct 24 01:03:26 2014

@author: phan
"""

import pmk
import numpy as np
import math 

FINEST_SIDE_LENGTH = 0.01 #0.01
SIDE_LENGTH_FACTOR = 2
DISCRETIZE_ORDER = 4

def normalize_kernel(kernel):
    size = len(kernel)
    diag = np.diag(kernel)
    newkernel = kernel.copy()
    for i in range(size):
        for j in range(i + 1):
            normval = newkernel[i, j] / math.sqrt(diag[i] * diag[j])
            newkernel[i, j] = normval
            newkernel[j, i] = normval
    
    return newkernel
    
def compute(X, Y=None, method='uniform', finest_side_length=FINEST_SIDE_LENGTH, \
side_length_factor=SIDE_LENGTH_FACTOR, discretize_order=DISCRETIZE_ORDER, normalized=True):
    if Y is None:
        kernel = pmk.compute_pmk_uniform(list(X), finest_side_length, side_length_factor, discretize_order, \
        0, True, True)
        if normalized:
            kernel = normalize_kernel(kernel)
    else:
        kernel = pmk.compute_pmk_uniform1(list(X), list(Y), finest_side_length, side_length_factor,\
        discretize_order, True, True)
        
    return  kernel

if __name__ == '__main__':
    X = np.random.rand(10 * 100 * 3).reshape((10, 100, 3))
    Y = np.random.rand(15 * 100 * 3).reshape((15, 100, 3))
    kernel_mat = compute(X, Y)
    
    print kernel_mat